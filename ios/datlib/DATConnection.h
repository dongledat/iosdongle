/**
 * @file Connection.h 
 * @brief Connection class declaration
 */


#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


/**
 * @brief sendPDU request result
 */

typedef enum {
  DATPDURequestResult_OK,
  DATPDURequestResult_CanIdError,
  DATPDURequestResult_CanSpeedError,
  DATPDURequestResult_CanProtocolError,
  DATPDURequestResult_VehicleSpeedInvalid
} DATPDURequestResult;


/**
 * @brief sendPDU response status
 */

typedef enum {
  DATPDUResponseStatus_OK,
  DATPDUResponseStatus_ServiceNotAllowed,
  DATPDUResponseStatus_ResponseSizeExceedsMaximumBufferSize,
  DATPDUResponseStatus_MemoryError,
  DATPDUResponseStatus_CanCommunicationError
} DATPDUResponseStatus;


/**
 * @brief Supported protocols
 */

typedef enum {
  DATPDUProtocol_TP20,
  DATPDUProtocol_ISO14229,
  DATPDUProtocol_ISO15765
} DATPDUProtocol;

@interface IntList : NSObject


@end


@interface DATIFWStatus : NSObject
@end

@interface IDATIOStream: NSObject
@end



@interface IConnectionListener: NSObject
@end

@interface DATIDataListener: NSObject
@end

@interface DATIUpdateProgressListener: NSObject
@end

@interface IDataListener: NSObject
@end


@interface Certificate: NSObject
@end

@interface FileInputStream: NSObject
@end

@interface InputStream: NSObject
@end

@interface OutputStream: NSObject
@end

@interface IUpdateCertificateListener: NSObject
@end

/**
 * @class Connection
 */

@interface DATConnection : NSObject {
}
/**
 * @brief Check if Bluetooth connection is established.
 * @return True if Smartphone is connected to Dongle over Bluetooth.
 */
@property(nonatomic, readonly) BOOL connected;
/**
 * @brief Check is Smartphone is authenticated with Dongle by PIN.
 * @return True if Smartphone is already authenticated with Dongle by PIN.
 * @see authenticateByPIN(String pin).
 */
@property(nonatomic, readonly) BOOL authenticatedByPIN;
/**
 * @brief Check is Smartphone is authenticated with Dongle by token.
 * @return True if Smartphone is already authenticated with Dongle by token.
 * @see authenticateByToken().
 */
@property(nonatomic, readonly) BOOL authenticatedByToken;
/**
 * @brief Retreive a list of available groups of diagnostics parameters.
 */
@property(nonatomic, retain, readonly) IntList * availableDiagParameters;
/**
 * @brief Retreive Dongle H/W revision.
 */
@property(nonatomic, retain, readonly) NSString * dongleHWRevision;
/**
 * @brief Retreive Dongle S/N.
 */
@property(nonatomic, retain, readonly) NSString * dongleSerialNumber;
/**
 * @brief Retreive Dongle firmware status.
 * @see IFWStatus.
 */
@property(nonatomic, retain, readonly) NSString * firmwareStatus;
/**
 * @brief Switch to service mode.
 * @see IFWStatus.
 */
@property(nonatomic, retain, readonly) NSString * switchToServiceMode;

-(id) WaitRespId: (int) transId;
/**
 * @brief Connect Smartphone to Dongle by Bluetooth.
 */
- (NSString*) connect:(IDATIOStream *)ioStream;
/**
 * @brief Stop Smartphone Bluetooth connection to Dongle.
 */
- (NSString*) disconnect;

/**
 * @brief First time Smartphone authentication by PIN.
 * @param pin Hexadecimal 8-digit number appearing on Donlge's sticker.
 * @note There is no need to specify user as in Dongle 1.0.
 * 
 * This function should be called instead of \a authenticate(String user, String password) in Dongle 1.0.
 * The result of PIN authentication is returned in \ref IConnectionListener::onAuthenticateByPIN() callback funtion.
 */
- (NSString*) authenticateByPIN:(NSString *)pin;
/**
 * @brief First time Smartphone authentication by user and PIN.
 * @param user user name.
 * @param pin Hexadecimal 8-digit number appearing on Donlge's sticker.
 * @note There is no need to specify user as in Dongle 1.0.
 * 
 * This API is kept for possible future extensions. In current implementation @b user parameter is always equal to @a 'default'.
 */
- (NSString*)  authenticateByPIN:(NSString *)user pin:(NSString *)pin;
/**
 * @brief Authenticate Dongle by token.
 * 
 * The result of token authentication is returned in @ref IConnectionListener::onAuthenticateByToken() callback funtion.
 */
- (NSString*) authenticateByToken:(NSString *)token;
/**
 * @brief Subscribe a group of diagnostics parameters.
 * @param url Represents a group of diagnostics parameters.
 * @param val Refresh timeout.
 * @note The URL type is changed from String to int.
 */
- (NSString*) subscribeObject:(NSString*)url val:(NSString*)val;
/**
 * @brief Unsubscribe a group of diagnostics parameters.
 * @param url Represents a group of diagnostics parameters.
 * @note The URL type is changed from String to int.
 */
- (NSString*) unsubscribeObject:(NSString*)url;
/**
 * @brief Retreive a diagnostics parameter group by URL.
 * @note The URL type is changed from String to int.
 */
- (NSString *) getDataObject:(NSString *)url;
/**
 * @brief Add connection listener.
 * @see IConnectionListener.
 */
- (void) addConnectionListener:(IConnectionListener *)connectionListener;
/**
 * @brief Remove connection listener.
 * @see IConnectionListener.
 */
- (void) removeConnectionListener:(IConnectionListener *)connectionListener;
/**
 * @brief Remove all connection listeners.
 *//**
 * @brief Remove data listener.
 * @see IDataListener.
 */
- (void) clearConnectionListener;
- (void) addDataListener:(DATIDataListener *)dataListener;
- (void) removeDataListener:(DATIDataListener *)dataListener;
- (void) clearAllDataListeners;
/**
 * @brief Start asynchronous configuration update.
 */
- (void) performUpdateAsync:(NSInputStream *)updateFile signature:(NSInputStream *)signature listener:(DATIUpdateProgressListener *)listener;
/**
 * @brief Cancel asynchronous configuration update.
 */
- (void) cancelDongleUpdate;
/*
- (id<DATPDURequestResult>) sendPDU:(int)canID canSpeed:(int)canSpeed canSP:(int)canSP canSJW:(int)canSJW canResponseTimeout:(int)canResponseTimeout tp20ChannelAddress:(int)tp20ChannelAddress protocol:(id<DATPDUProtocol>)protocol reqPDU:(NSString *)reqPDU pduResponseStatus:(id<DATPDUResponseStatus>)pduResponseStatus rspPDU:(NSMutableArray *)rspPDU;
 */
/*
- (void) updateCertificate:(DATCertificate *)certificate listener:(DATIUpdateCertificateListener *)listener;
 */
- (NSString *) availableDiagParameters;
@end
