/**
 * @file    Url.h
 * @brief   Url class declaration
 */
extern int const DATTYPE_FUNTION;
extern int const DATTYPE_OBJECT;

@interface DATUrl : NSObject {
  BOOL isSubscribed;
  int type;
  NSString * url;
  int id;
}

@property(nonatomic, retain, readonly) NSString * name;
@property(nonatomic, retain, readonly) NSString * id;
@property(nonatomic, readonly) int type;
@property(nonatomic, readonly) BOOL subscribed;
- (id) init:(NSString *)urlName type:(int)type isSubscribed:(BOOL)isSubscribed;
- (id) initWithUrlName:(NSString *)urlName;
- (id) initWithId:(int)id;
+ (BOOL) isMatch:(NSString *)url urlPattern:(NSString *)urlPattern;
- (NSString *) description;
- (int) hash;
- (BOOL) isEqualTo:(NSObject *)obj;
/**
 * @brief Convert URL string to command parameter ID
 */
+ (int) urlToId:(NSString *)url;
/**
 * @brief Convert command/parameter ID to URL string
 */
+ (int) idToUrl:(int)id;
@end
