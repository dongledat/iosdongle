#import "DataObject.h"

@implementation DataObject

@synthesize immutable;
@synthesize url;
@synthesize timeStamp;
@synthesize timeStampTZ;


/**
 * Default constructor
 */
- (id) init {
  if (self = [super init]) {
    isImmutable = NO;
    url = nil;
    timeStamp = nil;
    dataElements = [[[NSMutableArray alloc] init] autorelease];
  }
  return self;
}


/**
 * @note The URL type has been changed from to @a String to @a int
 */
- (id) initWithUrl:(int)url {
  if (self = [super init]) {
    isImmutable = NO;
    [self init:url];
  }
  return self;
}


/**
 * @note The URL type has been changed from to @a String to @a int
 */
- (id) init:(int)url interfaces:(NSMutableDictionary *)interfaces {
  if (self = [super init]) {
    isImmutable = NO;
    [self initDataObject:url typeRef:url interfaces:interfaces];
  }
  return self;
}


/**
 * @note The URL type has been changed from to @a String to @a int
 */
- (id) init:(int)functionUrl interfaces:(NSMutableDictionary *)interfaces forInputArguments:(BOOL)forInputArguments {
  if (self = [super init]) {
    isImmutable = NO;
    [self init:functionUrl];
    AbstractInterface * iface = (AbstractInterface *)[interfaces objectForKey:functionUrl];
    if (iface == nil || [iface interfaceType] != 0) {
      @throw [[[IllegalArgumentException alloc] init:[[@"The interface for function: " stringByAppendingString:functionUrl] stringByAppendingString:@" does not exist in the given interfaces"]] autorelease];
    }
    NSEnumerator * ifaceMembers;
    if (forInputArguments) {
      ifaceMembers = [((FunctionInterface *)iface) elementsIn];
    }
     else {
      ifaceMembers = [((FunctionInterface *)iface) elementsOut];
    }
    [self initDataObjectMembers:functionUrl interfaces:interfaces members:ifaceMembers];
  }
  return self;
}


/**
 * Copy constructor
 */
- (id) initWithDataObject:(DataObject *)dataObject {
  if (self = [super init]) {
    isImmutable = NO;
    dataElements = [[[NSMutableArray alloc] init] autorelease];
    if ([dataObject timeStampTZ] != nil) {
      timeStamp = [[[DateTZ alloc] init:[[dataObject timeStampTZ] time] param1:[[dataObject timeStampTZ] offset]] autorelease];
    }
     else {
      timeStamp = nil;
    }
    if ([dataObject url] != nil) {
      url = [dataObject url];
    }
     else {
      url = nil;
    }

    for (int cnt = 0; cnt < [dataObject size]; cnt++) {
      [self addElement:[[[DataElement alloc] init:[dataObject getElement:cnt]] autorelease]];
    }

  }
  return self;
}

- (id) init:(int)url interfaces:(NSMutableDictionary *)interfaces typeRef:(NSString *)typeRef {
  if (self = [super init]) {
    isImmutable = NO;
    [self initDataObject:url typeRef:typeRef interfaces:interfaces];
  }
  return self;
}

- (void) init:(int)url {
  if (url == nil) {
    @throw [[[IllegalArgumentException alloc] init:@"Url parameter can not be null"] autorelease];
  }
  url = url;
  timeStamp = nil;
  dataElements = [[[NSMutableArray alloc] init] autorelease];
}

- (void) initDataObject:(int)url typeRef:(NSString *)typeRef interfaces:(NSMutableDictionary *)interfaces {
  [self init:url];
  AbstractInterface * iface = (AbstractInterface *)[interfaces objectForKey:typeRef];
  if (iface == nil || !([iface interfaceType] == 1 || [iface interfaceType] == 2)) {
    @throw [[[IllegalArgumentException alloc] init:[[@"The data object interface url: " stringByAppendingString:typeRef] stringByAppendingString:@" does not exist in the given interfaces"]] autorelease];
  }
  [self initDataObjectMembers:url interfaces:interfaces members:[((TypeInterface *)iface) elementsMember]];
}

- (void) initDataObjectMembers:(int)url interfaces:(NSMutableDictionary *)interfaces members:(NSEnumerator *)members {

  while ([members hasMoreElements]) {
    ExlapType * member = (ExlapType *)[members nextObject];
    if ([member type] == 5) {
      [dataElements addElement:[[[DataElement alloc] init:[member name] param1:[[[DataObjectList alloc] init] autorelease] param2:[member type] param3:1 param4:nil] autorelease]];
    }
     else if ([member type] == 6) {
      [dataElements addElement:[[[DataElement alloc] init:[member name] param1:[[[DataObject alloc] init:[url stringByAppendingString:@"."] + [member name] param1:interfaces param2:[((ObjectEntity *)member) typeRef]] autorelease] param2:[member type] param3:1 param4:nil] autorelease]];
    }
     else {
      [dataElements addElement:[[[DataElement alloc] init:[member name] param1:nil param2:[member type] param3:0 param4:nil] autorelease]];
    }
  }

}

- (void) lock {
  isImmutable = YES;

  for (int cnt = 0; cnt < [self size]; cnt++) {
    [[self getElement:cnt] lock];
  }

}

- (int) size {
  return [dataElements size];
}

- (NSEnumerator *) elements {
  return [dataElements elements];
}

- (DataElement *) getElement:(int)index {
  return (DataElement *)[dataElements elementAt:index];
}


/**
 * @note The URL type has been changed from to @a String to @a int
 */
- (DataElement *) getElement:(int)elementUrl {
  int dataElems = [dataElements size];

  for (int cnt = 0; cnt < dataElems; cnt++) {
    if ([[[self getElement:cnt] name] isEqualTo:elementName]) {
      return [self getElement:cnt];
    }
  }

  return nil;
}


/**
 * Add data element to data object
 */
- (void) addElement:(DataElement *)dataElement {
  if (isImmutable) {
    @throw [[[IllegalArgumentException alloc] init:@"The data object is locked (immutable)."] autorelease];
  }
  int dataElems = [dataElements size];

  for (int cnt = 0; cnt < dataElems; cnt++) {
    if ([[[self getElement:cnt] name] isEqualTo:[dataElement name]]) {
      [dataElements setElementAt:dataElement param1:cnt];
      return;
    }
  }

  [dataElements addElement:dataElement];
}


/**
 * @note The URL type has been changed from to @a String to @a int
 */
- (void) setUrl:(int)url {
  if (isImmutable) {
    @throw [[[IllegalArgumentException alloc] init:@"The data object is locked (immutable)."] autorelease];
  }
  url = url;
}


/**
 * @note Is this method really needed?
 */
- (Date *) timeStamp {
  if (timeStamp == nil) {
    return nil;
  }
  return [[[Date alloc] init:([timeStamp time] + ((long)[[TimeZone default] getOffset:[timeStamp time]])) - ((long)[timeStamp offset])] autorelease];
}


/**
 * @note Is this method really needed?
 */
- (void) setTimeStamp {
  if (isImmutable) {
    @throw [[[IllegalArgumentException alloc] init:@"The data object is locked (immutable)."] autorelease];
  }
  timeStamp = [[[DateTZ alloc] init] autorelease];
}


/**
 * @note Is this method really needed?
 */
- (void) setTimeStamp:(Date *)date {
  if (isImmutable) {
    @throw [[[IllegalArgumentException alloc] init:@"The data object is locked (immutable)."] autorelease];
  }
  timeStamp = [[[DateTZ alloc] init:[date time] param1:[[TimeZone default] getOffset:[date time]]] autorelease];
}


/**
 * @note Is this method really needed?
 */
- (void) setTimeStampTZ:(DateTZ *)dateTZ {
  if (isImmutable) {
    @throw [[[IllegalArgumentException alloc] init:@"The data object is locked (immutable)."] autorelease];
  }
  timeStamp = dateTZ;
}


/**
 * @note The URL type has been changed from to @a String to @a int
 */
- (BOOL) containsElement:(int)elementUrl {
  return [self getElement:elementName] != nil;
}

- (int) hash {
  int i = 0;
  int hashCode = ((((dataElements == nil ? 0 : [dataElements hash]) + 31) * 31) + (timeStamp == nil ? 0 : [timeStamp hash])) * 31;
  if (url != nil) {
    i = [url hash];
  }
  return hashCode + i;
}


/**
 * Object comparator
 */
- (BOOL) isEqualTo:(NSObject *)obj {
  if (self == obj) {
    return YES;
  }
  if (obj == nil) {
    return NO;
  }
  if ([self class] != [obj class]) {
    return NO;
  }
  DataObject * other = (DataObject *)obj;
  if (dataElements == nil) {
    if (other.dataElements != nil) {
      return NO;
    }
  }
   else if (![dataElements isEqualTo:other.dataElements]) {
    return NO;
  }
  if (timeStamp == nil) {
    if (other.timeStamp != nil) {
      return NO;
    }
  }
   else if (![timeStamp isEqualTo:other.timeStamp]) {
    return NO;
  }
  if (url == nil) {
    if (other.url != nil) {
      return NO;
    }
    return YES;
  }
   else if (url == other.url) {
    return YES;
  }
   else {
    return NO;
  }
}


/**
 * Convert DataObject to printable representation
 */
- (NSString *) description {
  StringBuffer * result = [[[StringBuffer alloc] init:100] autorelease];
  [result append:@"[DataObject: url="];
  [result append:[self url]];
  if ([self timeStamp] != nil) {
    [result append:@", timeStamp="];
    [result append:[[self timeStampTZ] description]];
  }
  if ([self size] == 0) {
    [result append:@", no data elements!"];
  }
   else {

    for (int cnt = 0; cnt < [self size]; cnt++) {
      [result append:@"\n  "];
      [result append:[[self getElement:cnt] description]];
    }

  }
  [result append:@"]"];
  return [result description];
}

- (void) dealloc {
  [dataElements release];
  [timeStamp release];
  [super dealloc];
}

@end
