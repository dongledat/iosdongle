#import "UrlList.h"

@implementation UrlList

- (id) initWithDirectoryElements:(NSMutableArray *)directoryElements {
  if (self = [super init]) {
    directoryElements = nil;
    if (directoryElements == nil) {
      directoryElements = [[[NSMutableArray alloc] init] autorelease];
    }
     else {
      directoryElements = directoryElements;
    }
  }
  return self;
}

- (int) size {
  return [directoryElements size];
}

- (Url *) getElement:(int)elementNr {
  return (Url *)[directoryElements elementAt:elementNr];
}

- (Url *) getElement:(NSString *)urlName {
  int dirElems = [directoryElements size];

  for (int cnt = 0; cnt < dirElems; cnt++) {
    if ([[[self getElement:cnt] name] isEqualTo:urlName]) {
      return [self getElement:cnt];
    }
  }

  return nil;
}

- (NSEnumerator *) elements {
  return [directoryElements elements];
}

- (BOOL) containsUrl:(NSString *)urlName {
  return [self getElement:urlName] != nil;
}

- (int) hash {
  return (directoryElements == nil ? 0 : [directoryElements hash]) + 31;
}

- (BOOL) isEqualTo:(NSObject *)obj {
  if (self == obj) {
    return YES;
  }
  if (obj == nil) {
    return NO;
  }
  if ([self class] != [obj class]) {
    return NO;
  }
  UrlList * other = (UrlList *)obj;
  if (directoryElements == nil) {
    if (other.directoryElements != nil) {
      return NO;
    }
    return YES;
  }
   else if ([directoryElements isEqualTo:other.directoryElements]) {
    return YES;
  }
   else {
    return NO;
  }
}

- (NSString *) description {
  StringBuffer * result = [[[StringBuffer alloc] init] autorelease];
  [result append:@"[UrlList: "];

  for (int cnt = 0; cnt < [self size]; cnt++) {
    [result append:@"\n  "];
    [result append:[[self getElement:cnt] description]];
  }

  [result append:@"]"];
  return [result description];
}

- (void) dealloc {
  [directoryElements release];
  [super dealloc];
}

@end
