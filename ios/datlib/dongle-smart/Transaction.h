#pragma once

#include <functional>

namespace dat {

//todo: Change this
#define DEFAULT_EXPIRY_TIME (3000) // 3 seconds
#define TRANSACTION_EXPIRE_TIME_DONT_EXPIRE (0)

	class Transaction
	{
	public:
		Transaction(
			unsigned int transactionID,
			time_t expiryTime,
			unsigned int requestType,
			std::function<void(unsigned int)> expiryCallback);

		Transaction(
			unsigned int transactionID,
			unsigned int requestType,
			std::function<void(unsigned int)> expiryCallback);

		bool isExpired();
		void callExpiryCallback();

		static time_t current_time();
		static time_t miliseconds_to_time_t(unsigned int miliseconds);

	private:
		unsigned int m_transactionID;
		time_t m_startTime;
		time_t m_expiryTime;
		unsigned int m_requestType;
		std::function<void(unsigned int)> m_expiryCallback;
	};

}