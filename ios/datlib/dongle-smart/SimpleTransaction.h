#ifndef _SIMPLE_TRANSACTION_H_
#define _SIMPLE_TRANSACTION_H_

#include "ITransaction.h"

namespace dat {

	class SimpleTransaction : public ITransaction {
	public:
		SimpleTransaction(DongleSocket& socket, IDataListener& dataListener, TransactionType transactionType);
		
		int onMessage(TDongleRsp & rsp) override;

		void getAvailableDiagParameters();
		void getDataObject(unsigned int url);
		void getHWVersion();
		void getSerialNumber();
		void getFirmwareStatus();

	private:
		bool isMessageTypeExpected(unsigned int responseType) const;

		IDataListener & m_dataListener;
	};

}

#endif /* _SIMPLE_TRANSACTION_H_ */