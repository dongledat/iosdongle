#include "PinAuthenticationTransaction.h"

using namespace dat;

PinAuthenticationTransaction::PinAuthenticationTransaction(DongleSocket & dongleSocket, IDongleStatusListener & dongleStatusListener) :
	m_dongleStatusListener(dongleStatusListener),
	ITransaction(dongleSocket, TransactionType_PinAuth, true, true) // Expirable, and wait for reap
{
}

int PinAuthenticationTransaction::onMessage(TDongleRsp & rsp)
{
	if (!isMessageTypeExpected(rsp.which_response)) {
		return -1;
	}

	return 0;
}

bool PinAuthenticationTransaction::isMessageTypeExpected(unsigned int responseType) const
{
	return false;
}

void PinAuthenticationTransaction::beginAuthentication()
{
}
