#ifndef _PDU_TRANSACTION_H_
#define _PDU_TRANSACTION_H_

#include "ITransaction.h"
#include "DataAggregator.h"
#include "IConnection.h"

namespace dat {
	
	class PDUTransaction : public ITransaction {
	public:
		PDUTransaction(DongleSocket& socket, IDataListener& dataListener);
		int sendPDU(int canID, int canSpeed, int canSP, int canSJW, int canResponseTimeout, int tp20ChannelAddress, IConnection::PDUProtocol protocol, const std::string& reqPDU);
		int onMessage(TDongleRsp & rsp) override;

	private:
		bool isMessageTypeExpected(unsigned int responseType) const;

		bool m_requestSent;
		DataAggregator m_pduAggregator;
		IDataListener& m_dataListener;
	};

}

#endif /* _SIMPLE_TRANSACTION_H_ */