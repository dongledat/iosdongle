/**
 * @file    IConnection
 * @brief   IConnection interface class declaration
 */
#ifndef ICONNECTION_H
#define ICONNECTION_H

#include <string>
#include <list>
#include "dongle.pb.h"

namespace dat {

    class IOutputStream 
    {
    public:

        virtual void toNet(const unsigned char* data, size_t length) = 0;
    };

	class IDongleStatusListener
	{
	public:

		enum DongleStatus
		{
			UNKOWN, // ("Unknown")
			DEVICEISBUSY, // ("DeviceIsBusy")
			CONFIGURATIONVERIFYINPROGRESS, // ("ConfigurationVerifyInProgress")
			CONFIGUREDANDNOTCONNECTED, // ("ConfiguredAndNotConnected")
			NOTCONFIGUREDANDEOBDCOMMUNICATION, // ("NotConfiguredAndEOBDcommunication")
			CONFIGUREDANDCONNECTED, // ("ConfiguredAndConnected")
			NOTCONFIGUREDANDNOTCONNECTED, // ("NotConfiguredAndNotConnected")
			ERRORCORRUPTEDDEVICE, // ("ErrorCorruptedDevice")
			ERRORMEMORYPROBLEM, // ("ErrorMemoryProblem")
		};

		enum AuthenticationMethod 
		{
			AUTH_METHOD_NONE,
			AUTH_METHOD_PIN,
			AUTH_METHOD_TOKEN,
		};

		enum AuthenticationResult 
		{
			AUTH_RESULT_NONE,
			AUTH_RESULT_IN_PROGRESS,
			AUTH_RESULT_PASS,
			AUTH_RESULT_FAIL,
			AUTH_RESULT_TIMEOUT
		};

		// TODO: do we need this?
		enum CurrentAppMode
		{
			UNKNOWN, //  ("unknown")
			USER_APP, //  ("UserApp")
			SERVICE_APP, //  ("ServiceApp")
			ULOADER, //  ("uLoader")
		};

		virtual void onDongleStatus(DongleStatus dongleStatus) = 0;
		virtual void onAuthenticate(AuthenticationMethod authenticationMethod, AuthenticationResult authenticationResult) = 0;
	};

    class IDataListener
    {
    public:

		struct FWStatus
		{
			std::string m_dbVersion;
			std::string m_serviceAppVersion;
			std::string m_userAppVersion;
			std::string m_uLoaderVersion;
		};

        virtual void onData(int respID, TDataObject & dataObject, std::string & rspAsString) = 0;
        virtual void onAvailableDiagParameters(int respID, const std::list<TDataCatalogEntry>& l, std::string & rspAsString) = 0;
        virtual void onDongleHWRevision(int respID, const std::string&) = 0;
        virtual void onDongleSerialNumber(int respID, const std::string&) = 0;
        virtual void onFirmwareStatus(int respID, const FWStatus& fwStatus) = 0;
        virtual void onPDUResponse(int respID, const std::string& pduRawDataResponse) = 0;
    };

    class IUpdateProgressListener
    {
    public:

		enum UpdateStatus
		{
			UPDATE_NONE,
			UPDATE_STARTED,
			UPDATE_FAILED,
			UPDATE_COMPLETE,
			UPDATE_CHUNK_SENT
		};

        virtual void onUpdateStatus(UpdateStatus) = 0;
    };

    /**
     * @class IConnection
     */
    class IConnection {

    public:

        IConnection() 
        {}

        virtual void fromNet(const unsigned char* data, size_t length) = 0;

        /**
         * @brief First time Smartphone authentication by user and PIN.
         * @param user user name.
         * @param pin Hexadecimal 8-digit number appearing on Donlge's sticker.
         * @note There is no need to specify user as in Dongle 1.0.
         *
         * This API is kept for possible future extensions. In current implementation @b user parameter is always equal to @a 'default'.
         */
        virtual void authenticateByPIN(const std::string& user, const std::string& pin) = 0;

        /**
         * @brief Authenticate Dongle by token.
         *
         * The result of token authentication is returned in @ref IConnectionListener::onAuthenticateByToken() callback funtion.
         */
        virtual void authenticateByToken(const std::string& token) = 0;

        /**
         * @brief Check is Smartphone is authenticated with Dongle by PIN.
         * @return True if Smartphone is already authenticated with Dongle by PIN.
         * @see authenticateByPIN(String pin).
         */
        virtual bool isAuthenticatedByPIN() const = 0;

        /**
         * @brief Check is Smartphone is authenticated with Dongle by token.
         * @return True if Smartphone is already authenticated with Dongle by token.
         * @see authenticateByToken().
         */
        virtual bool isAuthenticatedByToken() const = 0;

        /**
         * @brief Subscribe a group of diagnostics parameters.
         * @param url Represents a group of diagnostics parameters.
         * @param val Refresh timeout.
         * @note The URL type is changed from String to int.
		 * @return request id
		 */
        virtual int subscribeObject(int url, int val) = 0;

        /**
         * @brief Unsubscribe a group of diagnostics parameters.
         * @param url Represents a group of diagnostics parameters.
         * @note The URL type is changed from String to int.
		 * @return request id
		 */
        virtual int unsubscribeObject(int transactionID) = 0;

        /**
         * @brief Retreive a list of available groups of diagnostics parameters.
		 * @return request id
         */
        virtual int getAvailableDiagParametersAsync() = 0;

        /**
         * @brief Retreive a diagnostics parameter group by URL.
		 * @return request id
		 */
        virtual int getDataObjectAsync(int url) = 0;

        /**
         * @brief Retreive Dongle H/W revision.
		 * @return request id
         */
        virtual int getDongleHWRevisionAsync() = 0;

        /**
         * @brief Retreive Dongle S/N.
		 * @return request id
         */
        virtual int getDongleSerialNumberAsync() = 0;

        /**
         * @brief Retreive Dongle firmware status.
         * @see IFWStatus.
		 * @return request id
         */
        virtual int getFirmwareStatusAsync() = 0;

        /**
         * @brief Start asynchronous configuration update.
         */
        virtual int startUpdate(const std::string& signature) = 0;

        /**
         * @brief Send next update chunk
         */
        virtual int sendUpdateChunk(int transactionID, const unsigned char* chunk, size_t size) = 0;

        /**
         * @brief Cancel asynchronous configuration update.
         */
        virtual int cancelUpdate(int transactionID) = 0;

		/**
		 * @brief Complete asynchronous configuration update.
		 */
		virtual int completeUpdate(int transactionID) = 0;

        /**
         * @brief sendPDU request result
         */
        enum PDURequestResult {
            RequestOK, /** */
            CanIdError,
            CanSpeedError,
            CanProtocolError,
            VehicleSpeedInvalid
        };

        /**
         * @brief sendPDU response status
         */
        enum PDUResponseStatus {
            ResponseOK,
            ServiceNotAllowed,
            ResponseSizeExceedsMaximumBufferSize,
            MemoryError,
            CanCommunicationError
        };

        /**
         * @brief Supported protocols
         */
        enum PDUProtocol {
            TP20,
            ISO14229,
            ISO15765
        };

        /**
         * @brief Send up to 5 PDUs
         * @param[in] canID min=0x200, max=0x17FC00C7
         * @param[in] canSpeed Kbit/s min=125, max=500
         * @param[in] canSP in % min=50, max=90
         * @param[in] canSJW min=1, max=4
         * @param[in] canResponseTimeout Timeout for the ECU response
         * @param[in] tp20ChannelAddress ECU address for the communication with TP20
         * @param[in] protocol (see @ref PDUProtocol)
         * @param[in] reqPDU array of bytes represented by a base 64 encoding corresponding to the payload sent directly the ECU
		 * @return request id
         */
        virtual int sendPDU(int canID, int canSpeed, int canSP, int canSJW, int canResponseTimeout, int tp20ChannelAddress, PDUProtocol protocol, const std::string& reqPDU) = 0;

        /**
         * @brief Start Backend certificate update.
         * @note New method.
         */
        //void updateCertificate(Certificate certificate, IUpdateCertificateListener listener);
    };

	IConnection* createDongleConnection(const std::string& version, IOutputStream*, IDongleStatusListener*, IDataListener*, IUpdateProgressListener*);
	bool destroyDongleConnection(IConnection*);
}

#endif
