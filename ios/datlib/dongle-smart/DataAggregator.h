#ifndef _DATA_AGGREGATOR_H_
#define _DATA_AGGREGATOR_H_

#include <string>
#include <vector>
#include <map>

namespace dat {

	class DataChunk
	{
	public:
		DataChunk(unsigned int dataElementID, unsigned int chunkIndex, unsigned int totalCount, std::string && data);
		DataChunk(DataChunk && other);

		unsigned int getDataElementID() { return m_dataElementID; };
		unsigned int getChunkIndex() { return m_chunkIndex; };
		unsigned int getTotalCount() { return m_totalCount; };
		std::string&& popData() { return std::move(m_data); };

	private:
		unsigned int m_dataElementID;
		unsigned int m_chunkIndex;
		unsigned int m_totalCount;
		std::string m_data;
	};

	class DataElement
	{
	public:
		explicit DataElement(DataChunk && firstChunk);
		void addChunk(DataChunk && chunk);
		bool isComplete();
		unsigned int getDataElementID();
		std::string popCompletedData();

	private:
		unsigned int m_dataElementID;
		unsigned int m_expectedChunkCount;
		std::map<unsigned int, std::string> m_dataChunks;
	};

	class DataAggregator
	{
	public:
		DataAggregator();

		void onDataFragment(DataChunk &&chunk);
		bool isCompleted(unsigned int dataElementID);
		std::string popAggregatedData(unsigned int dataElementID);
		unsigned int getDataCount();

	private:
		std::map<unsigned int, DataElement> m_dataElements;
	};

}

#endif /* _DATA_AGGREGATOR_H_*/