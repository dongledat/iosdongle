#include "SimpleTransaction.h"

using namespace dat;

SimpleTransaction::SimpleTransaction(DongleSocket & socket, IDataListener & dataListener, TransactionType transactionType) :
	m_dataListener(dataListener),
	ITransaction(socket, transactionType)
{
}

void dat::SimpleTransaction::getAvailableDiagParameters()
{
	TDongleReq req = TDongleReq_init_zero;
	req.which_request = TDongleReq_GetAvailableDiagnosticParameterReq_tag;
	sendMessage(req);
}

void dat::SimpleTransaction::getDataObject(unsigned int url)
{
	TDongleReq req = TDongleReq_init_zero;
	req.which_request = TDongleReq_GetDataObjectReq_tag;
	req.request.GetDataObjectReq.UrlCode = url;
	sendMessage(req);
}

void dat::SimpleTransaction::getHWVersion()
{
	TDongleReq req = TDongleReq_init_zero;
	req.which_request = TDongleReq_HwVersionReq_tag;
	sendMessage(req);
}

void dat::SimpleTransaction::getSerialNumber()
{
	TDongleReq req = TDongleReq_init_zero;
	req.which_request = TDongleReq_SerialNumberDeviceReq_tag;
	sendMessage(req);
}

void dat::SimpleTransaction::getFirmwareStatus()
{
	TDongleReq req = TDongleReq_init_zero;
	req.which_request = TDongleReq_FwStatusReq_tag;
	sendMessage(req);
}


int SimpleTransaction::onMessage(TDongleRsp & rsp)
{
	if (!isMessageTypeExpected(rsp.which_response)) {
		return -1;
	}

	switch (rsp.which_response)
	{
	case (TDongleRsp_HwVersionRsp_tag):
	{
		m_dataListener.onDongleHWRevision(rsp.id, std::string(rsp.response.HwVersionRsp.Board,
			sizeof(rsp.response.HwVersionRsp.Board)));
		break;
	}
	case (TDongleRsp_SerialNumberDeviceRsp_tag):
	{
		m_dataListener.onDongleSerialNumber(rsp.id, std::string(rsp.response.SerialNumberDeviceRsp.SerialNumber,
			sizeof(rsp.response.SerialNumberDeviceRsp.SerialNumber)));
		break;
	}
	case (TDongleRsp_FwStatusRsp_tag):
	{
		IDataListener::FWStatus status;
		status.m_serviceAppVersion = rsp.response.FwStatusRsp.Ver_serviceApp.ver;
		status.m_uLoaderVersion = rsp.response.FwStatusRsp.Ver_uLoader.ver;
		status.m_userAppVersion = rsp.response.FwStatusRsp.Ver_userApp.ver;
		//todo: what about status.m_dbVersion?

		m_dataListener.onFirmwareStatus(rsp.id, status);
		break;
	}
	case (TDongleRsp_GetAvailableDiagnosticParameterRsp_tag):
	{
		std::list<TDataCatalogEntry> params;

		for (int i = 0; i < rsp.response.GetAvailableDiagnosticParameterRsp.DataCatalogEntry_count; ++i) {
			params.push_back(rsp.response.GetAvailableDiagnosticParameterRsp.DataCatalogEntry[i]);
		}

		// Serialize the response
		unsigned char buffer[4096];
		pb_ostream_t ostream = pb_ostream_from_buffer(buffer, 4096);
		if (!pb_encode(&ostream, TDongleRsp_fields, &rsp)) {
			return -1;
		}
		std::string rspAsString(buffer, buffer + ostream.bytes_written);

		m_dataListener.onAvailableDiagParameters(rsp.id, params, rspAsString);
		break;
	}
	case (TDongleRsp_GetDataObjectRsp_tag):
	{

		// Serialize the response
		unsigned char buffer[4096];
		pb_ostream_t ostream = pb_ostream_from_buffer(buffer, 4096);
		if (!pb_encode(&ostream, TDongleRsp_fields, &rsp)) {
			return -1;
		}
		std::string rspAsString(buffer, buffer + ostream.bytes_written);

		m_dataListener.onData(rsp.id, rsp.response.GetDataObjectRsp.DataObject, rspAsString);
		break;
	}

	default:
		// Unknown response
		return -1;
	}

	m_isCompleted = true;
	return 0;
}

bool dat::SimpleTransaction::isMessageTypeExpected(unsigned int responseType) const
{
	auto test = getTransactionType();
	switch (responseType)
	{
	case TDongleRsp_GetAvailableDiagnosticParameterRsp_tag:
		return getTransactionType() == TransactionType_GetAvailableDiagnosticParameter;

	case TDongleRsp_GetDataObjectRsp_tag:
		return getTransactionType() == TransactionType_GetDataObject;

	case TDongleRsp_HwVersionRsp_tag:
		return getTransactionType() == TransactionType_GetHwVersion;

	case TDongleRsp_SerialNumberDeviceRsp_tag:
		return getTransactionType() == TransactionType_GetSerialNumberDevice;

	case TDongleRsp_FwStatusRsp_tag:
		return getTransactionType() == TransactionType_GetFwStatus;

	default:
		//todo: Unknown..
		return false;
	}
}