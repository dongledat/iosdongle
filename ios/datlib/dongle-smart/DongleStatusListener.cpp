#include "DongleStatusListener.h"

using namespace dat;

void DongleStatusListener::onDongleStatus(DongleStatus dongleStatus)
{
	m_isCalled_onDongleStatus = true;
}

void DongleStatusListener::onAuthenticate(AuthenticationMethod authenticationMethod, AuthenticationResult authenticationResult)
{
	m_isCalled_onAuthenticate = true;
}
