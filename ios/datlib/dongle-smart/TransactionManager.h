#ifndef _TRANSACTION_MANAGER_H
#define _TRANSACTION_MANAGER_H

#include "ITransaction.h"
#include "PDUTransaction.h"
#include "DataSubscriptionTransaction.h"
#include "SimpleTransaction.h"
#include "ConfigurationUpdateTransaction.h"
#include "PinAuthenticationTransaction.h"
#include "TokenAuthenticationTransaction.h"

#include <map>

namespace dat {

	class TransactionManager {
	public:

		TransactionManager(IDataListener & dataListener, IUpdateProgressListener& m_updateProgressListener, IDongleStatusListener& dongleStatusListener, DongleSocket& dongleSocket);
		~TransactionManager();

		// Factory methods
		SimpleTransaction* newSimpleTransaction(TransactionType type);
		DataSubscriptionTransaction* newDataSubscriptionTransaction();
		PDUTransaction* newPDUTransaction();
		ConfigurationUpdateTransaction* newConfigurationUpdateTransaction();
		PinAuthenticationTransaction* newPinAuthenticationTransaction();
		TokenAuthenticationTransaction* newTokenAuthenticationTransaction();

		// Collection methods
		ITransaction* findTransaction(TransactionID_t id);
		ITransaction* findTransaction(TransactionID_t id, TransactionType type);
		void cleanupTransactions();
		unsigned int getTransactionCount();

		// Getters
		std::map<TransactionID_t, ITransaction*> & getTransactions() { return m_transactions; };

	private:
		bool existsActiveTransaction(TransactionType type);

		IDataListener & m_dataListener;
		IUpdateProgressListener& m_updateProgressListener;
		IDongleStatusListener& m_dongleStatusListener;
		DongleSocket& m_dongleSocket;

		std::map<TransactionID_t, ITransaction*> m_transactions;
	};


}



#endif
