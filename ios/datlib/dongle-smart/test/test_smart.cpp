#include "CppUTest/TestHarness.h"
#include "CppUTest/TestOutput.h"

#include "net/Net.h"
#include "os/Thread.h"
#include "dongle-sim-lib/DongleSimulator.h"
#include "dongle-sim-lib/CarSimulatorSupportedDiagnostics.h"

#include "net/UdpClient.h"
#include "dongle-smart\IConnection.h"
#include "dongle-smart\Connection.h"
#include "dongle-smart\DongleStatusListener.h"
#include "dongle-smart\DataListener.h"
#include "dongle-smart\UpdateProgressListener.h"

#include "dongle-smart\DataAggregator.h"
#include "dongle-sim-lib\DataSubscription.h"

#define SERVER_UDP_PORT (11112)//(13371)
#define CLIENT_UDP_SRC_PORT (13372)

#define BUFFER_SIZE (1000)

#define JAVA_STUBS

class MyTestOutputStream : public dat::IOutputStream
{

public:
	MyTestOutputStream(const net::UdpClient & client) :
		m_client(client) {
	}

	virtual void toNet(const unsigned char* data, size_t length) {
		m_client.send(reinterpret_cast<const char*>(data), length);
	}

private:
	net::UdpClient m_client;
};

static dat::DongleStatusListener dongleStatusListener;
static dat::DataListener dataListener;
static dat::UpdateProgressListener updateProgressListener;

static MyTestOutputStream* outputStream;
static DongleSimulator * simulator;
static dat::Connection* con;

static net::UdpClient* smartphoneUdp;

extern SubscriptionManager g_DiagCodeSubscriptionManager;

TEST_GROUP(Smart)
{

	TEST_SETUP()
	{
		ConsoleTestOutput output;
		MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
		auto hack  = DongleSimulator::get();
		MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();

		net::init();

		// [DONGLE] Start simulator in new thread, let it initialize
		/*
		simulator = DongleSimulator::get();
		simulator->start(SERVER_UDP_PORT);
		SleepEx(500, TRUE);
		*/
		// [SMARTPHONE] Initialize connection, bind to src port, set target port as the simulator's server port
		smartphoneUdp = new net::UdpClient("127.0.0.1", SERVER_UDP_PORT);
		smartphoneUdp->bindSrcPort(CLIENT_UDP_SRC_PORT);
		
		output << "Smart:: connected..." << "127.0.0.1" << "," << SERVER_UDP_PORT;
		outputStream = new MyTestOutputStream(*smartphoneUdp);
		con = new dat::Connection(*outputStream, dongleStatusListener, dataListener, updateProgressListener);
		output << "Smart:: after Init";
#ifdef JAVA_STUBS
		dongleStatusListener.reset();
		dataListener.reset();
		updateProgressListener.reset();
#endif

	}

	TEST_TEARDOWN()
	{
		simulator->stop();
		delete con;
		delete outputStream;
		delete smartphoneUdp;
		g_DiagCodeSubscriptionManager.clear();
		

		net::done();
		//MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();
	}

};

TEST_GROUP(Smart_NoDongle)
{

	TEST_SETUP()
	{
		MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
		net::init();

		// [SMARTPHONE] Initialize connection, bind to src port, set target port as the simulator's server port
		smartphoneUdp = new net::UdpClient("127.0.0.1", SERVER_UDP_PORT);
		smartphoneUdp->bindSrcPort(CLIENT_UDP_SRC_PORT);
		outputStream = new MyTestOutputStream(*smartphoneUdp);
		con = new dat::Connection(*outputStream, dongleStatusListener, dataListener, updateProgressListener);

#ifdef JAVA_STUBS
		dongleStatusListener.reset();
		dataListener.reset();
		updateProgressListener.reset();
#endif
	}

	TEST_TEARDOWN()
	{
		delete con;
		delete outputStream;
		delete smartphoneUdp;

		net::done();
		MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();
	}

};


TEST_GROUP(DataAggregator)
{
};

TEST(Smart, SmartGetDongleHWRevision)
{
	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->getDongleHWRevisionAsync();

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// [SMARTPHONE] Get the raw response from the net
	char buffer[BUFFER_SIZE] = { 0 };
	int length = smartphoneUdp->recv(buffer, BUFFER_SIZE);

	// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
	TDongleRsp rsp = TDongleRsp_init_zero;
	pb_istream_t stream = pb_istream_from_buffer((const unsigned char*)buffer, length);
	if (!pb_decode(&stream, TDongleRsp_fields, &rsp)) {
		FAIL("pb_decode failed on response");
	}

	CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
	CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
	CHECK_TEXT(rsp.which_response == TDongleRsp_HwVersionRsp_tag, "response type wrong");

	dataListener.reset();

	// [SMARTPHONE] Pass the data for further handling
	con->fromNet((const unsigned char*)buffer, length);

#ifdef JAVA_STUBS
	// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
	CHECK_TEXT(dataListener.m_isCalled_onDongleHWRevision, "JAVA callback not called");
#endif
}

TEST(Smart, SmartMultipleSimpleRequests)
{
	int first_trans_id = 0;

	for (int i = 0; i < 10; ++i) {
		// [SMARTPHONE] Request something from the dongle
		int transaction_id = con->getDongleHWRevisionAsync();

		if (i == 0) {
			first_trans_id = transaction_id;
		}
		else {
			CHECK_TEXT(i +  first_trans_id == transaction_id, "Transaction ID static");
		}
		

		// Now a UDP request is sent to the dongle simulator
		// Now the dongle simulator sends a UDP response

		// [SMARTPHONE] Get the raw response from the net
		char buffer[BUFFER_SIZE] = { 0 };
		int length = smartphoneUdp->recv(buffer, BUFFER_SIZE);

		// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
		TDongleRsp rsp = TDongleRsp_init_zero;
		pb_istream_t stream = pb_istream_from_buffer((const unsigned char*)buffer, length);
		if (!pb_decode(&stream, TDongleRsp_fields, &rsp)) {
			FAIL("pb_decode failed on response");
		}

		CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
		CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
		CHECK_TEXT(rsp.which_response == TDongleRsp_HwVersionRsp_tag, "response type wrong");

		dataListener.reset();

		// [SMARTPHONE] Pass the data for further handling
		con->fromNet((const unsigned char*)buffer, length);

#ifdef JAVA_STUBS
		// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
		CHECK_TEXT(dataListener.m_isCalled_onDongleHWRevision, "JAVA callback not called");
#endif
	}
}

TEST(Smart, SmartGetDataObject)
{
	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->getDataObjectAsync(DIAG_URL_CODE_THROTTLE_POSITION);

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// [SMARTPHONE] Get the raw response from the net
	char buffer[BUFFER_SIZE] = { 0 };
	int length = smartphoneUdp->recv(buffer, BUFFER_SIZE);

	// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
	TDongleRsp rsp = TDongleRsp_init_zero;
	pb_istream_t stream = pb_istream_from_buffer((const unsigned char*)buffer, length);
	if (!pb_decode(&stream, TDongleRsp_fields, &rsp)) {
		FAIL("pb_decode failed on response");
	}

	CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
	CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
	CHECK_TEXT(rsp.which_response == TDongleRsp_GetDataObjectRsp_tag, "response type wrong");

	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.UrlCode == DIAG_URL_CODE_THROTTLE_POSITION, "wrong url code");
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement_count == 2, "bad element count");
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].code == DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "expected DIAG_CODE_THROTTLE_POSITION_ABSOLUTE");
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].code == DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected DIAG_CODE_THROTTLE_POSITION_RELATIVE");
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].which_value == TDataElemenent_u_tag, "expected TDataElemenent_u_tag");
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].which_value == TDataElemenent_u_tag, "expected TDataElemenent_u_tag");
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.u == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "expected SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_ABSOLUTE");
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].value.u == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE");

	pb_release(TDongleRsp_fields, &rsp);

	dataListener.reset();

	// [SMARTPHONE] Pass the data for further handling
	con->fromNet((const unsigned char*)buffer, length);

#ifdef JAVA_STUBS
	// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
	CHECK_TEXT(dataListener.m_isCalled_onData, "JAVA callback not called");
#endif
}


TEST(Smart, SmartSubscribe)
{
	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->subscribeObject(DIAG_URL_CODE_THROTTLE_POSITION, 200);
	bool got_sub_response = false;

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// Get 10 updates
	for (int i = 0; i < 5; ++i) {

		// [SMARTPHONE] Get the raw response from the net
		char buffer[BUFFER_SIZE] = { 0 };
		int length = smartphoneUdp->recv(buffer, BUFFER_SIZE);

		// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
		TDongleRsp rsp = TDongleRsp_init_zero;
		pb_istream_t stream = pb_istream_from_buffer((const unsigned char*)buffer, length);
		if (!pb_decode(&stream, TDongleRsp_fields, &rsp)) {
			FAIL("pb_decode failed on response");
		}

		if (TDongleRsp_SubscribeObjectRsp_tag == rsp.which_response) {
			if (got_sub_response) {
				FAIL("Got subscription response twice");
			}

			CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
			CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
			CHECK_TEXT(rsp.response.SubscribeObjectRsp.result == TSubscribeObjectRsp_EResult_ok, "Subscribe error");
			got_sub_response = true;
			continue;
		}

		CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
		CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
		CHECK_TEXT(rsp.which_response == TDongleRsp_DataObjectUpdateRsp_tag, "response type wrong");

		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.UrlCode == DIAG_URL_CODE_THROTTLE_POSITION, "wrong url code");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement_count == 2, "bad element count");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].code == DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "expected DIAG_CODE_THROTTLE_POSITION_ABSOLUTE");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].code == DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected DIAG_CODE_THROTTLE_POSITION_RELATIVE");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].which_value == TDataElemenent_u_tag, "expected TDataElemenent_u_tag");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].which_value == TDataElemenent_u_tag, "expected TDataElemenent_u_tag");
		auto expected_value = DATA_SUBSCRIPTION_SIMULATION_BASE_VALUE +
			(DATA_SUBSCRIPTION_SIMULATION_VALUE_INCREMENTS * i);
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.u == expected_value, "updated value not as expected");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].value.u == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE");

		pb_release(TDongleRsp_fields, &rsp);

		dataListener.reset();

		// [SMARTPHONE] Pass the data for further handling
		con->fromNet((const unsigned char*)buffer, length);

#ifdef JAVA_STUBS
		// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
		CHECK_TEXT(dataListener.m_isCalled_onData, "JAVA callback not called");
#endif

	}
}

TEST(Smart, SmartSubscribeUnsubscribe)
{
	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->subscribeObject(DIAG_URL_CODE_THROTTLE_POSITION, 200);
	bool got_sub_response = false;

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// Get 10 updates
	for (int i = 0; i < 5; ++i) {

		// [SMARTPHONE] Get the raw response from the net
		char buffer[BUFFER_SIZE] = { 0 };
		int length = smartphoneUdp->recv(buffer, BUFFER_SIZE);
		if (0 == length) {
			FAIL("Unexpected: got 0 bytes from UDP");
			break;
		}

		// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
		TDongleRsp rsp = TDongleRsp_init_zero;
		pb_istream_t stream = pb_istream_from_buffer((const unsigned char*)buffer, length);
		if (!pb_decode(&stream, TDongleRsp_fields, &rsp)) {
			FAIL("pb_decode failed on response");
		}

		if (TDongleRsp_SubscribeObjectRsp_tag == rsp.which_response) {
			if (got_sub_response) {
				FAIL("Got subscription response twice");
			}

			CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
			CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
			CHECK_TEXT(rsp.response.SubscribeObjectRsp.result == TSubscribeObjectRsp_EResult_ok, "Subscribe error");
			got_sub_response = true;
			continue;
		}

		CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
		CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
		CHECK_TEXT(rsp.which_response == TDongleRsp_DataObjectUpdateRsp_tag, "response type wrong");

		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.UrlCode == DIAG_URL_CODE_THROTTLE_POSITION, "wrong url code");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement_count == 2, "bad element count");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].code == DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "expected DIAG_CODE_THROTTLE_POSITION_ABSOLUTE");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].code == DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected DIAG_CODE_THROTTLE_POSITION_RELATIVE");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].which_value == TDataElemenent_u_tag, "expected TDataElemenent_u_tag");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].which_value == TDataElemenent_u_tag, "expected TDataElemenent_u_tag");
		auto expected_value = DATA_SUBSCRIPTION_SIMULATION_BASE_VALUE +
			(DATA_SUBSCRIPTION_SIMULATION_VALUE_INCREMENTS * i);
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.u == expected_value, "updated value not as expected");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].value.u == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE");

		pb_release(TDongleRsp_fields, &rsp);

		dataListener.reset();

		// [SMARTPHONE] Pass the data for further handling
		con->fromNet((const unsigned char*)buffer, length);

#ifdef JAVA_STUBS
		// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
		CHECK_TEXT(dataListener.m_isCalled_onData, "JAVA callback not called");
#endif

	}

	auto uns_transaction_id = con->unsubscribeObject(transaction_id);
	CHECK(transaction_id == uns_transaction_id);

	// [SMARTPHONE] Get the raw response from the net
	char buffer[BUFFER_SIZE] = { 0 };
	int length = smartphoneUdp->recv(buffer, BUFFER_SIZE);

	// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
	TDongleRsp rsp = TDongleRsp_init_zero;
	pb_istream_t stream = pb_istream_from_buffer((const unsigned char*)buffer, length);
	if (!pb_decode(&stream, TDongleRsp_fields, &rsp)) {
		FAIL("pb_decode failed on response");
	}

	CHECK_TEXT(rsp.which_response == TDongleRsp_UnsubscribeObjectRsp_tag, "response type wrong");
	CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
	CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
	CHECK_TEXT(rsp.response.UnsubscribeObjectRsp.result == TUnsubscribeObjectRsp_EResult_ok, "Unsubscribe error");

}

TEST(Smart_NoDongle, CheckExpiry)
{
	int transaction_id = con->getDongleHWRevisionAsync();
	auto& trans = con->getTransactions();

	if (trans.end() == trans.find(transaction_id)) {
		FAIL("Our request wasn't saved in the transaction mapping")
	}

	Sleep(500);

	int transaction_id2 = con->getDongleHWRevisionAsync();

	if (trans.end() == trans.find(transaction_id)) {
		FAIL("Our request expired too soon (?)")
	}
	if (trans.end() == trans.find(transaction_id2)) {
		FAIL("Our request wasn't saved in the transaction mapping")
	}

	Sleep(2000);

	// This should clean up the first transaction
	int transaction_id3 = con->getDongleHWRevisionAsync();

	if (trans.end() != trans.find(transaction_id)) {
		FAIL("Our old request hasn't expired")
	}

}

TEST(Smart_NoDongle, CheckSubscribeNoExpiry)
{
	auto transaction_id = con->subscribeObject(DIAG_URL_CODE_THROTTLE_POSITION, 100);
	auto& trans = con->getTransactions();

	if (trans.end() == trans.find(transaction_id)) {
		FAIL("Our 1st request wasn't saved in the transaction mapping")
	}

	Sleep(3000);

	// This should NOT clean up the first transaction
	int transaction_id2 = con->getDongleHWRevisionAsync();

	if (trans.end() == trans.find(transaction_id)) {
		FAIL("Our subscribe transaction expired, even though it shouldn't have")
	}

	if (trans.end() == trans.find(transaction_id2)) {
		FAIL("Our 2nd request wasn't saved in the transaction mapping")
	}

}

TEST(DataAggregator, SimpleAggregator)
{
	dat::DataAggregator a;

	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");
	a.onDataFragment(dat::DataChunk(5, 0, 3, "aaa"));
	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");
	CHECK_TEXT(!a.isCompleted(3), "Should not be completed");
	a.onDataFragment(dat::DataChunk(5, 1, 3, "bbb"));
	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");
	a.onDataFragment(dat::DataChunk(5, 2, 3, "ccc"));

	CHECK_TEXT(a.isCompleted(5), "Should be completed");
	CHECK_TEXT(!a.isCompleted(3), "Should not be completed");

	std::string result = a.popAggregatedData(5);
	std::string bad_result = a.popAggregatedData(1);

	CHECK_TEXT(0 == strcmp("", bad_result.c_str()), "Wrong output");
	CHECK_TEXT(0 == strcmp("aaabbbccc", result.c_str()), "Wrong output");


	result = a.popAggregatedData(5);

	CHECK_TEXT(0 == strcmp("", result.c_str()), "Wrong output");
}

TEST(DataAggregator, SimpleAggregatorMixedOrder)
{
	dat::DataAggregator a;

	a.onDataFragment(dat::DataChunk(5, 0, 3, "aaa"));
	a.onDataFragment(dat::DataChunk(5, 2, 3, "ccc"));
	a.onDataFragment(dat::DataChunk(5, 1, 3, "bbb"));

	CHECK_TEXT(a.isCompleted(5), "Should be completed");
	CHECK_TEXT(!a.isCompleted(1), "Should not be completed");

	std::string result = a.popAggregatedData(5);
	std::string bad_result = a.popAggregatedData(1);

	CHECK_TEXT(0 == strcmp("", bad_result.c_str()), "Wrong output");
	CHECK_TEXT(0 == strcmp("aaabbbccc", result.c_str()), "Wrong output");


	result = a.popAggregatedData(5);

	CHECK_TEXT(0 == strcmp("", result.c_str()), "Wrong output");
}

TEST(DataAggregator, WrongIndex)
{
	dat::DataAggregator a;

	a.onDataFragment(dat::DataChunk(5, 0, 3, "aaa"));
	a.onDataFragment(dat::DataChunk(5, 2, 3, "ccc"));
	a.onDataFragment(dat::DataChunk(5, 5, 3, "bbb")); // wrong index
	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");

	std::string result = a.popAggregatedData(5);
	CHECK_TEXT(0 == strcmp("", result.c_str()), "Wrong output");

	a.onDataFragment(dat::DataChunk(5, 1, 4, "bbb")); // wrong count
	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");

	result = a.popAggregatedData(5);
	CHECK_TEXT(0 == strcmp("", result.c_str()), "Wrong output");
}

TEST(DataAggregator, AggregatorPopping)
{
	dat::DataAggregator a;

	CHECK_TEXT(0 == a.getDataCount(), "Should be zero");

	a.onDataFragment(dat::DataChunk(5, 0, 3, "aaa"));

	CHECK_TEXT(1 == a.getDataCount(), "Should be 1");

	a.onDataFragment(dat::DataChunk(5, 2, 3, "ccc"));

	CHECK_TEXT(1 == a.getDataCount(), "Should be 1");

	a.onDataFragment(dat::DataChunk(5, 1, 3, "bbb"));

	CHECK_TEXT(a.isCompleted(5), "Should be completed");
	CHECK_TEXT(1 == a.getDataCount(), "Should be 1");

	std::string result = a.popAggregatedData(5);
	CHECK_TEXT(0 == a.getDataCount(), "Should be 0");

	CHECK_TEXT(0 == strcmp("aaabbbccc", result.c_str()), "Wrong output");
}