#pragma once

#include <string>
#include <vector>
#include <map>

namespace dat {

	class PDUDataChunk
	{
	public:
		PDUDataChunk(unsigned int transactionID, unsigned int chunkIndex, unsigned int totalCount, std::string && data);
		PDUDataChunk(PDUDataChunk && other);

		unsigned int getTransactionID() { return m_transactionID; };
		unsigned int getChunkIndex() { return m_chunkIndex; };
		unsigned int getTotalCount() { return m_totalCount; };
		std::string&& popData() { return std::move(m_data); };

	private:
		unsigned int m_transactionID;
		unsigned int m_chunkIndex;
		unsigned int m_totalCount;
		std::string m_data;
	};

	class PDUResponse
	{
	public:
		explicit PDUResponse(PDUDataChunk && firstChunk);
		void addChunk(PDUDataChunk && chunk);
		bool isComplete();
		unsigned int getTransactionID();
		std::string popCompletedData();

	private:
		unsigned int m_transactionID;
		unsigned int m_expectedChunkCount;
		std::map<unsigned int, std::string> m_dataChunks;
	};

	class PDUAggregator
	{
	public:
		PDUAggregator();

		void onPDUResponse(PDUDataChunk &&chunk);
		bool isCompleted(unsigned int transactionID);
		std::string popCompletedPDU(unsigned int transactionID);
		void onPDUTransactionExpired(unsigned int transactionID);
		unsigned int getResponseCount();

	private:
		std::map<unsigned int, PDUResponse> m_responses;
	};

}