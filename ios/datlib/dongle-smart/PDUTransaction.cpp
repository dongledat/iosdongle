#include "PDUTransaction.h"
#include "../crypto/dat_base64.h"
#include "NanopbRAIIUtilities.h"

using namespace dat;

PDUTransaction::PDUTransaction(DongleSocket & socket, IDataListener & dataListener) :
	m_dataListener(dataListener),
	ITransaction(socket, TransactionType_PDU)
{
}

int PDUTransaction::sendPDU(int canID, int canSpeed, int canSP, int canSJW, int canResponseTimeout, int tp20ChannelAddress, IConnection::PDUProtocol protocol, const std::string & reqPDU)
{
	TDongleReq req = TDongleReq_init_zero;
#if 0
	// Calculate de-base64'd size
	unsigned int buffer_size = 0;
	if (0 != dat_base64_decode(nullptr, 0, &buffer_size, (const unsigned char*)(reqPDU.c_str()), reqPDU.size())) {
		return -1; //todo
	}

	// Build nanopb pb_bytes_array, with De-base64'd size and actual raw (de-base64'd) data
	NanopbBytesRAII bytes(buffer_size);
	if (0 != dat_base64_decode((unsigned char*)(bytes.getDataPointer()), buffer_size, nullptr, (const unsigned char*)(reqPDU.c_str()), reqPDU.size())) {
		return -1; //todo
	}
	// Build request
	req.which_request = TDongleReq_PDUReq_tag;
	req.request.PDUReq.CanID = canID;
	req.request.PDUReq.CanSpeed = canSpeed;
	req.request.PDUReq.CanSP = canSP;
	req.request.PDUReq.CanSJW = canSJW;
	req.request.PDUReq.CanResponseTimeout = canResponseTimeout;
	req.request.PDUReq.TP20ChannelAddress = tp20ChannelAddress;
	req.request.PDUReq.PDUProtocol = protocol;
	req.request.PDUReq.Data = bytes.get();
	
	sendMessage(req);
	m_requestSent = true;
#endif
	return 0;
}

int PDUTransaction::onMessage(TDongleRsp & rsp)
{
	if (!isMessageTypeExpected(rsp.which_response)) {
		return -1;
	}

	auto& pdu = rsp.response.PDURsp;

	m_pduAggregator.onDataFragment(DataChunk(pdu.TransactionID,
		pdu.TotalChunkCount,
		pdu.ChunkIndex,
		std::string(reinterpret_cast<char*>(&(pdu.Data->bytes)),
					reinterpret_cast<char*>(&(pdu.Data->bytes) + pdu.Data->size))));

	if (!m_pduAggregator.isCompleted(pdu.TransactionID)) {
		// PDU is not fully aggregated yet
		return 0;
	}

	// Completed PDU
	m_dataListener.onPDUResponse(rsp.id, m_pduAggregator.popAggregatedData(pdu.TransactionID));
	m_isCompleted = true;
	return 0;
}

bool PDUTransaction::isMessageTypeExpected(unsigned int responseType) const
{
	if (!m_requestSent) {
		return false;
	}

	return responseType == TDongleRsp_PDURsp_tag;
}
