#include "PDUAggregator.h"

using namespace dat;

PDUDataChunk::PDUDataChunk(unsigned int transactionID, unsigned int chunkIndex, unsigned int totalCount, std::string && data) :
	m_transactionID(transactionID),
	m_chunkIndex(chunkIndex),
	m_totalCount(totalCount),
	m_data(data)
{
}

PDUDataChunk::PDUDataChunk(PDUDataChunk && other) :
	m_transactionID(other.m_transactionID),
	m_chunkIndex(other.m_chunkIndex),
	m_totalCount(other.m_totalCount),
	m_data(std::move(other.m_data))
{
}

PDUResponse::PDUResponse(PDUDataChunk && firstChunk)
{
	m_expectedChunkCount = firstChunk.getTotalCount();
	m_transactionID = firstChunk.getTransactionID();
	m_dataChunks.emplace(firstChunk.getChunkIndex(), firstChunk.popData());
}

void PDUResponse::addChunk(PDUDataChunk && chunk)
{
	if (this->isComplete()) {
		//todo: exception
		return;
	}

	// Verify the chunk's metadata matches ours
	if ((m_expectedChunkCount != chunk.getTotalCount()) ||
		(m_transactionID != chunk.getTransactionID())) {
		//todo: exception
		return;
	}

	// Verify the chunk's index is inside the range of indexes expected
	if (m_expectedChunkCount <= chunk.getChunkIndex()) {
		//todo: exception
		return;
	}


	// Verify we don't have this chunk's index already
	if (m_dataChunks.end() != m_dataChunks.find(chunk.getChunkIndex())) {
		//todo: exception
		return;
	}

	// Map guaranetees order
	m_dataChunks.emplace(chunk.getChunkIndex(), chunk.popData());
}

bool PDUResponse::isComplete()
{
	return m_dataChunks.size() == m_expectedChunkCount;
}

unsigned int PDUResponse::getTransactionID()
{
	return m_transactionID;
}

std::string PDUResponse::popCompletedData()
{
	if (!isComplete()) {
		return "";
	}

	std::string output;

	for (auto chunk = m_dataChunks.begin(); chunk != m_dataChunks.end(); )
	{
		output += std::move(chunk->second);
		m_dataChunks.erase(chunk++);
	}

	return output;
}

PDUAggregator::PDUAggregator()
{
}

void PDUAggregator::onPDUResponse(PDUDataChunk && chunk)
{
	auto i = m_responses.find(chunk.getTransactionID());
	if (m_responses.end() == i) {
		m_responses.emplace(chunk.getTransactionID(), PDUResponse(std::forward<PDUDataChunk>(chunk)));
	}
	else {
		i->second.addChunk(std::forward<PDUDataChunk>(chunk));
	}
}

bool PDUAggregator::isCompleted(unsigned int transactionID)
{
	auto i = m_responses.find(transactionID);

	if (m_responses.end() == i) {
		return false;
	}

	return i->second.isComplete();
}

std::string PDUAggregator::popCompletedPDU(unsigned int transactionID)
{
	if (!isCompleted(transactionID)) {
		return "";
	}

	auto i = m_responses.find(transactionID);
	auto pduData = i->second.popCompletedData();

	m_responses.erase(i->first);
	return pduData;
}

void PDUAggregator::onPDUTransactionExpired(unsigned int transactionID)
{
	auto transaction = m_responses.find(transactionID);
	if (m_responses.end() != transaction) {
		m_responses.erase(transaction);
	}
}

unsigned int dat::PDUAggregator::getResponseCount()
{
	return m_responses.size();
}
