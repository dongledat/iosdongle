#include "DataListener.h"

using namespace dat;

void DataListener::onData(int respID, TDataObject & dataObject, std::string & rspAsString)
{
	m_isCalled_onData = true;
}

void DataListener::onAvailableDiagParameters(int respID, const std::list<TDataCatalogEntry>& l, std::string & rspAsString)
{
	m_isCalled_onAvailableDiagParameters = true;
}

void DataListener::onDongleHWRevision(int respID, const std::string &)
{
	m_isCalled_onDongleHWRevision = true;
}

void DataListener::onDongleSerialNumber(int respID, const std::string &)
{
	m_isCalled_onDongleSerialNumber = true;
}

void DataListener::onFirmwareStatus(int respID, const FWStatus & fwStatus)
{
	m_isCalled_onFirmwareStatus = true;
}

void DataListener::onPDUResponse(int respID, const std::string & pduRawDataResponse)
{
	m_isCalled_onPDUResponse = true;
	printf("\nonPDUResponse. Length: %d\n", pduRawDataResponse.size());
}
