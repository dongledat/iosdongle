LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

DONGLE_S := $(LOCAL_PATH)/../dongle-smart
DONGLE := $(LOCAL_PATH)/../dongle
MBEDTLS := $(LOCAL_PATH)/../mbedtls/include
CRYPTO := $(LOCAL_PATH)/../crypto
LOCAL_C_INCLUDES := $(DONGLE_S) $(DONGLE) $(MBEDTLS) $(CRYPTO)

LOCAL_MODULE    := dongles
#LOCAL_C_INCLUDES := $(LOCAL_PATH)/../dongle

#LOCAL_SRC_FILES := ConfigurationUpdateTransaction.cpp   Connection.cpp                       DataAggregator.cpp \
#                   DataListener.cpp                     DataSubscriptionTransaction.cpp      DongleSocket.cpp \
#                   DongleStatusListener.cpp             ITransaction.cpp                     NanopbRAIIUtilities.cpp \
#                   PDUAggregator.cpp                    PDUTransaction.cpp                   PinAuthenticationTransaction.cpp \
#                   SimpleTransaction.cpp                TokenAuthenticationTransaction.cpp   Transaction.cpp \
#                   TransactionManager.cpp               UpdateProgressListener.cpp

LOCAL_SRC_FILES := ConfigurationUpdateTransaction.cpp   Connection.cpp                       DataAggregator.cpp	\
	DataListener.cpp                     DataSubscriptionTransaction.cpp      DongleSocket.cpp	\
	DongleStatusListener.cpp             ITransaction.cpp                     NanopbRAIIUtilities.cpp	\
	PDUTransaction.cpp                   PinAuthenticationTransaction.cpp     SimpleTransaction.cpp	\
	TokenAuthenticationTransaction.cpp   TransactionManager.cpp               UpdateProgressListener.cpp



LOCAL_CFLAGS += -DPB_ENABLE_MALLOC
LOCAL_SHARED_LIBRARIES += dongle

#include $(BUILD_STATIC_LIBRARY)
include $(BUILD_SHARED_LIBRARY)