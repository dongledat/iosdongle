#include "Connection.h"
#include <functional>
#include <memory>
#include "../crypto/dat_base64.h"
#include "NanopbRAIIUtilities.h"

using namespace dat;

Connection::Connection(IOutputStream& outputStream, IDongleStatusListener& dongleStatusListener, IDataListener& dataListener, IUpdateProgressListener& updateProgressListener) :
	IConnection(),
	m_outputStream(outputStream),
	m_dongleStatusListener(dongleStatusListener),
	m_dataListener(dataListener),
	m_updateProgressListener(updateProgressListener),
	m_isAuthenticatedByPIN(false),
	m_isAuthenticatedByToken(false),
	m_dongleSocket(outputStream),
	m_transactionManager(m_dataListener, m_updateProgressListener, m_dongleStatusListener, m_dongleSocket)
{
}

/****************************************************************/
/************************ TX ************************************/
/****************************************************************/

void Connection::authenticateByPIN(const std::string& user, const std::string& pin)
{
	m_transactionManager.cleanupTransactions();
	if (m_isAuthenticatedByPIN) {
		// Already authenticated
		return;
	}

	PinAuthenticationTransaction* transaction = m_transactionManager.newPinAuthenticationTransaction();
	if (nullptr == transaction) {
		m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_FAIL);
		return;
	}

	transaction->beginAuthentication();
    m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_IN_PROGRESS);
    // Should be:
	// return;
	// And the rest would be handled by the transaction

	// TODO: temporary
    m_isAuthenticatedByPIN = true;
    m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_PASS);

}

void Connection::authenticateByToken(const std::string& token)
{
	m_transactionManager.cleanupTransactions();
	if (m_isAuthenticatedByToken) {
		// Already authenticated
		return;
	}

	TokenAuthenticationTransaction* transaction = m_transactionManager.newTokenAuthenticationTransaction();
	if (nullptr == transaction) {
		m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_TOKEN, IDongleStatusListener::AUTH_RESULT_FAIL);
		return;
	}

	transaction->beginAuthentication();
	m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_TOKEN, IDongleStatusListener::AUTH_RESULT_IN_PROGRESS);
	// Should be:
	// return;
	// And the rest would be handled by the transaction

	// TODO: temporary
	m_isAuthenticatedByToken = true;
	m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_TOKEN, IDongleStatusListener::AUTH_RESULT_PASS);
}

int Connection::subscribeObject(int url, int val)
{
	m_transactionManager.cleanupTransactions();
	DataSubscriptionTransaction* transaction = m_transactionManager.newDataSubscriptionTransaction();
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->subscribe(url, val);
	return transaction->getTransactionID();
}

int Connection::unsubscribeObject(int transactionID)
{
	m_transactionManager.cleanupTransactions();
	DataSubscriptionTransaction* transaction = reinterpret_cast<DataSubscriptionTransaction*>(m_transactionManager.findTransaction(transactionID, TransactionType_DataSubscription));
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->unsubscribe();
	return transactionID;
}

int Connection::getAvailableDiagParametersAsync()
{
	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetAvailableDiagnosticParameter);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getAvailableDiagParameters();
	return transaction->getTransactionID();
}

int Connection::getDataObjectAsync(int url)
{
	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetDataObject);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getDataObject(url);
	return transaction->getTransactionID();
}

int Connection::getDongleHWRevisionAsync()
{
	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetHwVersion);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getHWVersion();
	return transaction->getTransactionID();
}

int Connection::getDongleSerialNumberAsync()
{
	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetSerialNumberDevice);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getSerialNumber();
	return transaction->getTransactionID();
}

int Connection::getFirmwareStatusAsync()
{
	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetFwStatus);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getFirmwareStatus();
	return transaction->getTransactionID();
}

int Connection::startUpdate(const std::string& signature)
{
	m_transactionManager.cleanupTransactions();
	ConfigurationUpdateTransaction* transaction = m_transactionManager.newConfigurationUpdateTransaction();
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->setSignature(signature);
	return transaction->getTransactionID();
}

int Connection::sendUpdateChunk(int transactionID, const unsigned char* chunk, size_t size)
{
	m_transactionManager.cleanupTransactions();
	ConfigurationUpdateTransaction* transaction = reinterpret_cast<ConfigurationUpdateTransaction*>(
		m_transactionManager.findTransaction(transactionID, TransactionType_ConfigurationUpdate));

	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->setDataChunk(chunk, size);
	return transaction->getTransactionID();
}

int Connection::completeUpdate(int transactionID)
{
	m_transactionManager.cleanupTransactions();
	ConfigurationUpdateTransaction* transaction = reinterpret_cast<ConfigurationUpdateTransaction*>(
		m_transactionManager.findTransaction(transactionID, TransactionType_ConfigurationUpdate));

	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->complete();
	return transaction->getTransactionID();
}

int Connection::cancelUpdate(int transactionID)
{
	m_transactionManager.cleanupTransactions();
	ConfigurationUpdateTransaction* transaction = reinterpret_cast<ConfigurationUpdateTransaction*>(
		m_transactionManager.findTransaction(transactionID, TransactionType_ConfigurationUpdate));

	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->cancel();
	return transaction->getTransactionID();
}

int Connection::sendPDU(int canID, int canSpeed, int canSP, int canSJW, int canResponseTimeout, int tp20ChannelAddress, PDUProtocol protocol, const std::string& reqPDU)
{
	m_transactionManager.cleanupTransactions();
	PDUTransaction* transaction = m_transactionManager.newPDUTransaction();
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->sendPDU(canID, canSpeed, canSP, canSJW, canResponseTimeout, tp20ChannelAddress, protocol, reqPDU);
	return transaction->getTransactionID();
}

/****************************************************************/
/************************ RX ************************************/
/****************************************************************/

void Connection::fromNet(const unsigned char* data, size_t length)
{
	m_dongleSocket.fromNet(data, length);
	onNetUpdate();
}

void Connection::onNetUpdate()
{
	while (m_dongleSocket.getMessageCount() > 0) {
		// Get one received message
		TDongleRsp* rsp = m_dongleSocket.getMessage();
		if (nullptr == rsp) {
			return;
		}

		// Will clear out the message when this function ends
		NanopbDynamicMessageGuard<TDongleRsp> g(TDongleRsp_fields, rsp);

		// Find the relevant ongoing transaction
		ITransaction* transaction = m_transactionManager.findTransaction(rsp->id);
		if (nullptr == transaction) {
			return;
		}

		// Let transaction handle this message
		transaction->onMessage(*rsp);

		// Extend expiration time
		transaction->extendExpiryTime();

		// Completed / expired transactions might change the state of our connection
		monitorTransactionsState();

		// Cleans expired or completed transactions
		m_transactionManager.cleanupTransactions();
	}

}

/*******************************************************************/
/************************ State ************************************/
/*******************************************************************/

bool Connection::isAuthenticatedByPIN() const
{
	return m_isAuthenticatedByPIN;
}

bool Connection::isAuthenticatedByToken() const
{
	return m_isAuthenticatedByToken;
}

void Connection::monitorTransactionsState()
{
	auto& transactions = m_transactionManager.getTransactions();

	for (auto i = transactions.begin(); i != transactions.end(); ++i) {
		switch (i->second->getTransactionType())
		{
		case TransactionType_PinAuth:
		{
			PinAuthenticationTransaction* t = reinterpret_cast<PinAuthenticationTransaction*>(i->second);

			if (t->isCompleted()) {
				// Authentication transaction complete; Check result
				if (IDongleStatusListener::AUTH_RESULT_PASS == t->getResult()) {
					m_isAuthenticatedByPIN = true;
				}

				// We got the result, transaction manager can clean it from now on
				t->setReaped();
			}
			else if (t->isExpired()) {
				// Failed to authenticate before expiration
				m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_TIMEOUT);
				
				// We got the result, transaction manager can clean it from now on
				t->setReaped();
			}
			else {
				// Not completed, not expired. Game on
			}

			break;
		}
		case TransactionType_TokenAuth:
		{
			TokenAuthenticationTransaction* t = reinterpret_cast<TokenAuthenticationTransaction*>(i->second);

			if (t->isCompleted()) {
				// Authentication transaction complete; Check result
				if (IDongleStatusListener::AUTH_RESULT_PASS == t->getResult()) {
					m_isAuthenticatedByToken = true;
				}

				// We got the result, transaction manager can clean it from now on
				t->setReaped();
			}
			else if (t->isExpired()) {
				// Failed to authenticate before expiration
				m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_TOKEN, IDongleStatusListener::AUTH_RESULT_TIMEOUT);

				// We got the result, transaction manager can clean it from now on
				t->setReaped();
			}
			else {
				// Not completed, not expired. Game on
			}

			break;
		}

		default:
			break;
		}
	}
}

/**
* @brief Start Backend certificate update.
* @note New method.
*/
//void updateCertificate(Certificate certificate, IUpdateCertificateListener listener);
