#ifndef _ITRANSACTION_H_
#define _ITRANSACTION_H_

#include "dat.h"
#include "IConnection.h"
#include "DongleSocket.h"

#include <functional>
#include <list>
#include <set>
#include <map>
#include <atomic>
#include <ctime>

namespace dat {

	typedef unsigned int TransactionID_t;

	typedef enum {

		TransactionType_GetSerialNumberDevice = 0,
		TransactionType_GetBtMacAddress,
		TransactionType_GetHwVersion,
		TransactionType_GetFwStatus,
		TransactionType_GetDevStatus,
		TransactionType_GetActivationStatus,
		TransactionType_GetSetProtocolMode,
		TransactionType_GetAvailableDiagnosticParameter,
		//TransactionType_AvailableDiagParametersAreChanged, //todo
		//TransactionType_GetDir, //todo
		TransactionType_GetProtocol,
		TransactionType_GetDataObject,
		TransactionType_DataSubscription,
		TransactionType_ConfigurationUpdate,
		TransactionType_PDU,
		TransactionType_PinAuth,
		TransactionType_TokenAuth

	} TransactionType;

	class ITransaction {
	public:

		// Init start & expiry times,
		ITransaction(DongleSocket& socket, TransactionType transactionType, bool expirable = true, bool waitForReap = false);
		void sendMessage(TDongleReq& msg);
		
		// Getters
		bool isCompleted() const;
		bool isExpired() const;
		bool isReaped() const;
		TransactionID_t getTransactionID() const { return m_transactionID; };
		time_t getStartTime() const { return m_startTime; };
		time_t getExpiryTime() const { return m_expiryTime; };
		TransactionType getTransactionType() const { return m_transactionType; };

		// Setters
		void setReaped() { m_isReaped = true; };
		void setCompleted() { m_isCompleted = true; };
		void extendExpiryTime() { m_expiryTime = getCurrentTime() + DEFAULT_EXPIRY_INTERVAL_IN_SECONDS; };

		// Virtual methods
		virtual int onMessage(TDongleRsp & rsp) = 0;

		// Static methods
		time_t getCurrentTime() const { return time(0); };

		// Constants
		static const unsigned int INITIAL_TRANSACTION_ID;
		static const unsigned int INVALID_TRANSACTION_ID;
		static const unsigned int DEFAULT_EXPIRY_INTERVAL_IN_SECONDS;
		static const unsigned int TRANSACTION_EXPIRE_TIME_DONT_EXPIRE;

	protected:
		bool m_isCompleted;
		bool m_isReaped;
		bool m_waitForReap;
		bool m_expirable;
		DongleSocket& m_dongleSocket;

	private:
		TransactionID_t m_transactionID;
		TransactionType m_transactionType;
		time_t m_startTime;
		time_t m_expiryTime;
		static std::atomic<TransactionID_t> s_transactionId;
	};


}
#endif /* _ITRANSACTION_H_ */