#ifndef CONNECTION_H
#define CONNECTION_H

#include "IConnection.h"
#include "ITransaction.h"
#include "DongleSocket.h"
#include "TransactionManager.h"

#include <atomic>
#include <map>

#include "pb_encode.h"
#include "pb_decode.h"
#include "dongle.pb.h"


namespace dat {

    /**
     * @class Connection
     */
    class Connection : public IConnection
    {

    public:

		Connection(IOutputStream& outputStream, IDongleStatusListener& dongleStatusListener, IDataListener& dataListener, IUpdateProgressListener& updateProgressListener);

        virtual void fromNet(const unsigned char* data, size_t length);

        /**
        * @brief First time Smartphone authentication by user and PIN.
        * @param user user name.
        * @param pin Hexadecimal 8-digit number appearing on Donlge's sticker.
        * @note There is no need to specify user as in Dongle 1.0.
        *
        * This API is kept for possible future extensions. In current implementation @b user parameter is always equal to @a 'default'.
        */
        virtual void authenticateByPIN(const std::string& user, const std::string& pin);

        /**
        * @brief Authenticate Dongle by token.
        *
        * The result of token authentication is returned in @ref IConnectionListener::onAuthenticateByToken() callback funtion.
        */
        virtual void authenticateByToken(const std::string& token);

        /**
        * @brief Check is Smartphone is authenticated with Dongle by PIN.
        * @return True if Smartphone is already authenticated with Dongle by PIN.
        * @see authenticateByPIN(String pin).
        */
        virtual bool isAuthenticatedByPIN() const;

        /**
        * @brief Check is Smartphone is authenticated with Dongle by token.
        * @return True if Smartphone is already authenticated with Dongle by token.
        * @see authenticateByToken().
        */
        virtual bool isAuthenticatedByToken() const;

        /**
        * @brief Subscribe a group of diagnostics parameters.
        * @param url Represents a group of diagnostics parameters.
        * @param val Refresh timeout.
        * @note The URL type is changed from String to int.
        */
        virtual int subscribeObject(int url, int val);

        /**
        * @brief Unsubscribe a group of diagnostics parameters.
        * @param url Represents a group of diagnostics parameters.
        * @note The URL type is changed from String to int.
        */
        virtual int unsubscribeObject(int transactionID);

        /**
        * @brief Retreive a list of available groups of diagnostics parameters.
        */
        virtual int getAvailableDiagParametersAsync();

        /**
        * @brief Retreive a diagnostics parameter group by URL.
        * @note The URL type is changed from String to int.
        */
        virtual int getDataObjectAsync(int url);

        /**
        * @brief Retreive Dongle H/W revision.
        */
        virtual int getDongleHWRevisionAsync();

        /**
        * @brief Retreive Dongle S/N.
        */
        virtual int getDongleSerialNumberAsync();

        /**
        * @brief Retreive Dongle firmware status.
        * @see IFWStatus.
        */
        virtual int getFirmwareStatusAsync();

        /**
        * @brief Start asynchronous configuration update.
        */
        virtual int startUpdate(const std::string& signature);

        /**
        * @brief Send next update chunk
        */
        virtual int sendUpdateChunk(int transactionID, const unsigned char* chunk, size_t size);

		/**
		* @brief Complete asynchronous configuration update.
		*/
		virtual int completeUpdate(int transactionID);

        /**
        * @brief Cancel asynchronous configuration update.
        */
        virtual int cancelUpdate(int transactionID);

        /**
        * @brief Send up to 5 PDUs
        * @param[in] canID min=0x200, max=0x17FC00C7
        * @param[in] canSpeed Kbit/s min=125, max=500
        * @param[in] canSP in % min=50, max=90
        * @param[in] canSJW min=1, max=4
        * @param[in] canResponseTimeout Timeout for the ECU response
        * @param[in] tp20ChannelAddress ECU address for the communication with TP20
        * @param[in] protocol (see @ref PDUProtocol)
        * @param[in] reqPDU array of bytes represented by a base 64 encoding corresponding to the payload sent directly the ECU
        */
        virtual int sendPDU(int canID, int canSpeed, int canSP, int canSJW, int canResponseTimeout, int tp20ChannelAddress, PDUProtocol protocol, const std::string& reqPDU);

        /**
        * @brief Start Backend certificate update.
        * @note New method.
        */
        //void updateCertificate(Certificate certificate, IUpdateCertificateListener listener);

		//TODO: remove this
		std::map<TransactionID_t, ITransaction*> & getTransactions() {
			return m_transactionManager.getTransactions();
		}

    private:
		/**
		* @brief Assing the request a transaction ID, send the request and return the transaction ID.
		*/
		void onNetUpdate();
		void monitorTransactionsState();

        IOutputStream&              m_outputStream;
        IDongleStatusListener&      m_dongleStatusListener;
        IDataListener&              m_dataListener;
        IUpdateProgressListener&    m_updateProgressListener;
        bool                        m_isAuthenticatedByPIN;
        bool                        m_isAuthenticatedByToken;

		DongleSocket m_dongleSocket;
		TransactionManager m_transactionManager;
    };
}

#endif // !CONNECTION_H

