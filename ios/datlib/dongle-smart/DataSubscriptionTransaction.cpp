#include "DataSubscriptionTransaction.h"

using namespace dat;

DataSubscriptionTransaction::DataSubscriptionTransaction(DongleSocket & dongleSocket, IDataListener & dataListener) :
	m_dataListener(dataListener),
	ITransaction(dongleSocket, TransactionType_DataSubscription, false) // No expiry
{
}

int DataSubscriptionTransaction::subscribe(int url, int timeout)
{
	TDongleReq req = TDongleReq_init_zero;
	req.which_request = TDongleReq_SubscribeObjectReq_tag;
	req.request.SubscribeObjectReq.Timeout = timeout;
	req.request.SubscribeObjectReq.UrlCode = url;
	
	sendMessage(req);

	m_isSubscribed = true;
	m_url = url;
	m_timeout = timeout;
	return 0;
}

int DataSubscriptionTransaction::unsubscribe()
{
	TDongleReq req = TDongleReq_init_zero;
	req.which_request = TDongleReq_UnsubscribeObjectReq_tag;
	req.request.UnsubscribeObjectReq.ObjectId = m_url;

	sendMessage(req);

	m_isCompleted = true;
	m_isSubscribed = false;
	return 0;
}

int DataSubscriptionTransaction::onMessage(TDongleRsp & rsp)
{
	if (!isMessageTypeExpected(rsp.which_response)) {
		return -1;
	}

	switch (rsp.which_response)
	{
	case TDongleRsp_SubscribeObjectRsp_tag:
	{
		//todo: Error in subscribing - need to notify listeners,
		// m_someListener.onSubscribeResult(rsp.response.SubscribeObjectRsp.result);
		if (TSubscribeObjectRsp_EResult_ok != rsp.response.SubscribeObjectRsp.result) {
			// todo: if failed, and notified listener, this transaction is done
			//m_isCompleted = true;
		} 
		break;
	}
	case TDongleRsp_UnsubscribeObjectRsp_tag:
	{
		//todo: Error in unsubscribing - need to notify listeners,
		// m_someListener.onUnsubscribeResult(rsp.response.UnsubscribeObjectRsp.result);
		if (TUnsubscribeObjectRsp_EResult_ok != rsp.response.UnsubscribeObjectRsp.result) {
			// todo: if failed, and notified listener, this transaction is done
			//m_isCompleted = true;
		}
		break;
	}
	case TDongleRsp_DataObjectUpdateRsp_tag:
	{
		if (m_url != rsp.response.DataObjectUpdateRsp.DataObject.UrlCode) {
			//todo: unexpected condition
		}

		// Serialize the response
		unsigned char buffer[4096];
		pb_ostream_t ostream = pb_ostream_from_buffer(buffer, 4096);
		if (!pb_encode(&ostream, TDongleRsp_fields, &rsp)) {
			return -1;
		}
		std::string rspAsString(buffer, buffer + ostream.bytes_written);

		m_dataListener.onData(getTransactionID(), rsp.response.DataObjectUpdateRsp.DataObject, rspAsString);
	}
	}

	return 0;
}

bool DataSubscriptionTransaction::isMessageTypeExpected(unsigned int responseType) const
{
	if (!m_isSubscribed) {
		return TDongleRsp_UnsubscribeObjectRsp_tag == responseType;
	}

	return ((TDongleRsp_DataObjectUpdateRsp_tag == responseType) ||
			(TDongleRsp_SubscribeObjectRsp_tag == responseType));
}
