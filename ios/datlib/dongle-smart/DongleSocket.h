#ifndef _DONGLE_SOCKET_H_
#define _DONGLE_SOCKET_H_

#include "IConnection.h"
#include <queue>
#include <memory>

namespace dat {

	class DongleSocket {
	public:

		DongleSocket(IOutputStream& outputStream);
		
		void fromNet(const unsigned char* data, size_t length);
		
		TDongleRsp* getMessage();
		void sendMessage(TDongleReq* msg);
		unsigned int getMessageCount();

	private:
		IOutputStream & m_outputStream;
		std::queue<TDongleRsp*> m_messages;
	};

}

#endif /* _DONGLE_SOCKET_H_ */