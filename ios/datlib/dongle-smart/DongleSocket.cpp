#include "DongleSocket.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "pb_common.h"

const unsigned int REQUEST_MAX_BUFFER_SIZE = 4096;

using namespace dat;

DongleSocket::DongleSocket(IOutputStream & outputStream) :
	m_outputStream(outputStream)
{
}

void DongleSocket::fromNet(const unsigned char * data, size_t length)
{
	//Future: this is the area that can be edited to allow for smarter manipulation
	//		  of incoming buffer, such as size of messages etc.

	// Build the msg struct
	TDongleRsp* rsp = new TDongleRsp;
	pb_istream_t stream = pb_istream_from_buffer(data, length);
	if (!pb_decode(&stream, TDongleRsp_fields, rsp)) {
		// Error in decoding
		return;
	}

	m_messages.emplace(rsp);
}

TDongleRsp* DongleSocket::getMessage()
{
	if (m_messages.size() == 0) {
		return nullptr;
	}

	auto rsp = m_messages.front();
	m_messages.pop();
	return rsp;
}

//todo: bool return value
void DongleSocket::sendMessage(TDongleReq * msg)
{
	unsigned char buffer[REQUEST_MAX_BUFFER_SIZE];

	// Serialize the request
	pb_ostream_t ostream = pb_ostream_from_buffer(buffer, REQUEST_MAX_BUFFER_SIZE);
	if (!pb_encode(&ostream, TDongleReq_fields, msg)) {
		return;
	}

	m_outputStream.toNet(buffer, ostream.bytes_written);
}

unsigned int DongleSocket::getMessageCount()
{
	return m_messages.size();
}
