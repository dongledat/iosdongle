#include "IConnection.h"

namespace dat {

	class DongleStatusListener : public IDongleStatusListener {
	public:
		virtual void onDongleStatus(DongleStatus dongleStatus) override;
		virtual void onAuthenticate(AuthenticationMethod authenticationMethod, AuthenticationResult authenticationResult) override;

		bool m_isCalled_onDongleStatus;
		bool m_isCalled_onAuthenticate;

		void reset() {
			m_isCalled_onDongleStatus = false;
			m_isCalled_onAuthenticate = false;
		}

	private:
	};

}