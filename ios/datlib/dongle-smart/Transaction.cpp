#include "Transaction.h"
#include <ctime>

using namespace dat;

#define MILISECONDS_PER_TIME_T (1000)

Transaction::Transaction(	unsigned int transactionID,
							time_t expiryTime,
							unsigned int requestType,
							std::function<void(unsigned int)> expiryCallback) :
	m_transactionID(transactionID),
	m_startTime(time(0)),
	m_expiryTime(expiryTime),
	m_requestType(requestType),
	m_expiryCallback(expiryCallback) {

}

Transaction::Transaction(unsigned int transactionID, unsigned int requestType, std::function<void(unsigned int)> expiryCallback) :
	m_transactionID(transactionID),
	m_startTime(time(0)),
	m_expiryTime(m_startTime + Transaction::miliseconds_to_time_t(DEFAULT_EXPIRY_TIME)),
	m_requestType(requestType),
	m_expiryCallback(expiryCallback)
{
}

bool Transaction::isExpired()
{
	if (TRANSACTION_EXPIRE_TIME_DONT_EXPIRE == m_expiryTime) {
		return false;
	}

	return current_time() >= m_expiryTime;
}

void Transaction::callExpiryCallback()
{
	if (m_expiryCallback) {
		m_expiryCallback(m_transactionID);
	}
}

time_t Transaction::current_time()
{
	return time(0);
}

time_t Transaction::miliseconds_to_time_t(unsigned int miliseconds)
{
	return (int)((float)miliseconds / MILISECONDS_PER_TIME_T);
}
