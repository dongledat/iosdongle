#include "ITransaction.h"

using namespace dat;

const unsigned int ITransaction::INITIAL_TRANSACTION_ID = 1;
const unsigned int ITransaction::INVALID_TRANSACTION_ID = 0;

std::atomic<TransactionID_t> ITransaction::s_transactionId(INITIAL_TRANSACTION_ID);

//todo: Change this
const unsigned int ITransaction::DEFAULT_EXPIRY_INTERVAL_IN_SECONDS = 2; // 2 seconds

ITransaction::ITransaction(DongleSocket& socket, TransactionType transactionType, bool expirable, bool waitForReap) :
	m_transactionID(s_transactionId++),
	m_startTime(ITransaction::getCurrentTime()),
	m_expiryTime(m_startTime + DEFAULT_EXPIRY_INTERVAL_IN_SECONDS),
	m_transactionType(transactionType),
	m_isCompleted(false),
	m_dongleSocket(socket),
	m_isReaped(false),
	m_waitForReap(waitForReap),
	m_expirable(expirable)
{
}

bool ITransaction::isCompleted() const
{
	return m_isCompleted;
}

bool ITransaction::isExpired() const
{
	if (!m_expirable) {
		return false;
	}

	if (ITransaction::getCurrentTime() >= m_expiryTime) {
		return true;
	}

	return false;
}

bool ITransaction::isReaped() const
{
	if (!m_isCompleted) {
		return false; // Can't be done before completing
	}

	if (!m_waitForReap) {
		return true; // No need to wait for reaping, so treat it as reaped
	}

	return m_isReaped;
}

void ITransaction::sendMessage(TDongleReq& msg)
{
	msg.id = m_transactionID;
	msg.has_id = true;

	m_dongleSocket.sendMessage(&msg);
}