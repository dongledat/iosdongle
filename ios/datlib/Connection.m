#import "Connection.h"

/*
DATPDURequestResult PDURequestResultValueOf(NSString *text) {
  if (text) {
    if ([text isEqualToString:@"OK"])
      return OK;
    else if ([text isEqualToString:@"CanIdError"])
      return CanIdError;
    else if ([text isEqualToString:@"CanSpeedError"])
      return CanSpeedError;
    else if ([text isEqualToString:@"CanProtocolError"])
      return CanProtocolError;
    else if ([text isEqualToString:@"VehicleSpeedInvalid"])
      return VehicleSpeedInvalid;
  }
  return -1;
}

PDURequestResult PDURequestResultDescription(PDURequestResult value) {
  switch (value) {
    case OK:
      return @"OK";
    else case CanIdError:
      return @"CanIdError";
    else case CanSpeedError:
      return @"CanSpeedError";
    else case CanProtocolError:
      return @"CanProtocolError";
    else case VehicleSpeedInvalid:
      return @"VehicleSpeedInvalid";
  }
  return nil;
}

PDUResponseStatus PDUResponseStatusValueOf(NSString *text) {
  if (text) {
    if ([text isEqualToString:@"OK"])
      return OK;
    else if ([text isEqualToString:@"ServiceNotAllowed"])
      return ServiceNotAllowed;
    else if ([text isEqualToString:@"ResponseSizeExceedsMaximumBufferSize"])
      return ResponseSizeExceedsMaximumBufferSize;
    else if ([text isEqualToString:@"MemoryError"])
      return MemoryError;
    else if ([text isEqualToString:@"CanCommunicationError"])
      return CanCommunicationError;
  }
  return -1;
}
 */

/*
PDUResponseStatus PDUResponseStatusDescription(PDUResponseStatus value) {
  switch (value) {
    case OK:
      return @"OK";
    else case ServiceNotAllowed:
      return @"ServiceNotAllowed";
    else case ResponseSizeExceedsMaximumBufferSize:
      return @"ResponseSizeExceedsMaximumBufferSize";
    else case MemoryError:
      return @"MemoryError";
    else case CanCommunicationError:
      return @"CanCommunicationError";
  }
  return nil;
}

PDUProtocol PDUProtocolValueOf(NSString *text) {
  if (text) {
    if ([text isEqualToString:@"TP20"])
      return TP20;
    else if ([text isEqualToString:@"ISO14229"])
      return ISO14229;
    else if ([text isEqualToString:@"ISO15765"])
      return ISO15765;
  }
  return -1;
}

PDUProtocol PDUProtocolDescription(PDUProtocol value) {
  switch (value) {
    case TP20:
      return @"TP20";
    else case ISO14229:
      return @"ISO14229";
    else case ISO15765:
      return @"ISO15765";
  }
  return nil;
}
*/


@implementation Connection : NSObject

/*
@synthesize connected;
@synthesize authenticatedByPIN;
@synthesize authenticatedByToken;
@synthesize availableDiagParameters;
@synthesize dongleHWRevision;
@synthesize dongleSerialNumber;
@synthesize firmwareStatus;
*/


/**
 * @brief Connect Smartphone to Dongle by Bluetooth.
 */
- (void) connect:(InputStream *)inputStream outputStream:(OutputStream *)outputStream {
}


/**
 * @brief Stop Smartphone Bluetooth connection to Dongle.
 */
- (void) disconnect {
}


/**
 * @brief First time Smartphone authentication by PIN.
 * @param pin Hexadecimal 8-digit number appearing on Donlge's sticker.
 * @note There is no need to specify user as in Dongle 1.0.
 * 
 * This function should be called instead of \a authenticate(String user, String password) in Dongle 1.0.
 * The result of PIN authentication is returned in \ref IConnectionListener::onAuthenticateByPIN() callback funtion.
 */
- (void) authenticateByPIN:(NSString *)pin {
  [self authenticateByPIN:@"default" pin:pin];
}


/**
 * @brief First time Smartphone authentication by user and PIN.
 * @param user user name.
 * @param pin Hexadecimal 8-digit number appearing on Donlge's sticker.
 * @note There is no need to specify user as in Dongle 1.0.
 * 
 * This API is kept for possible future extensions. In current implementation @b user parameter is always equal to @a 'default'.
 */
- (void) authenticateByPIN:(NSString *)user pin:(NSString *)pin {
}


/**
 * @brief Authenticate Dongle by token.
 * 
 * The result of token authentication is returned in @ref IConnectionListener::onAuthenticateByToken() callback funtion.
 */
- (void) authenticateByToken {
}


/**
 * @brief Check if Bluetooth connection is established.
 * @return True if Smartphone is connected to Dongle over Bluetooth.
 */
- (BOOL) connected {
    return true;
}


/**
 * @brief Check is Smartphone is authenticated with Dongle by PIN.
 * @return True if Smartphone is already authenticated with Dongle by PIN.
 * @see authenticateByPIN(String pin).
 */
- (BOOL) authenticatedByPIN {
    return true;
}


/**
 * @brief Check is Smartphone is authenticated with Dongle by token.
 * @return True if Smartphone is already authenticated with Dongle by token.
 * @see authenticateByToken().
 */
- (BOOL) authenticatedByToken {
    return true;
}


/**
 * @brief Subscribe a group of diagnostics parameters.
 * @param url Represents a group of diagnostics parameters.
 * @param val Refresh timeout.
 * @note The URL type is changed from String to int.
 */
- (void) subscribeObject:(int)url val:(int)val {
}


/**
 * @brief Unsubscribe a group of diagnostics parameters.
 * @param url Represents a group of diagnostics parameters.
 * @note The URL type is changed from String to int.
 */
- (void) unsubscribeObject:(int)url {
}


/**
 * @brief Retreive a list of available groups of diagnostics parameters.
 */
- (IntList *) availableDiagParameters {
    return 0;
}


/**
 * @brief Retreive a diagnostics parameter group by URL.
 * @note The URL type is changed from String to int.
 */
- (DataObject *) getDataObject:(int)url {
    return 0;
}


/**
 * @brief Add connection listener.
 * @see IConnectionListener.
 */
- (void) addConnectionListener:(IConnectionListener *)connectionListener {
}


/**
 * @brief Remove connection listener.
 * @see IConnectionListener.
 */
- (void) removeConnectionListener:(IConnectionListener *)connectionListener {
}


/**
 * @brief Remove all connection listeners.
 */
- (void) clearConnectionListener {
}


/**
 * @brief Add data listener.
 * @see IDataListener.
 */
- (void) addDataListener:(IDataListener *)dataListener {
}


/**
 * @brief Remove data listener.
 * @see IDataListener.
 */
- (void) removeDataListener:(IDataListener *)dataListener {
}


/**
 * @brief Remove all data listeners.
 */
- (void) clearAllDataListeners {
}


/**
 * @brief Retreive Dongle H/W revision.
 */
- (NSString *) dongleHWRevision {
    return 0;
}


/**
 * @brief Retreive Dongle S/N.
 */
- (NSString *) dongleSerialNumber {
    return 0;
}


/**
 * @brief Retreive Dongle firmware status.
 * @see IFWStatus.
 */
- (IFWStatus *) firmwareStatus {
    return 0;
}


/**
 * @brief Start asynchronous configuration update.
 */
- (void) performUpdateAsync:(FileInputStream *)updateFile signature:(FileInputStream *)signature listener:(IUpdateProgressListener *)listener {
}


/**
 * @brief Cancel asynchronous configuration update.
 */
- (void) cancelDongleUpdate {
}


/**
 * @brief Send up to 5 PDUs
 * @param[in] canID min=0x200, max=0x17FC00C7
 * @param[in] canSpeed Kbit/s min=125, max=500
 * @param[in] canSP in % min=50, max=90
 * @param[in] canSJW min=1, max=4
 * @param[in] canResponseTimeout Timeout for the ECU response
 * @param[in] tp20ChannelAddress ECU address for the communication with TP20
 * @param[in] protocol (see @ref PDUProtocol)
 * @param[in] reqPDU array of bytes represented by a base 64 encoding corresponding to the payload sent directly the ECU
 * @param[out] pduResponseStatus response status (see @ref ResponseStatus)
 * @param[out] rspPDU list of array of bytes represented by a base 64 encoding corresponding to the payload sent directly from the ECU
 * @return @ref PDURequestResult
 */
/*
- (id<PDURequestResult>) sendPDU:(int)canID canSpeed:(int)canSpeed canSP:(int)canSP canSJW:(int)canSJW canResponseTimeout:(int)canResponseTimeout tp20ChannelAddress:(int)tp20ChannelAddress protocol:(id<PDUProtocol>)protocol reqPDU:(NSString *)reqPDU pduResponseStatus:(id<PDUResponseStatus>)pduResponseStatus rspPDU:(NSMutableArray *)rspPDU {
}
 */


/**
 * @brief Start Backend certificate update.
 * @note New method.
 */
- (void) updateCertificate:(Certificate *)certificate listener:(IUpdateCertificateListener *)listener {
}

@end
