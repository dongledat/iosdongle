/**
 * @file    DataElement.h
 * @brief   DataElement class declaration
 */

extern int const DATSTATE_ERROR;
extern int const DATSTATE_NODATA;
extern int const DATSTATE_OK;

@interface DATDataElement : NSObject {
  int eType;
  BOOL isImmutable;
  NSString * msg;
  int url;
  int state;
  NSObject * value;
}

@property(nonatomic, readonly) BOOL immutable;
@property(nonatomic, readonly) int url;
@property(nonatomic, retain, readonly) NSString * msg;
@property(nonatomic, retain) NSObject * value;
@property(nonatomic, readonly) int state;
@property(nonatomic, retain, readonly) NSString * valueAsString;
@property(nonatomic, retain, readonly) NSNumber * valueAsDouble;
@property(nonatomic, retain, readonly) NSNumber * valueAsInteger;
@property(nonatomic, retain, readonly) NSNumber * valueAsLong;
@property(nonatomic, retain, readonly) Date * valueAsDate;
@property(nonatomic, retain, readonly) DateTZ * valueAsDateTZ;
@property(nonatomic, retain, readonly) NSNumber * valueAsBoolean;
@property(nonatomic, readonly) int type;
- (id) init:(int)elementUrl value:(NSObject *)value exlapType:(int)exlapType;
- (id) init:(int)elementUrl value:(NSObject *)value exlapType:(int)exlapType msg:(NSString *)msg;
- (id) init:(int)elementUrl value:(NSObject *)value exlapType:(int)exlapType state:(int)state msg:(NSString *)msg;
- (id) initWithElement:(DATDataElement *)element;
- (void) lock;
- (void) setValue:(NSObject *)newValue;
- (BOOL) hasValue;
- (void) setValueByString:(NSString *)newValue;
- (void) setValueByInteger:(NSNumber *)newValue;
- (void) setValueByBoolean:(NSNumber *)newValue;
- (void) setValueByLong:(NSNumber *)newValue;
- (void) setValueByDouble:(NSNumber *)newValue;
- (void) setValueByDate:(Date *)newValue;
- (void) setValueByDateTZ:(DateTZ *)newValue;
- (int) hash;
- (BOOL) isEqualTo:(NSObject *)obj;
- (NSString *) description;
@end
