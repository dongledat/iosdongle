/**
 * @file    IDongleStatus.h
 * @brief   IDongleStatus class declaration
 */

typedef enum {
  DATDiagnosticStatus_UNKOWN,
  DATDiagnosticStatus_DEVICEISBUSY,
  DATDiagnosticStatus_CONFIGURATIONVERIFYINPROGRESS,
  DATDiagnosticStatus_CONFIGUREDANDNOTCONNECTED,
  DATDiagnosticStatus_NOTCONFIGUREDANDEOBDCOMMUNICATION,
  DATDiagnosticStatus_CONFIGUREDANDCONNECTED,
  DATDiagnosticStatus_NOTCONFIGUREDANDNOTCONNECTED,
  DATDiagnosticStatus_ERRORCORRUPTEDDEVICE,
  DATDiagnosticStatus_ERRORMEMORYPROBLEM
} DATDiagnosticStatus;

typedef struct {
  NSString * DATDiagnosticStatus_Fields_stringValue;
} DATDiagnosticStatus_Fields;

static DATDiagnosticStatus_Fields DiagnosticStatus_Data[9] = {
  { @"Unknown"}
  , { @"DeviceIsBusy"}
  , { @"ConfigurationVerifyInProgress"}
  , { @"ConfiguredAndNotConnected"}
  , { @"NotConfiguredAndEOBDcommunication"}
  , { @"ConfiguredAndConnected"}
  , { @"NotConfiguredAndNotConnected"}
  , { @"ErrorCorruptedDevice"}
  , { @"ErrorMemoryProblem"}
};

DATDiagnosticStatus DiagnosticStatusValueOf(NSString *text);
DATDiagnosticStatus DiagnosticStatusDescription(DATDiagnosticStatus value);

id<DATDiagnosticStatus> DiagnosticStatus_parse(id<DATDiagnosticStatus> e, NSString * valueAsString);
NSString * DiagnosticStatus_stringValue(id<DATDiagnosticStatus> e);
NSString * DiagnosticStatus_description(id<DATDiagnosticStatus> e);

/**
 * @class IDongleStatus interface class
 */

@protocol DATIDongleStatus <NSObject>
- (id<DATDiagnosticStatus>) getDiagnosticStatus;
- (BOOL) isAuthenticatedByPIN;
- (BOOL) isAuthenticatedByToken;
@end
