
/**
 * @file    IDataListiner.h
 * @brief   IDataListiner class declaration
 */

* @class IDataListener interface class declaration
 */

@protocol DATIDataListener <NSObject>
- (void) onData:(DATDataObject *)dataObject;
- (void) onDeviceStatus:(DATIDongleStatus *)iDongleStatus;
@end
