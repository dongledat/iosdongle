/**
 * @file    DataObject.h
 * @brief   DataObject class declaration
 */

@interface DATDataObject : NSObject {
  NSMutableArray * dataElements;
  BOOL isImmutable;
  DateTZ * timeStamp;
  int url;
}

@property(nonatomic, readonly) BOOL immutable;
@property(nonatomic) int url;
@property(nonatomic, retain) Date * timeStamp;
@property(nonatomic, retain) DateTZ * timeStampTZ;
- (id) init;
- (id) initWithUrl:(int)url;
- (id) init:(int)url interfaces:(NSMutableDictionary *)interfaces;
/**
 * @note The URL type has been changed from to @a String to @a int
 */
- (id) init:(int)functionUrl interfaces:(NSMutableDictionary *)interfaces forInputArguments:(BOOL)forInputArguments;
- (id) initWithDataObject:(DATDataObject *)dataObject;
- (void) lock;
- (int) size;
- (NSEnumerator *) elements;
- (DataElement *) getElement:(int)index;
- (DataElement *) getElement:(int)elementUrl;
/**
 * Add data element to data object
 */
- (void) addElement:(DATDataElement *)dataElement;
/**
 * @note The URL type has been changed from to @a String to @a int
 */
- (void) setUrl:(int)url;
- (void) setTimeStamp;
- (void) setTimeStamp:(Date *)date;
- (void) setTimeStampTZ:(DateTZ *)dateTZ;
/**
 * @note The URL type has been changed from to @a String to @a int
 */
- (BOOL) containsElement:(int)elementUrl;
- (int) hash;
- (BOOL) isEqualTo:(NSObject *)obj;
- (NSString *) description;
@end
