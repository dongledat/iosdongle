
/**
 * @file IUpdateCertificateListiner.h
 * @class IUpdateCertificateListener interface class
 * 
 * In order to define application behavior of certificate update progress status,
 * the application class should be derived from this one.
 */

@protocol DATIUpdateCertificateListener <NSObject>
- (void) onUpdateFailed:(NSException *)exception;
- (void) onUpdateStarted;
- (void) onUpdateSuccess;
@end
