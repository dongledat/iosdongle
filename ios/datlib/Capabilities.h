/**
 * @file    Capabilities.h
 * @brief   Capabilities class declaration
 */

extern int const DATOPTION_AUTHENTICATE_CMD;
extern int const DATOPTION_DAT_TIMESTAMP;
extern int const DATOPTION_HEARTBEAT_CMD;
extern int const DATOPTION_INTERFACE_CMD;

@interface DATCapabilities : NSObject {
  NSString * description;
  NSString * remoteManagementAddress;
  int remoteManagementPort;
  NSMutableArray * serverCapabilityElements;
  int serviceMajorVersion;
  int serviceMinorVersion;
  NSString * serviceName;
}

@property(nonatomic, retain, readonly) NSString * description;
@property(nonatomic, readonly) int protocolMajorVersion;
@property(nonatomic, readonly) int protocolMinorVersion;
@property(nonatomic, readonly) int serviceMajorVersion;
@property(nonatomic, readonly) int serviceMinorVersion;
@property(nonatomic, retain) NSString * remoteManagementAddress;
@property(nonatomic) int remoteManagementPort;
@property(nonatomic, retain, readonly) NSString * serviceName;
- (id) init:(NSString *)description serverCapabilityElements:(NSMutableArray *)serverCapabilityElements;
- (id) init:(NSMutableArray *)serverCapabilityElements description:(NSString *)description serviceName:(NSString *)serviceName serviceVersionMajor:(int)serviceVersionMajor serviceVersionMinor:(int)serviceVersionMinor remoteManagementAddress:(NSString *)remoteManagementAddress;
- (DATCapabilitiesOfVersion *) getProtocolVersionCapabilities:(int)majorVersion;
- (void) setRemoteManagementAddress:(NSString *)newRemoteManagementAddress;
- (BOOL) isProtocolOptionSupported:(int)option;
- (void) setRemoteManagementPort:(int)remoteManagementPort;
- (NSString *) description;
@end
