#import "Capabilities.h"

int const OPTION_AUTHENTICATE_CMD = 0;
int const OPTION_DAT_TIMESTAMP = 4;
int const OPTION_HEARTBEAT_CMD = 2;
int const OPTION_INTERFACE_CMD = 3;

@implementation Capabilities

@synthesize description;
@synthesize protocolMajorVersion;
@synthesize protocolMinorVersion;
@synthesize serviceMajorVersion;
@synthesize serviceMinorVersion;
@synthesize remoteManagementAddress;
@synthesize remoteManagementPort;
@synthesize serviceName;

- (id) init:(NSString *)description serverCapabilityElements:(NSMutableArray *)serverCapabilityElements {
  if (self = [super init]) {
    serverCapabilityElements = nil;
    description = description;
    if (serverCapabilityElements == nil) {
      serverCapabilityElements = [[[NSMutableArray alloc] init] autorelease];
    }
     else {
      serverCapabilityElements = serverCapabilityElements;
    }
  }
  return self;
}

- (id) init:(NSMutableArray *)serverCapabilityElements description:(NSString *)description serviceName:(NSString *)serviceName serviceVersionMajor:(int)serviceVersionMajor serviceVersionMinor:(int)serviceVersionMinor remoteManagementAddress:(NSString *)remoteManagementAddress {
  if (self = [super init]) {
    serverCapabilityElements = nil;
    description = description;
    serviceMajorVersion = serviceVersionMajor;
    serviceMinorVersion = serviceVersionMinor;
    serviceName = serviceName;
    [self setRemoteManagementAddress:remoteManagementAddress];
    if (serverCapabilityElements == nil) {
      serverCapabilityElements = [[[NSMutableArray alloc] init] autorelease];
    }
     else {
      serverCapabilityElements = serverCapabilityElements;
    }
  }
  return self;
}

- (CapabilitiesOfVersion *) getProtocolVersionCapabilities:(int)majorVersion {

  for (int cnt = OPTION_AUTHENTICATE_CMD; cnt < [serverCapabilityElements size]; cnt++) {
    CapabilitiesOfVersion * data = (CapabilitiesOfVersion *)[serverCapabilityElements elementAt:cnt];
    if ([data protocolVersionMajor] == majorVersion) {
      return data;
    }
  }

  return nil;
}

- (int) protocolMajorVersion {
  return [[self getProtocolVersionCapabilities:1] protocolVersionMajor];
}

- (int) protocolMinorVersion {
  return [[self getProtocolVersionCapabilities:1] protocolVersionMinor];
}

- (void) setRemoteManagementAddress:(NSString *)newRemoteManagementAddress {
  remoteManagementAddress = newRemoteManagementAddress;
  if (remoteManagementAddress != nil) {

    @try {
      remoteManagementPort = [Integer parseInt:[remoteManagementAddress substring:[remoteManagementAddress lastIndexOf:@":"] + 1 param1:[remoteManagementAddress length]]];
    }
    @catch (NumberFormatException * e) {
      @throw [[[IllegalArgumentException alloc] init:@"Illegal address format (error in port number scheme)."] autorelease];
    }
  }
}

- (NSString *) serviceName {
  if (serviceName == nil) {
    return BuildConfig.FLAVOR;
  }
  return serviceName;
}

- (BOOL) isProtocolOptionSupported:(int)option {
  CapabilitiesOfVersion * data = [self getProtocolVersionCapabilities:1];

  switch (option) {
  case OPTION_AUTHENTICATE_CMD:
    return [data authCmdSupported];
  case OPTION_HEARTBEAT_CMD:
    return [data heartbeatCmdSupported];
  case OPTION_INTERFACE_CMD:
    return [data interfaceCmdSupported];
  case OPTION_DAT_TIMESTAMP:
    return [data timeStampingDatSupported];
  default:
    @throw [[[IllegalArgumentException alloc] init:@"Illegal option value."] autorelease];
  }
}

- (NSString *) description {
  StringBuffer * buf = [[[StringBuffer alloc] init:100] autorelease];
  [buf append:[[[[[@"[Capabilities: description=" stringByAppendingString:[self description]] stringByAppendingString:@", service:"] + [self serviceName] stringByAppendingString:@", version="] + [self serviceMajorVersion] stringByAppendingString:@"."] + [self serviceMajorVersion] stringByAppendingString:@", remoteManagementAddress="] + [self remoteManagementAddress]];
  if (serverCapabilityElements != nil) {

    for (int cnt = OPTION_AUTHENTICATE_CMD; cnt < [serverCapabilityElements size]; cnt++) {
      [buf append:@"\r  "];
      [buf append:[((CapabilitiesOfVersion *)[serverCapabilityElements elementAt:cnt]) description]];
    }

  }
  [buf append:@"]"];
  return [buf description];
}

- (void) dealloc {
  [description release];
  [remoteManagementAddress release];
  [serverCapabilityElements release];
  [serviceName release];
  [super dealloc];
}

@end
