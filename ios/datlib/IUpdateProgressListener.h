
/**
 * @file IUpdateProgressListiner.h
 * @class IDongleStatus interface class
 * 
 * In order to define application behavior of configuration update progress,
 * the application class should be derived from this one.
 */

@protocol DATIUpdateProgressListener <NSObject>
- (void) onProgressChanged:(NSNumber *)f;
- (void) onUpdateFailed:(NSException *)exception;
- (void) onUpdateStarted;
- (void) onUpdateSuccess;
@end
