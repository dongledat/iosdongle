/**
 * @file    IConnectionListiner.h
 * @brief   IConnectionListiner class declaration
 */

/**
 * @class IConnectionListener interface class
 */

@protocol DATIConnectionListener <NSObject>
- (void) onConnectionSuccessful:(Capabilities *)capability;
- (void) onConnectionClosed:(BOOL)b;
- (void) onConnectionClosedOnError:(NSException *)e;
- (void) onConnectionFailed:(NSException *)e;
- (void) onAuthenticateByPIN:(BOOL)b;
- (void) onAuthenticateByToken:(BOOL)b;
- (void) onDataloss;
- (void) onReconnect;
@end
