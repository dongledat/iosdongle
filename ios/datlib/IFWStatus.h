/**
 * @file    IFWStatus.h
 * @brief   IFWStatus class declaration
 */
typedef enum {
  DATCurrentAppMode_UNKNOWN,
  DATCurrentAppMode_USER_APP,
  DATCurrentAppMode_SERVICE_APP,
  DATCurrentAppMode_ULOADER
} DATCurrentAppMode;

typedef struct {
  NSString * stringValue;
} DATCurrentAppMode_Fields;

static DATCurrentAppMode_Fields CurrentAppMode_Data[4] = {
  { @"unknown"}
  , { @"UserApp"}
  , { @"ServiceApp"}
  , { @"uLoader"}
};

DATCurrentAppMode CurrentAppModeValueOf(NSString *text);
DATCurrentAppMode CurrentAppModeDescription(DATCurrentAppMode value);

id<DATCurrentAppMode> CurrentAppMode_parse(id<DATCurrentAppMode> e, NSString * valueAsString);
NSString * CurrentAppMode_stringValue(id<DATCurrentAppMode> e);
NSString * CurrentAppMode_description(id<DATCurrentAppMode> e);

/**
 * @class IFWStatus interface class
 */

@protocol DATIFWStatus <NSObject>
- (id<DATCurrentAppMode>) getCurrentAppMode;
- (NSString *) getDbVersion;
- (NSString *) getServiceAppVersion;
- (NSString *) getUserAppVersion;
- (NSString *) getuLoaderVersion;
- (BOOL) isUpdateInProgress;
@end
