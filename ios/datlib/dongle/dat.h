/** @file dat.h */

#ifndef DAT_H
#define DAT_H

#if defined(__cplusplus)
extern "C" {
#endif

#include "pb_encode.h"
#include "pb_decode.h"
#include "dongle.pb.h"

/**
 * @enum dat_result_e
 * @brief Result codes returned by library APIs 
 */
typedef enum
{
	DAT_ERROR = -1,
    DAT_OK	
} dat_result_e;
 
#define TDongleRsp_MAX_SIZE (1024)

/** 
 * @struct dat_context_t
 * @brief DAT library context
 *
 * The structure should be filled with correct values before calling eithet DAT API.
 */
typedef struct _dat_context_t
{     
  	/** 
	 * @fn dat_result_e (*dat_to_net_cb)(dat_context_t* context, const unsigned char* buffer, size_t length);
	 * @brief Mandatory pointer to callback function
     * @param buffer Pointer to buffer
     * @param length Length of buffer to be passed to the network layer
	 * 
	 * The callback is called by DAT security layer to pass encoded and encrypted
	 * data to the network layer.
	 */
    dat_result_e (*dat_to_net_cb)(const unsigned char* buffer, size_t length);

	/** 
	 * @fn dat_result_e (*dat_to_car_cb)(const TDongleReq* req, TDongleRsp* resp);
 	 * @brief Mandatory pointer to callback function
	 * @param req Request in DAT format
	 * @param resp Response in DAT format
	 *
	 */
	dat_result_e (*dat_to_car_cb)(const TDongleReq* req, TDongleRsp* resp);

	/** 
	 * @fn void* (*p_malloc)(size_t bytes);
	 * @brief Optional pointer to low level memory allocation function
     *  
     * In case p_malloc is NULL, the standard \a malloc() will be used.	 
	 */
	void* (*p_malloc)(size_t bytes);
	
	/** 
	 * @fn void (*p_free)(void* addr);
	 * @brief Optional pointer to low level memory free function
     *  
     * In case p_free is NULL, the standard \a free() will be used.	 
	 */
    void (*p_free)(void* addr);
	
	/** 
	 * @fn void* (*p_lock_init)();
	 * @brief Optional pointer to low level mutex initialization function
     *  
     * In case p_free is NULL, the standard OS \a mutex_init will be used.
	 */
	void* (*p_lock_init)();
	
	/** 
	 * @fn void (*p_lock_done)(void* lock);
	 * @brief Optional pointer to low level mutex de-initialization function
     *  
     * In case p_free is NULL, the standard OS \a mutex_deinit will be used.
	 */
	void (*p_lock_done)(void* lock);

	/** 
	 * @fn void (*p_lock)(void* lock);
	 * @brief Optional pointer to low level mutex lock function
     *  
     * In case p_free is NULL, the standard OS \a mutex_lock will be used.
	 */
    void (*p_lock)(void* lock);

	/** 
	 * @fn void (*p_unlock)(void* lock);
	 * @brief Optional pointer to low level mutex unlock function
     *  
     * In case p_free is NULL, the standard OS \a mutex_unlock will be used.
	 */
    void (*p_unlock)(void* lock);
	
	/** 
	 * @fn int (*p_flash_read)(void* address, unsigned char* buffer, size_t size);
	 * @brief Mandatory pointer to low level flash read funtion
     * @param[in] logicalAddress source address in the range [0, p_flash_size() -1] of the flash
     * @param[in] buffer pointer to the array that will be updated with the data stored at address logicalAddress of the flash. 
     * @param[in]  size number of bytes that will be read from the flash.
     * @return the number of bytes read
	 */
    uint32_t (*p_flash_read)(uint32_t logicalAddress, uint8_t *buffer, uint32_t size);
	
	/** 
	 * @fn int (*p_flash_write)(void* address, unsigned char* buffer, size_t size);
	 * @brief Mandatory pointer to low level flash write funtion
     * @param[in] logicalAddress destination address in the range [0, p_flash_size() -1] of the flash
     * @param[in] buffer pointer to the array that will be copied at address logicalAddress of the flash.
     * @param[in]  size number of bytes that will be writeb into the flash
     * @return the number of bytes writen
	 */
    uint32_t (*p_flash_write)(uint32_t logicalAddress, const uint8_t *buffer, uint32_t size);

	/** 
	 * @fn int (*p_flash_delete)(void);
	 * @brief Mandatory pointer to low level flash delete function
	 */
    int (*p_flash_deleteAll)(void);

	/**
	 * @fn int (*p_flash_size)(void);
	 * @brief Mandatory pointer to low level flash to get the size of the flash memory
	 */
    uint32_t (*p_flash_size)(void);
    
    /**
	 * @fn int (*p_GetRandomBuffer)(void *p_rng, uint8_t *output, uint32_t output_len);  
     * @brief Function random, it uses the Random number generator (RNG) of STM32F4xx family devices
     * @param p_rng    		nothing at the moment
     * @param output  		output buffer
     * @param output_len  	length of output buffer
     * @return               0 if success
     */
    int (*p_GetRandomBuffer)(void *p_rng, uint8_t *output, uint32_t output_len);

    #define DAT_PRIVATE_SIZE 200

    char private_data[DAT_PRIVATE_SIZE];
    
} dat_context_t; 

/**
 * @fn dat_result_e dat_init(dat_context_t* context)
 * @brief Initialize DAT security library.
 * @param context Pointer to DAT context.
 *
 * The context must be saved into local variable and it should be 
 * available for other fucntions (dat_from_net for instance)
 */
dat_result_e dat_init(dat_context_t* context);

/** 
 * @fn dat_result_e dat_done(dat_context_t* context)
 * @brief DAT security library destructor.
 */
dat_result_e dat_done();

/** 
 * @fn dat_result_e dat_from_net(dat_context_t* context, const unsigned char* buffer, size_t length)
 * @brief Handle buffer from network layer
 * @param buffer Pointer to buffer from network layer
 * @param length Buffer length in bytes
 */
dat_result_e dat_from_net(const unsigned char* buffer, size_t length);

dat_result_e dat_from_car(const TDongleRsp* resp);

#if defined(__cplusplus)
}
#endif

#endif
