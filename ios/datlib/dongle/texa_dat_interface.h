#ifndef TEXA_DAT_INTERFACE_H
#define TEXA_DAT_INTERFACE_H

#include "txcm/txcm_cmnd.h"
#include "txcm/txcm_cmnd_def.h"
#include "dongle.pb.h"
#include "pb_helpers.h"

#if defined(__cplusplus)
extern "C" {
#endif

//int dat_create_texa_diag_response(TGetDataObjectRsp* dat_diag_rsp, TXCM_GET_DIAG_PARAM_PAYLOAD_RESPONSE* texa_diag_response);
//int dat_create_texa_diag_request(TGetDataObjectReq* dat_diag_req, TXCM_GET_DIAG_PARAM_PAYLOAD_REQUEST* texa_diag_request);	

#if defined(__cplusplus)
}
#endif

#endif