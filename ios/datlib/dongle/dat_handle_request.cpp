#include "dat_handle_request.h"
#include "DongleStateMachine.h"
#include "common/check_type.h"
#include "dat.h"

typedef struct 
{
	DongleStateMachine sm;
} dat_private_t;

CHECK_TYPE_SIZE_LE(dat_private_t, DAT_PRIVATE_SIZE);

int handle_response(dat_context_t* dat, TDongleRsp * rsp);
int send_response(dat_context_t* dat, TDongleRsp * rsp);
int handle_pdu_response(dat_context_t* dat, TDongleRsp * rsp);

int dat_handle_request(dat_context_t* dat, const unsigned char* buffer, size_t length)
{
	// Deserialize request
	TDongleReq req = TDongleReq_init_zero;
	pb_istream_t stream = pb_istream_from_buffer(buffer, length);
	if (!pb_decode(&stream, TDongleReq_fields, &req)) {
		return DAT_ERROR;
	}

	// Pass the request to the "car" and get the car's response
	TDongleRsp rsp = TDongleRsp_init_zero;
	dat->dat_to_car_cb(&req, &rsp);

	// Request is done - release it
	pb_release(TDongleReq_fields, &req);

	// We have a ready response - continue handling it
	return handle_response(dat, &rsp);
}

int handle_response(dat_context_t* dat, TDongleRsp * rsp)
{
	int status = DAT_ERROR;

	switch (rsp->which_response)
	{
	case TDongleRsp_PDURsp_tag:
		//status = handle_pdu_response(dat, rsp);
		//break;

	//TODO

	default:
		status = send_response(dat, rsp);
	}

	return status;
}

int send_response(dat_context_t* dat, TDongleRsp * rsp)
{
	// Serialize response
	unsigned char response_buffer[TDongleRsp_MAX_SIZE] = { 0 };
	pb_ostream_t ostream = pb_ostream_from_buffer(response_buffer, TDongleRsp_MAX_SIZE);
	if (!pb_encode(&ostream, TDongleRsp_fields, rsp)) {
		return DAT_ERROR;
	}

	// Send it out
	dat->dat_to_net_cb(response_buffer, ostream.bytes_written);
	return DAT_OK;
}

