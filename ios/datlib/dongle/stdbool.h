#ifndef STDBOOL_H
#define STDBOOL_H

#ifndef __cplusplus

#define false   0
#define true    1

#define bool int

#endif

#endif