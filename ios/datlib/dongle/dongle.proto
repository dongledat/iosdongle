// Compile file.pb from file.proto by: 'protoc -ofile.pb file.proto'. Output will
// be written to file.pb.h and file.pb.c.
// Afterwards compile file.pb by nanopb_generator.exe to file.pb.h and file.pb.c

syntax = "proto2";

import "nanopb.proto";

//import "service.proto";

//  <Users> 
//    <!-- "default" user (with admin permission) - also implicitly defined/used --> 
//    <!-- the BLUETOOTH_PIN is a label that represents the code printed on the  --> 
//    <!-- the cover of the VCI                                                  --> 
//    <User name="default" password="BLUETOOTH_PIN"> 
//        <Permit group="default"/> 
//    </User> 
//  </Users>

//  <!-- @description "Version" version structure --> 
//  <Type url="VersionType" context="global"> 
//    <!-- for instance, ver = "1_13_1_0" -->
//     <Text name="ver" regExp="([0-9]{1,4}_){3}([0-9]{1,4})"/>    
//  </Type>  
message TVersionType
{
    required string ver = 1 [(nanopb).max_size = 20];
} 
  
//  <!-- @description "SerialNumberDevice" serial number of the VCI device        -->   
//  <Object url="SerialNumberDevice" context="global"  characteristic="static" required="true">
//     <Text name="SerialNumber" regExp=".+"/>
//  </Object> 
//  TODO: may S/N size be more than 11 characters?
message TSerialNumberDeviceReq
{
}

message TSerialNumberDeviceRsp
{
    required string SerialNumber = 1 [(nanopb).max_size = 11];
}

//  <!-- @description "BtMacAddress" mac address of the bluetooth module          -->   
//  <Object url="BtMacAddress" context="global"  characteristic="static" required="true">
//     <!-- for instance, ver = "11:22:33:AB:CD:12" -->     
//     <Text name="Mac" regExp="([0-9A-F][0-9A-F]:){5}[0-9A-F][0-9A-F]"/>
//  </Object> 
message TBtMacAddressReq
{
}

message TBtMacAddressRsp
{
    required string Mac = 1 [(nanopb).max_size = 17];
}

//  <!-- @description "HwVersion" hw version of the VCI device                    -->   
//  <Object url="HwVersion" context="global"  characteristic="static" required="true">
//     <text name="Board" regExp=".*"/> 
//  </Object> 
//  TODO: define max string size
message THwVersionReq
{
}

message THwVersionRsp
{
    required string Board = 1 [(nanopb).max_size = 20];
}

//  <!-- @description "FwStatus" fw status                                        -->   
//  <Object url="FwStatus" context="global"  characteristic="static" required="true">
//    <ObjectEntity name="Ver_userApp"     typeRef="VersionType"/>
//    <ObjectEntity name="Ver_serviceApp"  typeRef="VersionType"/>
//    <ObjectEntity name="Ver_uLoader"     typeRef="VersionType"/>
message TFwStatusReq
{
}

message TFwStatusRsp
{
    required TVersionType Ver_userApp = 1;
    required TVersionType Ver_serviceApp = 2;
    required TVersionType Ver_uLoader = 3;
}
    
//  <!-- @description Ver_Db version of the diagnostic database    
//  if the database is valid, the value is > 0
//  if the database is corrupted or not authentic, the value is 0   
//  -->
//  <text name="Ver_db"                  regExp="([0-9]*)"/>
//  TODO: when it is used?

//    <Enumeration name="CurrentApp">
//        <Member id="UserApp"    />
//        <Member id="ServiceApp" />
//        <Member id="uLoader"    />         
//    </Enumeration> 
enum ECurrentApp
{
    UserApp = 1;
    ServiceApp = 2;
    uLoader = 3;
}
    
//  <!-- @description UpgradeProcedureIsInProgress this flag is set to true    
//  if updating procedure is in progress.   
//  -->
//  <Activity name="UpgradeProcedureIsInProgress"/>
//  </Object> 
//  TODO: Format is unclear

//  <!-- @description "DevStatus" status of the device -->   
//  <Object url="DevStatus" context="global"  characteristic="event" required="true">
//    <Enumeration name="DiagnosticStatus"> 
//        <Member id="DeviceIsBusy"                      />
//        <Member id="ConfigurationVerifyInProgress"     />
//        <Member id="ConfiguredAndNotConnected"         />
//        <Member id="NotConfiguredAndEOBDcommunication" />
//        <Member id="ConfiguredAndConnected"            />
//        <Member id="NotConfiguredAndNotConnected"      />
//        <Member id="ErrorCorruptedDevice"              />
//        <Member id="ErrorMemoryProblem"                />
//   </Enumeration> 
//    <Activity name="AuthenticationStatus"/>       
//  </Object> 
message TDevStatusReq
{
}

message TDevStatusRsp
{
    enum EDiagnosticStatus
    {
        DeviceIsBusy = 1;
        ConfigurationVerifyInProgress = 2;
        ConfiguredAndNotConnected = 3;
        NotConfiguredAndEOBDcommunication = 4;
        ConfiguredAndConnected = 5;
        NotConfiguredAndNotConnected = 6;
        ErrorCorruptedDevice = 7;
        ErrorMemoryProblem = 8;
    }
    
    required EDiagnosticStatus DiagnosticStatus = 1;
    required bool AuthenticationStatus = 2;
}   

//  <!-- @description "ActivationStatus" information regarding activation status
//    This url is typically used only by the service department. 
//  -->   
//  <Object url="ActivationStatus" context="global"  characteristic="static" required="true">
//     
//     <!-- date of the activation  date (date of the first use of the dongle)
//          The value is represented using UTC conventions according the following format, 
//          derived by ISO8601 specification:
//         
//          YYYY-MM-DDTHH:MM:SSZ
//          
//          This date must be greater than value of the productionDate attribute 
//          
//          If the dongle is has not yet been activated (isActived == false), the value of 
//          activationDate is:
//          0000-00-00T00:00:00Z 
//     -->
//
//     <Text name="activationDate"/>    
//
//     <!-- This field represents the production date of the dongle.       
//          The value is represented using UTC conventions according the following format, 
//          derived by ISO8601 specification:
//         
//          YYYY-MM-DDTHH:MM:SSZ     
//     -->
//     <Text name="productionDate"/>    
//  
//     <!-- The value is true if and only if the dongle has been activated -->      
//     <Activity name="isActive" />                 
//  
//  </Object> 
message TActivationStatusReq
{
}

message TActivationStatusRsp
{
    message TDate
    {
        required string Date = 1 [(nanopb).max_size = 20];
    }   
    
    required TDate ActivationDate = 1;
    required TDate ProductionDate = 2;
    required bool IsActive = 3;
}

//  <!-- @description SetProtocolMode This function sets the protocol mode --> 
//  <Function url="SetProtocolMode"> 
//    <In> 
//      <!-- @param mode protocol mode --> 
//      <Enumeration name="Mode">
//         <!-- @description  exlapMode this protocol is exlap compliant therefore is a human readable protocol           -->
//         <Member id="exlapMode"/> 
//         <!-- @description binaryMode this protocol is binary oriented and It is used to begin an upgrade procedure rebooting 
//         the dongle in "user app context"-->
//         <Member id="binaryMode"/> 
//         <!-- @description binaryMode this protocol is binary oriented and It is used to begin an upgrade procedure rebooting 
//         the dongle in "service app context"-->
//         <Member id="binaryMode_serviceApp"/> 
//      </Enumeration> 
//    </In> 
//    <Out> 
//      <!-- @param Result Result status of the operation --> 
//      <Enumeration name="Result"> 
//        <!--  @enum ok The operation was successful --> 
//        <Member id="ok"/> 
//        <!--  @enum error An other error has happened --> 
//        <Member id="error"/> 
//      </Enumeration>           
//    </Out>         
//  </Function> 
message TSetProtocolModeReq
{
    enum EMode
    {
        ExlapMode = 1; 
        BinaryMode = 2; 
        BinaryMode_serviceApp = 3;
    }
    
    required EMode Mode = 1; 
}

message TSetProtocolModeRsp
{
    enum EResult
    {
        ok = 1; 
        error = 2;
    }
    
    required EResult Result = 1; 
}

//   <!-- UPDATING PROCEDURE-->

//   <!-- @description OpenUpdatingSession This function starts the updating procedure 
//  In other word, it opens a valid session for the updating procedure. During 
//  this session, the client can update the uC using the DownloadDataBlock() url 
//  functions.
//  --> 
//  <Function url="OpenUpdatingSession"> 
//    <In>      
//        <!--  @enum ok: number of bytes of the new database file   --> 
//      <Absolute name="numberOfBytes" />
//    </In> 
//    <Out> 
//      <!-- @param Result Result status of the operation --> 
//      <Enumeration name="Result"> 
//        <!--  @enum ok: update procedure is ready  --> 
//        <Member id="ok"/> 
//        <!--  @enum error: update procedure can not be executed--> 
//        <Member id="error"/> 
//      </Enumeration>           
//    </Out>         
//  </Function> 
message TOpenUpdatingSessionReq
{
    required bytes Signature = 1 [(nanopb).type = FT_POINTER];
}

message TOpenUpdatingSessionRsp
{
    enum EResult
    {
        ok = 1;
        error = 2;
    }
    
    required EResult result = 1;
}

//  <!-- @description DownloadDataBlock This function downloads a chunk
//     of the file with the upgrade of the configuration files (database).
//     A data block is subset of the database resource. 
//     A database file can be represented by an array of 
//     "numberOfBytes" bytes:
//     
//     byte Database[numberOfBytes] = {,...,}
//     
//     and it can be divided in blocks of 512 bytes, except the last block 
//     In particular, the number of blocks can be calculated using this 
//     rule:
//        int numBlocks = numberOfBytes/512;
//        if ( numBlocks % 512 ){            // if numBlocks is not multiple of the 512 
//            numBlocks = numBlocks + 1;
//        }
//        
//     the i-th block is the subset defined by these ranges:
//     
//     a) i-th block is not the last block:
//        UpgradeResources[512*i-th, 512*(i-th + 1) - 1], 0 < i-th <  numBlocks-2
//     b) i-th block is the last block: 
//        UpgradeResources[512*i-th, numberOfBytes -1], i-th =  numBlocks - 1    
//  --> 
//  <Function url="DownloadDataBlock"> 
//    <In> 
//          
//        <!-- @param blockIndex index of the current block.
//        see comment of the url for details         
//        --> 
//      <Absolute name="blockIndex" unit="1" min="0"/>
//      
//        <!-- @param blockContent content of the blockIndex-th data block         
//             It is an array of bytes represented using a base 64 encoding
//           Example.
//           If the blockIndex-th data block is not last block then it is an array of 512 bytes.
//           This array must be represented 
//        --> 
//      <Binary name="blockContent"/>     
//   </In>  
//    <Out> 
//      <!-- @param Result Result status of the operation --> 
//      <Enumeration name="Result"> 
//        <!--  @enum ok: update procedure is ready  --> 
//        <Member id="ok"/> 
//        
//        <!--  @enum blockIndexError: blockIndex input parameter is out the valid range--> 
//        <Member id="blockIndexError"/> 
//
//        <!--  @enum writeError: writing phase is failed--> 
//        <Member id="writeError"/> 
//
//        <!--  @enum sizeError: size of array defined by memoryBlock parameter is not correct--> 
//        <Member id="sizeError"/> 
//
//      </Enumeration>           
//    </Out>             
//  </Function>
message TDownloadDataBlockReq
{
    required bytes BlockContent = 1 [(nanopb).type = FT_POINTER];
}

message TDownloadDataBlockRsp
{
    enum EResult
    {
        ok = 1;
        blockIndexError = 2;
        writeError = 3;
        sizeError = 4;
    }

    required EResult Result = 1;
}
  
//  <!-- @description CloseUpdatingSession 
//  This command close an updating session and checks if 
//  the updating procedure is been completed with success!
//    
//  -->
//  <Function url="CloseUpdatingSession"> 
//    <In> 
//                  
//        <!-- @param signature digital signature of the upgrade resource
//             It is an array of 256 bytes represented using a base 64 encoding
//             (==> 256*8/6 = 341 characthers)          
//        --> 
//      <Binary name="signature"/>      
//    </In> 
//    <Out> 
//      <!-- @param Result Result status of the operation --> 
//      <Enumeration name="Result"> 
//        <!--  @enum ok: update procedure is ready  --> 
//        <Member id="ok"/> 
//        <!--  @enum error: update procedure can not be executed--> 
//        <Member id="error"/> 
//      </Enumeration>           
//    </Out>             
//  </Function>
message TCloseUpdatingSessionReq
{
    required string Signature = 1 [(nanopb).max_size = 256]; //TODO: wtf is this good for?
}

message TCloseUpdatingSessionRsp
{
    enum EResult
    {
        ok = 1;
        error = 2;
    }
    
    required EResult Result = 1;
}

// Object type
enum EObjectCategory
{
    Service = 1;
    RPC = 2;
    Diag = 3;
}

message TDataCatalogEntry
{
    required int32 UrlCode = 1;
    required EObjectCategory ObjectCategory = 2;
    required bool IsSubscribed = 3;
}

//  <Type url="UrlObject" characteristic="dynamic" context="type"> 
//      <Text name="urlName" regExp=".+"/> 
//  </Type> 
//      
//  <!-- @description GetAvailableDiagnosticParameter returns the available diagnostic parameters -->
//  <!-- in other words, this fiunction returns diagnostic parameter that VCI can read --> 
//  <Function url="GetAvailableDiagnosticParameter"> 
//    <Out> 
//      <!-- @param Result Result status of the operation --> 
//      <ListEntity name="diagParameters" typeRef="UrlObject" /> 
//    </Out>         
//  </Function> 
//  TODO: what is the max count of parameters? It utilizes too much memory
message TGetAvailableDiagnosticParameterReq
{
}

message TGetAvailableDiagnosticParameterRsp
{
    repeated TDataCatalogEntry DataCatalogEntry = 1 [(nanopb).max_count = 128];
}

//  <Object url="AvailableDiagParametersAreChanged"  context="global"  characteristic="event" required="true">
//  </Object>
message TAvailableDiagParametersAreChangedReq
{
}

message TAvailableDiagParametersAreChangedRsp
{
    repeated TDataCatalogEntry DataCatalogEntry = 1 [(nanopb).max_count = 128];
}

// Dir command
// TODO: is there a need to support regex patterns?
message TDirReq
{
    optional int32 FromEntry = 1;
    optional int32 NumOfEntries = 2;
}

message TDirRsp
{
    repeated TDataCatalogEntry DataCatalogEntry = 1 [(nanopb).max_count = 128];
}

// Protocol command
// TODO: what is the max size of 'version'?
message TProtocolReq
{
    required string Version = 1 [(nanopb).max_size = 10, default = "1"];
    required bool ReturnCapabilities = 2 [default = true];
}

message TProtocolRsp
{
    message TCapabilities
    {
        // TODO: what is the max size of description, service, version?
        required string Description = 1 [(nanopb).max_size = 32];  
        required string Service = 2 [(nanopb).max_size = 32];
        required string Version = 3 [(nanopb).max_size = 10];
        
        message TSupports
        {
            required string Protocol = 1 [(nanopb).max_size = 10];
            required bool Interface = 2; 
            required bool Authenticate = 3; 
            required bool Heartbeat = 4; 
            required bool DatTimeStamp = 5; 
        }
        
        // TODO: Should it be set only if 'returnCapabilities" is true?
        optional TSupports Supports = 4;
    }   
    
    required TCapabilities Capabilities = 1;
}

message TGetDataObjectReq
{
    required int32 Reserved = 1;
	required int32 UrlCode = 2;
}

message TDataElemenent
{
    required int32 code = 1;	
    oneof value
	{
	    int32 i = 2;
		uint32 u = 3;
        bool b = 4;
		float f = 5;
		bytes s = 6 [(nanopb).type = FT_POINTER];
	}
}

message TDataObject
{
    required int32 UrlCode = 1;
    repeated TDataElemenent DataElement = 2 [(nanopb).type = FT_POINTER];
}


message TGetDataObjectRsp
{
    required TDataObject DataObject = 1;
}

message TSubscribeObjectReq
{
    required int32 Reserved = 1;
	required int32 UrlCode = 2;
    required int32 Timeout = 3;
}

message TSubscribeObjectRsp
{
    enum EResult
    {
        ok = 1;
        error = 2;
    }
    
    required EResult result = 1;
}

message TUnsubscribeObjectReq
{
    required int32 ObjectId = 1;	
}

message TUnsubscribeObjectRsp
{
    enum EResult
    {
        ok = 1;
        error = 2;
    }
    
    required EResult result = 1;
}

message TDataObjectUpdateRsp
{
    required TDataObject DataObject = 1;
}

message TPDUReq
{
    required int32 CanID = 1;
    required int32 CanSpeed = 2;
    required int32 CanSP = 3;
    required int32 CanSJW = 4;
    required int32 CanResponseTimeout = 5;
    required int32 TP20ChannelAddress = 6;
    required int32 PDUProtocol = 7;
    required bytes Data = 8 [(nanopb).type = FT_POINTER];
}

message TPDURsp
{
    required int32 TransactionID = 1;
    required int32 ChunkIndex = 2;
    required int32 TotalChunkCount = 3;
    required bytes Data = 4 [(nanopb).type = FT_POINTER];
}

message TSRPUserBytesA
{
    required string user = 1 [(nanopb).max_size = 32];
	required string bytes_A = 2 [(nanopb).max_size = 512];
}

message TSRPSaltBytesB
{
    required string salt = 1 [(nanopb).max_size = 32];
	required string bytes_B = 2 [(nanopb).max_size = 512];
}

message TSRPKeyProof
{
    required string bytes = 1 [(nanopb).max_size = 512];
}

message TECCPublicKey
{
	required string x = 1 [(nanopb).max_size = 28];
	required string y = 2 [(nanopb).max_size = 28];
}

message TAuthenticationReq
{
    enum EState
    {
        public_exchange = 1;
		additional_state = 2;
    }

	required EState state = 1;
	optional TECCPublicKey ECCPublicKey = 2 [(nanopb).max_size = 28];
}

message TAuthenticationRsp
{
    required int32 Reserved = 1;
	required int32 UrlCode = 2;
    required int32 Timeout = 3;
}

message TDongleReq
{
    optional int32 id = 255;            // Request ID
    oneof request
	{
        TSerialNumberDeviceReq SerialNumberDeviceReq = 1;
        TBtMacAddressReq BtMacAddressReq = 2;
        THwVersionReq HwVersionReq = 3;
        TFwStatusReq FwStatusReq = 4;
        TDevStatusReq DevStatusReq = 5;
        TActivationStatusReq ActivationStatusReq = 6;
        TSetProtocolModeReq SetProtocolModeReq = 7;
        TOpenUpdatingSessionReq OpenUpdatingSessionReq = 8;
        TDownloadDataBlockReq DownloadDataBlockReq = 9;
        TCloseUpdatingSessionReq CloseUpdatingSessionReq = 10;
        TGetAvailableDiagnosticParameterReq GetAvailableDiagnosticParameterReq = 11;
        TAvailableDiagParametersAreChangedReq AvailableDiagParametersAreChangedReq = 12; //todo: remove?
        TDirReq DirReq = 13; //todo: remove?
        TProtocolReq ProtocolReq = 14;
        TGetDataObjectReq GetDataObjectReq = 15;
		TSubscribeObjectReq SubscribeObjectReq = 16;
		TUnsubscribeObjectReq UnsubscribeObjectReq = 17;
		TPDUReq PDUReq = 18;
		TSRPUserBytesA SRPUserBytesA = 19;
		TAuthenticationReq AuthReq = 20;
		TSRPKeyProof SRPKeyProof = 21;
	}
}
    
message TDongleRsp
{
    optional int32 id = 255;            // Request ID
    oneof response
	{
        TSerialNumberDeviceRsp SerialNumberDeviceRsp = 1;
        TBtMacAddressRsp BtMacAddressRsp = 2;
        THwVersionRsp HwVersionRsp = 3;
        TFwStatusRsp FwStatusRsp = 4;
        TDevStatusRsp DevStatusRsp = 5;
        TActivationStatusRsp ActivationStatusRsp = 6;
        TSetProtocolModeRsp SetProtocolModeRsp = 7;
        TOpenUpdatingSessionRsp OpenUpdatingSessionRsp = 8;
        TDownloadDataBlockRsp DownloadDataBlockRsp = 9;
        TCloseUpdatingSessionRsp CloseUpdatingSessionRsp = 10;
        TGetAvailableDiagnosticParameterRsp GetAvailableDiagnosticParameterRsp = 11;
        TAvailableDiagParametersAreChangedRsp AvailableDiagParametersAreChangedRsp = 12;
        TDirRsp DirRsp = 13;
        TProtocolRsp ProtocolRsp = 14;
        TGetDataObjectRsp GetDataObjectRsp = 15;
		TSubscribeObjectRsp SubscribeObjectRsp = 16;
		TUnsubscribeObjectRsp UnsubscribeObjectRsp = 17;
        TDataObjectUpdateRsp DataObjectUpdateRsp = 18;
		TPDURsp PDURsp = 19;
		TSRPSaltBytesB SRPSaltBytesB = 20;
		TSRPKeyProof SRPKeyProof = 21;
	}
}