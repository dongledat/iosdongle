#include "dat.h"
#include "dat_handle_request.h"

static dat_context_t* dat;

dat_result_e dat_init(dat_context_t* context)
{
    dat = context;
    return DAT_OK;
}

dat_result_e dat_done()
{
    return DAT_OK;
}

dat_result_e dat_from_net(const unsigned char* buffer, size_t length)
{
    return dat_handle_request(dat, buffer, length);
}

dat_result_e dat_from_car(const TDongleRsp* resp)
{
	unsigned char buffer[TDongleRsp_MAX_SIZE];
	size_t length = 0;

	// Serialize the response
	memset(buffer, 0, sizeof(buffer));
	pb_ostream_t ostream = pb_ostream_from_buffer(buffer, TDongleRsp_MAX_SIZE);
	if (!pb_encode(&ostream, TDongleRsp_fields, resp)) {
		printf("pb_encode failed on async car data object send");
		return DAT_ERROR;
	}

	return dat->dat_to_net_cb(buffer, ostream.bytes_written);
}
