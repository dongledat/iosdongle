/* pb_helpers.c: Common support functions

 */

#include "pb_helpers.h"
#include "pb_encode.h"
#include "pb_decode.h"


uint8_t pb_buffer[MAXIMIM_PROTO_BUFFER];
pb_ostream_t stream;

uint8_t current_request_id = 0;
uint8_t status;

void dat_pb_init()
{
	stream = pb_ostream_from_buffer(pb_buffer, sizeof(pb_buffer));
}

TDongleReq dat_pb_create_request(int32_t requestType)
{
	TDongleReq req = TDongleReq_init_zero;
	req.id = current_request_id++;
	req.has_id = 1;
	req.which_request = requestType;

	return req;
}

uint8_t dat_pb_encode(TDongleReq req, uint8_t** buffer)
{
	status = pb_encode(&stream, TDongleReq_fields, &req);
	*buffer = pb_buffer;

	return stream.bytes_written;
}