#include "dat_txcm.h"

static dat_txcm_t* g_dat_txcm = NULL;

void dat_txcm_init(dat_txcm_t* dat_txcm)
{
	g_dat_txcm = dat_txcm;
}

void dat_txcm_done()
{
	g_dat_txcm = NULL;
}

static void _txcm_scheduler_diag_status_is_changed()
{
	TXCM_DIAG_STATUS_PAYLOAD_NOTIFICATION txcm_resp;
	TDongleRsp dat_resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut = 0;
	dat_result_e res;

	(*g_dat_txcm->txcmCmndCbk_notification.p_DiagStatusNotification_cbk)(&txcm_resp, numOfBytesIn, &numOfBytesOut);

	// TODO: convert TXCM_DIAG_URL_PAYLOAD_NOTIFICATION to TDongleRsp

	res = dat_from_car(&dat_resp);
}

static void _txcm_scheduler_diag_url_is_changed()
{
	TXCM_DIAG_URL_PAYLOAD_NOTIFICATION txcm_resp;
	TDongleRsp dat_resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut = 0;
	dat_result_e res;

	(*g_dat_txcm->txcmCmndCbk_notification.p_DiagUrlNotification_cbk)(&txcm_resp, numOfBytesIn, &numOfBytesOut);

	// TODO: convert TXCM_DIAG_URL_PAYLOAD_NOTIFICATION to TDongleRsp

	res = dat_from_car(&dat_resp);
}

static void _txcm_scheduler_availbale_diag_parameters_are_changed()
{
	TXCM_ADP_ARE_CHANGED_PAYLOAD_NOTIFICATION txcm_resp;
	TDongleRsp dat_resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut = 0;
	dat_result_e res;

	(*g_dat_txcm->txcmCmndCbk_notification.p_ADP_AreChangedNotification_cbk)(&txcm_resp, numOfBytesIn, &numOfBytesOut);

	// TODO: convert TXCM_ADP_ARE_CHANGED_PAYLOAD_NOTIFICATION to TDongleRsp

	res = dat_from_car(&dat_resp);
}

int dat_from_txcm_scheduler(TXCM_SCHEDULER_EVENTS events)
{
	switch (events) {
	case TXCM_SCHEDULER_BT_CONNECTION:
		// TODO: ???
		break;

	case TXCM_SCHEDULER_BT_DISCONNECTION:
		// TODO: ???
		break;

	case TXCM_SCHEDULER_DIAG_STATUS_IS_CHANGED:
		_txcm_scheduler_diag_status_is_changed();
		break;

	case TXCM_SCHEDULER_DIAG_URL_IS_CHANGED:
		_txcm_scheduler_diag_url_is_changed();
		break;

	case TXCM_SCHEDULER_AVAILBALE_DIAG_PARAMETERS_ARE_CHANGED:
		_txcm_scheduler_availbale_diag_parameters_are_changed();
		break;
	}

	return 0;
}

static void _txcm_GetFwStatusRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_GET_FW_STATUS_PAYLOAD_REQUEST req;
	TXCM_GET_FW_STATUS_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_GetFwStatusRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_GetDiagStatusRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_GET_DIAG_STATUS_PAYLOAD_REQUEST req;
	TXCM_GET_DIAG_STATUS_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_GetDiagStatusRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_GetActivationStatusRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_GET_ACTIVATION_STATUS_PAYLOAD_REQUEST req;
	TXCM_GET_ACTIVATION_STATUS_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_GetActivationStatusRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_GetDeviceInfoRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_GET_DEVICE_INFO_PAYLOAD_REQUEST req;
	TXCM_GET_DEVICE_INFO_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_GetDeviceInfoRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_GetDiagUrlRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_GET_DIAG_URL_PAYLOAD_REQUEST req;
	TXCM_GET_DIAG_URL_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_GetDiagUrlRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_SubscribeDiagUrlRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_SUBSCRIBE_DIAG_URL_PAYLOAD_REQUEST req;
	TXCM_SUBSCRIBE_DIAG_URL_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_SubscribeDiagUrlRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_UnSubscribeDiagUrlRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_UNSUBSCRIBE_DIAG_URL_PAYLOAD_REQUEST req;
	TXCM_UNSUBSCRIBE_DIAG_URL_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_UnSubscribeDiagUrlRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_GetAvailableDiagUrlRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_GET_AVAILABLE_DIAG_URL_PAYLOAD_REQUEST req;
	TXCM_GET_AVAILABLE_DIAG_URL_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_GetAvailableDiagUrlRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_GetSupportedDiagUrlRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_GET_SUPPORTED_DIAG_URL_PAYLOAD_REQUEST req;
	TXCM_GET_SUPPORTED_DIAG_URL_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_GetSupportedDiagUrlRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_OpenUpdatingSessionRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_OPEN_UPDATING_SESSION_PAYLOAD_REQUEST req;
	TXCM_OPEN_UPDATING_SESSION_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_OpenUpdatingSessionRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_DownloadDataBlockRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_DOWNLOAD_DATA_BLOCK_PAYLOAD_REQUEST req;
	TXCM_DOWNLOAD_DATA_BLOCK_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_DownloadDataBlockRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_CloseUpdatingSessionRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_CLOSE_UPDATING_SESSION_PAYLOAD_REQUEST req;
	TXCM_CLOSE_UPDATING_SESSION_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_CloseUpdatingSessionRequest_cbk)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_PduRequest(TXCM_CMND_CBK_clientRequest* cr, const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
	TXCM_PDU_PAYLOAD_REQUEST req;
	TXCM_PDU_PAYLOAD_RESPONSE resp;
	uint32_t numOfBytesIn = 0; // TODO: ???
	uint32_t numOfBytesOut;

	(*cr->p_PduRequest)(&req, &resp, numOfBytesIn, &numOfBytesOut);

	// TODO: translate resp to dat_resp
}

static void _txcm_StartDiag()
{
	(*g_dat_txcm->txcmServiceCbk.p_StartDiag_cbk)();
}

static void _txcm_StopDiag()
{
	(*g_dat_txcm->txcmServiceCbk.p_StartDiag_cbk)();
}

dat_result_e dat_txcm_cb(const TDongleReq* req, TDongleRsp* resp)
{
	TXCM_CMND_CBK_clientRequest* cr = &g_dat_txcm->txcmCmndCbk_clientRequest;

	_txcm_GetFwStatusRequest(cr, req, resp);
	_txcm_GetDiagStatusRequest(cr, req, resp);
	_txcm_GetActivationStatusRequest(cr, req, resp);
	_txcm_GetDeviceInfoRequest(cr, req, resp);
	_txcm_GetDiagUrlRequest(cr, req, resp);
	_txcm_SubscribeDiagUrlRequest(cr, req, resp);
	_txcm_UnSubscribeDiagUrlRequest(cr, req, resp);
	_txcm_GetAvailableDiagUrlRequest(cr, req, resp);
	_txcm_GetSupportedDiagUrlRequest(cr, req, resp);
	_txcm_OpenUpdatingSessionRequest(cr, req, resp);
	_txcm_DownloadDataBlockRequest(cr, req, resp);
	_txcm_CloseUpdatingSessionRequest(cr, req, resp);
	_txcm_PduRequest(cr, req, resp);

	_txcm_StartDiag();
	_txcm_StopDiag();

	return DAT_OK;
}
