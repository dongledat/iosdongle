#ifndef DAT_TEXA_H
#define DAT_TEXA_H

#include "txcm/txcm_cmnd_cbk.h"
#include "txcm/txcm_scheduler_cbk.h"
#include "txcm/txcm_scheduler_def.h"

typedef struct _dat_texa_t
{
	/**
	* \brief set of callback funtions implemented by the TEXA command manager.
	* Mainly, these callback are "auxiliary" functions that the caller should
	* use to build the responses and the notifications.
	*/
	TXCM_CMND_CBK_common txcmCmndCbk_common;

	/**
	* \brief set of callback fucntions implemented by the TEXA command manager.
	* These functions should be used to build the response related to a specific
	* client (smartphone) request.
	*/
	TXCM_CMND_CBK_clientRequest txcmCmndCbk_clientRequest;

	/**
	* \brief set of callback functions implemented by the TEXA command manager.
	* These functions should be used to build a notification (for the client)
	* when TEXA scheduler invokes the function @p dat_from_txcm_scheduler
	*/
	TXCM_CMND_CBK_notification txcmCmndCbk_notification;

	/**
	* \brief set of callback functions implemented by the TEXA scheduler
	* (for instance, the caller can start or stop the diagnostic module)
	*/
	TXCM_SCHEDULER_CBK txcmServiceCbk;

} dat_texa_t;

#define txcmCbk_common txcmCmndCbk_common 
#define txcmCbk_clientRequest txcmCmndCbk_clientRequest 
#define txcmCbk_notification txcmCmndCbk_notification 

/*! \fn dat_result_e dat_from_txcm_scheduler(dat_context_t* context, TDongleRsp* response);
*  \brief Handle diagnostics response/subscription from car diagnostics layer
*  \param events events signaled by the TEXA scheduler
*  Some events represent a notification that the dat_from_txcm_scheduler
*  should build. The following table defines the link between these events and the related
*  callback funtions defined in the structure called TXCM_CMND_CBK_notification:
*  TXCM_SCHEDULER_DIAG_STATUS_IS_CHANGED                   -> p_DiagStatusNotification_cbk
*  TXCM_SCHEDULER_DIAG_URL_IS_CHANGED                      -> p_DiagUrlNotification_cbk
*  TXCM_SCHEDULER_AVAILBALE_DIAG_PARAMETERS_ARE_CHANGED    -> p_AvailableDiagParametersAreChanged_cbk
*/
int dat_from_txcm_scheduler(TXCM_SCHEDULER_EVENTS events);

#endif

