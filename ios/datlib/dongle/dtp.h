#ifndef DTP_H
#define DTP_H

#include "common/net_buf.h"

#if defined(__cplusplus)
extern "C" {
#endif

typedef enum
{
	DTP_OK = 0,
	DTP_OVERFLOW = -1,
	DTP_CRC_ERROR = -2
} dtp_result_e;

typedef void* dtp_handle_t;

dtp_handle_t dtp_init(unsigned char* buf, unsigned size, void (*from_net_cb)(unsigned char* buf, unsigned size), void (*to_net_cb)(net_buf_t* net_buf));
void dtp_done(dtp_handle_t dtp);
dtp_result_e dtp_from_net(dtp_handle_t dtp, unsigned char* buf, unsigned size);
dtp_result_e dtp_to_net(dtp_handle_t dtp, net_buf_t* net_buf);

#if defined(__cplusplus)
}
#endif

#endif
