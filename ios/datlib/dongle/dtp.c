#include <string.h>
#include "dtp.h"
#define CRC16
#include "crypto/dat_crc.h"
#include "os/types.h"

typedef struct
{
    unsigned char* buf;
	unsigned buf_size;
	unsigned pos;
	void (*from_net_cb)(unsigned char* buf, unsigned size);
	void (*to_net_cb)(net_buf_t* net_buf);

} dtp_t;

typedef struct
{
    uint16_t len;
	uint16_t crc;

} dtp_preamble_t;

dtp_handle_t dtp_init(unsigned char* buf, unsigned size, void (*from_net_cb)(unsigned char* buf, unsigned size), void (*to_net_cb)(net_buf_t* net_buf))
{
    enum { MIN_BUFFER_SIZE = 10 };

    dtp_t* dtp = (dtp_t*)buf;

    if (size < sizeof(dtp_t) + MIN_BUFFER_SIZE)
        return NULL;

    dtp->buf_size = size - sizeof(dtp_t);
    dtp->from_net_cb = from_net_cb;
    dtp->to_net_cb = to_net_cb;
    dtp->pos = 0;
	dtp->buf = buf + sizeof(dtp_t);

	crcInit();

    return (dtp_handle_t)dtp;
}

void dtp_done(dtp_handle_t* handle)
{
	dtp_t* dtp = (dtp_t*)handle;
}

dtp_result_e dtp_from_net(dtp_handle_t handle, unsigned char* buf, unsigned size)
{
	dtp_t* dtp = (dtp_t*)handle;
	dtp_preamble_t* preamble = (dtp_preamble_t*)dtp->buf;
	unsigned char* data = NULL;
	unsigned data_size;
	uint16_t crc, len;

	/* 
	 * The data is too big to fit the buffer. Buffer overflow.
	 */
    if (dtp->pos + size > dtp->buf_size)
        return DTP_OVERFLOW;

	memcpy(dtp->buf + dtp->pos, buf, size);

	crc = ntohs(preamble->crc);
	len = ntohs(preamble->len);

	if (dtp->pos + size < sizeof(dtp_preamble_t) || 
		dtp->pos + size - sizeof(dtp_preamble_t) < len)
	{
		dtp->pos += size;
	}

	else
	{
		unsigned char* data = dtp->buf + sizeof(dtp_preamble_t);

		/*
		 * CRC error
		 */
		if (crc != crcFast(data, len))
			return DTP_CRC_ERROR;

        if (*dtp->from_net_cb != NULL)
		    (*dtp->from_net_cb)(data, len);

		if (dtp->pos + size - sizeof(dtp_preamble_t) > len)
		{
			data = dtp->buf + sizeof(dtp_preamble_t) + len;
			data_size = dtp->pos + size - len - sizeof(dtp_preamble_t);
			memmove(dtp->buf, data, data_size);
			dtp->pos = data_size;
		}
		else
			dtp->pos = 0;
	}

	return DTP_OK;
}

dtp_result_e dtp_to_net(dtp_handle_t handle, net_buf_t* net_buf)
{
	dtp_t* dtp = (dtp_t*)handle;
	dtp_preamble_t preamble;

	preamble.len = htons(net_buf_data_len(net_buf));
	preamble.crc = htons(crcFast(net_buf_data(net_buf), net_buf_data_len(net_buf)));

	if (net_buf_add_head(net_buf, (unsigned char*)&preamble, sizeof(preamble)) < 0)
		return DTP_OVERFLOW;
	
    if (*dtp->to_net_cb != NULL)
	    (*dtp->to_net_cb)(net_buf);

	return DTP_OK;
}
