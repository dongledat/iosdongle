#include "texa_dat_interface.h"

static bool read_string(pb_istream_t *stream, const pb_field_t *field, void **arg)
{
	uint8_t buf[20] = { 0 };
	size_t len = stream->bytes_left;

	if (len > sizeof(buf) - 1 || !pb_read(stream, buf, len))
		return false;

	return true;
}

TEST(Dongle, InterfaceTest)
{
	TGetDataObjectReq dat_diag_req;
	TXCM_GET_DIAG_PARAM_PAYLOAD_REQUEST texa_req;
	memset(&dat_diag_req, 0, sizeof(dat_diag_req));
	memset(&texa_req, 0, sizeof(texa_req));
	
	dat_diag_req.UrlCode = 1234;
	dat_create_texa_diag_request(&dat_diag_req, &texa_req);
	LONGS_EQUAL_TEXT(texa_req.urlCode, 1234, "code is not the same");

	TGetDataObjectRsp dat_diag_rsp;
	TXCM_GET_DIAG_PARAM_PAYLOAD_RESPONSE texa_diag_rsp;

	memset(&dat_diag_rsp, 0, sizeof(dat_diag_rsp));
	memset(&texa_diag_rsp, 0, sizeof(texa_diag_rsp));
	texa_diag_rsp.nAttributes = 100;
	dat_create_texa_diag_response(&dat_diag_rsp, &texa_diag_rsp);
	LONGS_EQUAL_TEXT(texa_diag_rsp.nAttributes, 0, "No attributes at all");

	memset(&dat_diag_rsp, 0, sizeof(dat_diag_rsp));
	memset(&texa_diag_rsp, 0, sizeof(texa_diag_rsp));
	dat_diag_rsp.UrlCode = 1234;
	dat_create_texa_diag_response(&dat_diag_rsp, &texa_diag_rsp);
	LONGS_EQUAL_TEXT(texa_diag_rsp.urlCode, 1234, "urlcode in response invalid");

	dat_diag_rsp.DataObject.DataElement_count = 1;
	dat_diag_rsp.DataObject.DataElement[0].code = 100;
	dat_diag_rsp.DataObject.DataElement[0].which_value = TDataElemenentWhichValue_i;
	dat_diag_rsp.DataObject.DataElement[0].value.has_i = true;
	dat_diag_rsp.DataObject.DataElement[0].value.i = 12;
	dat_create_texa_diag_response(&dat_diag_rsp, &texa_diag_rsp);
	LONGS_EQUAL_TEXT(*(texa_diag_rsp.attributes[0].value), 12, "value is not correct");

	dat_diag_rsp.DataObject.DataElement_count = 1;
	dat_diag_rsp.DataObject.DataElement[0].code = 100;
	dat_diag_rsp.DataObject.DataElement[0].which_value = TDataElemenentWhichValue_s;
	dat_diag_rsp.DataObject.DataElement[0].value.s.funcs.decode = &read_string;
	dat_diag_rsp.DataObject.DataElement[0].value.s.arg = "Hello";
	
	dat_create_texa_diag_response(&dat_diag_rsp, &texa_diag_rsp);
	LONGS_EQUAL_TEXT(memcmp(texa_diag_rsp.attributes[0].value, "Hello", texa_diag_rsp.attributes[0].valueSize), 0, "value is not correct");
	LONGS_EQUAL_TEXT((char*)texa_diag_rsp.attributes[0].valueSize, 5, "value is not correct");
}