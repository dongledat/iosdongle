#include "pb_encode.h"
#include "pb_decode.h"
#include "dongle.pb.h"
#include "pb_helpers.h"

TEST(Dongle, protobuf)
/* Encode our message */
{
	static const int ID = 10;
    uint8_t buffer[MAXIMIM_PROTO_BUFFER];
    size_t message_length;
    bool status;

	{
		/* Allocate space on the stack to store the message data.
			*
			* Nanopb generates simple struct definitions for all the messages.
			* - check out the contents of simple.pb.h!
			* It is a good idea to always initialize your structures
			* so that you do not have garbage data from RAM in there.
			*/
		TDongleReq req = TDongleReq_init_zero;
		req.id = ID;
		req.has_id = 1;
		req.which_request = TDongleReq_SubscribeObjectReq_tag;
        
		/* Create a stream that will write to our buffer. */
		pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
               
		/* Now we are ready to encode the message! */
		status = pb_encode(&stream, TDongleReq_fields, &req);
		CHECK(status);
		message_length = stream.bytes_written;
		CHECK(message_length > 0);
	}

	{
		/* Now we could transmit the message over network, store it in a file or
		* wrap it to a pigeon's leg.
		*/

		/* But because we are lazy, we will just decode it immediately. */

		/* Allocate space for the decoded message. */
		TDongleReq req2 = TDongleReq_init_zero;
        
		/* Create a stream that reads from the buffer. */
		pb_istream_t stream = pb_istream_from_buffer(buffer, message_length);
        
		/* Now we are ready to decode the message. */
		status = pb_decode(&stream, TDongleReq_fields, &req2);
		CHECK(status);
		CHECK_EQUAL(req2.which_request, TDongleReq_SubscribeObjectReq_tag);
		CHECK_EQUAL(req2.id, ID);
	}
}
    
#if 0
TEST(Dongle, Response)
/* Encode our message */
{
    /* Allocate space on the stack to store the message data.
        *
        * Nanopb generates simple struct definitions for all the messages.
        * - check out the contents of simple.pb.h!
        * It is a good idea to always initialize your structures
        * so that you do not have garbage data from RAM in there.
        */
	{
		TDongleRsp rsp = TDongleReq_init_zero;
		rsp.id = 12;
		rsp.has_id = 1;
		rsp.which_response = TDongleRsp_EvaporativeSystem_tag;
		rsp.response.EvaporativeSystem.PurgeValve = 111;        
		rsp.response.EvaporativeSystem.Pressure = 222;        

		/* Create a stream that will write to our buffer. */
		pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
               
		/* Now we are ready to encode the message! */
		status = pb_encode(&stream, TDongleRsp_fields, &rsp);
		CHECK(status);
		message_length = stream.bytes_written;
		CHECK(message_length > 0);
	}

	/* Now we could transmit the message over network, store it in a file or
		* wrap it to a pigeon's leg.
		*/

	/* But because we are lazy, we will just decode it immediately. */

    /* Allocate space for the decoded message. */
	{
		TDongleRsp rsp = TDongleReq_init_zero;
        
		/* Create a stream that reads from the buffer. */
		pb_istream_t stream = pb_istream_from_buffer(buffer, message_length);
        
		/* Now we are ready to decode the message. */
		status = pb_decode(&stream, TDongleRsp_fields, &rsp);
		CHECK(status);
		CHECK_EQUAL(rsp.which_response, TDongleRsp_EvaporativeSystem_tag);
	}
}
#endif
