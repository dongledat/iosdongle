#include "dtp.h"

static int to_net = 0, from_net = 0;

static void from_net_cb(unsigned char* buf, unsigned size)
{
    from_net = 1;
}

static void to_net_cb(net_buf_t* net_buf)
{
    to_net = 1;
}

TEST(Dongle, dtp)
{
    /* Small buffer */
    unsigned char small_buf[10];
    dtp_handle_t dtp = dtp_init(small_buf, sizeof(small_buf), from_net_cb, to_net_cb);
    CHECK(dtp == NULL);

    /* Big enough buffer */
    unsigned char buf[40];
    dtp = dtp_init(buf, sizeof(buf), from_net_cb, to_net_cb);
    CHECK(dtp != NULL);

    /* Prepare network buffer */
    unsigned char bbb[100];
    net_buf_t* nb = net_buf_init(bbb, sizeof(bbb));
    CHECK(nb != NULL);

    /* Set initial data to net buffer */
    const int packet_len = 10;
    unsigned char s[] = "0123456789abcdefgh";
    int res = net_buf_add_tail(nb, s, packet_len);
    CHECK(res == 0);

    /* Build transport packet */
    res = dtp_to_net(dtp, nb);
    CHECK(res == DTP_OK);
    int data_len = net_buf_data_len(nb);
    CHECK(data_len == packet_len + 4);
    CHECK(to_net == 1);

    /* Put only a part transport packet */
    res = dtp_from_net(dtp, net_buf_data(nb), 6);
    CHECK(res == 0);
    CHECK(from_net == 0);

    /* Put the remaining part of transport packet */
    res = dtp_from_net(dtp, net_buf_data(nb) + 6, 8);
    CHECK(res == 0);
    CHECK(from_net == 1);
}
