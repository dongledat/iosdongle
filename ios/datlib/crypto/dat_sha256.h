#ifndef _DAT_SHA256_H_
#define _DAT_SHA256_H_

#if defined(__cplusplus)
extern "C" {
#endif

int sha256(unsigned char* input, unsigned input_length, unsigned char* output);

#if defined(__cplusplus)
}
#endif

#endif /* _DAT_SHA256_H_*/
