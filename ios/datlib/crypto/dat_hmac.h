#ifndef _DAT_HMAC_H_
#define _DAT_HMAC_H_

#if defined(__cplusplus)
extern "C" {
#endif

int hmac(unsigned char* input, unsigned input_length, unsigned char* key, unsigned key_length, unsigned char* output);

#if defined(__cplusplus)
}
#endif

#endif /* _DAT_HMAC_H_ */