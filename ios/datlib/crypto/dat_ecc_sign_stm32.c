static DAT_CRYPTO_RESULT _ecc_stm32_SHA256_HASH_DigestCompute(unsigned char* InputMessage, unsigned int InputMessageLength, unsigned char* MessageDigest, unsigned int* MessageDigestLength);

DAT_CRYPTO_RESULT dat_ecc_sign_message(dat_ecc_ctx_t* ecc, unsigned char* message, unsigned int msg_len, unsigned char* private_key, unsigned char* sign_r, unsigned char* sign_s)
{
	unsigned int ecc_status = ECC_ERR_BAD_OPERATION;
	unsigned int hash_status = HASH_ERR_BAD_OPERATION;

	/* Private Key Structure */
	ECCprivKey_stt *ecc_private_key = NULL;

	ECDSAsignature_stt *sign = NULL;
	ECDSAsignCtx_stt signCtx = { 0 };

	unsigned char digest[CRL_SHA256_SIZE] = { 0 };
	unsigned int digest_size = 0;
	signed int o_sign_size = 0;

	if ((NULL == ecc) || (NULL == message) || (NULL == private_key) || (NULL == sign_r) || (NULL == sign_s)) {
		return DAT_CRYPTO_FAILURE;
	}

	/* Init Private Key */
	ecc_status = ECCinitPrivKey(&ecc_private_key, &ecc->curve_state, &ecc->crypto_buffer);
	if (ECC_SUCCESS != ecc_status) {
		// TODO!!!
		return DAT_CRYPTO_FAILURE;
	}

	/* Set Private Key Value */
	ecc_status = ECCsetPrivKeyValue(ecc_private_key, private_key, DAT_ECC_KEY_LENGTH);
	if (ECC_SUCCESS != ecc_status) {
		// TODO!!!
		return DAT_CRYPTO_FAILURE;
	}

	/* Init signing object */
	ecc_status = ECDSAinitSign(&sign, &ecc->curve_state, &ecc->crypto_buffer);
	if (ECC_SUCCESS != ecc_status) {
		// TODO!!!
		return DAT_CRYPTO_FAILURE;
	}

	signCtx.pmEC = &(ecc->curve_state);
	signCtx.pmPrivKey = ecc_private_key;
	signCtx.pmRNG = &g_stm32_RNG_state;

	hash_status = _ecc_stm32_SHA256_HASH_DigestCompute(message, msg_len, digest, &digest_size);
	if (HASH_SUCCESS != hash_status) {
		//TODO!!
		return DAT_CRYPTO_FAILURE;
	}

	ecc_status = ECDSAsign(digest, digest_size, sign, &signCtx, &ecc->crypto_buffer);
	if (ECC_SUCCESS != ecc_status) {
		// TODO!!!
		return DAT_CRYPTO_FAILURE;
	}

	ecc_status = ECDSAgetSignature(sign, E_ECDSA_SIGNATURE_R_VALUE, sign_r, &o_sign_size);
	if (ECC_SUCCESS != ecc_status) {
		// TODO!!!
		return DAT_CRYPTO_FAILURE;
	}

	ecc_status = ECDSAgetSignature(sign, E_ECDSA_SIGNATURE_S_VALUE, sign_s, &o_sign_size);
	if (ECC_SUCCESS != ecc_status) {
		// TODO!!!
		return DAT_CRYPTO_FAILURE;
	}

	ecc_status = ECCfreePrivKey(&ecc_private_key, &ecc->crypto_buffer);
	if (ECC_SUCCESS != ecc_status) {
		// TODO!!!
		return DAT_CRYPTO_FAILURE;
	}

	return DAT_CRYPTO_SUCCESS;
}

DAT_CRYPTO_RESULT _ecc_stm32_SHA256_HASH_DigestCompute(unsigned char* InputMessage, unsigned int InputMessageLength,
	unsigned char* MessageDigest, unsigned int* MessageDigestLength)
{
	SHA256ctx_stt P_pSHA256ctx = { 0 };
	int error_status = HASH_SUCCESS;

	/* Set the size of the desired hash digest */
	P_pSHA256ctx.mTagSize = CRL_SHA256_SIZE;

	/* Set flag field to default value */
	P_pSHA256ctx.mFlags = E_HASH_DEFAULT;

	error_status = SHA256_Init(&P_pSHA256ctx);
	/* check for initialization errors */
	if (HASH_SUCCESS != error_status) {
		return DAT_CRYPTO_FAILURE;
	}

	/* Add data to be hashed */
	error_status = SHA256_Append(&P_pSHA256ctx,
		InputMessage,
		InputMessageLength);
	if (HASH_SUCCESS != error_status) {
		return DAT_CRYPTO_FAILURE;
	}

	/* retrieve */
	error_status = SHA256_Finish(&P_pSHA256ctx, MessageDigest, (int32_t*)MessageDigestLength);
	if (HASH_SUCCESS != error_status) {
		return DAT_CRYPTO_FAILURE;
	}

	return (error_status == HASH_SUCCESS) ? DAT_CRYPTO_SUCCESS : DAT_CRYPTO_FAILURE;
}

