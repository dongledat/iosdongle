#include "dat_aes.h"
#include <assert.h>
#include <string.h>

DAT_CRYPTO_RESULT dat_aes_encrypt_message(unsigned char* key, unsigned int key_len, unsigned char* message, unsigned int message_len, unsigned char* iv, unsigned char* output)
{
	unsigned int aes_status = AES_ERR_BAD_OPERATION;

	AESCBCctx_stt AESctx_st = { 0 };
	int output_len = 0;

	if (message_len < CRL_AES_BLOCK) {
		return DAT_CRYPTO_FAILURE;
	}

	/* Initialize context struct */
	AESctx_st.mFlags = E_SK_DEFAULT;
	AESctx_st.mIvSize = DAT_AES_IV_SIZE;
	AESctx_st.mKeySize = key_len;

	/* Perform encryption */
	aes_status = AES_CBC_Encrypt_Init(&AESctx_st, key, iv);
	if (AES_SUCCESS != aes_status) {
		return DAT_CRYPTO_FAILURE;
	}

	aes_status = AES_CBC_Encrypt_Append(&AESctx_st, message, message_len, output, (int32_t*)&output_len);
	if (AES_SUCCESS != aes_status) {
		return DAT_CRYPTO_FAILURE;
	}

	aes_status = AES_CBC_Encrypt_Finish(&AESctx_st, output, (int32_t*)output_len);
	if (AES_SUCCESS != aes_status) {
		return DAT_CRYPTO_FAILURE;
	}

	return DAT_CRYPTO_SUCCESS;
}

DAT_CRYPTO_RESULT dat_aes_decrypt_message(unsigned char* key, unsigned int key_len, unsigned char* message, unsigned int message_len, unsigned char* iv, unsigned char* output)
{
	unsigned int aes_status = AES_ERR_BAD_OPERATION;

	AESCBCctx_stt AESctx_st = { 0 };
	unsigned int output_len = 0;

	if (message_len < CRL_AES_BLOCK) {
		return DAT_CRYPTO_FAILURE;
	}

	/* Initialize context struct */
	AESctx_st.mFlags = E_SK_DEFAULT;
	AESctx_st.mIvSize = DAT_AES_IV_SIZE;
	AESctx_st.mKeySize = key_len;

	/* Perform decryption */
	aes_status = AES_CBC_Decrypt_Init(&AESctx_st, key, iv);
	if (AES_SUCCESS != aes_status) {
		return DAT_CRYPTO_FAILURE;
	}

	aes_status = AES_CBC_Decrypt_Append(&AESctx_st, message, message_len, output, (int32_t*)&output_len);
	if (AES_SUCCESS != aes_status) {
		return DAT_CRYPTO_FAILURE;
	}

	aes_status = AES_CBC_Decrypt_Finish(&AESctx_st, output, (int32_t*)output_len);
	if (AES_SUCCESS != aes_status) {
		return DAT_CRYPTO_FAILURE;
	}

	return DAT_CRYPTO_SUCCESS;
}
