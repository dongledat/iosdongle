#ifndef DAT_BASE64_H
#define DAT_BASE64_H

#include "mbedtls/base64.h"

#define dat_base64_encode(dst,dlen,olen,src,slen) \
    mbedtls_base64_encode(dst,dlen,olen,src,slen)

#define dat_base64_decode(dst,dlen,olen,src,slen) \
    mbedtls_base64_decode(dst,dlen,olen,src,slen)

#endif // BASE64_H
