#ifndef _DAT_CHISQR_H_
#define _DAT_CHISQR_H_

#if defined(__cplusplus)
extern "C" {
#endif


	double chi2UniformDistance(double *ds, int dslen);
double chi2Probability(int dof, double distance);
int chiIsUniform(double *dset, int dslen, double significance);

#if defined(__cplusplus)
}
#endif


#endif /* _DAT_CHISQR_H_ */
