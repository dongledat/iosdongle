#ifndef _DAT_HKDF_STM32_H_
#define _DAT_HKDF_STM32_H_

#include "dat_crypto_common.h"

#define HKDF_OUTPUT_SIZE (32)

DAT_CRYPTO_RESULT dat_ecc_hkdf_sign(unsigned char* key, unsigned int key_len, unsigned char* salt, unsigned char* output);

#endif /* _DAT_HKDF_STM32_H_ */
