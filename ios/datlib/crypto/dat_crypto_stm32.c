#include "dat_crypto_stm32.h"

/**** Definitions ****************************************************/
/*	DRBG type
    Allows selecting automatically the hardware or software random number generator*/
//#ifndef DAT_CRYPTO_STM32_USE_HW_RNG
//#define DAT_STM32_DRBG_TYPE (C_DRBG_AES128)
//#else
#define DAT_STM32_DRBG_TYPE (C_HW_RNG)
//#endif

/* Nonce */
//static unsigned char g_nonce[] = { 0, 1, 2, 3 };

/**** Private function prototypes ************************************/

/**** Globals ********************************************************/

/**** Exported functions *********************************************/

DAT_CRYPTO_RESULT dat_crypto_stm32_init()
{
    /* DeInitialize STM32 Cryptographic Library */
    Crypto_DeInit();

    return DAT_CRYPTO_SUCCESS;
}

DAT_CRYPTO_RESULT dat_crypto_stm32_deinit()
{
    /* DeInitialize STM32 Cryptographic Library */
    Crypto_DeInit();

    return DAT_CRYPTO_SUCCESS;
}


/**
* @brief  initialise the RNG in order to be used during the ECC signature.
*           need to indicate if it is an hardware or software RNG.
* @param  P_pRNGstate: pointer to input RNG structure type.
* @retval error status: can be ECC_SUCCESS if success or RNG_ERR_UNINIT_STATE
*                        if error occured.
*/
DAT_CRYPTO_RESULT dat_crypto_stm32_rng_init(RNGstate_stt* rng)
{
    unsigned int rng_status = RNG_ERR_BAD_OPERATION;

    /* Structure for the parmeters of initialization */
    // RNGinitInput_stt RNGinit_st = { 0 };

    /* Initialize the RNGinit structure */
    // RNGinit_st.pmEntropyData = NULL;
    // RNGinit_st.mEntropyDataSize = 0;
    // RNGinit_st.pmNonce = g_nonce;
    // RNGinit_st.mNonceSize = sizeof(g_nonce);

    /* There is no personalization data in this case */
    // RNGinit_st.mPersDataSize = 0;
    // RNGinit_st.pmPersData = NULL;

    /* Init the random engine */
    rng_status = RNGinit(NULL, DAT_STM32_DRBG_TYPE, rng);
    if (RNG_SUCCESS != rng_status) 	{
        return DAT_CRYPTO_FAILURE;
    }
        
    return DAT_CRYPTO_SUCCESS;
}

DAT_CRYPTO_RESULT dat_crypto_stm32_rng_deinit(RNGstate_stt* rng)
{
    /* Best effort */
    RNGfree(rng);

    return DAT_CRYPTO_SUCCESS;
}
