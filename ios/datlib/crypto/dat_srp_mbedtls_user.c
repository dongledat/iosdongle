#ifdef DAT_CRYPTO_MBEDTLS

#include <stdlib.h>
#include <string.h>
#include "dat_srp_mbedtls_user.h"

extern mbedtls_entropy_context entropy_ctx;
extern mbedtls_ctr_drbg_context ctr_drbg_ctx;
extern mbedtls_mpi * RR;

SRPUser *srp_user_new(SRP_HashAlgorithm algo, SRP_NGType ngtype, byte * pass, int pass_len, byte * n_hex, byte * g_hex)
{
	SRPUser* user = NULL;
	NGConstant* ng = NULL;
	byte* pass_dup = NULL;

	if (NULL == pass ||
		0 == pass_len) {
		return NULL;
	}

	if (ngtype == SRP_NG_CUSTOM &&
		((n_hex == NULL) || (g_hex == NULL))) {
		return NULL;
	}

	init_random(); /* Only happens once */

	user = (SRPUser *)malloc(sizeof(SRPUser));
	if (!user) {
		goto lblError;
	}

	ng = new_ng(ngtype, n_hex, g_hex);
	if (!ng) {
		goto lblError;
	}

	pass_dup = malloc(pass_len);
	if (!pass_dup) {
		goto lblError;
	}
	memcpy(pass_dup, pass, pass_len);

	user->hash_alg = algo;
	user->ng = ng;
	user->authenticated = 0;
	user->pass = pass_dup;
	user->pass_len = pass_len;

	return user;

lblError:
	if (ng) {
		delete_ng(ng);
	}

	return NULL;
}

//todo: swoop in and check mbedtls res, and input args

SRP_RESULT srp_user_set_salt(SRPUser * user, byte * salt, int salt_len)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;

	user->s = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (!user->s) {
		goto lblCleanup;
	}

	mbedtls_mpi_init(user->s);
	mbedtls_res = mbedtls_mpi_read_binary(user->s, salt, salt_len);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}


SRP_RESULT srp_user_calculate_x(SRPUser* user)
{
	SRP_RESULT res = SRP_ERROR;

	user->x = calculate_x(user->hash_alg, user->s, user->pass, user->pass_len);
	if (NULL == user->x) {
		goto lblCleanup;
	}

	/* s, password - no longer needed */
	free(user->pass);
	user->pass = NULL;

	mbedtls_mpi_free(user->s);
	free(user->s);
	user->s = NULL;

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}


SRP_RESULT srp_user_calculate_A(SRPUser * user)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;

	user->a = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (!user->a) {
		goto lblCleanup;
	}
	mbedtls_mpi_init(user->a);

	mbedtls_res = mbedtls_mpi_fill_random(user->a, 256, &mbedtls_ctr_drbg_random, &ctr_drbg_ctx);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	user->A = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (!user->A) {
		goto lblCleanup;
	}
	mbedtls_mpi_init(user->A);

	mbedtls_res = mbedtls_mpi_exp_mod(user->A, user->ng->g, user->a, user->ng->N, RR);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_user_get_A(SRPUser * user, byte ** A, int * A_len)
{
	SRP_RESULT res = SRP_ERROR;
	byte* bytes_A = NULL;
	int len_A = 0;
	int mbedtls_res = 1;

	len_A = mbedtls_mpi_size(user->A);
	bytes_A = malloc(len_A);
	if (NULL == bytes_A) {
		goto lblCleanup;
	}

	mbedtls_res = mbedtls_mpi_write_binary(user->A, (unsigned char*)bytes_A, len_A);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	*A = bytes_A;
	*A_len = len_A;

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_user_set_B_u(SRPUser * user, byte * B, int B_len, byte * u, int u_len)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;

	user->B = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (NULL == user->B) {
		goto lblCleanup;
	}

	mbedtls_mpi_init(user->B);
	mbedtls_res = mbedtls_mpi_read_binary(user->B, B, B_len);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* SRP-6a safety check - B != 0 */
	if (mbedtls_mpi_cmp_int(user->B, 0) == 0) {
		goto lblCleanup;
	}

	user->u = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (NULL == user->u) {
		goto lblCleanup;
	}

	mbedtls_mpi_init(user->u);
	mbedtls_res = mbedtls_mpi_read_binary(user->u, u, u_len);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* SRP-6a safety check - u != 0 */
	if (mbedtls_mpi_cmp_int(user->u, 0) == 0) {
		goto lblCleanup;
	}

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_user_calculate_secret(SRPUser * user)
{
	SRP_RESULT res = SRP_ERROR;
	BIGNUM* S = NULL;
	BIGNUM* temp = NULL;
	int mbedtls_res = 1;

	/* S = (B - (g^x)) ^ (a + ux) */
	user->S = (BIGNUM *)malloc(sizeof(BIGNUM));
	mbedtls_mpi_init(user->S);
	temp = (BIGNUM *)malloc(sizeof(BIGNUM));
	mbedtls_mpi_init(temp);

	/* S = g^x */
	mbedtls_res = mbedtls_mpi_exp_mod(user->S, user->ng->g, user->x, user->ng->N, RR);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* tmp = u*x */
	mbedtls_res = mbedtls_mpi_mul_mpi(temp, user->u, user->x);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

		/* x not longer required */
		mbedtls_mpi_free(user->x);
		free(user->x);
		user->x = NULL;

		/* u no longer required */
		mbedtls_mpi_free(user->u);
		free(user->u);
		user->u = NULL;
	
	/* S = B - S  .... [ S = B - g^x ] */
	mbedtls_res = mbedtls_mpi_sub_mpi(user->S, user->B, user->S);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* tmp = a + tmp .... [ tmp = a + u*x ] */
	mbedtls_res = mbedtls_mpi_add_mpi(temp, user->a, temp);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

		/* a no longer required */
		mbedtls_mpi_free(user->a);
		free(user->a);
		user->a = NULL;

	/* S = S ^ tmp .... [ S = (B - (g^x)) ^ (a + ux) ] */
	mbedtls_res = mbedtls_mpi_exp_mod(user->S, user->S, temp, user->ng->N, RR);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	res = SRP_SUCCESS;

lblCleanup:
	if (temp) {
		mbedtls_mpi_free(temp);
		free(temp);
	}

	return res;
}

SRP_RESULT srp_user_calculate_key(SRPUser * user)
{
	SRP_RESULT res = SRP_ERROR;

	user->K = malloc(hash_length(user->hash_alg));
	if (NULL == user->K) {
		goto lblCleanup;
	}
	hash_num(user->hash_alg, user->S, user->K);

	/* S no longer needed */
	mbedtls_mpi_free(user->S);
	free(user->S);
	user->S = NULL;

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_user_calculate_M(SRPUser * user)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;

	//todo....

	user->M = malloc(hash_length(user->hash_alg));
	if (NULL == user->M) {
		goto lblCleanup;
	}

	calculate_M(user->hash_alg, user->ng, user->M, user->A, user->B, user->K);
		/* B not longer required */
		mbedtls_mpi_free(user->B);
		free(user->B);
		user->B = NULL;

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_user_get_M(SRPUser * user, byte ** M, int * M_len)
{
	/* Do not transfer ownership */
	*M = user->M;
	*M_len = hash_length(user->hash_alg);

	return SRP_SUCCESS;
}

SRP_RESULT srp_user_calculate_HAMK(SRPUser * user)
{
	SRP_RESULT res = SRP_ERROR;

	user->HAMK = malloc(hash_length(user->hash_alg));
	if (NULL == user->HAMK) {
		goto lblCleanup;
	}

	calculate_H_AMK(user->hash_alg, user->HAMK, user->A, user->M, user->K);
		/* A, M not longer required */
		mbedtls_mpi_free(user->A);
		free(user->A);
		user->A = NULL;

		free(user->M);
		user->M = NULL;

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_user_verify_HAMK(SRPUser * user, byte * HAMK, int HAMK_len)
{
	SRP_RESULT res = SRP_ERROR;

	if (NULL == user || NULL == HAMK || HAMK_len != hash_length(user->hash_alg)) {
		return SRP_ERROR;
	}

	if (memcmp(user->HAMK, HAMK, HAMK_len) != 0) {
		return SRP_ERROR;
	}

	/* HAMK no longer required */
	free(user->HAMK);
	user->HAMK = NULL;

	user->authenticated = 1;
	return SRP_SUCCESS;
}

SRP_RESULT srp_user_get_K(SRPUser * user, byte ** K, int * K_len)
{
	SRP_RESULT res = SRP_ERROR;

	if (NULL == user || NULL == K || NULL == K_len) {
		return SRP_ERROR;
	}

	/*	Transfer ownership of K.
		K no longer needed - caller has it, and should free it himself */
	*K = user->K;
	*K_len = hash_length(user->hash_alg);
	user->K = NULL;

	return SRP_SUCCESS;
}

SRP_RESULT srp_user_delete(SRPUser * user)
{
	if (NULL == user) {
		return SRP_ERROR;
	}

	if (user->s) {
		mbedtls_mpi_free(user->s);
		free(user->s);
		user->s = NULL;
	}

	if (user->x) {
		mbedtls_mpi_free(user->x);
		free(user->x);
		user->x = NULL;
	}

	if (user->a) {
		mbedtls_mpi_free(user->a);
		free(user->a);
		user->a = NULL;
	}
	
	if (user->A) {
		mbedtls_mpi_free(user->A);
		free(user->A);
		user->A = NULL;
	}

	if (user->B) {
		mbedtls_mpi_free(user->B);
		free(user->B);
		user->B = NULL;
	}

	if (user->u) {
		mbedtls_mpi_free(user->u);
		free(user->u);
		user->u = NULL;
	}

	if (user->S) {
		mbedtls_mpi_free(user->S);
		free(user->S);
		user->S = NULL;
	}


	if (user->M) {
		free(user->M);
		user->M = NULL;
	}

	if (user->HAMK) {
		free(user->HAMK);
		user->HAMK = NULL;
	}

	if (user->K) {
		free(user->K);
		user->K = NULL;
	}

	if (user->pass) {
		free(user->pass);
		user->pass = NULL;
	}

	if (user->ng) {
		delete_ng(user->ng);
		user->ng = NULL;
	}

	free(user);

	return SRP_SUCCESS;
}

#endif /* DAT_CRYPTO_MBEDTLS */