#ifndef DAT_ECC_MBEDTLS_H
#define DAT_ECC_MBEDTLS_H

/*! \struct dat_ecc_ctx
*  \brief ECC key agreement context.
*
*  The structure should be initialized with a call to dat_ecc_init before using the DAT ECC API.
*/

typedef struct
{
	mbedtls_ctr_drbg_context ctr_drbg;
	mbedtls_entropy_context entropy;

} dat_ecc_ctx_t;

#endif
