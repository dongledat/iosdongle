#include "dat_hkdf_stm32.h"

DAT_CRYPTO_RESULT dat_ecc_hkdf_sign(byte* key, unsigned int key_len, uint8_t* salt, uint8_t* output)
{
	uint8_t digest[CRL_SHA256_SIZE];
        unsigned int hash_status = HASH_ERR_BAD_OPERATION;

	if ((NULL == key) || (NULL == salt) || (NULL == output) || (0 == key_len)) {
		return DAT_CRYPTO_FAILURE;
	}

	dat_crypto_stm32_init();

	/********* EXTRACT HKDF **********/

	HMAC_SHA256ctx_stt context_st;

	// Initialize Context Flag with default value
	context_st.mFlags = E_HASH_DEFAULT;
	// Set the required digest size to CRL_SHA256_SIZE
	context_st.mTagSize = CRL_SHA256_SIZE;
	// Set the pointer to the Key ==> salt
	context_st.pmKey = salt;
	// Set the key size
	context_st.mKeySize = 32;

	// call init function
	hash_status = HMAC_SHA256_Init(&context_st);
	if (hash_status != HASH_SUCCESS) return DAT_CRYPTO_FAILURE;

	hash_status = HMAC_SHA256_Append(&context_st, key, key_len);
	if (hash_status != HASH_SUCCESS) return DAT_CRYPTO_FAILURE;

	//Generate the message digest
	int32_t outSize = 0;
	hash_status = HMAC_SHA256_Finish(&context_st, digest, &outSize);
	if (hash_status != HASH_SUCCESS) return DAT_CRYPTO_FAILURE;

	/********* EXPAND HKDF **********/
	int remainingBytes = HKDF_OUTPUT_SIZE;

	// Set Key
	context_st.pmKey = digest;
	// Set the key size
	context_st.mKeySize = outSize;

	uint8_t iteration[1];

	iteration[0] = 0;

	uint8_t expand_digest[CRL_SHA256_SIZE];

	while (remainingBytes > 0)
	{
		iteration[0]++;

		// call init function
		hash_status = HMAC_SHA256_Init(&context_st);
		if (hash_status != HASH_SUCCESS) return DAT_CRYPTO_FAILURE;

		if (iteration[0] > 1)
		{
			hash_status = HMAC_SHA256_Append(&context_st, expand_digest, sizeof(expand_digest));
			if (hash_status != HASH_SUCCESS) return DAT_CRYPTO_FAILURE;
		}

		hash_status = HMAC_SHA256_Append(&context_st, iteration, sizeof(iteration));
		if (hash_status != HASH_SUCCESS) return DAT_CRYPTO_FAILURE;

		hash_status = HMAC_SHA256_Finish(&context_st, expand_digest, &outSize);
		if (hash_status != HASH_SUCCESS) return DAT_CRYPTO_FAILURE;

		// Copy digest bytes to output buffer
		if (outSize >= remainingBytes)
		{
			outSize = remainingBytes;
			remainingBytes = 0;
		}
		else
		{
			remainingBytes -= outSize;
		}

		uint32_t start = HKDF_OUTPUT_SIZE - remainingBytes - outSize;


		for (uint32_t i = 0; i < outSize; i++)
		{
			output[start + i] = expand_digest[i];
		}
	}

	return DAT_CRYPTO_SUCCESS;
}
