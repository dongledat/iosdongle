#ifdef DAT_CRYPTO_MBEDTLS

#include "dat_aes.h"
#include <assert.h>

DAT_CRYPTO_RESULT dat_aes_encrypt_message(unsigned char* key, unsigned int key_len, unsigned char* message, unsigned int message_len, unsigned char * iv, unsigned char* output)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
	unsigned int mbedtls_status = MBEDTLS_FAILURE;;
	mbedtls_aes_context aes_ctx = { 0 };

	mbedtls_aes_init(&aes_ctx);

	mbedtls_status = mbedtls_aes_setkey_enc(&aes_ctx, key, key_len * 8);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup;
	}

	mbedtls_status = mbedtls_aes_crypt_cbc(&aes_ctx, MBEDTLS_AES_ENCRYPT, message_len, iv, message, output);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup;
	}

	/* Success */
	status = DAT_CRYPTO_SUCCESS;

lblCleanup:

	mbedtls_aes_free(&aes_ctx);
	return status;
}

DAT_CRYPTO_RESULT dat_aes_decrypt_message(unsigned char* key, unsigned int key_len, unsigned char* message, unsigned int message_len, unsigned char * iv, unsigned char* output)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
	unsigned int mbedtls_status = MBEDTLS_FAILURE;
	mbedtls_aes_context aes_ctx = { 0 };

	mbedtls_aes_init(&aes_ctx);

	mbedtls_status = mbedtls_aes_setkey_dec(&aes_ctx, key, key_len * 8);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup;
	}

	mbedtls_status = mbedtls_aes_crypt_cbc(&aes_ctx, MBEDTLS_AES_DECRYPT, message_len, iv, message, output);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup;
	}

	/* Success */
	status = DAT_CRYPTO_SUCCESS;

lblCleanup:

	mbedtls_aes_free(&aes_ctx);
	return status;
}


#endif