#ifndef DAT_ECC_STM32_H
#define DAT_ECC_STM32_H

/*! DAT_ECC_STM32_MEMBUF_BUFFER_SIZE
* \brief The size, in bytes, of the internal buffer used by STM32's ECC API.
*
*	It is not documented anywhere how big this should be, but 2048 seems to work.
*/
#define DAT_ECC_STM32_MEMBUF_BUFFER_SIZE (2048) //TODO: what the hell is this number?

/*! DAT_ECC_STM32_CONTEXT_BUFFER_ALLOC_HEAP
* \brief If defined, allocate the internal buffer from dynamic memory.
*/
//#define DAT_ECC_STM32_CONTEXT_BUFFER_ALLOC_HEAP

/*! DAT_ECC_STM32_USE_SINGLETON_CURVE
* \brief If defined, use the same curve for all ECC contexts. Default is to use a separate curve for each context.
*/
//#define DAT_ECC_STM32_USE_SINGLETON_CURVE

/*! DAT_ECC_STM32_CONTEXT_BUFFER_ALLOC_STACK
* \brief If defined, allocate the internal buffer as part of the struct (will take stack memory, if the struct is allocated as a local variable).
*/
#define DAT_ECC_STM32_CONTEXT_BUFFER_ALLOC_STACK

#if !defined(DAT_ECC_STM32_CONTEXT_BUFFER_ALLOC_STACK) && !defined(DAT_ECC_STM32_CONTEXT_BUFFER_ALLOC_HEAP)
#error Must define a way for storing the preallocated buffer for the ECC API
#endif

#if defined(DAT_ECC_STM32_CONTEXT_BUFFER_ALLOC_STACK) && defined(DAT_ECC_STM32_CONTEXT_BUFFER_ALLOC_HEAP)
#error Must define one way for storing the preallocated buffer for the ECC API
#endif

typedef struct
{
	/* Structure that will contain the curve's parameter */
	EC_stt curve_state;

	/* structure to store the preallocated buffer for crypto computation*/
	membuf_stt crypto_buffer;

	unsigned char mem_buffer[DAT_ECC_STM32_MEMBUF_BUFFER_SIZE];

	int curve_init;

	RNGstate_stt rng;

} dat_ecc_ctx_t;

#endif
