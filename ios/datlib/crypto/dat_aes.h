#ifndef _DAT_AES_H_
#define _DAT_AES_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "crypto/dat_crypto_common.h"

#define DAT_AES_IV_SIZE		(16)
#define DAT_AES_KEY_SIZE	(32)

DAT_CRYPTO_RESULT dat_aes_encrypt_message(unsigned char* key, unsigned int key_len, unsigned char* message, unsigned int message_len, unsigned char* iv, unsigned char* output);
DAT_CRYPTO_RESULT dat_aes_decrypt_message(unsigned char* key, unsigned int key_len, unsigned char* message, unsigned int message_len, unsigned char* iv, unsigned char* output);


#if defined(__cplusplus)
}
#endif
#endif /* _DAT_AES_H_ */
