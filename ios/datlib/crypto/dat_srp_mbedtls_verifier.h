#ifndef _SRP_VERIFIER_H_
#define _SRP_VERIFIER_H_

#if defined(__cplusplus)
extern "C" {
#endif

#include "dat_srp_mbedtls_common.h"

	SRPVerifier* srp_verifier_new(
		SRP_HashAlgorithm algo,
		SRP_NGType ngtype,
		byte * pass,
		int pass_len,
		byte * n_hex,
		byte * g_hex);

	SRP_RESULT srp_verifier_calculate_s_v(
		SRPVerifier * ver);

	SRP_RESULT srp_verifier_get_salt(
		SRPVerifier * ver,
		byte ** s,
		int * s_len);

	SRP_RESULT srp_verifier_set_A(
		SRPVerifier* ver,
		byte * A,
		int A_len);

	SRP_RESULT srp_verifier_calculate_B_u(
		SRPVerifier* ver);

	SRP_RESULT srp_verifier_get_B_u(
		SRPVerifier* ver,
		byte ** B,
		int * B_len,
		byte ** u,
		int * u_len);

	SRP_RESULT srp_verifier_calculate_secret(
		SRPVerifier* ver);

	SRP_RESULT srp_verifier_calculate_key(
		SRPVerifier* ver);

	SRP_RESULT srp_verifier_calculate_M(
		SRPVerifier* ver);

	SRP_RESULT srp_verifier_verify_M(
		SRPVerifier* ver,
		byte * M,
		int M_len);

	SRP_RESULT srp_verifier_calculate_HAMK(
		SRPVerifier* ver);

	SRP_RESULT srp_verifier_get_HAMK(
		SRPVerifier* ver,
		byte ** HAMK,
		int * HAMK_len);

	SRP_RESULT srp_verifier_get_K(
		SRPVerifier * ver,
		byte ** K,
		int * K_len);

	SRP_RESULT srp_verifier_delete(
		SRPVerifier* ver);


#if defined(__cplusplus)
}
#endif

#endif /* Include Guard */