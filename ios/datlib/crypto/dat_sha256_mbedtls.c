#ifdef DAT_CRYPTO_MBEDTLS

#include "mbedtls/sha256.h"
#include "dat_sha256.h"

int sha256(unsigned char* input, unsigned input_length, unsigned char* output)
{
	mbedtls_sha256_context ctx;

	mbedtls_sha256_init(&ctx);
	mbedtls_sha256_starts(&ctx, 0);
	mbedtls_sha256_update(&ctx, input, input_length);
	mbedtls_sha256_finish(&ctx, output);
	mbedtls_sha256_free(&ctx);

	return 0;
}

#endif /* DAT_CRYPTO_MBEDTLS */