#include <memory.h>
#include "dat_btls.h"
#include "dat_ecc.h"
#include "dat_hmac.h"
#include "dat_srp_mbedtls_common.h"

DAT_CRYPTO_RESULT build_btls_packet_handshake(btls_ctx* ctx, enum HandshakeType handshake_type, unsigned char* handshake_data, int handshake_length);

DAT_CRYPTO_RESULT init_btls(btls_ctx* ctx)
{
	ctx->buffer_size = 0;
	ctx->version.major = 0;
	ctx->version.minor = 1;
	ctx->is_verified = 0;
	memset(ctx->secret, 0, DAT_AES_KEY_SIZE);
	memset(ctx->iv_state.encryption_iv, 0, DAT_AES_IV_SIZE);
	memset(ctx->iv_state.decryption_iv, 0, DAT_AES_IV_SIZE);

	// Init entropy
	mbedtls_entropy_init(&(ctx->entropy));

	DAT_CRYPTO_RESULT mbedtls_status = mbedtls_ctr_drbg_seed(&ctx->ctr_drbg, mbedtls_entropy_func, &ctx->entropy, (const unsigned char *) "RANDOM_GEN", 10);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		return DAT_CRYPTO_FAILURE;
	}

	return DAT_CRYPTO_SUCCESS;

}

void release_btls(btls_ctx* ctx)
{	
	mbedtls_ctr_drbg_free(&ctx->ctr_drbg);
	mbedtls_entropy_free(&ctx->entropy);
}

DAT_CRYPTO_RESULT load_btls_keys(btls_ctx* ctx, ecc_keys keys)
{
	return DAT_CRYPTO_FAILURE;
}

DAT_CRYPTO_RESULT generate_btls_keys(btls_ctx* ctx)
{
	// TODO - Do we need to move ecc context to init
	// TODO - if yes we can remove the ctr_drbg from btls_ctx
	dat_ecc_ctx_t ecc;
	dat_ecc_init(&ecc);

	int status = dat_ecc_generate_keys(&ecc, ctx->keys.private_key, ctx->keys.public_key_x, ctx->keys.public_key_y);

	dat_ecc_deinit(&ecc);

	return status;
}

DAT_CRYPTO_RESULT compute_btls_secret(btls_ctx* ctx)
{
	unsigned char secret_point[DAT_ECC_KEY_LENGTH];

	dat_ecc_ctx_t ecc;
	dat_ecc_init(&ecc);
	

	int status = dat_ecc_calculate_secret_point(&ecc, ctx->keys.private_key, 
		ctx->remote_keys.public_key_x, ctx->remote_keys.public_key_y, secret_point);

	// TODO add salt and hkdf
	memset(ctx->secret, 0x3f, DAT_AES_KEY_SIZE);
	memcpy(ctx->secret, secret_point, DAT_ECC_KEY_LENGTH);

	dat_ecc_deinit(&ecc);

	return status;
}

DAT_CRYPTO_RESULT decode_btls_handshake_data(btls_ctx* ctx, unsigned char* handshake_fragment, int length)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_SUCCESS;

	enum HandshakeType t_handshake = handshake_fragment[0];
	
	int handshake_length = handshake_fragment[1] * 0xff0000 + handshake_fragment[2] * 0x00ff00 + handshake_fragment[3] * 0x0000ff;

	unsigned char* data_fragment = handshake_fragment + 4;
	switch (t_handshake)
	{
	case server_hello:
		memcpy(ctx->shared_salt, data_fragment + 20, 12);
	case client_hello:
		{
			//UINT32 TIME - NOT ENABLED
			//memcpy(ctx->buffer, data_fragment, 4);

			// Get random data for iv decryption - only 16 bytes
			memcpy(ctx->iv_state.decryption_iv, data_fragment + 4, 16);
			//ctx->buffer_size = 36;

			// Get Type
			ctx->encryption_type = data_fragment[33];
		}
		break;
	case server_key_exchange: 
	case client_key_exchange: 
		memcpy(ctx->remote_keys.public_key_x, data_fragment, DAT_ECC_KEY_LENGTH);
		memcpy(ctx->remote_keys.public_key_y, data_fragment + DAT_ECC_KEY_LENGTH, DAT_ECC_KEY_LENGTH);

		// Compute secret
		status = compute_btls_secret(ctx);
		break;
	default: break;
	}


	return status;
}

DAT_CRYPTO_RESULT decode_btls_application_data(btls_ctx* ctx, unsigned char* data_fragment, int length)
{
	DAT_CRYPTO_RESULT status;
	
	// CAN we do input and output the same?
	status = dat_aes_decrypt_message(ctx->secret, DAT_ECC_KEY_LENGTH, 
	                                  data_fragment, length, ctx->iv_state.decryption_iv, data_fragment);

	int padding_size = data_fragment[length - 1];
	uint8_t* mac_hash = data_fragment + length - padding_size - 32 - 1;
			
	uint8_t computedhash[32];
			
	int datalength = length - padding_size - 32 - 1;

	hmac(data_fragment, datalength, ctx->secret, 32, computedhash);

	// HASHES are valid
	if (memcmp(computedhash, mac_hash, 32) == 0)
	{
		ctx->buffer_size = datalength;
		memcpy(ctx->buffer, data_fragment, datalength);
	}
	else
	{
		status = DAT_CRYPTO_FAILURE;
	}

	return status;
}

DAT_CRYPTO_RESULT decode_btls_packet(btls_ctx* ctx, unsigned char* fragment, int fragment_size)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_SUCCESS;
	struct SSLCiphertext data;

	data.type = fragment[0];
	data.version.major = fragment[1];
	data.version.minor = fragment[2];
	data.length = fragment[4] + fragment[3] * 0xFF;

	unsigned char* fragment_data = malloc(data.length);
	memcpy(fragment_data, fragment + 5, data.length);

	switch (data.type)
	{
		case change_cipher_spec: break;
		case alert: break;
		case handshake: 
			status = decode_btls_handshake_data(ctx, fragment_data, data.length);
			break;
		case application_data:
		{
			status = decode_btls_application_data(ctx, fragment_data, data.length);
			if (status == DAT_CRYPTO_SUCCESS)
			{
				ctx->state = application;
			}
			break;
		}
		default: 
			status = DAT_CRYPTO_FAILURE;
		break;
	}

	free(fragment_data);
	if (status == DAT_CRYPTO_FAILURE)
	{
		ctx->state = error;
	}
	return status;
}


DAT_CRYPTO_RESULT build_btls_packet_handshake_hello(btls_ctx* ctx, int is_client, enum EncryptionType type)
{
	unsigned char handshake_data[33];

	// No support for time yet
	memset(handshake_data, 0, 4);

	// Generate encryption iv
	mbedtls_ctr_drbg_random(&ctx->ctr_drbg, handshake_data + 4, 28);

	// Copy only 16 bytes to iv
	memcpy(ctx->iv_state.encryption_iv, handshake_data + 4, 16);

	handshake_data[33] = type;
	if (is_client != 0)
	{
		build_btls_packet_handshake(ctx, client_hello, handshake_data, 33);
	}
	else
	{
		memcpy(ctx->shared_salt, handshake_data + 20, 12);
		build_btls_packet_handshake(ctx, server_hello, handshake_data, 33);
	}
	
	
	return DAT_CRYPTO_SUCCESS;
}

DAT_CRYPTO_RESULT build_btls_packet_handshake(btls_ctx* ctx, enum HandshakeType handshake_type, unsigned char* handshake_data, int handshake_length)
{

	int total_size = 0;
	
	total_size = handshake_length + 9;

	ctx->buffer[0] = handshake;
	ctx->buffer[1] = ctx->version.major;
	ctx->buffer[1] = ctx->version.minor;
	ctx->buffer[3] = total_size & 0xff00;
	ctx->buffer[4] = total_size & 0x00ff;

	ctx->buffer[5] = handshake_type;
	// uint24
	ctx->buffer[6] = handshake_length & 0xff0000;
	ctx->buffer[7] = handshake_length & 0x00ff00;
	ctx->buffer[8] = handshake_length & 0x0000ff;

	memcpy(ctx->buffer + 9, handshake_data, handshake_length);
	ctx->buffer_size = handshake_length + 9;

	return DAT_CRYPTO_SUCCESS;
}
DAT_CRYPTO_RESULT build_btls_packet_application(btls_ctx* ctx, unsigned char* content, int content_size)
{
	// Calculate size needed
	int padding_size = 16 - ((content_size + DAT_AES_KEY_SIZE + 1) % 16);
	int total_size = content_size + padding_size + DAT_AES_KEY_SIZE + 1;

	uint8_t* plaintext_message = malloc(total_size);

	memcpy(plaintext_message, content, content_size);
	
	// HASH HMAC
	hmac(content, content_size, ctx->secret, 32, plaintext_message + content_size);
	
	// Includes padding_length at end
	for (int i = 0; i <= padding_size; i++)
	{
		plaintext_message[content_size + DAT_AES_KEY_SIZE + i] = i;
	}
	
	ctx->buffer[0] = application_data;
	ctx->buffer[1] = ctx->version.major;
	ctx->buffer[1] = ctx->version.minor;
	ctx->buffer[3] = total_size & 0xff00;
	ctx->buffer[4] = total_size & 0x00ff;

	DAT_CRYPTO_RESULT status = dat_aes_encrypt_message(ctx->secret, DAT_AES_KEY_SIZE, plaintext_message,
		total_size, ctx->iv_state.encryption_iv, 5 + ctx->buffer);
	
	ctx->buffer_size = 5 + total_size;

	free(plaintext_message);
	
	return status;
}

DAT_CRYPTO_RESULT build_btls_packet_handshake_keyexchange(btls_ctx* ctx, int is_client)
{
	int total_size = DAT_ECC_KEY_LENGTH * 2;
	unsigned char handshake_data[DAT_ECC_KEY_LENGTH * 2];

	memcpy(handshake_data, ctx->keys.public_key_x, DAT_ECC_KEY_LENGTH);
	memcpy(handshake_data + DAT_ECC_KEY_LENGTH, ctx->keys.public_key_y, DAT_ECC_KEY_LENGTH);

	if (is_client != 0)
	{
		build_btls_packet_handshake(ctx, client_key_exchange, handshake_data, total_size);
	}
	else
	{
		build_btls_packet_handshake(ctx, server_key_exchange, handshake_data, total_size);
	}


	return DAT_CRYPTO_SUCCESS;
}