#include "ecc.h"
#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/random.h>
#include <wolfssl/wolfcrypt/ecc.h>
#include <wolfssl/wolfcrypt/hmac.h>
#include <wolfssl/wolfcrypt/aes.h>

RNG rng;

const char* none = "00000000000000000000000000000000000000000000000000000000";

int32_t dat_ecc_init_crypto()
{
	return wc_InitRng(&rng); // initialize rng
}

int32_t dat_ecc_deinit_crypto()
{
	return wc_FreeRng(&rng);
}

int32_t dat_ecc_generate_keys(uint8_t * private_key, uint8_t * public_key_x, uint8_t * public_key_y)
{
	ecc_key ecc_key;
	int status;
	int private_size;

	status = wc_ecc_init(&ecc_key); // initialize key
	if (status != MP_OKAY) return status;

	wc_ecc_make_key_ex(&rng, KEYSIZE, &ecc_key, ECC_SECP224R1);

	mp_to_unsigned_bin(ecc_key.pubkey.x, public_key_x);
	mp_to_unsigned_bin(ecc_key.pubkey.y, public_key_y);

	status = wc_ecc_export_private_only(&ecc_key, private_key, &private_size);
	wc_ecc_free(&ecc_key);

	return status;
}

//TODO: If required, move to aes/dat_aes_wolfssl.cpp
//
//int32_t dat_ecc_decrypt_message(uint8_t* secret_key, char* message, uint8_t* input, uint32_t input_length)
//{
//	// encrypt
//	Aes enc;
//	int status;
//
//	status = wc_AesSetKey(&enc, secret_key, AES_KEY_LENGTH, iv, AES_DECRYPTION);
//	if (status != MP_OKAY) return status;
//
//	status = wc_AesCbcDecrypt(&enc, message, input, input_length);
//
//	return status;
//}
//
//int32_t dat_ecc_encrypt_message(uint8_t* secret_key, char* message, int32_t message_size, uint8_t* output)
//{
//
//	// encrypt
//	Aes enc;
//	int status;
//
//	status = wc_AesSetKey(&enc, secret_key, AES_KEY_LENGTH, iv, AES_ENCRYPTION);
//	if (status != MP_OKAY) return status;
//
//	status = wc_AesCbcEncrypt(&enc, output, message, message_size);
//
//
//
//	return status;
//}

int32_t dat_ecc_calculate_secret_point(uint8_t * private_key, uint8_t * public_key_x, uint8_t * public_key_y, uint8_t * output, uint8_t * salt, int create_salt)
{
	ecc_key ecc_public_key;
	ecc_key ecc_private_key;
	uint8_t secretPoint[KEYSIZE];
	int status;
	int secretSz;

	status = wc_ecc_init(&ecc_public_key); // initialize key
	status &= wc_ecc_init(&ecc_private_key); // initialize key
	if (status != MP_OKAY) return status;

	wc_ecc_import_raw(&ecc_public_key, public_key_x, public_key_y, none, "SECP224R1");
	wc_ecc_import_raw(&ecc_private_key, none, none, private_key, "SECP224R1");

	status = wc_ecc_shared_secret(&ecc_private_key, &ecc_public_key, secretPoint, &secretSz);
	if (status != MP_OKAY) return status;

	if (create_salt)
	{
		status = wc_RNG_GenerateBlock(&rng, salt, EXCHANGE_SALT_SZ);
		if (status != MP_OKAY) return status;
	}

	status = wc_HKDF(SHA256, secretPoint, KEYSIZE, salt, EXCHANGE_SALT_SZ,
		NULL, 0, output, AES_KEY_LENGTH);
	if (status != MP_OKAY) return status;

	wc_ecc_free(&ecc_public_key);
	wc_ecc_free(&ecc_private_key);

	return status;

}
