#ifdef DAT_CRYPTO_MBEDTLS

#include <stdlib.h>
#include <string.h>
#include "dat_srp_mbedtls_verifier.h"

extern mbedtls_entropy_context entropy_ctx;
extern mbedtls_ctr_drbg_context ctr_drbg_ctx;
extern mbedtls_mpi * RR;

SRPVerifier * srp_verifier_new(SRP_HashAlgorithm algo, SRP_NGType ngtype, byte * pass, int pass_len, byte * n_hex, byte * g_hex)
{
	SRPVerifier* ver = NULL;
	NGConstant* ng = NULL; 
	byte* pass_dup = NULL;

	if (NULL == pass ||
		0 == pass_len) {
		return NULL;
	}

	if (ngtype == SRP_NG_CUSTOM &&
		((n_hex == NULL) || (g_hex == NULL))) {
		return NULL;
	}

	init_random(); /* Only happens once */
	
	ver = (SRPVerifier *)malloc(sizeof(SRPVerifier));
	if (!ver) {
		goto lblError;
	}

	ng = new_ng(ngtype, n_hex, g_hex);
	if (!ng) {
		goto lblError;
	}

	pass_dup = malloc(pass_len);
	if (!pass_dup) {
		goto lblError;
	}
	memcpy(pass_dup, pass, pass_len);

	ver->hash_alg = algo;
	ver->ng = ng;
	ver->authenticated = 0;
	ver->pass = pass_dup;
	ver->pass_len = pass_len;

	return ver;

lblError:
	if (ng) {
		delete_ng(ng);
	}

	return NULL;
}

SRP_RESULT srp_verifier_calculate_s_v(
	SRPVerifier * ver)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;
	BIGNUM* s = NULL;
	BIGNUM* v = NULL;
	BIGNUM* x = NULL;

	if (NULL == ver) {
		return SRP_ERROR;
	}

	ver->s = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (!ver->s) {
		goto lblCleanup;
	}

	ver->v = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (!ver->v) {
		goto lblCleanup;
	}

	mbedtls_mpi_init(ver->s);
	mbedtls_mpi_init(ver->v);

	mbedtls_res = mbedtls_mpi_fill_random(ver->s, 32,
		&mbedtls_ctr_drbg_random,
		&ctr_drbg_ctx);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	x = calculate_x(ver->hash_alg, ver->s, ver->pass, ver->pass_len);
	if (!x) {
		goto lblCleanup;
	}

	mbedtls_res = mbedtls_mpi_exp_mod(ver->v, ver->ng->g, x, ver->ng->N, RR);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	res = SRP_SUCCESS;

lblCleanup:
	if (x) {
		mbedtls_mpi_free(x);
		free(x);
	}

	return res;
}

SRP_RESULT srp_verifier_get_salt(SRPVerifier * ver, byte ** s, int * s_len)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;
	int salt_len = 0;
	byte * salt = NULL;

	if (NULL == ver || NULL == s || NULL == s_len) {
		return SRP_ERROR;
	}

	salt_len = mbedtls_mpi_size(ver->s);
	salt = (byte *)malloc(salt_len);
	if (!salt) {
		goto lblCleanup;
	}

	mbedtls_res = mbedtls_mpi_write_binary(ver->s, salt, salt_len);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* salt no longer required */
	mbedtls_mpi_free(ver->s);
	free(ver->s);
	ver->s = NULL;

	*s = salt;
	*s_len = salt_len;
	res = SRP_SUCCESS;

lblCleanup:
	if (SRP_ERROR == res) {
		if (salt) {
			free(salt);
		}
	}

	return res;
}

SRP_RESULT srp_verifier_set_A(SRPVerifier * ver, byte * A, int A_len)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;
	BIGNUM* safety_calc_tmp = NULL;

	if (NULL == ver || NULL == A || 0 == A_len) {
		return SRP_ERROR;
	}

	ver->A = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (NULL == ver->A) {
		goto lblCleanup;
	}

	mbedtls_mpi_init(ver->A);
	mbedtls_res = mbedtls_mpi_read_binary(ver->A, A, A_len);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* SRP-6a safety check - must not be true: A % N == 0 */
	safety_calc_tmp = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (NULL == safety_calc_tmp) {
		goto lblCleanup;
	}

	mbedtls_mpi_init(safety_calc_tmp);

	mbedtls_res = mbedtls_mpi_mod_mpi(safety_calc_tmp, ver->A, ver->ng->N);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	if (mbedtls_mpi_cmp_int(safety_calc_tmp, 0) == 0) {
		goto lblCleanup;
	}

	res = SRP_SUCCESS;

lblCleanup:
	if (safety_calc_tmp) {
		mbedtls_mpi_free(safety_calc_tmp);
		free(safety_calc_tmp);
	}

	return res;
}

SRP_RESULT srp_verifier_calculate_B_u(SRPVerifier * ver)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;
	BIGNUM* B_calc_temp = NULL;

	/* b */
	ver->b = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (NULL == ver->b) {
		goto lblCleanup;
	}
	mbedtls_mpi_init(ver->b);

	mbedtls_res = mbedtls_mpi_fill_random(ver->b, 256,
		&mbedtls_ctr_drbg_random,
		&ctr_drbg_ctx);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* B = v + g^b */
	ver->B = (BIGNUM *)malloc(sizeof(BIGNUM));
	mbedtls_mpi_init(ver->B);

	B_calc_temp = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (NULL == B_calc_temp) {
		goto lblCleanup;
	}
	mbedtls_mpi_init(B_calc_temp);

	mbedtls_res = mbedtls_mpi_exp_mod(B_calc_temp, ver->ng->g, ver->b, ver->ng->N, RR);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	mbedtls_res = mbedtls_mpi_add_mpi(ver->B, ver->v, B_calc_temp);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* u */
	ver->u = H_nn(ver->hash_alg, ver->A, ver->B);
	if (!ver->u) {
		goto lblCleanup;
	}

	res = SRP_SUCCESS;

lblCleanup:
	if (NULL != B_calc_temp) {
		mbedtls_mpi_free(B_calc_temp);
		free(B_calc_temp);
	}

	return res;
}

SRP_RESULT srp_verifier_get_B_u(SRPVerifier * ver, byte ** B, int * B_len, byte ** u, int * u_len)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;
	byte * bytes_B = NULL;
	int len_B = 0;
	byte * bytes_u = NULL;
	int len_u = 0;

	if (NULL == ver || NULL == B || NULL == B_len || NULL == u || NULL == u_len) {
		goto lblCleanup;
	}

	len_B = mbedtls_mpi_size(ver->B);
	len_u = mbedtls_mpi_size(ver->u);
	bytes_B = malloc(len_B);
	if (NULL == bytes_B) {
		goto lblCleanup;
	}

	bytes_u = malloc(len_u);
	if (NULL == bytes_u) {
		goto lblCleanup;
	}

	mbedtls_res = mbedtls_mpi_write_binary(ver->B, (unsigned char*)bytes_B, len_B);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	mbedtls_res = mbedtls_mpi_write_binary(ver->u, (unsigned char*)bytes_u, len_u);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	*B = bytes_B;
	*B_len = len_B;
	*u = bytes_u;
	*u_len = len_u;

	res = SRP_SUCCESS;

lblCleanup:
	if (SRP_ERROR == res) {
		if (bytes_B) {
			free(bytes_B);
		}

		if (bytes_u) {
			free(bytes_u);
		}
	}
	return res;
}

SRP_RESULT srp_verifier_calculate_secret(SRPVerifier * ver)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;

	ver->S = (BIGNUM *)malloc(sizeof(BIGNUM));
	if (NULL == ver->S) {
		goto lblCleanup;
	}
	mbedtls_mpi_init(ver->S);
	/* S = (A * (v^u)) ^ b */

	/* S = v^u */
	mbedtls_res = mbedtls_mpi_exp_mod(ver->S, ver->v, ver->u, ver->ng->N, RR); // ***** <--- This peaks @ 12.7 KB (SHA512) *******
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* u no longer needed */
	mbedtls_mpi_free(ver->u);
	free(ver->u);
	ver->u = NULL;

	/* v no longer needed */
	mbedtls_mpi_free(ver->v);
	free(ver->v);
	ver->v = NULL;

	/* */
	mbedtls_res = mbedtls_mpi_mul_mpi(ver->S, ver->S, ver->A);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	mbedtls_res = mbedtls_mpi_exp_mod(ver->S, ver->S, ver->b, ver->ng->N, RR);
	if (0 != mbedtls_res) {
		goto lblCleanup;
	}

	/* b no longer needed */
	mbedtls_mpi_free(ver->b);
	free(ver->b);
	ver->b = NULL;

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_verifier_calculate_key(SRPVerifier * ver)
{
	SRP_RESULT res = SRP_ERROR;

	//todo: fix srp_common.c
	ver->K = malloc(hash_length(ver->hash_alg));
	if (NULL == ver->K) {
		goto lblCleanup;
	}
	hash_num(ver->hash_alg, ver->S, ver->K);

	/* S no longer needed */
	mbedtls_mpi_free(ver->S);
	free(ver->S);
	ver->S = NULL;

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_verifier_calculate_M(SRPVerifier * ver)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;

	//todo: fix srp_common.c
	ver->M = malloc(hash_length(ver->hash_alg));
	if (NULL == ver->M) {
		goto lblCleanup;
	}

	calculate_M(ver->hash_alg, ver->ng, ver->M, ver->A, ver->B, ver->K);
	
	/* B no longer needed */
	mbedtls_mpi_free(ver->B);
	free(ver->B);
	ver->B = NULL;

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_verifier_verify_M(SRPVerifier * ver, byte * M, int M_len)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;

	if (memcmp(ver->M, M, hash_length(ver->hash_alg)) != 0) {
		goto lblCleanup;
	}

	ver->authenticated = 1;
	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_verifier_calculate_HAMK(SRPVerifier * ver)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;

	ver->HAMK = malloc(hash_length(ver->hash_alg));
	if (NULL == ver->HAMK) {
		goto lblCleanup;
	}

	calculate_H_AMK(ver->hash_alg, ver->HAMK, ver->A, ver->M, ver->K);

	/* A, M no longer required */
	mbedtls_mpi_free(ver->A);
	free(ver->A);
	ver->A = NULL;

	free(ver->M);
	ver->M = NULL;

	res = SRP_SUCCESS;

lblCleanup:

	return res;
}

SRP_RESULT srp_verifier_get_HAMK(SRPVerifier * ver, byte ** HAMK, int * HAMK_len)
{
	SRP_RESULT res = SRP_ERROR;
	int mbedtls_res = 1;

	if (NULL == ver || NULL == HAMK || NULL == HAMK_len) {
		return SRP_ERROR;
	}

	/*	Transfer ownership of HAMK.
		HAMK no longer needed - caller has it, and should free it himself */
	*HAMK = ver->HAMK;
	*HAMK_len = hash_length(ver->hash_alg);
	ver->HAMK = NULL;

	return SRP_SUCCESS;
}

SRP_RESULT srp_verifier_get_K(SRPVerifier * ver, byte ** K, int * K_len)
{
	if (NULL == ver || NULL == K || NULL == K_len) {
		return SRP_ERROR;
	}

	/*	Transfer ownership of K.
		K no longer needed - caller has it, and should free it himself */
	*K = ver->K;
	*K_len = hash_length(ver->hash_alg);
	ver->K = NULL;

	return SRP_SUCCESS;
}

SRP_RESULT srp_verifier_delete(SRPVerifier * ver)
{
	if (NULL == ver) {
		return SRP_ERROR;
	}

	if (ver->s) {
		mbedtls_mpi_free(ver->s);
		free(ver->s);
		ver->s = NULL;
	}

	if (ver->v) {
		mbedtls_mpi_free(ver->v);
		free(ver->v);
		ver->v = NULL;
	}

	if (ver->A) {
		mbedtls_mpi_free(ver->A);
		free(ver->A);
		ver->A = NULL;
	}

	if (ver->b) {
		mbedtls_mpi_free(ver->b);
		free(ver->b);
		ver->b = NULL;
	}

	if (ver->B) {
		mbedtls_mpi_free(ver->B);
		free(ver->B);
		ver->B = NULL;
	}

	if (ver->u) {
		mbedtls_mpi_free(ver->u);
		free(ver->u);
		ver->u = NULL;
	}

	if (ver->S) {
		mbedtls_mpi_free(ver->S);
		free(ver->S);
		ver->S = NULL;
	}


	if (ver->M) {
		free(ver->M);
		ver->M = NULL;
	}

	if (ver->HAMK) {
		free(ver->HAMK);
		ver->HAMK = NULL;
	}

	if (ver->K) {
		free(ver->K);
		ver->K = NULL;
	}

	if (ver->pass) {
		free(ver->pass);
		ver->pass = NULL;
	}

	if (ver->ng) {
		delete_ng(ver->ng);
		ver->ng = NULL;
	}

	free(ver);
	
	return SRP_SUCCESS;
}

#endif /* DAT_CRYPTO_MBEDTLS */