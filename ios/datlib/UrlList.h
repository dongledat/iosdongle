/**
 * @file    UrlList.h
 * @brief   UrlList class declaration
 */
@interface DATUrlList : NSObject {
  NSMutableArray * directoryElements;
}

- (id) initWithDirectoryElements:(NSMutableArray *)directoryElements;
- (int) size;
- (Url *) getElement:(int)elementNr;
- (Url *) getElement:(NSString *)urlName;
- (NSEnumerator *) elements;
- (BOOL) containsUrl:(NSString *)urlName;
- (int) hash;
- (BOOL) isEqualTo:(NSObject *)obj;
- (NSString *) description;
@end
