#import "DataElement.h"

int const STATE_ERROR = -1;
int const STATE_NODATA = 0;
int const STATE_OK = 1;

@implementation DataElement

@synthesize immutable;
@synthesize url;
@synthesize msg;
@synthesize value;
@synthesize state;
@synthesize valueAsString;
@synthesize valueAsDouble;
@synthesize valueAsInteger;
@synthesize valueAsLong;
@synthesize valueAsDate;
@synthesize valueAsDateTZ;
@synthesize valueAsBoolean;
@synthesize type;


/**
 * @note elementUrl type has been changed from to @a String to @a int
 */
- (id) init:(int)elementUrl value:(NSObject *)value exlapType:(int)exlapType {
  if (self = [super init]) {
    isImmutable = NO;
    url = elementUrl;
    eType = exlapType;
    msg = nil;
    [self setValue:value];
  }
  return self;
}


/**
 * @note elementUrl type has been changed from to @a String to @a int
 */
- (id) init:(int)elementUrl value:(NSObject *)value exlapType:(int)exlapType msg:(NSString *)msg {
  if (self = [super init]) {
    isImmutable = NO;
    url = elementUrl;
    eType = exlapType;
    msg = msg;
    [self setValue:value];
  }
  return self;
}


/**
 * @note elementUrl type has been changed from to @a String to @a int
 */
- (id) init:(int)elementUrl value:(NSObject *)value exlapType:(int)exlapType state:(int)state msg:(NSString *)msg {
  if (self = [super init]) {
    isImmutable = NO;
    url = elementUrl;
    eType = exlapType;
    msg = msg;
    [self setValue:value];
    state = state;
  }
  return self;
}


/**
 * Copy constructor
 */
- (id) initWithElement:(DataElement *)element {
  if (self = [super init]) {
    isImmutable = NO;
    if ([element name] != nil) {
      url = [element url];
    }
     else {
      url = nil;
    }
    if ([element msg] != nil) {
      msg = [[[NSString alloc] init:[element msg]] autorelease];
    }
     else {
      msg = nil;
    }
    eType = [element type];
    state = [element state];
    NSObject * srcValue = [element value];
    if (srcValue != nil) {

      switch ([element type]) {
      case STATE_NODATA:
      case Barcode.TEXT:
        value = [[[NSNumber alloc] init:[((NSNumber *)srcValue) doubleValue]] autorelease];
        return;
      case STATE_OK:
        value = [[[NSNumber alloc] init:[((NSNumber *)srcValue) booleanValue]] autorelease];
        return;
      case AuthCommand.HASH_SHA256:
      case Barcode.SMS:
        value = [[[DataObject alloc] init:(DataObject *)srcValue] autorelease];
        return;
      case AuthCommand.HASH_TEXA:
        int streamLength = ((NSArray *)srcValue).length;
        value = [NSArray array];
        [System arraycopy:srcValue param1:STATE_NODATA param2:value param3:STATE_NODATA param4:streamLength];
        return;
      case Barcode.PHONE:
      case TexaHash.PASSWORD_MAX_LENGTH:
        value = [[[NSString alloc] init:(NSString *)srcValue] autorelease];
        return;
      case Barcode.PRODUCT:
        value = [[[DataObjectList alloc] init:(DataObjectList *)srcValue] autorelease];
        return;
      case TexaHash.PASSWORD_ARRAY_SIZE:
        value = [[[DateTZ alloc] init:[((DateTZ *)srcValue) time] param1:[((DateTZ *)srcValue) offset]] autorelease];
        return;
      default:
        @throw [[[NSException alloc] init:@"Illegal EXLAP type"] autorelease];
      }
    }
    value = nil;
  }
  return self;
}

- (void) lock {
  isImmutable = YES;
  if ([self type] == 6 || [self type] == 2) {
    [((DataObject *)value) lock];
  }
   else if ([self type] == 5) {
    [((DataObjectList *)value) lock];
  }
}

- (void) setValue:(NSObject *)newValue {
  if (isImmutable) {
    @throw [[[IllegalArgumentException alloc] init:@"The data element is locked (immutable)."] autorelease];
  }
   else if (newValue == nil) {
    value = nil;
    state = STATE_NODATA;
  }
   else {

    @try {

      switch (eType) {
      case STATE_NODATA:
        if ([newValue conformsToProtocol:@protocol(NSNumber)]) {
          value = newValue;
          break;
        }
         else if ([newValue conformsToProtocol:@protocol(NSNumber)]) {
          value = [[[NSNumber alloc] init:[((NSNumber *)newValue) doubleValue]] autorelease];
          break;
        }
         else if ([newValue conformsToProtocol:@protocol(NSNumber)]) {
          value = [[[NSNumber alloc] init:[((NSNumber *)newValue) doubleValue]] autorelease];
          break;
        }
         else {
          @throw [[[IllegalArgumentException alloc] init:@"The given Java type does not match the EXLAP type of this element."] autorelease];
        }
      case STATE_OK:
      case AuthCommand.HASH_SHA256:
      case AuthCommand.HASH_TEXA:
      case Barcode.PHONE:
      case Barcode.PRODUCT:
      case Barcode.SMS:
      case Barcode.TEXT:
      case TexaHash.PASSWORD_MAX_LENGTH:
        value = newValue;
        break;
      case TexaHash.PASSWORD_ARRAY_SIZE:
        if (([newValue conformsToProtocol:@protocol(Date)]) && !([newValue conformsToProtocol:@protocol(DateTZ)])) {
          value = [[[DateTZ alloc] init:[((Date *)newValue) time] param1:[[TimeZone default] getOffset:[((Date *)newValue) time]]] autorelease];
          break;
        }
        value = newValue;
        break;
      default:
        @throw [[[NSException alloc] init:@"Illegal member type"] autorelease];
      }
      state = STATE_OK;
    }
    @catch (ClassCastException * e) {
      state = STATE_ERROR;
      @throw [[[IllegalArgumentException alloc] init:[@"The given Java type does not match the EXLAP type of this element. Root cause: " stringByAppendingString:[e message]]] autorelease];
    }
  }
}

- (void) checkForValue {
  if (![self hasValue]) {
    @throw [[[NullPointerException alloc] init:@"Can't access value, since value is null or its state is 'nodata' or 'error'"] autorelease];
  }
}

- (NSString *) valueAsString {
  [self checkForValue];

  switch ([self type]) {
  case STATE_NODATA:
  case Barcode.TEXT:
    return [((NSNumber *)value) description];
  case STATE_OK:
    return [((NSNumber *)value) description];
  case AuthCommand.HASH_TEXA:
    return [Base64 byteArrayToBase64:(NSArray *)value];
  case Barcode.PHONE:
  case TexaHash.PASSWORD_MAX_LENGTH:
    return (NSString *)value;
  case TexaHash.PASSWORD_ARRAY_SIZE:
    return [XSDate dateTZToXs:(DateTZ *)value];
  default:
    @throw [[[ClassCastException alloc] init:@"Type can not be represented as string"] autorelease];
  }
}

- (NSNumber *) valueAsDouble {
  [self checkForValue];
  if ([self type] == 0) {
    return (NSNumber *)value;
  }
  if ([self type] == 7) {
    return (NSNumber *)value;
  }
  @throw [[[ClassCastException alloc] init:@"Value is not an Absolute or Relative"] autorelease];
}

- (NSNumber *) valueAsInteger {
  [self checkForValue];
  if ([self type] == 0) {
    double val = [((NSNumber *)value) doubleValue];
    if (val < 0.0d) {
      return [[[NSNumber alloc] init:[[[[NSNumber alloc] init:val - 0.5d] autorelease] intValue]] autorelease];
    }
    return [[[NSNumber alloc] init:[[[[NSNumber alloc] init:0.5d + val] autorelease] intValue]] autorelease];
  }
  @throw [[[ClassCastException alloc] init:@"Value is not an Absolute"] autorelease];
}

- (NSNumber *) valueAsLong {
  [self checkForValue];
  if ([self type] == 0) {
    double val = [((NSNumber *)value) doubleValue];
    if (val < 0.0d) {
      return [[[NSNumber alloc] init:[[[[NSNumber alloc] init:val - 0.5d] autorelease] longValue]] autorelease];
    }
    return [[[NSNumber alloc] init:[[[[NSNumber alloc] init:0.5d + val] autorelease] longValue]] autorelease];
  }
  @throw [[[ClassCastException alloc] init:@"Value is not an Absolute"] autorelease];
}

- (BOOL) hasValue {
  return value != nil && [self state] == STATE_OK;
}

- (Date *) valueAsDate {
  [self checkForValue];
  if ([self type] == 9) {
    return [[[Date alloc] init:([((DateTZ *)value) time] + ((long)[[TimeZone default] getOffset:[((DateTZ *)value) time]])) - ((long)[((DateTZ *)value) offset])] autorelease];
  }
  @throw [[[ClassCastException alloc] init:@"Value is not a Time"] autorelease];
}

- (DateTZ *) valueAsDateTZ {
  [self checkForValue];
  if ([self type] == 9) {
    return (DateTZ *)value;
  }
  @throw [[[ClassCastException alloc] init:@"Value is not a Time"] autorelease];
}

- (NSNumber *) valueAsBoolean {
  [self checkForValue];
  if ([self type] == STATE_OK) {
    return (NSNumber *)value;
  }
  @throw [[[ClassCastException alloc] init:@"Value is not an Activity"] autorelease];
}

- (DataObject *) getValueAsDataObject {
  [self checkForValue];
  if ([self type] == 6 || [self type] == 2) {
    return (DataObject *)value;
  }
  @throw [[[ClassCastException alloc] init:@"Value is not an ObjectEntity or Alternative"] autorelease];
}

- (DataObjectList *) getValueAsDataObjectList {
  [self checkForValue];
  if ([self type] == 5) {
    return (DataObjectList *)value;
  }
  @throw [[[ClassCastException alloc] init:@"Value is not an ListEntity"] autorelease];
}

- (NSArray *) getValueAsByteArray {
  [self checkForValue];
  if ([self type] == 3) {
    return (NSArray *)value;
  }
  @throw [[[ClassCastException alloc] init:@"Value is not an ListEntity"] autorelease];
}

- (void) setValueByString:(NSString *)newValue {
  [self setValue:newValue];
}

- (void) setValueByInteger:(NSNumber *)newValue {
  [self setValue:newValue];
}

- (void) setValueByBoolean:(NSNumber *)newValue {
  [self setValue:newValue];
}

- (void) setValueByLong:(NSNumber *)newValue {
  [self setValue:newValue];
}

- (void) setValueByDouble:(NSNumber *)newValue {
  [self setValue:newValue];
}

- (void) setValueByDate:(Date *)newValue {
  [self setValue:newValue];
}

- (void) setValueByDateTZ:(DateTZ *)newValue {
  [self setValue:newValue];
}

- (void) setValueByDataObject:(DataObject *)newValue {
  [self setValue:newValue];
}

- (void) setValueByDataObjectList:(DataObjectList *)newValue {
  [self setValue:newValue];
}

- (void) setValueByByteArray:(NSArray *)newValue {
  [self setValue:newValue];
}

- (int) hash {
  int i = STATE_NODATA;
  int hashCode = ((((url == nil ? STATE_NODATA : [url hash]) + 31) * 31) + state) * 31;
  if (value != nil) {
    i = [value hash];
  }
  return hashCode + i;
}


/**
 * Element comparator
 */
- (BOOL) isEqualTo:(NSObject *)obj {
  if (self == obj) {
    return YES;
  }
  if (obj == nil) {
    return NO;
  }
  if ([self class] != [obj class]) {
    return NO;
  }
  DataElement * other = (DataElement *)obj;
  if (url == nil) {
    if (other.url != nil) {
      return NO;
    }
  }
   else if (url != other.url) {
    return NO;
  }
  if (state != other.state) {
    return NO;
  }
  if (value == nil) {
    if (other.value != nil) {
      return NO;
    }
    return YES;
  }
   else if ([value isEqualTo:other.value]) {
    return YES;
  }
   else {
    return NO;
  }
}


/**
 * Convert DataElement to printable representation
 */
- (NSString *) description {
  StringBuffer * result = [[[StringBuffer alloc] init:100] autorelease];
  [result append:@"[Element: url="];
  [result append:[self name]];
  [result append:@", state="];

  switch ([self state]) {
  case STATE_ERROR:
    [result append:ExlapML.STATUS_MSG_ERROR];
    break;
  case STATE_NODATA:
    [result append:@"nodata"];
    break;
  case STATE_OK:
    [result append:ExlapML.STATUS_MSG_OK];
    break;
  }
  [result append:@", exlapType="];
  [result append:[ExlapElementFactory getExlapTypeAsString:[self type]]];
  [result append:@", val="];
  if (value == nil || [self state] != STATE_OK) {
    [result append:@"N.A."];
  }
   else if (eType == 5) {
    [result append:[((DataObjectList *)value) description]];
  }
   else if (eType == 6 || eType == 2) {
    [result append:[((DataObject *)value) description]];
  }
   else {
    [result append:[self valueAsString]];
  }
  [result append:@"]"];
  return [result description];
}

- (void) dealloc {
  [msg release];
  [value release];
  [super dealloc];
}

@end
