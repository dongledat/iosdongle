#import "Url.h"

int const TYPE_FUNTION = 0;
int const TYPE_OBJECT = 1;

@implementation Url

@synthesize name;
@synthesize id;
@synthesize type;
@synthesize subscribed;

- (id) init:(NSString *)urlName type:(int)type isSubscribed:(BOOL)isSubscribed {
  if (self = [super init]) {
    url = urlName;
    isSubscribed = isSubscribed;
    type = type;
    id = [self urlToId:urlName];
  }
  return self;
}

- (id) initWithUrlName:(NSString *)urlName {
  if (self = [super init]) {
    url = urlName;
    isSubscribed = NO;
    type = TYPE_FUNTION;
    id = [self urlToId:urlName];
  }
  return self;
}

- (id) initWithId:(int)id {
  if (self = [super init]) {
    id = id;
    isSubscribed = NO;
    type = TYPE_FUNTION;
    url = [self idToUrl:id];
  }
  return self;
}

+ (BOOL) isMatch:(NSString *)url urlPattern:(NSString *)urlPattern {
  if ([urlPattern isEqualToString:@"*"]) {
    return YES;
  }
  if ([urlPattern rangeOfString:42] == -1) {
    return [url isEqualToString:urlPattern];
  }
  if ([urlPattern rangeOfString:42] > 0) {
    return [[url substringFromIndex:TYPE_FUNTION param1:[urlPattern rangeOfString:42]] isEqualTo:[urlPattern substringFromIndex:TYPE_FUNTION param1:[urlPattern rangeOfString:42]]];
  }
  if ([urlPattern lastIndexOf:42] == 0) {
    return [url hasSuffix:[urlPattern substringFromIndex:TYPE_OBJECT]];
  }
  return [url rangeOfString:[urlPattern substringFromIndex:TYPE_OBJECT param1:[urlPattern lastIndexOf:42]]] >= 0;
}

- (NSString *) description {
  StringBuffer * result = [[[StringBuffer alloc] init:@"[Url: url="] autorelease];
  [result append:[self name]];
  if ([self type] == TYPE_OBJECT) {
    [result append:@", isSubscribed="];
    if ([self subscribed]) {
      [result append:ExlapML.SUBSCRIBE_CMD_ATTRIBUTE_TIMESTAMP_TRUE];
    }
     else {
      [result append:ExlapML.SUBSCRIBE_CMD_ATTRIBUTE_CONTENT_FALSE];
    }
  }
  [result append:@", type="];

  switch ([self type]) {
  case TYPE_FUNTION:
    [result append:@"function"];
    break;
  case TYPE_OBJECT:
    [result append:@"object"];
    break;
  }
  [result append:@"]"];
  return [result description];
}

- (int) hash {
  return (((((isSubscribed ? 1231 : 1237) + 31) * 31) + type) * 31) + (url == nil ? TYPE_FUNTION : [url hash]);
}

- (BOOL) isEqualTo:(NSObject *)obj {
  if (self == obj) {
    return YES;
  }
  if (obj == nil) {
    return NO;
  }
  if ([self class] != [obj class]) {
    return NO;
  }
  Url * other = (Url *)obj;
  if (isSubscribed != other.isSubscribed) {
    return NO;
  }
  if (type != other.type) {
    return NO;
  }
  if (url == nil) {
    if (other.url != nil) {
      return NO;
    }
    return YES;
  }
   else if ([url isEqualTo:other.url]) {
    return YES;
  }
   else {
    return NO;
  }
}


/**
 * @brief Convert URL string to command parameter ID
 */
+ (int) urlToId:(NSString *)url {
}


/**
 * @brief Convert command/parameter ID to URL string
 */
+ (int) idToUrl:(int)id {
}

- (void) dealloc {
  [url release];
  [super dealloc];
}

@end
