//
//  MyXmlParserDelegate.m
//  Test
//
//  Created by Yuli Stremovsky on 06/10/2016.
//  Copyright © 2016 Yuli Stremovsky. All rights reserved.
//

//#import <Foundation/Foundation.h>

#import "MyXmlParserDelegate.h"

@implementation MyXmlParserDelegate

NSString *testName = nil;

- (void) parserDidStartDocument:(NSXMLParser *)parser {
    NSLog(@"parserDidStartDocument");
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    NSLog(@"didStartElement --> %@, %@", elementName, qName);
    
    if([elementName isEqualToString:@"scenario"])
    {
        testName =[attributeDict valueForKey:@"name"];
    }
    if([elementName isEqualToString:@"api"])
    {
        NSString *nameValue=[attributeDict valueForKey:@"name"];
        NSString *param0Value=[attributeDict valueForKey:@"param_0"];
        NSString *delayValue=[attributeDict valueForKey:@"delay"];
        NSLog(@"%@ api name %@, p %@, d %@", testName, nameValue,param0Value,delayValue);
    }
    
}

-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    NSLog(@"foundCharacters --> %@", string);
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    NSLog(@"didEndElement   --> %@", elementName);
    if([elementName isEqualToString:@"scenario"])
    {
        testName = nil;
    }
}

- (void) parserDidEndDocument:(NSXMLParser *)parser {
    NSLog(@"parserDidEndDocument");
}
@end
