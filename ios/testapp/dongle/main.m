//
//  main.m
//  dongle
//
//  Created by David Olodovsky on 04/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MyXmlParserDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        NSError *error = nil;
        NSLog(@"Main Started");
        NSString* filePath = [[NSBundle mainBundle] pathForResource:@"tests" ofType:@"xml" ];
        NSLog(@"\n\nthe string %@",filePath);
        // Load the file and check the result
        NSData *data = [NSData dataWithContentsOfFile:filePath
                                              options:NSDataReadingUncached
                                                error:&error];
        if(error) {
            NSLog(@"Error %@", error);
            
            return 1;
        }
        // Create a parser and point it at the NSData object containing the file we just loaded
        NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
        
        // Create an instance of our parser delegate and assign it to the parser
        MyXmlParserDelegate *parserDelegate = [[MyXmlParserDelegate alloc] init];
        [parser setDelegate:parserDelegate];
        
        // Invoke the parser and check the result
        [parser parse];
        error = [parser parserError];
        if(error)
        {
            NSLog(@"Error %@", error);
        }

        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
