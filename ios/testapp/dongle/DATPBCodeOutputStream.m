//
//  DATPBCodeOutputStream.m
//  dongle
//
//  Created by David Olodovsky on 13/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import "DATPBCodeOutputStream.h"

@class PBMessage_Builder;

@implementation DATPBCodeOutputStream


const int32_t DEFAULT_RECURSION_LIMIT = 64;
const int32_t DEFAULT_SIZE_LIMIT = 64 << 20;  // 64MB
const int32_t BUFFER_SIZE = 4096;


- (void) commonInit {
}


- (id) initWithData:(NSData*) data {
    if ((self = [super init])) {
         [self commonInit];
    }
    
    return self;
}


- (id) initWithInputStream:(NSInputStream*) input_ {
    if ((self = [super init])) {
              [self commonInit];
    }
    
    return self;
}





/**
 * Attempt to read a field tag, returning zero if we have reached EOF.
 * Protocol message parsers use this to read tags, since a protocol message
 * may legally end wherever a tag occurs, and zero is not a valid tag number.
 */
- (int32_t) readTag {
        return 0;
    }
/**
 * Verifies that the last call to readTag() returned the given tag value.
 * This is used to verify that a nested group ended with the correct
 * end tag.
 *
 * @throws InvalidProtocolBufferException {@code value} does not match the
 *                                        last tag.
 */
- (void) checkLastTagWas:(int32_t) value {
}

/**
 * Reads and discards a single field, given its tag value.
 *
 * @return {@code NO} if the tag is an endgroup tag, in which case
 *         nothing is skipped.  Otherwise, returns {@code YES}.
 */
- (BOOL) skipField:(int32_t) tag {
            return YES;
}


/**
 * Reads and discards an entire message.  This will read either until EOF
 * or until an endgroup tag, whichever comes first.
 */
- (void) skipMessage {
    while (YES) {
        int32_t tag = [self readTag];
        if (tag == 0 || ![self skipField:tag]) {
            return;
        }
    }
}












@end
