//
//  DATPBExtandableMessage.h
//  dongle
//
//  Created by David Olodovsky on 13/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PBExtendableMessage_Builder : PBGeneratedMessage_Builder {
}

- (id) getExtension:(id<PBExtensionField>) extension;
- (BOOL) hasExtension:(id<PBExtensionField>) extension;
- (PBExtendableMessage_Builder*) setExtension:(id<PBExtensionField>) extension
                                        value:(id) value;
- (PBExtendableMessage_Builder*) addExtension:(id<PBExtensionField>) extension
                                        value:(id) value;
- (PBExtendableMessage_Builder*) setExtension:(id<PBExtensionField>) extension
                                        index:(int32_t) index
                                        value:(id) value;
- (PBExtendableMessage_Builder*) clearExtension:(id<PBExtensionField>) extension;

/* @protected */
- (void) mergeExtensionFields:(PBExtendableMessage*) other;

@end
Contact GitHub
@end
