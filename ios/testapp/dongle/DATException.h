//
//  DATException.h
//  dongle
//
//  Created by David Olodovsky on 16/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import <Foundation/Foundation.h>
NSUInteger const DATException_id[] = {1000,2000,3000,4000,5000,6000};
NSString * const DATException_name[] = { @"DATIllegalArgumentException",
                                         @"DATRunTimeException",
                                         @"DATClassCastException",
                                         @"DATNullPointerException",
                                         @"DATNumberFormatException",
                                         @"DATExlapException"};
    
NSString * const DATException_reason[] = { @"Illegal option value.",
    @"The data element is locked (immutable).",
    @"The given type does not match the EXLAP type of this element.",
    @"Illegal member type",
    @"Type can not be represented as string",
    @"Value is not an Absolute or Relative",
    @"Can't access value, since value is null or its state is 'nodata' or 'error'",
    @"Illegal address format (error in port number scheme).",
    @"Problem with unsubscribing of value",
    @"Problem with subscribing of value  ",
    @"Illegal EXLAP type"};


@interface DATException : NSException
+ (void)raise:(NSExceptionName)name format:(NSString *)format, ... NS_FORMAT_FUNCTION(2,3);
+ (void)raise:(NSExceptionName)name format:(NSString *)format arguments:(va_list)argList NS_FORMAT_FUNCTION(2,0);


@end
