//
//  ViewViewController.m
//  dongle
//
//  Created by David Olodovsky on 30/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import "ViewViewController.h"

@interface ViewViewController ()

@end

@implementation ViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
