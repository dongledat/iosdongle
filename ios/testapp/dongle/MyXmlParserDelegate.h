//
//  MyXmlParserDelegate.h
//  Test
//
//  Created by Yuli Stremovsky on 06/10/2016.
//  Copyright © 2016 Yuli Stremovsky. All rights reserved.
//

#ifndef MyXmlParserDelegate_h
#define MyXmlParserDelegate_h

#import <Foundation/Foundation.h>

@interface MyXmlParserDelegate : NSObject <NSXMLParserDelegate>

@end

#endif /* MyXmlParserDelegate_h */
