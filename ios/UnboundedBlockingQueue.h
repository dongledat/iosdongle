//
//  UnboundedBlockingQueue.h

#import <Foundation/Foundation.h>
#import <sys/time.h>
#import "Node.h"
#import <pthread.h>

@interface UnboundedBlockingQueue : NSObject{
@private
    pthread_mutex_t lock;
    pthread_cond_t notEmpty;
    Node *first, *last;
}

- (UnboundedBlockingQueue*) init;
- (void) put: (id) data;
- (id) take: (int) timeout;

@end
