//
//  Node.m

#import "Node.h"

@implementation Node 

@synthesize data;
@synthesize next;

- (void) dealloc {
    NSLog(@"Node geting destroyed: %@", data);
}

@end
