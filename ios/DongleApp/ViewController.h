//
//  ViewController.h
//  DongleApp
//
//  Created by David Olodovsky on 30/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCDAsyncUdpSocket.h"

@interface ViewController : UIViewController

{
    long tag;
    GCDAsyncUdpSocket *udpSocket;
    NSString* conObj;


}
- (void)send:(NSData *)data;
@end

