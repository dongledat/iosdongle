//
//  DATUDPSocket.h
//  DongleApp
//
//  Created by Yuli Stremovsky on 12/11/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#ifndef DATUDPSocket_h
#define DATUDPSocket_h


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

extern NSString* SimulatorIPString;

#endif /* DATUDPSocket_h */

class DAT_UDPSocket {
private:
    int _sock;
    struct sockaddr_in _si_other;
    socklen_t _slen;
    static DAT_UDPSocket *_Inst;
    
    DAT_UDPSocket()
    {
        _sock = 0;
        _slen = 0;
        _si_other = {0};
    }
    
    ~DAT_UDPSocket()
    {
        if (_sock != 0)
        {
            close(_sock);
        }
    }
public:
    static DAT_UDPSocket *getInstance()
    {
        if (_Inst == NULL)
        {
            Init();
        }
        return _Inst;
    }
    static void Init()
    {
        _Inst = new DAT_UDPSocket();
    }
    int is_connected()
    {
        if (_sock == 0)
            return 0;
        return _sock;
    }
    int connect()
    {
        int port = 11115;
        if (port <= 0 || port > 65535)
        {
            return 0;
        }
        
        int s = 0;
        int i = 0;
        _slen=sizeof(_si_other);
        
        if ( (_sock=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        {
            return 0;
        }
        memset((char *) &_si_other, 0, sizeof(_si_other));
        _si_other.sin_family = AF_INET;
        _si_other.sin_port = htons(port);
        
        const char *stringAsChar = [SimulatorIPString cStringUsingEncoding:[NSString defaultCStringEncoding]];
        
        if (inet_aton(stringAsChar, &_si_other.sin_addr) == 0)
        {
            fprintf(stderr, "inet_aton() failed\n");
            return 0;
        }
        return 1;
    }
    
    int disconnect()
    {
        if (_sock)
        {
            close(_sock);
            memset((char *) &_si_other, 0, sizeof(_si_other));
            _sock = 0;
        }
        return 0;
    }
    
    int write(const unsigned char * buf, int b_size)
    {
        if (_sock == 0)
        {
            return 0;
        }
        //send the message
        sendto(_sock, buf, b_size, 0 , (struct sockaddr *) &_si_other, _slen);
        return b_size;
    }

    int has_data()
    {
        unsigned char c = 0;
    
        if (_sock == 0)
            return 0;
        
        int x = recv(_sock, &c, 1, MSG_PEEK);
        if (x > 0) {
            /* ...have data, leave it in socket buffer until B connects */
        } else if (x == 0) {
            /* ...handle FIN from A */
        } else {
            /* ...handle errors */
        }
        return x;
    }
    
    int read()
    {
        char result[1024] = {0};
        if (_sock == 0)
            return 0;
        int res = recvfrom(_sock, result, 1024, 0, (struct sockaddr *) &_si_other, &_slen);
        if (res > 0)
        {
            CDAT_LibNet * con = CDAT_LibNet::getInstance();
            con->fromNet((const unsigned char*)result, res);
        }
        return res;
    }

};
