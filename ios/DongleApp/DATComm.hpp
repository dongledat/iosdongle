//
//  DATComm.hpp
//  DongleApp
//
//  Created by Yuli Stremovsky on 04/11/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#ifndef DATComm_hpp
#define DATComm_hpp

#include <stdio.h>

#include <Connection.h>
#include <pthread.h>

class CDAT_LibNet:
public dat::IOutputStream,
public dat::IDongleStatusListener,
public dat::IDataListener,
public dat::IUpdateProgressListener{
private:
    dat::Connection * m_Connection;
    void * m_SockObj;
    CDAT_LibNet()
    {
        m_Connection = new dat::Connection(*this, *this, *this, * this, false, true);
        m_SockObj = NULL;
        pthread_mutex_init(&mutex, NULL);
    }
    ~CDAT_LibNet()
    {
        if (m_Connection)
        {
            delete m_Connection;
        }
    }

    static CDAT_LibNet *_Inst;
    pthread_mutex_t mutex;
    void lock()
    {
        //pthread_mutex_lock(&mutex);
    }
    void unlock()
    {
        //pthread_mutex_unlock(&mutex);
    }
public:
    static CDAT_LibNet *getInstance()
    {
        if (_Inst == NULL)
        {
            Init();
        }
        return _Inst;
    }
    static dat::Connection * getLib() {
        return CDAT_LibNet::getInstance()->m_Connection;
    }
    static void Init()
    {
        _Inst = new CDAT_LibNet();
    }
    void setSockObj(void * obj)
    {
        m_SockObj = obj;
    }
    
    int getDongleHWRevision()
    {
        printf("Calling getDongleHWRevision\n");
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            tranid = m_Connection->getDongleHWRevisionAsync();
            unlock();
        }
        return tranid;
    }
    int connect()
    {
        printf("Calling connect\n");
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            //tranid = m_Connection->c();
            unlock();
        }
        return tranid;
    }

    
    int disconnect()
    {
        printf("Calling disconnect\n");
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            //tranid = m_Connection->getDongleHWRevisionAsync();
            unlock();
        }
        return tranid;
    }
    int subscribeObject(const std::string &url, const std::string &val)
    {
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            tranid = m_Connection->subscribeObject(atoi(url.c_str()), atoi(val.c_str()));
            unlock();
        }
        return tranid;
    }
    int unsubscribeObject(const std::string &url)
    {
        // translate url to tranid
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            tranid = m_Connection->unsubscribeObject(atoi(url.c_str()));
            unlock();
        }
        return tranid;
    }
    int getDongleSerialNumber()
    {
        printf("Calling getDongleSerialNumber\n");
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            tranid = m_Connection->getDongleSerialNumberAsync();
            unlock();
        }
        return tranid;
    }
    int getAvailableDiagParameters()
    {
        printf("Calling getAvailableDiagParameters\n");
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            tranid = m_Connection->getAvailableDiagParametersAsync();
            unlock();
        }
        return tranid;
    }
    int switchToServiceMode()
    {
        printf("Calling switchToServiceMode\n");
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            tranid = m_Connection->switchToServiceMode();
            unlock();
        }
        return tranid;
    }
    int getFirmwareStatus()
    {
        printf("Calling getFirmwareStatus\n");
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            tranid = m_Connection->getFirmwareStatusAsync();
            unlock();
        }
        return tranid;
    }
    int getDataObject (const std::string &url)
    {
        printf("Calling getDataObject\n");
        int tranid = 0;
        if (m_Connection)
        {
            lock();
            tranid = m_Connection->getDataObjectAsync(atoi(url.c_str()));
            unlock();
        }
        return tranid;
    }
    int AuthenticateByPin(const std::string &identity, const std::string &pin)
    {
        printf("Calling AuthenticateByPin\n");
        int tranid = 1000;
        if (m_Connection)
        {
            lock();
            m_Connection->authenticateByPIN(identity,pin);
            unlock();
        }
        return tranid;
    }
    int AuthenticateByToken(const std::string &token)
    {
        printf("Calling AuthenticateByToken\n");
        int tranid = 1000;
        if (m_Connection)
        {
            lock();
            m_Connection->authenticateByToken(token);
            unlock();
        }
        return tranid;
    }
    int isConnected()
    {
        printf("Callig isConnected request \n");
        int tranid = 1000;
        if (m_Connection)
        {
            lock();
            m_Connection->isAuthenticatedByPIN();
            unlock();
        }
        return tranid;
    }
    int isAuthenticatedByPIN()
    {
        printf("Callig isAuthenticatedByPIN request \n");
        int tranid = 1000;
        if (m_Connection)
        {
            lock();
            m_Connection->isAuthenticatedByPIN();
            unlock();
        }
        return tranid;
    }
    int isAuthenticatedByToken()
    {
        printf("Callig isConnected request \n");
        int tranid = 1000;
        if (m_Connection)
        {
            lock();
            m_Connection->isAuthenticatedByToken();
            unlock();
        }
        return tranid;
    }
    virtual size_t send(const unsigned char * buf, size_t b_size);
    void fromNet(const unsigned char* pdata, int len)
    {
        if (m_Connection)
        {
            lock();
            m_Connection->fromNet(pdata, len);
            unlock();
        }
    }
    virtual void onAuthenticate(AuthenticationMethod method, AuthenticationResult result);
    virtual void onFirmwareStatus(int respID, const FWStatus& fwStatus);
    virtual void onSwitchResponse(int respId);
    virtual void onData(int respId, TDataObject &dataObj, std::string & resp);
    virtual void onAvailableDiagParameters(int respID, const std::list<TDataCatalogEntry>& l, std::string & rspAsString);
    virtual void onDongleHWRevision(int respId, const std::string & res);
    virtual void onDongleSerialNumber(int respId, const std::string & res);
    //virtual void onFirmwareStatus(int respID, const FWStatus& /*fwStatus*/, std::string &rspAsString)
    //{
    //
    //}
    virtual void onUpdateStatus(UpdateStatus _st)
    {
        printf("Missing handler for onUpdateStatus !!!\n");
    }
    virtual void onPDUNotify(int respID, const std::string& pduRawDataResponse, bool success)
    {
        printf("Missing handler for onPDUNotify !!!\n");

    }
    virtual void onDongleStatus(DongleStatus status)
    {
        printf("Missing handler for onDongleStatus !!!\n");

    }
};



#endif /* DATComm_hpp */
