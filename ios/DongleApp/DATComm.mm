//
//  DATComm.cpp
//  DongleApp
//
//  Created by Yuli Stremovsky on 04/11/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//


#include "DATComm.hpp"
#import "AsyncUdpSocket.h"
#import "UnboundedBlockingQueue.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#import "ViewController.h"
#include "DATUDPSocket.hpp"

#import <Foundation/Foundation.h>
#import <dispatch/dispatch.h>
#import <TargetConditionals.h>
#import <Availability.h>

extern UnboundedBlockingQueue *command_queue;
extern NSMutableDictionary *id_command_queue;
extern NSString* SimulatorIPString;
using namespace dat;

CDAT_LibNet *CDAT_LibNet::_Inst = NULL;
DAT_UDPSocket *DAT_UDPSocket::_Inst = NULL;

size_t CDAT_LibNet::send(const unsigned char * buf, size_t b_size)
{
    if (m_SockObj == NULL)
    {
        return 0;
    }
    DAT_UDPSocket * sock = DAT_UDPSocket::getInstance();
    if (sock->is_connected() == 0)
    {
        sock->connect();
    }
    if (sock->is_connected() == 0)
    {
        // error;
        return 0;
    }
    
    return sock->write(buf, b_size);
    
    /* yuli*/
    /*
    ViewController *pViewController;
    pViewController = (ViewController*)CFBridgingRelease(m_SockObj);
    NSData* data = [NSData dataWithBytes:(const void *)buf length:sizeof(unsigned char)*b_size];
    [pViewController send: data];
     */
    /*
    dispatch_queue_t myQueue = dispatch_get_current_queue();
    
    dispatch_async(myQueue, ^{ @autoreleasepool {
        
        [pViewController send: data];
    }});
    */
    
    //return 0;
 
    /*
    int port = 11115;
    if (port <= 0 || port > 65535)
    {
        return 0;
    }
    
    struct sockaddr_in si_other;
    int s = 0;
    int i = 0;
    socklen_t slen=sizeof(si_other);

    if ( (s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        return 0;
    }
    memset((char *) &si_other, 0, sizeof(si_other));
    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(port);
    const char *stringAsChar = [SimulatorIPString cStringUsingEncoding:[NSString defaultCStringEncoding]];

    if (inet_aton(stringAsChar, &si_other.sin_addr) == 0)
    {
        fprintf(stderr, "inet_aton() failed\n");
        return 0;
    }
    
    //send the message
    sendto(s, buf, b_size, 0 , (struct sockaddr *) &si_other, slen);
    char result[1024] = {0};
    int res = recvfrom(s, result, 1024, 0, (struct sockaddr *) &si_other, &slen);
    if (res > 0)
    {
        m_Connection->fromNet((const unsigned char*)result, res);
    }
    
    //AsyncUdpSocket *udpSocket;
    //udpSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
    
    //NSData* data = [NSData dataWithBytes:(const void *)buf length:sizeof(unsigned char)*b_size];
    
    //[(__bridge AsyncUdpSocket*) m_SockObj sendData:data toHost:host port:port withTimeout:-1 tag:0];
    */
    /*
     int MyObjectDoSomethingWith (void *self, void *aParameter)
     {
     // Call the Objective-C method using Objective-C syntax
     return [(id) self doSomethingWith:aParameter];
     }
     */
    //return res;
}

void CDAT_LibNet::onDongleHWRevision(int respId, const std::string & res)
{
    try {
    printf("We got dongle hw revision: %s\n", res.c_str());
    NSString *nsres = [NSString stringWithCString:res.c_str()
                                               encoding:[NSString defaultCStringEncoding]];
    NSNumber *nsrespid = [NSNumber numberWithInt:respId];
    id_command_queue[nsrespid] = nsres;

    }
    catch (...)
    {
        
    }
    // log to screen
}

void CDAT_LibNet::onDongleSerialNumber(int respId, const std::string & res)
{
    printf("We got dongle serial number : %s\n", res.c_str());
    NSString *nsres = [NSString stringWithCString:res.c_str()
                                encoding:[NSString defaultCStringEncoding]];
    NSNumber *nsrespid = [NSNumber numberWithInt:respId];
    id_command_queue[nsrespid] = nsres;
}

void CDAT_LibNet::onData(int respId, TDataObject &dataObj, std::string & resp)
{
    try {
       
        NSString *nsres = [NSString stringWithFormat:@"TDataObject DataElementCount: %d, urlCode : %d, str: %s\n",
                             dataObj.DataElement_count, dataObj.UrlCode, resp.c_str() ];
        printf("We got ondata: %s\n", resp.c_str());
       
        NSNumber *nsrespid = [NSNumber numberWithInt:respId];
        if (!id_command_queue[nsrespid])
        {
            id_command_queue[nsrespid] = nsres;
        } else {
            // probably subscriber response.
            //id_command_queue[nsrespid] = nsres;
            ViewController *pViewController;
            pViewController = (ViewController*)CFBridgingRelease(m_SockObj);
            [pViewController performSelectorOnMainThread:@selector(updateResultOutput:)
                                   withObject: nsres
                                waitUntilDone:FALSE];

        }
    }
    catch (...)
    {
        
    }
}

void CDAT_LibNet::onAuthenticate(AuthenticationMethod method, AuthenticationResult result)
{
    std::string res;
    try {
        switch (result) {
            case AUTH_RESULT_NONE:
                res = "FAIL";
                break;
            case AUTH_RESULT_IN_PROGRESS:
                res = "OK";
                break;
            case AUTH_RESULT_PASS:
                res = "OK";
                break;
            case AUTH_RESULT_FAIL:
                res = "FAIL";
                break;
            case AUTH_RESULT_TIMEOUT:
                res = "FAIL";
                break;
        }
        printf("We got authenticated : %s\n", res.c_str());
        NSString *nsres = [NSString stringWithCString:res.c_str()
                                             encoding:[NSString defaultCStringEncoding]];
        NSNumber *nsrespid = [NSNumber numberWithInt:1000  ];
        id_command_queue[nsrespid] = nsres;
    }
    catch (...)
    {
        
    }
}


void CDAT_LibNet::onFirmwareStatus(int respID, const FWStatus& fwStatus)
{
    try {
        
        printf("We got firmware status : %s %s %s %s\n",
               fwStatus.m_dbVersion.c_str(),
               fwStatus.m_uLoaderVersion.c_str() ,
               fwStatus.m_serviceAppVersion.c_str(),
               fwStatus.m_userAppVersion.c_str());
        
        NSString *ndbVersion = [NSString stringWithCString:fwStatus.m_dbVersion.c_str()
                                             encoding:[NSString defaultCStringEncoding]];
        
        NSString *nLoaderVersion = [NSString stringWithCString:fwStatus.m_uLoaderVersion.c_str()
                                                  encoding:[NSString defaultCStringEncoding]];
        
        NSString *nserviceAppVersion = [NSString stringWithCString:fwStatus.m_serviceAppVersion.c_str()
                                                  encoding:[NSString defaultCStringEncoding]];
        
        NSString *nuserAppVersion = [NSString stringWithCString:fwStatus.m_userAppVersion.c_str()
                                                  encoding:[NSString defaultCStringEncoding]];
        
        NSString *ResultString = [NSString stringWithFormat:@"IFStatusImpl-> { %@ ver '%@', ver '%@'  service '%@' }",ndbVersion, nLoaderVersion, nuserAppVersion, nserviceAppVersion];
        
        
        NSNumber *nsrespid = [NSNumber numberWithInt:respID];
        id_command_queue[nsrespid] = ResultString;

    }
    catch (...)
    {
        
    }
}

void CDAT_LibNet::onAvailableDiagParameters(int respID, const std::list<TDataCatalogEntry>& l, std::string & rspAsString)
{
    try {
        printf("We got available diag parameters : %s\n", rspAsString.c_str());
        NSString *nsres = [NSString stringWithCString:rspAsString.c_str()
                                             encoding:[NSString defaultCStringEncoding]];
        NSNumber *nsrespid = [NSNumber numberWithInt:respID];
        id_command_queue[nsrespid] = nsres;

    }
    catch (...)
    {
    }
}

void CDAT_LibNet::onSwitchResponse(int respId)
{
    try {
        printf("We got switch response\n");
        NSString *nsres = @"none";
        NSNumber *nsrespid = [NSNumber numberWithInt:respId];
        id_command_queue[nsrespid] = nsres;

    }
    catch (...)
    {
    }
}



