//
//  ViewController.m
//  DongleApp
//
//  Created by David Olodovsky on 30/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import "ViewController.h"
#import "DATConnection.h"

#import "MyXmlParserDelegate.h"
#include "DATComm.hpp"

#include "DATUDPSocket.hpp"

extern NSMutableDictionary *id_command_queue;
using namespace dat;

#define FORMAT(format, ...) [NSString stringWithFormat:(format), ##__VA_ARGS__]

NSString *messageField = @"init filed";
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *Label;
@property (weak, nonatomic) IBOutlet UITextField *SimulatorIPTextFiled;
@property (weak, nonatomic) IBOutlet UIButton *Scenario;

@property (weak, nonatomic) IBOutlet UITextView *ScenarioOutput;
- (IBAction)SimulatorIPText:(id)sender;

@end
DATConnection *con;
NSString *SimulatorIPString = @"10.0.0.3";

@implementation ViewController
- (IBAction)SimulatorIpTextchanged:(id)sender {
   
    [_SimulatorIPTextFiled.window endEditing:YES];

    SimulatorIPString =_SimulatorIPTextFiled.text;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *finalPath = [path stringByAppendingPathComponent:@"info.plist"];
    NSMutableDictionary *dict =[NSMutableDictionary dictionaryWithContentsOfFile:finalPath];
    [dict setObject:SimulatorIPString forKey:@"simulatorip"];
    [dict writeToFile:finalPath atomically:YES];
    

}


- (NSString *) GetUTCDateTimeFromLocalTime:(NSString *)IN_strLocalTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate  *objDate    = [dateFormatter dateFromString:IN_strLocalTime];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSString *strDateTime   = [dateFormatter stringFromDate:objDate];
    return strDateTime;
}

- (void)GetCurrentTimeStamp
{
    NSDateFormatter *objDateformat = [[NSDateFormatter alloc] init];
    [objDateformat setDateFormat:@"yyyy-MM-dd"];
    NSString    *strTime = [objDateformat stringFromDate:[NSDate date]];
    NSString    *strUTCTime = [self GetUTCDateTimeFromLocalTime:strTime];//You can pass your date but be carefull about your date format of NSDateFormatter.
    NSDate *objUTCDate  = [objDateformat dateFromString:strUTCTime];
    long long milliseconds = (long long)([objUTCDate timeIntervalSince1970] * 1000.0);
    
    NSString *strTimeStamp = [NSString stringWithFormat:@"%lld",milliseconds];
    NSLog(@"The Timestamp is = %@",strTimeStamp);
}


- (void)send:(NSData *)data
{
    NSString *host =SimulatorIPString;
    if ([host length] == 0)
    {
        [self logError:@"Address required"];
        return;
    }
    
    int port = 11115;
    if (port <= 0 || port > 65535)
    {
        [self logError:@"Valid port required"];
        return;
    }
    
    [udpSocket sendData:data toHost:host port:port withTimeout:-1 tag:tag];
    
    tag++;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    
    NSLog(@"Ready");
}



- (void)logError:(NSString *)msg
{
    NSString *paragraph = [NSString stringWithFormat:@"%@\n", msg];
    _ScenarioOutput.text = paragraph;
}

- (void)logInfo:(NSString *)msg
{
    NSString *paragraph = [NSString stringWithFormat:@"%@\n", msg];
     _ScenarioOutput.text = paragraph;
}

- (void)logMessage:(NSString *)msg
{
    NSString *paragraph = [NSString stringWithFormat:@"%@\n", msg];
    _ScenarioOutput.text = paragraph;
  }

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    // You could add checks here
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    // You could add checks here
}

//(SEL) selector = "udpSocket:didReceiveData:fromAddress:withFilterContext:"


- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
      withFilterContext:(id)filterContext
{
    CDAT_LibNet * con = CDAT_LibNet::getInstance();
    char buffer[data.length];
    [data getBytes:buffer length: data.length];
 
    con->fromNet((const unsigned char*)buffer , data.length);
    return;
}

typedef void (^CaseBlock)();

- (void)handleSocket:(id) ref {
    
    DAT_UDPSocket * sock = DAT_UDPSocket::getInstance();
    while (1)
    {
        while (sock->is_connected() == 0)
        {
            [NSThread sleepForTimeInterval:1];
        }
        int status = sock->has_data();
        if (status > 0)
        {
            sock->read();
        }
        [NSThread sleepForTimeInterval:1];
    }


}

- (void)executionTest:(NSString *) testName {
    
    id_command_queue = [[NSMutableDictionary alloc] init];
    [_ScenarioOutput performSelectorOnMainThread:@selector(setText:)
                                      withObject: @""
                                   waitUntilDone:FALSE];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"[HH-mm-ss]"];
    

    __block NSString *outString1 = @"";
    __block NSString *outResult = @"";
    

    if ([TestValues objectForKey:testName] != nil) {
    
        NSMutableArray *apis = TestValues[testName];
        for (NSMutableDictionary *api in apis) {
            NSString *lookup = api[@"name"];
            NSDate *currentTime = [NSDate date];
            __block NSString *timeString = [dateFormatter stringFromDate: currentTime];
            
            NSDictionary *d = @{
                                @"getDongleSerialNumber":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling dongleSerialNumber=> \n"];
                                        outResult = [con dongleSerialNumber];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    },
                                
                                @"getAvailableDiagParameters":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling availableDiagParameters=> \n"];
                                        outResult = [con availableDiagParameters];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                     },
                                @"getDataObject":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling getDataObject => \n"];
                                        outResult = [con getDataObject:api[@"param_0"]];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString: outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    },
                                @"getDongleHWRevision":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling getDongleHWRevision=> \n"];
                                        outResult = [con dongleHWRevision];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                        
                                    },
                                @"isConnected":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling isConnected request=> \n"];
                                        BOOL res = [con connected];
                                        if (res)
                                            outResult = @"TRUE";
                                        else
                                            outResult = @"FALSE";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    },
                                @"isAuthenticatedByPIN":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling isAuthenticatedByPIN request=> \n"];
                                        BOOL res = [con authenticatedByPIN];
                                        if (res)
                                            outResult = @"TRUE";
                                        else
                                            outResult = @"FALSE";
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    },
                                @"isAuthenticatedByToken":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling isAuthenticatedByToken request=> \n"];
                                        BOOL res = [con authenticatedByToken];
                                        if (res)
                                            outResult = @"TRUE";
                                        else
                                            outResult = @"FALSE";
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];

                                    },
                                @"Connect":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling Connect=> \n"];
                                        outResult = [con connect:nil];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];

                                    },
                                @"Disconnect":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling Disconnect=> \n"];
                                        outResult = [con disconnect];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    },
                                @"authenticateByToken":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling AuthenticateByToken=> \n"];
                                        outResult = [con authenticateByToken:api[@"param_0"]];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    },
                                @"authenticateByPIN":
                                    ^{
                                        outString1= [timeString stringByAppendingString:@" Calling AuthenticateByPin=> \n"];
                                        outResult = [con authenticateByPIN:api[@"param_0"]];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    },
                                @"getFirmwareStatus":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling getFirmwareStatus=> \n"];
                                        outResult = [con firmwareStatus];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    },
                                @"subscribeObject":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling subscribeObject=> \n"];
                                        outResult = [con subscribeObject:api[@"param_0"] val:@"500"];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    },
                                @"unsubscribeObject":
                                    ^{
                                        outString1 = [timeString stringByAppendingString:@" Calling unsubscribeObject=> \n"];
                                        outResult = [con unsubscribeObject:api[@"param_0"]];
                                        if (outResult == nil) outResult = @"";
                                        outString1 = [outString1 stringByAppendingString:outResult];
                                        outString1 = [outString1 stringByAppendingString:@"\n"];
                                    }


                                
                                };
            
            if ([d objectForKey:lookup] != nil) {
                 ((CaseBlock)d[lookup])();
                 [self performSelectorOnMainThread:@selector(updateResultOutput:)
                    withObject: outString1
                    waitUntilDone:FALSE];
            }
            
        }
    }
   
    
}

- (void) updateResultOutput: (NSString *)addText {
    NSString *recentText = _ScenarioOutput.text;
    
    recentText = [recentText stringByAppendingString: addText];
    [_ScenarioOutput setText: recentText];
    
}

- (void)buttonPressed:(UIButton *)button {
    NSLog(@"Button Pressed %@", button.titleLabel.text);

    [NSThread detachNewThreadSelector:@selector(executionTest:) toTarget:self withObject:button.titleLabel.text];
 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSError *error = nil;

    
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"tests" ofType:@"xml" ];
    NSLog(@"\n\nthe string %@",filePath);
    // Load the file and check the result
    NSData *data = [NSData dataWithContentsOfFile:filePath
                                          options:NSDataReadingUncached
                                            error:&error];
    if(error) {
        NSLog(@"Error %@", error);
        _ScenarioOutput.text = @"xml parsing error";
        return;
    }
    
    TestNames = [[NSMutableArray alloc] init];
    TestValues = [[NSMutableDictionary alloc] init];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
    
    // Create an instance of our parser delegate and assign it to the parser
    MyXmlParserDelegate *parserDelegate = [[MyXmlParserDelegate alloc] init];
    [parser setDelegate:parserDelegate];
    
    // Invoke the parser and check the result
    [parser parse];
    error = [parser parserError];
    if(error)
    {
        NSLog(@"Error %@", error);
    }
    
    int i =0;
    for (NSString *testName in TestNames) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        [button setTitle:testName forState:UIControlStateNormal];
        [button sizeToFit];
        CGSize size = [testName sizeWithAttributes:
                      @{NSFontAttributeName: [UIFont systemFontOfSize:12.0f]}];
        button.center = CGPointMake(360/[TestNames count] +size.width*2 * i++, 70);
    
    // Add an action in current code file (i.e. target)
        [button addTarget:self action:@selector(buttonPressed:)
         forControlEvents:UIControlEventTouchUpInside];
    
        [self.view addSubview:button];
    }
    
    
    con = [[DATConnection alloc] init];
  
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *finalPath = [path stringByAppendingPathComponent:@"info.plist"];
    NSMutableDictionary *dict =[NSMutableDictionary dictionaryWithContentsOfFile:finalPath];
    if ([dict valueForKey:@"simulatorip"] !=nil)
    {
        SimulatorIPString =[dict valueForKey:@"simulatorip"];
    }
    _SimulatorIPTextFiled.text =SimulatorIPString;
    
     if (udpSocket == nil)
    {
       // udpSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
        
        udpSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:(id)self delegateQueue:dispatch_get_main_queue()];
        
        NSError *error = nil;
        
        if (![udpSocket bindToPort:0 error:&error])
        {
            NSLog(@"Error binding: %@", error);
            return;
        }
       
        if (![udpSocket beginReceiving:&error])
        {
            [self logError:FORMAT(@"Error receiving: %@", error)];
            return;
        }
        // [udpSocket receiveWithTimeout:-1 tag:0];
        CDAT_LibNet * con = CDAT_LibNet::getInstance();
        //con->setSockObj((void*)CFBridgingRetain(self));
        con->setSockObj((void*)CFBridgingRetain(self));
    }

    [NSThread detachNewThreadSelector:@selector(handleSocket:) toTarget:self withObject:nil];
    
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
