//
//  main.m
//  DongleApp
//
//  Created by David Olodovsky on 30/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

//#include <Connection.h>
//#include "DATComm.hpp"

//using namespace dat;

//extern dat_result_e dat_from_net(const unsigned char* buffer, size_t length);

int main(int argc, char * argv[]) {
    //CDAT_LibNet * con = CDAT_LibNet::getInstance();
    
    //Connection * con = new dat::Connection();
    //const unsigned char * buf = (const unsigned char * )"this is a test string";
    //size_t length = strlen((const char *) buf);
    //dat_result_e res = dat_from_net(buf, length);
    //int res = dat_from_net2(buf, length);
    //con->fromNet(buf, length);
    //con->send(buf, length);
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
