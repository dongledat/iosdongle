#ifndef DAT_HANDLE_REQUEST_H
#define DAT_HANDLE_REQUEST_H

#include "dat.h"

#ifdef __cplusplus
extern "C" {
#endif

dat_result_e dat_handle_request(dat_context_t* dat, const unsigned char* buffer, size_t length);
dat_result_e dat_build_request(dat_context_t* dat, const unsigned char* buffer, size_t length);

#ifdef __cplusplus
}
#endif

#endif