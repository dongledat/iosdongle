#include "dat_config.h"
#include "dat_txcm.h"
#include "dtp.h"
#include "log.h"
#include "os/os.h"
#include "malloc_prv.h"

void dat_connect();
void dat_disconnect();

static dat_txcm_t g_dat_txcm_copy;
static dat_txcm_t* g_dat_txcm = NULL;
static dat_context_t *g_dat_context;

void dat_txcm_init(dat_txcm_t* dat_txcm)
{
    memcpy(&g_dat_txcm_copy, dat_txcm, sizeof(g_dat_txcm_copy));
    g_dat_txcm = &g_dat_txcm_copy;
}

void dat_txcm_init_context(dat_context_t *dat_context)
{
    g_dat_context = dat_context;
}

void dat_txcm_done()
{
    g_dat_txcm = NULL;
}

static inline void *alloc_mem(size_t numOfBytes)
{
    return malloc_prv(numOfBytes);
}

static inline void free_mem(void* p)
{
    free_prv(p);
}

static void* alloc_resp(TXCM_CMND_ENUM cmd, uint32_t* numOfBytesIn)
{
    void* p;
    *numOfBytesIn = g_dat_txcm->txcmCmndCbk_common.p_GetMaxSizeOfTheResponse_cbk(cmd);
    p = alloc_mem(*numOfBytesIn);
    if (p)
        memset(p, '\0', *numOfBytesIn);
    return p;
}

static void free_resp(void* p)
{
    if (p)
        free_mem(p);
}

static dat_result_e send_response(dat_context_t *dat_context, const TDongleRsp * rsp)
{
	dat_result_e ret = DAT_ERROR;

	net_buf_t *net_buf = net_buf_alloc(DAT_MAX_PROTOBUF_MESSAGE_LEN);
	if (!net_buf)
		return DAT_ERROR;

	// Serialize response
	unsigned char *response_buffer = net_buf_put(net_buf, DAT_MAX_PROTOBUF_MESSAGE_LEN);
	if (!response_buffer)
		goto out;

	pb_ostream_t ostream = pb_ostream_from_buffer(response_buffer, DAT_MAX_PROTOBUF_MESSAGE_LEN);
	if (!pb_encode(&ostream, TDongleRsp_fields, rsp)) {
        LOG_ERROR("pb_encode failed");
		goto out;
    }

    net_buf_trim(net_buf, DAT_MAX_PROTOBUF_MESSAGE_LEN - ostream.bytes_written);
    dtp_add_header(net_buf);

    LOG("which_response=%d, length=%d", rsp->which_response, net_buf->length);

	// Send it out
	dat_context->dat_to_net_cb(net_buf->data, net_buf->length);
	ret = DAT_OK;

out:
    net_buf_free(net_buf);
	return ret;
}

static dat_result_e send_free(TDongleRsp* dat_resp)
{
    //send_response(g_dat_context, dat_resp);
    dat_from_car(dat_resp);

    if (dat_resp->which_response == TDongleRsp_GetDataObjectRsp_tag)
    {
        if (dat_resp->response.GetDataObjectRsp.DataObject.DataElement != NULL)
        {
            int i;

            for (i = 0; i < dat_resp->response.GetDataObjectRsp.DataObject.DataElement_count; i++)
            {
                if (dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].which_value == TDataElemenent_s_tag &&
                    dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].value.s != NULL)
                {
                    free_mem(dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].value.s);
                }
            }

            free_mem(dat_resp->response.GetDataObjectRsp.DataObject.DataElement);
        }
    }
        
    else if (dat_resp->which_response == TDongleRsp_PDUNotify_tag)
    {
        int i;

        for (i = 0; i < dat_resp->response.PDUNotify.PDUResponse_count; i++)
        {
            if (dat_resp->response.PDUNotify.PDUResponse[i].DataField != NULL)
                free_mem(dat_resp->response.PDUNotify.PDUResponse[i].DataField);
        }
    }

    return DAT_OK;
}

static dat_result_e send_error(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    dat_resp->has_id = 1;
    dat_resp->id = dat_req->id;
    dat_resp->which_response = TDongleRsp_ErrorRsp_tag;
    dat_resp->response.ErrorRsp.Reason = TErrorRsp_EReason_MemoryAllocation; // TODO: support different reasons

    LOG("Sending error response");
    //send_response(g_dat_context, dat_resp);
    dat_from_car(dat_resp);

    return DAT_OK;
}

static void _txcm_StartDiag()
{
    (*g_dat_txcm->txcmServiceCbk.p_StartDiag_cbk)();
}

static void _txcm_StopDiag()
{
    (*g_dat_txcm->txcmServiceCbk.p_StartDiag_cbk)();
}

static dat_result_e _txcm_scheduler_bt_connection()
{
    LOG("BT Connection");
    // TODO: Do nothing. Diagnostic service has to be started after successful authenatication.
	dat_connect();
    _txcm_StartDiag();
    return DAT_OK;
}

static dat_result_e _txcm_scheduler_bt_disconnection()
{
    LOG("BT Disconnection");
	dat_disconnect();
    _txcm_StopDiag();
    return DAT_OK;
}

static dat_result_e _txcm_scheduler_diag_status_is_changed(TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;

    TXCM_DIAG_STATUS_PAYLOAD_NOTIFICATION* resp = alloc_resp(TXCM_DIAG_STATUS_CODE_NOTIFICATION, &numOfBytesIn);
    if (!resp)
        return DAT_ERROR;

    TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_notification.p_DiagStatusNotification_cbk)(resp, numOfBytesIn, &numOfBytesOut);

    if (error != TXCM_CMND_OK)
    {
        free_resp(resp);
        return DAT_ERROR;
    }

    dat_resp->has_id = 0;
    dat_resp->which_response = TDongleRsp_DiagStatusRsp_tag;
    dat_resp->response.DiagStatusRsp.DiagnosticStatus = resp->diagnosticStatus + 1;	// TODO: switch-case
	
	free_resp(resp);
	
    return DAT_OK;
}

static dat_result_e _txcm_convert_diag_url(TXCM_DIAG_URL_VALUE* resp, TDongleRsp* dat_resp)
{
    dat_result_e res = DAT_ERROR;

    dat_resp->which_response = TDongleRsp_GetDataObjectRsp_tag;
    dat_resp->response.GetDataObjectRsp.DataObject.UrlCode = resp->urlCode;
    dat_resp->response.GetDataObjectRsp.DataObject.DataElement_count = resp->nAttributes;

    if ((dat_resp->response.GetDataObjectRsp.DataObject.DataElement = alloc_mem(resp->nAttributes * sizeof(TDataElemenent))) != NULL)
    {
        int i;

        for (i = 0; i < resp->nAttributes; i++)
        {
            dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].code = resp->attributes[i].code;
            dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].status = (int8_t)resp->attributes[i].status;

            switch (resp->attributes[i].type)
            {
            case TXCM_Float:
                if (resp->attributes[i].valueSize == sizeof(float))
                {
                    dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].which_value = TDataElemenent_f_tag;
                    dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].value.f = *((float*)resp->attributes[i].value);
                    res = DAT_OK;
                }
                break;

            case TXCM_Text:
                if ((dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].value.s = alloc_mem(resp->attributes[i].valueSize + sizeof(pb_size_t))) != NULL)
                {
                    dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].which_value = TDataElemenent_s_tag;
                    dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].value.s->size = resp->attributes[i].valueSize + 1;
                    memcpy(dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].value.s->bytes, resp->attributes[i].value, resp->attributes[i].valueSize);
                    dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].value.s->bytes[resp->attributes[i].valueSize] = '\0';
                    res = DAT_OK;
                }
                break;

            case TXCM_Boolean:
                dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].which_value = TDataElemenent_b_tag;
                dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].value.b = resp->attributes[i].value[0];
                res = DAT_OK;
                break;

            case TXCM_Enum:
                if (resp->attributes[i].valueSize == sizeof(int))
                {
                    dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].which_value = TDataElemenent_i_tag;
                    dat_resp->response.GetDataObjectRsp.DataObject.DataElement[i].value.i = *((int*)resp->attributes[i].value);
                    res = DAT_OK;
                }
                break;
            }
        }
    }

    return res;
}

static dat_result_e _txcm_scheduler_diag_url_is_changed(TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    dat_result_e res;

    TXCM_DIAG_URL_PAYLOAD_NOTIFICATION* resp = alloc_resp(TXCM_DIAG_URL_CODE_NOTIFICATION, &numOfBytesIn);
    if (!resp)
        return DAT_ERROR;

    TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_notification.p_DiagUrlNotification_cbk)(resp, numOfBytesIn, &numOfBytesOut);
    if (error != TXCM_CMND_OK)
    {
        free_resp(resp);
        return DAT_ERROR;
    }

    dat_resp->has_id = 0;
    res = _txcm_convert_diag_url(resp, dat_resp);
        
    free_resp(resp);

    return res;
}

static dat_result_e _txcm_scheduler_availbale_diag_parameters_are_changed(TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_ADP_ARE_CHANGED_PAYLOAD_NOTIFICATION* resp = alloc_resp(TXCM_AVAILABLE_DIAG_PARAMETERS_ARE_CHANGED_CODE_NOTIFICATION, &numOfBytesIn);;
    if (!resp)
        return DAT_ERROR;

    TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_notification.p_ADP_AreChangedNotification_cbk)(resp, numOfBytesIn, &numOfBytesOut);
    if (error != TXCM_CMND_OK)
    {
        free_resp(resp);
        return DAT_ERROR;
    }

    dat_resp->has_id = 0;
    dat_resp->which_response = TDongleRsp_AvailableDiagParametersAreChangedRsp_tag;
	free_resp(resp);
	
    return DAT_OK;
}

static dat_result_e _txcm_scheduler_diag_pdu_is_available(TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    unsigned i;
    TXCM_PDU_PAYLOAD_NOTIFICATION* resp = alloc_resp(TXCM_PDU_CODE_NOTIFICATION, &numOfBytesIn);;
    if (!resp)
        return DAT_ERROR;

    TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_notification.p_PduNotification_cbk)(resp, numOfBytesIn, &numOfBytesOut);
    if (error != TXCM_CMND_OK)
    {
        free_resp(resp);
        return DAT_ERROR;
    }

    dat_resp->has_id = 1;
    dat_resp->id = resp->ID;
    dat_resp->which_response = TDongleRsp_PDUNotify_tag;
    dat_resp->response.PDUNotify.ErrorCode = resp->ErrorCode;
    dat_resp->response.PDUNotify.PDUResponse_count = resp->nPDUResponse;

	uint32_t spaceConst_PDUResponse = sizeof(resp->PDUResponse[0]) - sizeof(resp->PDUResponse[0].DataField[0]);
	TXCM_DIAG_PDUResponse *p_txcm_PDUResponse = &resp->PDUResponse[0];
		
    // TODO: check it is < 256
    for (i = 0; i < resp->nPDUResponse; i++)
    {
        int data_size;
        dat_resp->response.PDUNotify.PDUResponse[i].CANID = resp->PDUResponse[i].CANID;
        dat_resp->response.PDUNotify.PDUResponse[i].ErrorCode = resp->PDUResponse[i].ErrorCode;
        dat_resp->response.PDUNotify.PDUResponse[i].PDUIndex = resp->PDUResponse[i].PDUIndex;
        dat_resp->response.PDUNotify.PDUResponse[i].Timestamp = resp->PDUResponse[i].Timestamp;
        // TODO: check alloc is OK
        data_size = p_txcm_PDUResponse->DataFieldLenght;
        dat_resp->response.PDUNotify.PDUResponse[i].DataField = alloc_mem(sizeof(pb_size_t) + data_size);
        dat_resp->response.PDUNotify.PDUResponse[i].DataField->size = data_size;
        memcpy(dat_resp->response.PDUNotify.PDUResponse[i].DataField->bytes, p_txcm_PDUResponse->DataField, data_size);
		
		p_txcm_PDUResponse = (TXCM_DIAG_PDUResponse *)((uint32_t)p_txcm_PDUResponse + spaceConst_PDUResponse + p_txcm_PDUResponse->DataFieldLenght);
    }

	free_resp(resp);
    return DAT_OK;
}

int dat_from_txcm_scheduler(TXCM_SCHEDULER_EVENTS events)
{
    TDongleRsp dat_resp = TDongleRsp_init_zero;
    dat_result_e res;
    
    LOG("event: %d", events);

    switch (events) {
    case TXCM_SCHEDULER_BT_CONNECTION:
        res = _txcm_scheduler_bt_connection();
        break;

    case TXCM_SCHEDULER_BT_DISCONNECTION:
        res = _txcm_scheduler_bt_disconnection();
        break;

    case TXCM_SCHEDULER_DIAG_STATUS_IS_CHANGED:
        res = _txcm_scheduler_diag_status_is_changed(&dat_resp);
        break;

    case TXCM_SCHEDULER_DIAG_URL_IS_CHANGED:
        res = _txcm_scheduler_diag_url_is_changed(&dat_resp);
        break;

    case TXCM_SCHEDULER_AVAILBALE_DIAG_PARAMETERS_ARE_CHANGED:
        res = _txcm_scheduler_availbale_diag_parameters_are_changed(&dat_resp);
        break;

    case TXCM_SCHEDULER_DIAG_PDU_IS_AVAILABLE:
        res = _txcm_scheduler_diag_pdu_is_available(&dat_resp);
        break;

    default:
        res = DAT_ERROR;
    }

    // send only valid responses
    if (res == DAT_OK && dat_resp.which_response)
        res = send_free(&dat_resp);

    return res;
}

static dat_result_e _txcm_GetFwStatusRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_GET_FW_STATUS_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_GET_FW_STATUS_CODE_REQUEST, &numOfBytesIn);
    TXCM_GET_FW_STATUS_PAYLOAD_REQUEST req = {0};
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_GetFwStatusRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
            dat_resp->which_response = TDongleRsp_FwStatusRsp_tag;
            memcpy(dat_resp->response.FwStatusRsp.Ver_serviceApp.ver, resp->ver_serviceApp, TXCM_CMND_FW_VER_SIZE);
            memcpy(dat_resp->response.FwStatusRsp.Ver_uLoader.ver, resp->ver_uLoader, TXCM_CMND_FW_VER_SIZE);
            memcpy(dat_resp->response.FwStatusRsp.Ver_userApp.ver, resp->ver_userApp, TXCM_CMND_FW_VER_SIZE);
            dat_resp->response.FwStatusRsp.ver_db = resp->ver_db;
            dat_resp->response.FwStatusRsp.IDBrand = resp->IDBrand;
            dat_resp->response.FwStatusRsp.IDGroup = resp->IDGroup;
            dat_resp->response.FwStatusRsp.updateInProgress = resp->updateInProgress;

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_GetDiagStatusRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_GET_DIAG_STATUS_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_GET_DIAG_STATUS_CODE_REQUEST, &numOfBytesIn);
    TXCM_GET_DIAG_STATUS_PAYLOAD_REQUEST req = {0};
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_GetDiagStatusRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
            dat_resp->which_response = TDongleRsp_DiagStatusRsp_tag;
            dat_resp->response.DiagStatusRsp.DiagnosticStatus = resp->diagnosticStatus + 1;

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_GetActivationStatusRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_GET_ACTIVATION_STATUS_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_GET_ACTIVATION_STATUS_CODE_REQUEST, &numOfBytesIn);
    TXCM_GET_ACTIVATION_STATUS_PAYLOAD_REQUEST req = {0};
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_GetActivationStatusRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
            dat_resp->which_response = TDongleRsp_ActivationStatusRsp_tag;
            memcpy(dat_resp->response.ActivationStatusRsp.ActivationDate.Date, resp->activationDate, TXCM_DATE_ISO8601_SIZE);
            memcpy(dat_resp->response.ActivationStatusRsp.ProductionDate.Date, resp->productionDate, TXCM_DATE_ISO8601_SIZE);

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_GetSerialNumberDeviceRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_GET_DEVICE_INFO_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_GET_DEVICE_INFO_CODE_REQUEST, &numOfBytesIn);
    TXCM_GET_DEVICE_INFO_PAYLOAD_REQUEST req = {0};
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_GetDeviceInfoRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
            dat_resp->which_response = TDongleRsp_SerialNumberDeviceRsp_tag;
            memcpy(dat_resp->response.SerialNumberDeviceRsp.SerialNumber, resp->serialNumber, TXCM_SERIAL_NUMBER_STRING_SIZE);

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_GetBtMacAddressRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_GET_DEVICE_INFO_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_GET_DEVICE_INFO_CODE_REQUEST, &numOfBytesIn);
    TXCM_GET_DEVICE_INFO_PAYLOAD_REQUEST req = {0};
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_GetDeviceInfoRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
            dat_resp->which_response = TDongleRsp_BtMacAddressRsp_tag;
            memcpy(dat_resp->response.BtMacAddressRsp.Mac, resp->mac, TXCM_BT_MAC_ADDRESS_STRING_SIZE);

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_GetHwVersionRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_GET_DEVICE_INFO_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_GET_DEVICE_INFO_CODE_REQUEST, &numOfBytesIn);
    TXCM_GET_DEVICE_INFO_PAYLOAD_REQUEST req = {0};
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_GetDeviceInfoRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
            dat_resp->which_response = TDongleRsp_HwVersionRsp_tag;
            memcpy(dat_resp->response.HwVersionRsp.Board, resp->board, TXCM_BOARD_STRING_SIZE);

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_GetDataObjectRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_GET_DIAG_URL_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_GET_DIAG_URL_CODE_REQUEST, &numOfBytesIn);
    TXCM_GET_DIAG_URL_PAYLOAD_REQUEST req = {0};
    dat_result_e res = DAT_ERROR;

    req.urlCode = dat_req->request.GetDataObjectReq.UrlCode;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_GetDiagUrlRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
            res = _txcm_convert_diag_url(resp, dat_resp);
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_SubscribeDiagUrlRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_SUBSCRIBE_DIAG_URL_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_SUBSCRIBE_DIAG_URL_CODE_REQUEST, &numOfBytesIn);
    TXCM_SUBSCRIBE_DIAG_URL_PAYLOAD_REQUEST req = {0};
    dat_result_e res = DAT_ERROR;

    req.urlCode = dat_req->request.SubscribeObjectReq.UrlCode;
    req.iVal = dat_req->request.SubscribeObjectReq.Timeout;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_SubscribeDiagUrlRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
			dat_resp->which_response = TDongleRsp_SubscribeObjectRsp_tag;

            switch (resp->result)
            {
            case TXCM_SUBSCRIBTION_IS_OK:
                dat_resp->response.SubscribeObjectRsp.result = TSubscribeObjectRsp_EResult_OK;
                res = DAT_OK;
                break;

            case TXCM_SUBSCRIBTION_URL_CANNOT_BE_SUBSCRIBED:
                dat_resp->response.SubscribeObjectRsp.result = TSubscribeObjectRsp_EResult_UrlCannotBeSubscribed;
                res = DAT_OK;
                break;

            case TXCM_SUBSCRIBTION_NO_MATCHING_URL:
                dat_resp->response.SubscribeObjectRsp.result = TSubscribeObjectRsp_EResult_NoMatchingUrl;
                res = DAT_OK;
                break;
            }
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_UnSubscribeDiagUrlRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_UNSUBSCRIBE_DIAG_URL_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_UNSUBSCRIBE_DIAG_URL_CODE_REQUEST, &numOfBytesIn);
    TXCM_UNSUBSCRIBE_DIAG_URL_PAYLOAD_REQUEST req;
    dat_result_e res = DAT_ERROR;

    req.urlCode = dat_req->request.SubscribeObjectReq.UrlCode;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_UnSubscribeDiagUrlRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
			dat_resp->which_response = TDongleRsp_UnsubscribeObjectRsp_tag;

            switch (resp->result)
            {
            case TXCM_UNSUBSCRIBTION_IS_OK:
                dat_resp->response.SubscribeObjectRsp.result = TUnsubscribeObjectRsp_EResult_OK;
                res = DAT_OK;
                break;

            case TXCM_UNSUBSCRIBTION_URL_CANNOT_BE_UNSUBSCRIBED:
                dat_resp->response.SubscribeObjectRsp.result = TUnsubscribeObjectRsp_EResult_UrlCannotBeUnsubscribed;
                res = DAT_OK;
                break;

            case TXCM_UNSUBSCRIBTION_NO_MATCHING_URL:
                dat_resp->response.SubscribeObjectRsp.result = TUnsubscribeObjectRsp_EResult_NoMatchingUrl;
                res = DAT_OK;
                break;
            }
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_GetAvailableDiagnosticParametersRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_GET_AVAILABLE_DIAG_URL_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_GET_AVAILABLE_DIAG_URL_CODE_REQUEST, &numOfBytesIn);
    TXCM_GET_AVAILABLE_DIAG_URL_PAYLOAD_REQUEST req;
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_GetAvailableDiagUrlRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        if (TXCM_CMND_OK == error)
        {
            if (resp->nUrlCodes <= 256)
            {
                unsigned i;

				dat_resp->which_response = TDongleRsp_GetAvailableDiagnosticParametersRsp_tag;
                dat_resp->has_id = 1;
                dat_resp->id = dat_req->id;
                dat_resp->response.GetAvailableDiagnosticParametersRsp.UrlCode.size = resp->nUrlCodes;
                dat_resp->response.GetAvailableDiagnosticParametersRsp.IsSubscribed.size = resp->nUrlCodes;

                for (i = 0; i < resp->nUrlCodes; i++)
                {
                    dat_resp->response.GetAvailableDiagnosticParametersRsp.UrlCode.bytes[i] = resp->urlCode[i].urlCode;
                    dat_resp->response.GetAvailableDiagnosticParametersRsp.IsSubscribed.bytes[i] = resp->urlCode[i].isSubscribed;
                }

                res = DAT_OK;
            }
        }

        free_resp(resp);
    }

    return res;
}

// Not supported by PROTOBUF
#if 0
static dat_result_e _txcm_GetSupportedDiagUrlRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_GET_SUPPORTED_DIAG_URL_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_GET_SUPPORTED_DIAG_URL_CODE_REQUEST, &numOfBytesIn);
    TXCM_GET_SUPPORTED_DIAG_URL_PAYLOAD_REQUEST req;
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_GetSupportedDiagUrlRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        // TODO: translate resp to dat_resp
        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}
#endif

static dat_result_e _txcm_OpenUpdatingSessionRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_OPEN_UPDATING_SESSION_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_OPEN_UPDATING_SESSION_CODE_REQUEST, &numOfBytesIn);
    TXCM_OPEN_UPDATING_SESSION_PAYLOAD_REQUEST req;
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_OpenUpdatingSessionRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        // TODO: translate resp to dat_resp
        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
			dat_resp->which_response = TDongleRsp_OpenUpdatingSessionRsp_tag;

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_DownloadDataBlockRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_DOWNLOAD_DATA_BLOCK_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_DOWNLOAD_DATA_BLOCK_CODE_REQUEST, &numOfBytesIn);
    TXCM_DOWNLOAD_DATA_BLOCK_PAYLOAD_REQUEST req;
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_DownloadDataBlockRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        // TODO: translate resp to dat_resp
        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
			dat_resp->which_response = TDongleRsp_DownloadDataBlockRsp_tag;

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_CloseUpdatingSessionRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_CLOSE_UPDATING_SESSION_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_CLOSE_UPDATING_SESSION_CODE_REQUEST, &numOfBytesIn);
    TXCM_CLOSE_UPDATING_SESSION_PAYLOAD_REQUEST req;
    dat_result_e res = DAT_ERROR;

    if (resp != NULL)
    {
        TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_CloseUpdatingSessionRequest_cbk)(&req, resp, numOfBytesIn, &numOfBytesOut);

        // TODO: translate resp to dat_resp
        if (TXCM_CMND_OK == error)
        {
            dat_resp->has_id = 1;
            dat_resp->id = dat_req->id;
			dat_resp->which_response = TDongleRsp_CloseUpdatingSessionRsp_tag;

            res = DAT_OK;
        }

        free_resp(resp);
    }

    return res;
}

#include "dat_txcm_pdu.h"
static dat_result_e _txcm_PduRequest(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesIn = 0, numOfBytesOut = 0;
    TXCM_PDU_PAYLOAD_RESPONSE* resp = alloc_resp(TXCM_PDU_CODE_REQUEST, &numOfBytesIn);
    dat_result_e res = DAT_ERROR;
	
	
    if (resp != NULL)
    {
        TXCM_CMND_ERROR error;
        int i;
		
		uint16_t nCommParams = dat_req->request.PDUReq.commParams_count;
		uint16_t nPDU = dat_req->request.PDUReq.dynamicList_count;        
		TXCM_PDU_REFERENCE pduRef = dat_txcm_pdu_getReferences(nCommParams, nPDU);
		
		if ( pduRef.req )
		{			
			pduRef.req->ID = dat_req->id;
			// TODO: use switch case instead
			pduRef.req->Pins = dat_req->request.PDUReq.Pins - 1;
			pduRef.req->Protocol = dat_req->request.PDUReq.Protocol - 1;
			pduRef.req->StartingTimeOut = dat_req->request.PDUReq.StartingTimeOut;
			pduRef.req->nCommParams = dat_req->request.PDUReq.commParams_count;
			pduRef.req->nPDU = dat_req->request.PDUReq.dynamicList_count;
			
			for (i = 0; i < pduRef.req->nCommParams; i++)
			{
				pduRef.pComPar[i].ID = dat_req->request.PDUReq.commParams[i].ID;
				pduRef.pComPar[i].Value = dat_req->request.PDUReq.commParams[i].Value;
			}

			for (i = 0; i < pduRef.req->nPDU; i++)
			{
				pduRef.pPDURequests[i].CP_P2Max = dat_req->request.PDUReq.dynamicList[i].CP_P2Max;
				pduRef.pPDURequests[i].CP_RepeatReqCountApp = dat_req->request.PDUReq.dynamicList[i].CP_RepeatReqCountApp;
				pduRef.pPDURequests[i].ContinueIfFail = dat_req->request.PDUReq.dynamicList[i].ContinueIfFail;
				pduRef.pPDURequests[i].index = dat_req->request.PDUReq.dynamicList[i].index;
				pduRef.pPDURequests[i].DataFieldLenght = (uint8_t)dat_req->request.PDUReq.dynamicList[i].DataField.size;
				pduRef.pPDURequests[i].reserved_1 = 0;
				pduRef.pPDURequests[i].reserved_2 = 0;
				// TODO: check data size buffer
				memcpy(pduRef.pPDURequests[i].DataField, dat_req->request.PDUReq.dynamicList[i].DataField.bytes, dat_req->request.PDUReq.dynamicList[i].DataField.size);
			}

			error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_PduRequest)(pduRef.req, resp, sizeof(TXCM_PDU_PAYLOAD_RESPONSE), &numOfBytesOut);

			if (TXCM_CMND_OK == error)
			{
				dat_resp->has_id = 1;
				dat_resp->id = dat_req->id;
				dat_resp->which_response = TDongleRsp_PDURsp_tag;
				dat_resp->response.PDURsp.Result = resp->result;

				res = DAT_OK;
			}
			dat_txcm_pdu_removeRequest(pduRef.req);	
		}
				
        free_resp(resp);
    }

    return res;
}

static dat_result_e _txcm_ServiceApp(const TDongleReq* dat_req, TDongleRsp* dat_resp)
{
    uint32_t numOfBytesOut = 0;
    TXCM_REBOOT_TO_SERVICE_APP_PAYLOAD_REQUEST req;
    TXCM_REBOOT_TO_SERVICE_APP_PAYLOAD_RESPONSE resp;
    dat_result_e res = DAT_ERROR;

    TXCM_CMND_ERROR error = (*g_dat_txcm->txcmCmndCbk_clientRequest.p_RebootToServiceAppRequest)(&req, &resp, sizeof(TXCM_REBOOT_TO_SERVICE_APP_PAYLOAD_RESPONSE), &numOfBytesOut);

    if (TXCM_CMND_OK == error)
    {
        dat_resp->has_id = 1;
        dat_resp->id = dat_req->id;
        dat_resp->which_response = TDongleRsp_ServiceAppRsp_tag;

        res = DAT_OK;
    }

    return res;
}

dat_result_e dat_txcm_cb(const TDongleReq* req, TDongleRsp* resp)
{
    dat_result_e res;

    LOG("which_request = %d", req->which_request);

    switch (req->which_request)
    {
    case TDongleReq_FwStatusReq_tag:
        res = _txcm_GetFwStatusRequest(req, resp);
        break;

    case TDongleReq_DiagStatusReq_tag:
        res = _txcm_GetDiagStatusRequest(req, resp);
        break;

    case TDongleReq_ActivationStatusReq_tag:
        res = _txcm_GetActivationStatusRequest(req, resp);
        break;

    case TDongleReq_SerialNumberDeviceReq_tag:
        res = _txcm_GetSerialNumberDeviceRequest(req, resp);
        break;

    case TDongleReq_BtMacAddressReq_tag:
        res = _txcm_GetBtMacAddressRequest(req, resp);
        break;

    case TDongleReq_HwVersionReq_tag:
        res = _txcm_GetHwVersionRequest(req, resp);
        break;

    case TDongleReq_GetDataObjectReq_tag:
        res = _txcm_GetDataObjectRequest(req, resp);
        break;

    case TDongleReq_SubscribeObjectReq_tag:
        res = _txcm_SubscribeDiagUrlRequest(req, resp);
        break;

    case TDongleReq_UnsubscribeObjectReq_tag:
        res = _txcm_UnSubscribeDiagUrlRequest(req, resp);
        break;

    case TDongleReq_GetAvailableDiagnosticParametersReq_tag:
        res = _txcm_GetAvailableDiagnosticParametersRequest(req, resp);
        break;

    case TDongleReq_OpenUpdatingSessionReq_tag:
        res = _txcm_OpenUpdatingSessionRequest(req, resp);
        break;

    case TDongleReq_DownloadDataBlockReq_tag:
        res = _txcm_DownloadDataBlockRequest(req, resp);
        break;

    case TDongleReq_CloseUpdatingSessionReq_tag:
        res = _txcm_CloseUpdatingSessionRequest(req, resp);
        break;

    case TDongleReq_PDUReq_tag:
        res = _txcm_PduRequest(req, resp);
        break;
        
    case TDongleReq_ServiceAppReq_tag:
        res = _txcm_ServiceApp(req, resp);
        break;

    default:
        res = DAT_ERROR;
        break;
    }

    return (DAT_OK == res) ? send_free(resp) :
                             send_error(req, resp);
}
