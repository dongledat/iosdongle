#include "dat.h"
#include "dat_handle_request.h"
#include "DongleStateMachine.h"
#include "check_type.h"
#include "log.h"
#include "dtp.h"
#include "dat_txcm.h"

typedef struct 
{
	DongleStateMachine sm;
} dat_private_t;

CHECK_TYPE_SIZE_LE(dat_private_t, DAT_PRIVATE_SIZE);

int handle_response(dat_context_t* dat, TDongleRsp * rsp);
int send_response(dat_context_t* dat, TDongleRsp * rsp);
int handle_pdu_response(dat_context_t* dat, TDongleRsp * rsp);

// TODO: Get rid of this global variables
extern "C" dtp_t *dtp_req;
extern "C" int g_dtp_enabled;

dat_result_e dat_build_request(dat_context_t* dat, const unsigned char* buffer, size_t length)
{
	dat_result_e res;

#ifdef DAT_ENABLE_DTP
	if (g_dtp_enabled) {

		// TODO: move to dat_init
		if (!dtp_req) {
			dtp_req = dtp_alloc(DTP_BUFFER_SIZE);
			if (!dtp_req) {
				//printf("dtp_init failed");
				return DAT_ERROR;
			}
		}

		if (dtp_from_net(dtp_req, buffer, length) != DTP_OK) {
			res = DAT_ERROR;
			goto free_dtp;
		}

		// dtp wasn't completed yet - for for further data
		if (!dtp_req->done)
			return DAT_OK;

		res = dat_handle_request(dat, dtp_data(dtp_req), dtp_len(dtp_req));

	free_dtp:
		dtp_free(dtp_req);
		dtp_req = NULL;

	}
	else {
		res = dat_handle_request(dat, buffer, length);
	}
#else

	res = dat_handle_request(dat, buffer, length);

#endif

	return res;
}

dat_result_e dat_handle_request(dat_context_t* dat, const unsigned char* buffer, size_t length)
{
	// Deserialize request
	TDongleReq req = TDongleReq_init_zero;
	pb_istream_t stream = pb_istream_from_buffer(buffer, length);
	if (!pb_decode(&stream, TDongleReq_fields, &req)) {
        LOG_ERROR("pb_decode failed");
		return DAT_ERROR;
	}

	// Pass the request to the "car" and get the car's response
	TDongleRsp rsp = TDongleRsp_init_zero;
#if defined(_WIN32)
    dat->dat_to_car_cb(&req, &rsp);
#else
    dat_txcm_cb(&req, &rsp);
#endif

    // Request is done - release it
	pb_release(TDongleReq_fields, &req);

    // TODO: don't use this macro. Instead OS_WINDOWS
#if defined(_WIN32)
	// We have a ready response - send it
	return dat_from_car(&rsp);
#else
    return DAT_OK;
#endif
}

#if 0
int handle_response(dat_context_t* dat, TDongleRsp * rsp)
{
	int status = DAT_ERROR;

	switch (rsp->which_response)
	{
	case TDongleRsp_PDURsp_tag:
		//status = handle_pdu_response(dat, rsp);
		//break;

	//TODO

	default:
		status = send_response(dat, rsp);
	}

	return status;
}

int send_response(dat_context_t* dat, TDongleRsp * rsp)
{
	// Serialize response
	unsigned char response_buffer[DAT_MAX_PROTOBUF_MESSAGE_LEN] = { 0 };
	pb_ostream_t ostream = pb_ostream_from_buffer(response_buffer, DAT_MAX_PROTOBUF_MESSAGE_LEN);
	if (!pb_encode(&ostream, TDongleRsp_fields, rsp)) {
		return DAT_ERROR;
	}

	// Send it out
	dat->dat_to_net_cb(response_buffer, ostream.bytes_written);

	return DAT_OK;
}
#endif
