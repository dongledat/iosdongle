#ifndef STDBOOL_H
#define STDBOOL_H

#ifndef __cplusplus

#ifndef false
#define false   0
#endif
#ifndef true
#define true    1
#endif

#ifndef bool
#define bool int
#endif
#endif

#endif
