#ifndef _CONFIG_H_
#define _CONFIG_H_


/////////////////////////////////////////////////////////////////
//////////////////// Configuration //////////////////////////////
/////////////////////////////////////////////////////////////////

// Enables DTP
#define DAT_ENABLE_DTP

// Enables TLS
//#define DAT_ENABLE_TLS 

//TODO: Which PSK mode to use
//#define DAT_TLS_PSK_MODE_PSK
//#define DAT_TLS_PSK_MODE_DH_PSK
//#define DAT_TLS_PSK_MODE_ECDHE_PSK

// If enabled, failure to decode a protobuf will cause dongle to terminate the connection
//#define DAT_TERMINATE_ON_BAD_PROTOBUF

// Do not change at the moment: non-dynamic protobuf not tested/supported
#define DAT_DYNAMIC_PROTOBUF

// Max protobuf size
#define DAT_MAX_PROTOBUF_MESSAGE_LEN (1024)

//TODO: Add more sizing constraints, for example for PDU etc...

#define DAT_PSK_IDENTITY ("PSKIdentity")
#define DAT_PSK_SECRET 0xDE, 0xAD, 0xBE, 0xEF


////////////////////////////////////////////////////////////
//////////////////// Internal definitions //////////////////
////////////////////////////////////////////////////////////

#ifdef DAT_ENABLE_DTP

	#define DTP_OVERHEAD (4) // Size
	#define DTP_BUFFER_SIZE (DAT_MAX_PROTOBUF_MESSAGE_LEN + DTP_OVERHEAD)

	// DTP is just for testing; Anyway, each DAT message is wrapped with DTP, 
	// so buffer must include space for that
	#define DAT_SESSION_LAYER_MESSAGE_SIZE (DTP_BUFFER_SIZE)

#else

	// Each DAT message is simply a single protobuf
	#define DAT_SESSION_LAYER_MESSAGE_SIZE (DAT_MAX_MESSAGE_LEN)

#endif

#ifdef DAT_ENABLE_TLS
	
	// This is how much we can read from the SSL layer at any point.
	// Since we don't buffer plaintext data, we have to read/write 
	// entire messages each time
	#define MBEDTLS_SSL_MAX_CONTENT_LEN (DAT_SESSION_LAYER_MESSAGE_SIZE)
	#define DAT_TLS_BUFFER_SIZE (MBEDTLS_SSL_MAX_CONTENT_LEN)

#endif

#ifdef DAT_DYNAMIC_PROTOBUF
#define PB_ENABLE_MALLOC
#endif



#endif /* _CONFIG_H_ */
