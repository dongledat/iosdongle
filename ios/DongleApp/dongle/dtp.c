#include <string.h>
#include "dtp.h"
#include "crypto/dat_crc.h"
#include "types.h"
#include "malloc_prv.h"
#include "os/os.h"

#define CRC16

// TODO: put it in os.h
#ifdef __ANDROID__
#include <arpa/inet.h>
#else
#if defined(__IAR_SYSTEMS_ICC__)
// TODO: use actual ntohs implementation / check byte order
#define ntohs(s) ((((s) & 0xff) << 8) | ((s) >> 8))
#define htons ntohs
#endif

#endif //__ANDROID__

#define DTP_MAGIC 0x445e // "D^"

typedef struct
{
    uint16_t len;
	uint16_t magic;

} dtp_preamble_t;

dtp_t *dtp_alloc(unsigned short size)
{
	dtp_t *dtp = malloc_prv(sizeof(*dtp));
	if (!dtp)
		return NULL;

	dtp->net_buf = net_buf_alloc(size + sizeof(dtp_preamble_t));
	if (!dtp->net_buf) {
		free_prv(dtp);
		return NULL;
	}

	dtp->done = false;

    return dtp;
}

void dtp_free(dtp_t *dtp)
{
	if (dtp) {
		if (dtp->net_buf) {
			net_buf_free(dtp->net_buf);
			dtp->net_buf = NULL;
		}

		free_prv(dtp);
	}
}

dtp_result_e dtp_from_net(dtp_t *dtp, const void *buf, unsigned size)
{
	net_buf_t *net_buf = dtp->net_buf;
	dtp_preamble_t* preamble = (void *)net_buf->data;
	unsigned char* data;
	uint16_t magic, len;

	// Don't do anything if we already received full dtp
	if (dtp->done)
		return DTP_ERROR;

	if (net_buf_add_tail(net_buf, buf, size) != 0)
		return DTP_ERROR;

	if (net_buf->length < sizeof(dtp_preamble_t))
        return DTP_OK;

	magic = ntohs(preamble->magic);
	len = ntohs(preamble->len);

	if (magic != DTP_MAGIC)
		return DTP_ERROR;
    
	// Check if we received the whole dtp
    if (net_buf->length < sizeof(dtp_preamble_t) + len)
        return DTP_OK;

    data = net_buf->data + sizeof(dtp_preamble_t);

	dtp->done = true;
	return DTP_OK;
}

dtp_result_e dtp_add_header(net_buf_t* net_buf)
{
	dtp_preamble_t preamble;

	preamble.len = htons((unsigned short)net_buf->length);
	preamble.magic = htons(DTP_MAGIC);

	if (net_buf_add_head(net_buf, (unsigned char*)&preamble, sizeof(preamble)) < 0)
		return DTP_ERROR;
	
	return DTP_OK;
}

unsigned char *dtp_data(dtp_t *dtp)
{
	if (!dtp->done)
		return NULL;

	return dtp->net_buf->data + sizeof(dtp_preamble_t);
}

size_t dtp_len(dtp_t *dtp)
{
	if (!dtp->done)
		return 0;

	return dtp->net_buf->length - sizeof(dtp_preamble_t);
}

