LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

DONGLE_S := $(LOCAL_PATH)/../dongle-smart
DONGLE := $(LOCAL_PATH)/../dongle
MBEDTLS := $(LOCAL_PATH)/../../ext/mbedtls/include
CRYPTO := $(LOCAL_PATH)/../crypto
COMMON :=$(LOCAL_PATH)/../common
PAR := $(LOCAL_PATH)/..
LOCAL_C_INCLUDES := $(DONGLE_S) $(DONGLE) $(MBEDTLS) $(CRYPTO) $(COMMON) $(PAR)

LOCAL_MODULE    := dongle
#LOCAL_C_INCLUDES := $(LOCAL_PATH)/../dongle

//LOCAL_CFLAGS += -DPB_ENABLE_MALLOC
LOCAL_SRC_FILES := pb_common.c dongle.pb.c pb_decode.c pb_encode.c dtp.c ../common/net_buf.c ../common/malloc_prv.cpp

#dat.c                    dat_handle_request.cpp   dat_txcm.c               dongle.pb.c	\
#DongleStateMachine.cpp   dtp.c                    pb_common.c              pb_decode.c	\
#pb_encode.c              pb_helpers.c

#include $(BUILD_STATIC_LIBRARY)
include $(BUILD_SHARED_LIBRARY)