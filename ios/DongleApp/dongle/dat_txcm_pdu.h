#ifndef _DAT_TXCM_PDU_H_
#define _DAT_TXCM_PDU_H_

#include <txcm/txcm_cmnd_def.h>

typedef struct _TXCM_PDU_REFERENCE {
	TXCM_PDU_PAYLOAD_REQUEST* req;
	TXCM_DIAG_PDU_CommunicationParams *pComPar;
	TXCM_DIAG_PDURequest *pPDURequests;
} TXCM_PDU_REFERENCE;

#ifdef __cplusplus 
extern "C" {
#define DAT_TXCM_EXTERN extern "C"
#else
#define DAT_TXCM_EXTERN extern
#endif

DAT_TXCM_EXTERN
TXCM_PDU_REFERENCE dat_txcm_pdu_getReferences(uint16_t nCommParams,  uint16_t nPDU);

DAT_TXCM_EXTERN
void dat_txcm_pdu_removeRequest(TXCM_PDU_PAYLOAD_REQUEST* req);

#ifdef __cplusplus 
}
#endif

#endif