//
//  ViewController.m
//  DongleApp
//
//  Created by David Olodovsky on 30/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import "ViewController.h"
#define FORMAT(format, ...) [NSString stringWithFormat:(format), ##__VA_ARGS__]

NSString *messageField = @"init filed";

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *Label;
@property (weak, nonatomic) IBOutlet UIButton *Scenario;

@property (weak, nonatomic) IBOutlet UITextView *ScenarioOutput;
@end


@implementation ViewController
- (IBAction)ScenarioAction:(id)sender {
    _ScenarioOutput.text = @"SC: printlog (>> START) \n SC: authenticated by PIN (123)";
    NSString *host = @"10.2.2.2";
    if ([host length] == 0)
    {
        [self logError:@"Address required"];
        return;
    }
    
    int port = 11115;
    if (port <= 0 || port > 65535)
    {
        [self logError:@"Valid port required"];
        return;
    }
    
    NSString *msg = messageField ;
    if ([msg length] == 0)
    {
        [self logError:@"Message required"];
        return;
    }
    
    NSData *data = [msg dataUsingEncoding:NSUTF8StringEncoding];
    [udpSocket sendData:data toHost:host port:port withTimeout:-1 tag:tag];
    
  //  [self logMessage:FORMAT(@"SENT (%i): %@", (int)tag, msg)];
    
    
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    udpSocket = [[AsyncUdpSocket alloc] initWithDelegate:self];
    
    NSError *error = nil;
    
    if (![udpSocket bindToPort:0 error:&error])
    {
        NSLog(@"Error binding: %@", error);
        return;
    }
    
    [udpSocket receiveWithTimeout:-1 tag:0];
    
    NSLog(@"Ready");
}



- (void)logError:(NSString *)msg
{
    NSString *paragraph = [NSString stringWithFormat:@"%@\n", msg];
    _ScenarioOutput.text = paragraph;
}

- (void)logInfo:(NSString *)msg
{
    NSString *paragraph = [NSString stringWithFormat:@"%@\n", msg];
     _ScenarioOutput.text = paragraph;
}

- (void)logMessage:(NSString *)msg
{
    NSString *paragraph = [NSString stringWithFormat:@"%@\n", msg];
    _ScenarioOutput.text = paragraph;
  }

- (void)onUdpSocket:(AsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    // You could add checks here
}

- (void)onUdpSocket:(AsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    // You could add checks here
}

- (BOOL)onUdpSocket:(AsyncUdpSocket *)sock
     didReceiveData:(NSData *)data
            withTag:(long)tag
           fromHost:(NSString *)host
               port:(UInt16)port
{
    NSString *msg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    if (msg)
    {
        [self logMessage:FORMAT(@"RECV: %@", msg)];
    }
    else
    {
        [self logInfo:FORMAT(@"RECV: Unknown message from: %@:%hu", host, port)];
    }
    
    [udpSocket receiveWithTimeout:-1 tag:0];
    return YES;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
