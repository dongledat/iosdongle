//
//  Node.h

#import <Foundation/Foundation.h>

@interface Node : NSObject
@property (nonatomic, strong) id data;
@property (nonatomic, strong) Node* next;
@end
