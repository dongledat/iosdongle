//
//  BTStream.h
//  dongle
//
//  Created by David Olodovsky on 20/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTStream : IDATIOStream
@property (nonatomic, assign, readonly) BOOL bluetoothAvailable;
@property (nonatomic, assign) int socket;


- (int) connectPeripheral:(CBPeripheral *) peripheral clientContext:(exlap_ClientCtxt*)pClientCtxt;
- (void) disconnectPeripheral:(CBPeripheral *) peripheral;

- (CBPeripheral *) discoveredPeripheral;
- (CBPeripheral *) peripheralForUUID:(uuid_t)uuid;

@end
