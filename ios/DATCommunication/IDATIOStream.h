//
//  IDATIOStream.h
//  dongle
//
//  Created by David Olodovsky on 20/10/2016.
//  Copyright © 2016 CyMotive. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef  enum { CONN_NOT_CONNECTED, CONN_IN_PROGRESS, CONN_CONNECTED, CONN_FAILED, CONN_ALREADY_CONNECTED,
    CONN_BLUETOOTH_NOT_AVAILABLE}
ConnectionStatus;


@interface IDATIOStream : NSStream

- (void)open;
- (void)close;
@property (nullable, assign) id <NSStreamDelegate> delegate;


@end
