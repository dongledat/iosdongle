// Protocol Buffers for Objective C
/
#import "AbstractMessage.h"

#import "CodedOutputStream.h"

@implementation PBAbstractMessage

- (id) init {
  if ((self = [super init])) {
  }

  return self;
}


- (NSData*) data {
  NSMutableData* data = [NSMutableData dataWithLength:self.serializedSize];
  PBCodedOutputStream* stream = [PBCodedOutputStream streamWithData:data];
  [self writeToCodedOutputStream:stream];
  return data;
}


- (BOOL) isInitialized {
  @throw [NSException exceptionWithName:@"ImproperSubclassing" reason:@"" userInfo:nil];
}


- (int32_t) serializedSize {
  @throw [NSException exceptionWithName:@"ImproperSubclassing" reason:@"" userInfo:nil];
}


- (void) writeToCodedOutputStream:(PBCodedOutputStream*) output {
  @throw [NSException exceptionWithName:@"ImproperSubclassing" reason:@"" userInfo:nil];
}


- (void) writeToOutputStream:(NSOutputStream*) output {
  PBCodedOutputStream* codedOutput = [PBCodedOutputStream streamWithOutputStream:output];
  [self writeToCodedOutputStream:codedOutput];
  [codedOutput flush];
}

- (void) writeDelimitedToOutputStream:(NSOutputStream *)output
{
    int serialized = [self serializedSize];
    PBCodedOutputStream* codedOutput = [PBCodedOutputStream streamWithOutputStream:output];
    [codedOutput writeRawVarint32:serialized];
    [self writeToCodedOutputStream:codedOutput];
    [codedOutput flush];
}

- (id<PBMessage>) defaultInstance {
  @throw [NSException exceptionWithName:@"ImproperSubclassing" reason:@"" userInfo:nil];
}


- (PBUnknownFieldSet*) unknownFields {
  @throw [NSException exceptionWithName:@"ImproperSubclassing" reason:@"" userInfo:nil];
}


- (id<PBMessage_Builder>) builder {
  @throw [NSException exceptionWithName:@"ImproperSubclassing" reason:@"" userInfo:nil];
}


- (id<PBMessage_Builder>) toBuilder {
  @throw [NSException exceptionWithName:@"ImproperSubclassing" reason:@"" userInfo:nil];
}


- (void) writeDescriptionTo:(NSMutableString*) output
                 withIndent:(NSString*) indent {
  @throw [NSException exceptionWithName:@"ImproperSubclassing" reason:@"" userInfo:nil];
}


- (NSString*) description {
  NSMutableString* output = [NSMutableString string];
  [self writeDescriptionTo:output withIndent:@""];
  return output;
}


@end
