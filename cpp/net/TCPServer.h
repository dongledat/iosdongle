#pragma once

#include "Net.h"
#include "TCPSocket.h"

namespace net {

	class TCPServer {
	public:
		TCPServer(unsigned short port) : m_port(port) {
			m_socketDescriptor = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (m_socketDescriptor < 0)
			{
				// Unable to create TCP socket
				return;
			}

			struct sockaddr_in socketAddress;

			memset((char*)&socketAddress, 0, sizeof(socketAddress));

			socketAddress.sin_family = AF_INET;
			socketAddress.sin_port = htons(m_port);
			socketAddress.sin_addr.s_addr = htonl(INADDR_ANY);

			if (bind(m_socketDescriptor, (const struct sockaddr*)&socketAddress, sizeof(socketAddress)) == -1)
			{
				closesocket(m_socketDescriptor);
				m_socketDescriptor = -1;
				// Unable to bind UDP socket to port %d", port
				return;
			}

			listen(m_socketDescriptor, 10);

		};

		int getDescriptor() { return m_socketDescriptor; };

		TCPSocket accept() {
			return TCPSocket(::accept(m_socketDescriptor, NULL, NULL));
		}

		void close() {
			::closesocket(m_socketDescriptor);
		}

		~TCPServer() {
			close();
		}

	private:
		unsigned int m_port;
		int m_socketDescriptor;

	};

}