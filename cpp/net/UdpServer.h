#ifndef UDP_SERVER_H
#define UDP_SERVER_H

#include "Net.h"
#include <string.h>

#define	SOCKERR	WSAGetLastError()

namespace net
{

class UdpServer
{
public:

    UdpServer(unsigned port, int recvBufferSize = 0) : 
        m_socketDescriptor(-1)
    {
        // ASSERT( port < ( 1 << 16 ) );
        createSocket(port, recvBufferSize);
    }

    ~UdpServer()
    {
        closesocket(m_socketDescriptor);
    }

	int UdpServer::setTimeoutInMS(unsigned int ms)
	{
		int newTimeout = ms;
		int res = ::setsockopt(m_socketDescriptor, SOL_SOCKET, SO_RCVTIMEO, (char *)&newTimeout, sizeof(newTimeout));
		if (SOCKET_ERROR == res) {
			int err = WSAGetLastError();
			m_socketDescriptor = INVALID_SOCKET;
			return SOCKET_ERROR;
		}

		return 0;
	}

#if 0
    void registerOn( OS::Selectador & selectador )
    {
        selectador.registerRead( _socketDescriptor );
    }

    bool ready( const OS::Selectador & selectador ) const
    {
        return selectador.readReady( _socketDescriptor );
    }
#endif

    int recv(unsigned char* data, size_t length)
    {
        static const int NO_FLAGS = 0;

        int result = ::recv(m_socketDescriptor, (char*)data, length, NO_FLAGS);
        //if ( result <= 0 )
        //    THROW_SPRINTF_DETAILED_EXCEPTION( Exception, ( "Unable to recv from socket: result %d", result ));
        return result;
    }

    int send(const unsigned char* data, size_t length)
    {
        static const int NO_FLAGS = 0;

        int result = ::send(m_socketDescriptor, (const char*)data, length, NO_FLAGS);
        //if ( result <= 0 )
        //    THROW_SPRINTF_DETAILED_EXCEPTION( Exception, ( "Unable to recv from socket: result %d", result ));
        return result;
    }

	int setTimeout(int timeoutInSec)
	{
		return setTimeoutInMS(timeoutInSec * 1000);
	}

    int recvfrom(unsigned char* data, size_t length, struct sockaddr_in& peer)
    {
        static const int NO_FLAGS = 0;

        socklen_t peerLength = sizeof( peer );

        int result = ::recvfrom(m_socketDescriptor, (char*)data, length, NO_FLAGS, (struct sockaddr*)&peer, &peerLength);
//        if (result <= 0)
//            THROW_SPRINTF_DETAILED_EXCEPTION( Exception, ( "Unable to recv from socket: result %d", result ));
//        if ( peerLength != sizeof( peer ) )
//            THROW_SPRINTF_DETAILED_EXCEPTION( Exception, ( "Unable to recv from socket: address length %d is not %d", peerLength, sizeof( peer ) ));
		if (WSAETIMEDOUT == WSAGetLastError()) {
			return 0;
		}
        return result;
    }

    int sendto(const unsigned char* data, size_t length, const struct sockaddr_in& peer)
    {
        static const int NO_FLAGS = 0;

        return ::sendto(m_socketDescriptor, (const char*)data, length, NO_FLAGS, (struct sockaddr*)&peer, sizeof(peer));
    }

    int getDescriptor() const { return m_socketDescriptor; }

    int getSocketReceiveBufferSize(int* socketReceiveBufferSize)
    {
        socklen_t length = sizeof(*socketReceiveBufferSize);
        return ::getsockopt(m_socketDescriptor, SOL_SOCKET, SO_RCVBUF, (char*)socketReceiveBufferSize, &length);
    }

    int setSocketReceiveBufferSize(int socketReceiveBufferSize)
    {
        socklen_t length = sizeof(socketReceiveBufferSize);
        return ::setsockopt(m_socketDescriptor, SOL_SOCKET, SO_RCVBUF, (const char*)&socketReceiveBufferSize, length);
    }

private:

    int m_socketDescriptor;

    bool createSocket(unsigned port, int recvBufferSize)
    {
        m_socketDescriptor = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

        if (m_socketDescriptor < 0)
        {
            // Unable to create UDP socket
            int err = SOCKERR;
            return false;
        }

        if (recvBufferSize != 0) 
        {
            if (setSocketReceiveBufferSize(recvBufferSize) != 0) 
            {
                closesocket(m_socketDescriptor);
                // Unable to set UDP socket receive buffer to size=%d", recvBufferSize
                return false;
            }

            int actualRecvBufferSize = 0;

            if (getSocketReceiveBufferSize(&actualRecvBufferSize) != 0) 
            {
                closesocket(m_socketDescriptor);
                // Unable to get UDP socket receive buffer size
                return false;
            }
        }

        struct sockaddr_in socketAddress;

        memset((char*)&socketAddress, 0, sizeof(socketAddress));

        socketAddress.sin_family = AF_INET;
        socketAddress.sin_port = htons(port);
        socketAddress.sin_addr.s_addr = htonl(INADDR_ANY);

        if (bind(m_socketDescriptor, (const struct sockaddr*)&socketAddress, sizeof(socketAddress)) == -1) 
        {
            closesocket(m_socketDescriptor);
			m_socketDescriptor = -1;
            // Unable to bind UDP socket to port %d", port
            return false;
        }

        return true;
    }
};

}

#endif
