#pragma once

#include "Net.h"

namespace net {

	class TCPSocket {
	public:
		TCPSocket() : m_descriptor(INVALID_SOCKET) {}

		TCPSocket(int descriptor) : m_descriptor(descriptor) {

		}

		TCPSocket(sockaddr_in * remote_address, int addrlen) {
			m_descriptor = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (INVALID_SOCKET == m_descriptor) {
				return;
			}

			int res = ::connect(m_descriptor, (const sockaddr*)remote_address, addrlen);
			if (SOCKET_ERROR == res) {
				::closesocket(m_descriptor);
				m_descriptor = INVALID_SOCKET;
				return;
			}
		}

		int recvTimeoutInMS(unsigned char * buffer, unsigned int buf_size, unsigned int timeout_in_ms) {
			int paramSize = 0;

			int oldTimeout = 0;
			int newTimeout = timeout_in_ms;
			int ret = 0;

			::getsockopt(m_descriptor, SOL_SOCKET, SO_RCVTIMEO, (char *)&oldTimeout, &paramSize);
			::setsockopt(m_descriptor, SOL_SOCKET, SO_RCVTIMEO, (char *)&newTimeout, sizeof(newTimeout));
			ret = ::recv(m_descriptor, (char *)buffer, buf_size, 0);
			auto lasterr = WSAGetLastError();
			::setsockopt(m_descriptor, SOL_SOCKET, SO_RCVTIMEO, (char *)&oldTimeout, sizeof(newTimeout));
			
			if (WSAETIMEDOUT == WSAGetLastError()) {
				return 0;
			}

			return ret;
		}

		int recvTimeout(unsigned char * buffer, unsigned int buf_size, unsigned int timeout_in_sec) {
			return recvTimeoutInMS(buffer, buf_size, timeout_in_sec * 1000);
		}

		int send(const unsigned char * buffer, unsigned int buf_size) {
			return ::send(m_descriptor, (char*)buffer, buf_size, 0);
		}

		int getDescriptor() {
			return m_descriptor;
		}

		void close() {
			::closesocket(m_descriptor);
		}

	private:
		int m_descriptor;
	};

}