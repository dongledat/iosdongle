#include "Net.h"

bool net::init()
{
	unsigned status = 0;
#if defined (OS_WINDOWS)

	WSADATA wsaData;
	WORD wVersionRequested = MAKEWORD(2, 2);
	if ((status = WSAStartup(wVersionRequested, &wsaData)) != 0)
	{
		return false;
	}

	// Confirm that the WinSock DLL supports the requested version. Note that if the
	// DLL supports	versions greater than requested, it will still return the requested
	// version in wVersion since that is the version we requested
	if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2)
	{
		// Tell the user that we could not find a usable WinSock DLL.
		return false;
	}

#endif

	return true;
}

bool net::done()
{
#ifdef OS_WINDOWS
	return WSACleanup() == 0;
#endif
}

// A thread-safe interface for ::gethostbyname which allocates its own memory.
// MUST be free with freeGetHostByName after use.
hostent* net::getHostByName(const char* name)
{
    //static CriticalSection lock;
    hostent* destHost = NULL;
    hostent* srcHost = NULL;

    //lock.lock();
    srcHost = gethostbyname (name);
    if (srcHost)
    {
        size_t length = sizeof(hostent) + srcHost->h_length;
        destHost = (hostent*)malloc(length);
        if (destHost)
        {
            memcpy(destHost, srcHost, length);
        }
    }
    //lock.release();

    return destHost;
}

void net::freeGetHostByName(hostent* host)
{
    if (host)
    {
        free(host);
    }
}
