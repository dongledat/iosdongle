#include "net/UdpServer.h"
#include "net/UdpClient.h"

TEST(Net, Udp)
{
    CHECK(net::init());

    static const int port = 81111;

    net::UdpServer server(port);
    CHECK(server.getDescriptor() >= 0);
    net::UdpClient client("127.0.0.1", port);
    CHECK(client.getDescriptor() >= 0);

    int res;

    const char sendBuf[] = "Hello, world!";
    res = client.send(sendBuf, sizeof(sendBuf));
    CHECK(res == sizeof(sendBuf));

    unsigned char buf[100];
    res = server.recv(buf, sizeof(buf));
    CHECK(res == sizeof(sendBuf));

    CHECK(net::done());
}
