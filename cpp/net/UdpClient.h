#ifndef UDP_CLIENT_H
#define UDP_CLIENT_H

#include "Net.h"
#include <string.h>

namespace net
{

#define DEFAULT_TIMEOUT_IN_SEC (3)

class UdpClient
{
public:

    UdpClient(const char* host, unsigned port, unsigned int timeout = DEFAULT_TIMEOUT_IN_SEC)
    {
//		ASSERT( port < ( 1 << 16 ) );
        m_socket = createSocket(host, port);

		int newTimeout = timeout * 1000;
		int res = ::setsockopt(m_socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&newTimeout, sizeof(newTimeout));
		if (SOCKET_ERROR == res) {
			int err = WSAGetLastError();
			m_socket = INVALID_SOCKET;
		}
    }

    ~UdpClient()
    {
        close();
    }

    void close()
    {
        closesocket(m_socket);
    }

#if 0
    void registerOn( OS::Selectador & selectador )
    {
        selectador.registerRead( _inSocket.descriptor() );
    }

    bool ready( const OS::Selectador & selectador ) const
    {
        return selectador.readReady( _inSocket.descriptor() );
    }

    void registerOnWrite( OS::Selectador & selectador )
    {
        selectador.registerWrite( _outSocket.descriptor() );
    }

    bool writeReady( const OS::Selectador & selectador ) const
    {
        return selectador.writeReady( _outSocket.descriptor() );
    }
#endif

	int bind(const struct sockaddr * name, int namelen)
	{
		int result = ::bind(m_socket, name, namelen);
		return result;
	}

	int bindSrcPort(unsigned short port)
	{
		sockaddr_in service;
		service.sin_family = AF_INET;
		InetPton(AF_INET, "127.0.0.1", &(service.sin_addr.s_addr));
		service.sin_port = htons(port);

		return bind((sockaddr*)&service, sizeof(service));
	}

    int recv(char* data, size_t length)
    {
        int result = ::recv(m_socket, data, length, 0);
#if 0
        if ( result <= 0 )
            THROW_DETAILED_EXCEPTION( Exception, "Unalbe to receive" );
#endif
		int ret = WSAGetLastError();
        return result;
    }

    int send(const char* data, size_t length)
    {
        return ::send(m_socket, data, length, 0);
    }

#if 0
    int nonBlockingSend(const void* data, unsigned length, int& error)
    {
        return m_outSocket.nonBlockingSend( data, length, error );
    }

    ssize_t writev(const SocketBuffers& buffers)
    {
        return m_outSocket.writev(buffers);
    }

    void setRecvBuffers(size_t bufferSize)
    {
        int error = setsockopt(_outSocket.descriptor(), SOL_SOCKET, SO_RCVBUF, &bufferSize, sizeof(bufferSize));
//		if( error < 0 )
//			THROW_ERRNO_DETAILED_EXCEPTION( Exception );
    }

    void setSendBuffers(size_t bufferSize)
    {
        int error =  setsockopt( _outSocket.descriptor(), SOL_SOCKET, SO_SNDBUF, & bufferSize, sizeof( bufferSize ) );
//		if( error < 0 )
//			THROW_ERRNO_DETAILED_EXCEPTION( Exception );
    }
#endif

    int getDescriptor() const { return m_socket; }

private:

    int m_socket;

    int createSocket(const char* host, unsigned port)
    {
        struct sockaddr_in address;

        memset(&address, 0, sizeof(address));
        address.sin_family = AF_INET;

        if (!net::aton(host, &address.sin_addr))
        {
            // Unable to convert address to sin_addr: %s", host 
            return -1;
        }

        address.sin_port = htons(port);
        int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (fd == -1) 
        {
            // Unable to create UDP socket
            int err = SOCKERR;
            return -1;
        }

        if (::connect(fd, (const struct sockaddr*)&address, sizeof(address)) == -1) 
        {
            closesocket(fd);
            // Unable to connect to %s:%d", host, port
        }

        return fd;
    }
};

}

#endif
