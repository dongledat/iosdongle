#ifndef NET_H
#define NET_H

#include "os/os.h"

#ifdef OS_WINDOWS

    #include <mswsock.h>
    #include <WS2tcpip.h>
    #include <wininet.h>

    #define	SOCKERR	WSAGetLastError()

#elif defined (OS_LINUX)

    #include <sys/socket.h>
    #include <sys/sendfile.h>
    #include <arpa/inet.h> 
    #include <netdb.h>
    #include <netinet/in.h>
    #include <sys/errno.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <ctype.h>
    #include <fcntl.h>

    #define SOCKET int
    #define HANDLE int
    #define INVALID_SOCKET -1
    #define	SOCKERR	errno
    #define	closesocket(s) ::close(s)

#endif

namespace net
{
    bool init();
    bool done();
    hostent* getHostByName(const char* name);
    void freeGetHostByName(hostent* host);

    inline bool aton(const char* addrStr, void* addrBuf)
    {
#ifdef OS_WINDOWS
        return InetPtonA(AF_INET, addrStr, addrBuf) != 0;
#elif defined (OS_LINUX)
        return inet_aton(addrStr, addrBuf) != 0;
#endif
    }
}

#endif
