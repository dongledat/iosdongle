#ifndef __LOCKSENTRY_H__
#define __LOCKSENTRY_H__

namespace os {
	
	template <class _Lock>
	class LockSentry
    {
	public:
		typedef _Lock lock_type; 
		LockSentry(lock_type* iLock) : m_lock(iLock)
        {
			if ( m_lock) 
				m_lock->lock();
		}
        LockSentry(lock_type* iLock, bool write) : m_lock(iLock)
        {
            if (m_lock) 
            {
                if (write)
                    m_lock->writeLock();
                else
                    m_lock->readLock();
            }
        }
		~LockSentry()
        {
			if (m_lock)
            {
				m_lock->release();
			}
		}
		lock_type* m_lock;
	}; // class LockSentry
	
} //namespace os

#endif


