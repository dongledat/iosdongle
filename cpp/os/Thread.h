#ifndef THREAD_H
#define THREAD_H

#include "os.h"
#include "CriticalSection.h"

#if defined(_WIN32) && !defined(__CYGWIN__)
    typedef unsigned (__stdcall *PTHREAD_START) (void *);
#define CREATETHREAD_W_BEGINTHREADEX(psa, cbStack, pfnStartAddr, \
     pvParam, fdwCreate, pdwThreadID)                \
       ((HANDLE) _beginthreadex(                     \
          (void *) (psa),                            \
          (unsigned) (cbStack),                      \
          (PTHREAD_START) (pfnStartAddr),            \
          (void *) (pvParam),                        \
          (unsigned) (fdwCreate),                    \
          (unsigned *) (pdwThreadID))) 
#endif
 
namespace os {
  
    class Thread 
    {
    public:
      
		enum { DEFAULT_STACK_SIZE = 1048576 };

		enum enum_ThreadState 
		{
			thread_state_prelaunch,
			thread_state_running,
			thread_state_suspended,
			thread_state_finish,
			thread_state_finished
		}; 

		Thread(const char* threadName = "", unsigned int priority = 0);

        virtual bool start(const size_t stack_size = DEFAULT_STACK_SIZE); 
        bool wait(); 
        void setState (enum_ThreadState p_state) { _state = p_state; }
        virtual void stop(bool block=true); 
        virtual bool kill();
        virtual void preRun() {};
        virtual void postRun(){};
        virtual ~Thread();
        enum_ThreadState getState() const { return _state; }
        void setName(const char* threadName) { m_threadName = threadName;}
        static unsigned int getCurrentThreadId();
        static bool setCurrentThreadNice(int nice);
        unsigned int getRes() {return m_res;}
        unsigned int getInstanceId(){return m_threadInstance;}
        static void sleep(unsigned iMilliseconds);
//        bool isHandleSet() const { return _handle != 0; }
        
    protected:
      
        void setCurrentThreadName();
        static inline RETVAL WINAPI _staticThreadProc(void *lpParameter);
        unsigned int WINAPI _objectThreadProc();
        bool changePrioirty();
        virtual unsigned int _run() = 0;
        
        const char* m_threadName;
        unsigned int m_res;
        
#if defined(OS_WINDOWS)
        
        HANDLE _handle; 
        DWORD  _id; 
        
#elif defined(OS_EMBOS)
        
        OS_STACKPTR int m_stack[128];
        OS_TASK m_task;
        void* _handle;
        
#else
        
        pthread_t _handle;
        pthread_attr_t* m_attp;
        pthread_attr_t m_att;
        
#endif
        
        enum_ThreadState _state; 
        CriticalSection	 m_stateLock;
        static CriticalSection m_idmtx;
        static unsigned int m_threadInstanceBase;

        unsigned int    m_threadInstance;
        int             m_priority;
    };

} // namespace os

#endif

