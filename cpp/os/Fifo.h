#ifndef __FIFO_H__
#define __FIFO_H__

#include "os.h"

namespace os {

class Fifo
{
public:

    Fifo(unsigned size = 1024 * 16, const char* buf = NULL);
    ~Fifo();

    int put(const unsigned char* buf, int size);
    int get(unsigned char* buf, int size);

private:

#ifdef OS_EMBOS
  
    OS_Q m_q;
    bool m_allocated;
    const char* m_buf;
    
#endif  
};

#ifdef OS_EMBOS

#include <stdio.h>

inline Fifo::Fifo(unsigned size, const char* buf) :
    m_allocated(buf == NULL),
    m_buf((buf == NULL) ? buf : new char[size])
{
    OS_Q_Create(&m_q, const_cast<char*>(m_buf), size);     
}  

inline Fifo::~Fifo()
{
    OS_Q_Delete(&m_q);
    if (m_allocated)
        delete [] m_buf;  
}  

inline int Fifo::put(const unsigned char* buf, int size)
{
    OS_Q_PutBlocked(&m_q, buf, size);
    return size;
}    

inline int Fifo::get(unsigned char* buf, int size)
{
    char *data;
    int len = OS_Q_GetPtr(&m_q, (void**)&data);
    if (len <= size)
        memcpy(buf, data, size);
    else
        printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    OS_Q_Purge(&m_q);
    return len;
}

#endif  

} // namespace os

#endif