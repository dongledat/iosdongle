#ifndef __PIPE_C_H__
#define __PIPE_C_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "os.h"

OS_HANDLE* os_pipe_init_server(const char* name, unsigned size);
OS_HANDLE* os_pipe_init_client(void);
OS_HANDLE* os_pipe_init_client1(OS_HANDLE pipe);
void os_pipe_done(OS_HANDLE pipe);
int os_pipe_connect(OS_HANDLE pipe);
int os_pipe_disconnect(OS_HANDLE pipe);
int os_pipe_read(OS_HANDLE pipe, unsigned char* buf, size_t size);
int os_pipe_write(OS_HANDLE thread, const unsigned char* buf, size_t size);

#ifdef __cplusplus
}
#endif

#endif
