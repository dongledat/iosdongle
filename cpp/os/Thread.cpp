#include "Thread.h"
#if !defined (WINDOWS_THREADS)
#include <errno.h>
#include <signal.h>
#if defined(OS_UNIX_LINUX)
#include <sys/time.h>
#include <sys/resource.h>
#endif
#endif

namespace os {

CriticalSection Thread::m_idmtx;
unsigned int    Thread::m_threadInstanceBase = 0;

#define KILL_RUNNING_THREAD_EXIT_CODE 30

Thread::Thread(const char* threadName, unsigned int priority) : 
    m_threadName(threadName),
    m_res(0), 
    _handle(0),
    _state(thread_state_prelaunch), 
    m_priority(priority)
{
    // note: this is dangerous, because we may call the descendant object before it's 
    // ctor is finished!
#if defined(OS_LINUX)
    m_attp = NULL;
    //Set the thread new priority (If higher than the max priority allowed for this thread set the max):

    int max_priority = sched_get_priority_max(SCHED_RR);
    int min_priority = sched_get_priority_min(SCHED_RR);
    if (max_priority != -1 && min_priority != -1)
    {
        m_priority = MAX(m_priority, min_priority);
        m_priority = MIN(m_priority, max_priority);
    }
    else
    {
        LOG_DEBUG("sched_get_priority_max/min failed. "
                        << "error=" << errno 
                        << ", Max=" << max_priority 
                        << ", MIN=" << min_priority 
                        << ", m_priority=" << m_priority);
    }
#endif
    
    CS_LOCK(&m_idmtx);
    m_threadInstance = ++m_threadInstanceBase;
}

Thread::~Thread()
{
#if defined(OS_WINDOWS)
  
    HANDLE temp_handle; 
    
#elif defined(OS_EMBOS)
    
    void* temp_handle;    
    
#else
    
    pthread_t temp_handle;
    
#endif
    
    stop(true);
    
    BEGIN_CRITICAL_SECTION(&m_stateLock)
    setState(thread_state_finished);
    temp_handle = _handle;
    END_CRITICAL_SECTION
      
    if(temp_handle != 0)
    {
#if defined(OS_LINUX)
        CALL_PTHREAD(pthread_cancel)(temp_handle);
        CALL_PTHREAD(pthread_join)(temp_handle, NULL);
#endif
        _handle = 0; 
    }
}

bool Thread::kill()
{ 
#if defined(OS_LINUX)
  
    m_attp = NULL;
    
#endif
    
//    LOG_DEBUG("Thread::kill. _handle=" << _handle);
    if(_handle == 0)
    {
        return true;
    }
    
    bool res = false;
    
#if defined(OS_WINDOWS)
    
    if (0 != TerminateThread(_handle, KILL_RUNNING_THREAD_EXIT_CODE))
      
#elif defined(OS_EMBOS)      
      
#else
      
    if (0 == CALL_PTHREAD(pthread_cancel)(_handle))
      
#endif
    {
        _handle = 0;
        res = true;
    }
    
    return res;
}

bool Thread::wait()
{ 
#if defined(OS_WINDOWS)
  
    HANDLE temp_handle; 
    
#elif defined(OS_EMBOS)
    
    void* temp_handle;
    
#else    
    
    pthread_t temp_handle;
    
#endif
    
    BEGIN_CRITICAL_SECTION(&m_stateLock)
      
    temp_handle = _handle;
    
    if (0 == temp_handle) 
    {
        return false;
    }
    
    END_CRITICAL_SECTION
      
#if defined(OS_WINDOWS)
      
    return (WAIT_OBJECT_0 == WaitForSingleObject(temp_handle, INFINITE)); 
    
#elif defined(OS_EMBOS)
    
    return true;
    
#else    
    
    int res = CALL_PTHREAD(pthread_join)(temp_handle, NULL);
    if (res != 0)
    {
        LOG_NOTIFY("pthread_join returned err:" << res); /*may happen if the thread already died.*/
    }
    return (0 == res);
    
#endif
}

void Thread::sleep(unsigned iMilliseconds) 
{
#if defined(OS_WINDOWS)
  
    Sleep(iMilliseconds); 
    
#elif defined(OS_EMBOS)
    
    OS_Delay(iMilliseconds);
    
#else
    
    usleep (iMilliseconds * 1000);
    
#endif
}

unsigned int Thread::getCurrentThreadId()
{
#if defined(OS_WINDOWS)
  
    return GetCurrentThreadId();

#elif defined(OS_EMBOS)
    
    return 0;
    
#else
    
    return syscall(SYS_gettid);
    
#endif
}

bool Thread::setCurrentThreadNice(int nice)
{
#if defined(OS_LINUX)
  
    int err = setpriority(PRIO_PROCESS, getCurrentThreadId(), nice);	
    if (err)
    {
        LOG_ERROR ("setCurrentThreadNice() - setpriority failed with err "<< err);
        return false;
    }
    
#endif
    
    return true;
}

bool Thread::changePrioirty()
{
    // Can only change priority before launching the thread
    if (_state != thread_state_prelaunch)
    {
        //LOG_ERROR("_state != thread_state_prelaunch");
        return false;
    }

#if defined(OS_LINUX)

    struct sched_param param;

    //Init the attribute struct
    if (CALL_PTHREAD(pthread_attr_init)(&m_att) != 0)
    {
        LOG_ERROR("pthread_attr_init");
        return false;
    }

    //Set the scheduling policy to not default - else the priority change will not have an effect.
    //The call to pthread_attr_setschedpolicy will work only if the calling process is root.
    if (CALL_PTHREAD(pthread_attr_setschedpolicy)(&m_att, SCHED_RR) != 0)
    {
        LOG_ERROR("pthread_attr_setschedpolicy");
        return false;
    } 

    //Set the thread new priority (If higher than the max priority allowed for this thread set the max):
    int max_priority = sched_get_priority_max(SCHED_RR);
    int min_priority = sched_get_priority_min(SCHED_RR);
    if (max_priority != -1 && min_priority != -1)
    {
        m_priority = MAX(m_priority, min_priority);
        m_priority = MIN(m_priority, max_priority);
        param.sched_priority = m_priority;
    }
    else
    {
        param.sched_priority = m_priority;
        LOG_DEBUG("sched_get_priority_max/min failed. "
                        << "error=" << errno 
                        << ", Max=" << max_priority 
                        << ", MIN=" << min_priority 
                        << ", m_priority=" << m_priority 
                        << ", sched_priority=" << param.sched_priority);
    }

    if (CALL_PTHREAD(pthread_attr_setschedparam )(&m_att, &param) != 0 )
    {
        LOG_ERROR("pthread_attr_setschedparam failed");
        return false;
    }

#endif

    return true;
}

bool Thread::start(const size_t stack_size)
{
    if (_handle != 0)
    {
        return false;
    }
    BEGIN_CRITICAL_SECTION(&m_stateLock)
    _state = thread_state_prelaunch;
    END_CRITICAL_SECTION
      
#if defined(OS_WINDOWS)
      
    _handle = CREATETHREAD_W_BEGINTHREADEX(NULL, stack_size, &Thread::_staticThreadProc, (LPVOID)this, 0, &_id); 
    
#elif defined(OS_EMBOS)
    
#else
    
    int rc;

    if (m_priority)
    {
        if (!changePrioirty())
        {
            LOG_ERROR("Thread::start() - setting thread priority failed.");
            rc = -1;
        }
        else
        {
            if (stack_size)
            {
                // pthread_attr_init is called inside changePrioirty.
                m_attp = &m_att; // changePrioirty initializes m_att
                CALL_PTHREAD(pthread_attr_setstacksize)(m_attp,stack_size);
            }
            rc = CALL_PTHREAD(pthread_create)(&_handle, &m_att, &Thread::_staticThreadProc, (void*)this);
        }
    }
    else
    {
        if (stack_size)
        {
            m_attp = &m_att;
            CALL_PTHREAD(pthread_attr_init)(m_attp);
            CALL_PTHREAD(pthread_attr_setstacksize)(m_attp,stack_size);
        }
        rc = CALL_PTHREAD(pthread_create)(&_handle, m_attp, &Thread::_staticThreadProc, (void*)this);
    }
    if (rc) 
    {
        _handle = 0;
    }
#endif
    
    if (_handle != 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

RETVAL WINAPI Thread::_staticThreadProc(void* lpParameter) {
    
    Thread* threadptr = static_cast<Thread*>(lpParameter);
    
#if defined(WINDOWS_THREADS)
    if (!threadptr) {
        return -1; 
    }
    return threadptr->_objectThreadProc(); 
#else
    if (!threadptr)
        return NULL; 
    return reinterpret_cast<void*>(threadptr->_objectThreadProc()); 
#endif
}

unsigned int WINAPI Thread::_objectThreadProc()
{
#if defined(OS_LINUX)
  
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGPIPE);
    sigaddset(&mask, SIGTERM);
    sigaddset(&mask, SIGINT);
    sigaddset(&mask, SIGUSR1);
    CALL_PTHREAD(pthread_sigmask)(SIG_BLOCK, &mask, NULL);
    
#endif
    
    // std::cout << "Created thread " << _handle << std::endl;
    // patch! see comment above.
    BEGIN_CRITICAL_SECTION(&m_stateLock)
        if (_state == thread_state_prelaunch)
            _state = thread_state_running;
    else 
        return (unsigned int)(-1);
    END_CRITICAL_SECTION
    setCurrentThreadName();
    preRun();
    m_res = _run(); 
    postRun();
#if 0
    if (m_threadName.size())
    {
        LOG_INFO ("Stop thread " << m_threadName);
    }
#endif

    BEGIN_CRITICAL_SECTION(&m_stateLock)
    _state = thread_state_finished;
#if defined(WINDOWS_THREADS)
    CloseHandle(_handle);
#endif
    _handle = 0;
    END_CRITICAL_SECTION
    // std::cout << "Exiting thread " << _handle << std::endl;
    return m_res; 
}

void Thread::stop(bool block)
{
//    LOG_DEBUG("Thread::stop. _handle=" << _handle << " _state=" << _state);

#if defined(OS_LINUX)
  
    m_attp = NULL;
    
#endif
    
    BEGIN_CRITICAL_SECTION(&m_stateLock)
    if (_state == thread_state_finished)
    {
        return;
    }
    setState(thread_state_finish);
    END_CRITICAL_SECTION
    if (block)
    {
        if(_handle != 0)
        {
            this->wait(); 
            _handle = 0; 
        }
    }
}

#if defined(WINDOWS_THREADS)
///------------------------------
//	struct: tagTHREADNAME_INFO	
///------------------------------
typedef struct tagTHREADNAME_INFO
{
    DWORD dwType; // must be 0x1000
    LPCSTR szName; // pointer to name (in user addr space)
    DWORD dwThreadID; // thread ID (-1=caller thread)
    DWORD dwFlags; // reserved for future use, must be zero
} THREADNAME_INFO;

//-----------------------------------------------------------------------------
//@name	    : setCurrentThreadName
//@abstract : set thread name
//@params   : iStr
//@returns  : void
//-----------------------------------------------------------------------------
void Thread::setCurrentThreadName ()
{
#if 0
    if (!m_threadName.size())
    {
        return;
    }

    THREADNAME_INFO info;
    info.dwType = 0x1000;
    info.szName = m_threadName.c_str();
    info.dwThreadID = -1;
    info.dwFlags = 0;

    Timestamp temp; 
    char tmp[256];
    struct tm _time;
    time_t sec = temp.getTime().tv_sec;
    localtime_s(&_time, &sec);
    strftime(tmp, 128, "%H:%M:%S %d/%m/%Y", &_time);
    std::cout << tmp << " start thread:" << m_threadName <<" handle:"<< _handle << " id:" << _id << std::endl;
    
    __try
    {
        RaiseException( 0x406D1388, 0, sizeof(info)/sizeof(DWORD), (ULONG_PTR*)&info );
    }
    __except(EXCEPTION_CONTINUE_EXECUTION)
    {

    }
#endif
}
#else //!WINDOWS_THREADS
//-----------------------------------------------------------------------------
//@name	    : setCurrentThreadName
//@abstract : prints thread name and handle to std output
//@params   : iStr
//@returns  : void
//-----------------------------------------------------------------------------
void Thread::setCurrentThreadName() 
{
//    LOG_NO_AGR_INFO("Start thread " << m_threadName 
//                          << ", handle=" << _handle 
//                          << ", tid= " << getCurrentThreadId());    
}	
#endif //WINDOWS_THREADS

//-----------------------------------------------------------------------------
//@name	    : setCurrentThreadAffinity (static)
//@abstract : prints thread name and handle to std output, functional only in
//			: linux
//@params   : cpu_set_t cpuMap
//@returns  : bool (true upon success)
//-----------------------------------------------------------------------------
#if 0
bool Thread::setCurrentThreadAffinity(BitSet<>& cpus)
{
#if defined(OS_UNIX_LINUX)
    int err;
    cpu_set_t cpuset;
    
    //reset the cpu map
    CPU_ZERO(&cpuset);
    //set the bit of each cpu in the array
    for (unsigned idx = 0; idx < cpus.size(); idx++)
    {
        if (cpus[idx])
        {
            CPU_SET(idx, &cpuset);
        }
    }
#if defined(OS_UNIX_LINUX_26)
    if ((err = sched_setaffinity(getCurrentThreadId(), sizeof(cpuset), &cpuset)) < 0)
#else
    if ((err = sched_setaffinity(getCurrentThreadId(), &cpuset)) < 0)
#endif
    {
        LOG_ERROR ("setAffinity() - failed to set affinity to thread " << getCurrentThreadId() << ", error " << err);
        return false;
    }
#endif //OS_UNIX_LINUX
    return true;
}
#endif

}
