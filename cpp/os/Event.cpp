#include "Event.h"
#include <time.h>

void Event::init(const char* evtName)
{
#if defined(WINDOWS_THREADS)
    m_eventHandle = CreateEvent(NULL, (m_isManual ? TRUE : FALSE), FALSE, TEXT("e")); 
    if (m_eventHandle == NULL)
    {
    }
#else
    m_isSignalled=false;
    CALL_PTHREAD(pthread_cond_init)(&m_condition, NULL);
    CALL_PTHREAD(pthread_mutex_init)(&m_mutex,NULL);
#endif
}

Event::Event(bool manual_reset, const char* evtName) : m_isManual(manual_reset)
{
    init(evtName); 
}
        
Event::Event(const Event& ev) : m_isManual(ev.m_isManual)
{
    init(); 	
}
        
Event::~Event()
{
#if defined(WINDOWS_THREADS)
    CloseHandle(m_eventHandle); 
#else
    CALL_PTHREAD(pthread_cond_destroy)(&m_condition);
    CALL_PTHREAD(pthread_mutex_destroy)(&m_mutex);
#endif
}
        
bool Event::operator!() const
{
#if defined(WINDOWS_THREADS)
    return (m_eventHandle == NULL); 
#else
    // :TBD: return Event status in Linux
    return false;
#endif
}

enum_WaitResult Event::wait(unsigned int iTimeoutMilliseconds)
{ 
#if defined(WINDOWS_THREADS)
    DWORD waitres = WaitForSingleObject(m_eventHandle, iTimeoutMilliseconds); 
    switch (waitres)
    {
    case WAIT_OBJECT_0:
        return wait_result_signalled; 
    case WAIT_TIMEOUT:
    default:
        return wait_result_timeout; 
    }
#else
    int res; 
    enum_WaitResult rc = wait_result_error;
    CALL_PTHREAD(pthread_mutex_lock)(&m_mutex);
    if(m_isSignalled)
    {
        if(!m_isManual)
        {
            m_isSignalled = false;
        }
        CALL_PTHREAD(pthread_mutex_unlock)(&m_mutex);
        return wait_result_signalled;
    }
         
    if (iTimeoutMilliseconds != INFINITE)
    {
        struct timespec to; 
        clock_gettime (CLOCK_REALTIME, &to);
        to.tv_sec  += (iTimeoutMilliseconds / 1000);//number of seconds since... (1.1.1970 00:00)
        to.tv_nsec += (iTimeoutMilliseconds % 1000) * 1000 * 1000;//number of nano seconds since (1970...) + to.tv_sec
        //If the number nano seconds is >= one billion (= one second) then add one second to the seconds
        //and subtract one second from the nanoseconds.
        if (to.tv_nsec > 999999999)
        {
            to.tv_sec += 1;
            to.tv_nsec -= 1000000000;
        }
        res = CALL_PTHREAD(pthread_cond_timedwait)(&m_condition,&m_mutex, &to);
    }
    else
    {
        res = CALL_PTHREAD(pthread_cond_wait)(&m_condition,&m_mutex); 
    }
    switch (res)
    {
        case 0:
            rc = wait_result_signalled; 
            break;
        case ETIMEDOUT: 
            rc = wait_result_timeout;
            break;
        default:
            rc = wait_result_error;
            break;
    }
    if(rc == wait_result_signalled)
    {
        if(m_isSignalled)
        {
            if(!m_isManual)
            {
                m_isSignalled = false;
            }
        }
        else
        {
            rc = wait_result_timeout;
        }
    }
    CALL_PTHREAD(pthread_mutex_unlock)(&m_mutex);
    return rc; 
#endif
}

bool Event::set()
{ 
#if defined(WINDOWS_THREADS)
    return (SetEvent(m_eventHandle) == TRUE); 
#else
    CALL_PTHREAD(pthread_mutex_lock)(&m_mutex);
    m_isSignalled = true;
    if (m_isManual)
        CALL_PTHREAD(pthread_cond_broadcast)(&m_condition);
    else
        CALL_PTHREAD(pthread_cond_signal)(&m_condition);
    CALL_PTHREAD(pthread_mutex_unlock)(&m_mutex);
    return true;
#endif
}

bool Event::reset()
{ 
#if defined(WINDOWS_THREADS)
    return (ResetEvent(m_eventHandle)==TRUE); 
#else
    CALL_PTHREAD(pthread_mutex_lock)(&m_mutex);
    m_isSignalled = false;
    CALL_PTHREAD(pthread_mutex_unlock)(&m_mutex);
    return true;
#endif
}


