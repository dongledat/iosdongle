#include "Pipe.h"
#include "Pipe_c.h"

#if !defined(OS_EMBOS)

OS_HANDLE* os_pipe_init_server(const char* name, unsigned size)
{
	return reinterpret_cast<OS_HANDLE*>(new os::Pipe(true, name, size));
}

OS_HANDLE* os_pipe_init_client()
{
	return reinterpret_cast<OS_HANDLE*>(new os::Pipe(false, ""));
}

OS_HANDLE* os_pipe_init_client1(OS_HANDLE pipe)
{
	os::Pipe* p = reinterpret_cast<os::Pipe*>(pipe);
	return reinterpret_cast<OS_HANDLE*>(new os::Pipe(*p));
}

void os_pipe_done(OS_HANDLE pipe)
{
	delete reinterpret_cast<os::Pipe*>(pipe);
}

int os_pipe_connect(OS_HANDLE pipe)
{
	return reinterpret_cast<os::Pipe*>(pipe)->connect() ? 0 : -1;
}

int os_pipe_disconnect(OS_HANDLE pipe)
{
	return reinterpret_cast<os::Pipe*>(pipe)->disconnect() ? 0 : -1;
}

int os_pipe_read(OS_HANDLE pipe, unsigned char* buf, size_t size)
{
	return reinterpret_cast<os::Pipe*>(pipe)->read(buf, size);
}

int os_pipe_write(OS_HANDLE pipe, const unsigned char* buf, size_t size)
{
	return reinterpret_cast<os::Pipe*>(pipe)->write(buf, size);
}

#else // OS_EMBOS

OS_HANDLE* os_pipe_init_server(const char* name, unsigned size)
{
	return NULL;
}

OS_HANDLE* os_pipe_init_client()
{
	return NULL;
}

OS_HANDLE* os_pipe_init_client1(OS_HANDLE pipe)
{
	return NULL;
}

void os_pipe_done(OS_HANDLE pipe)
{
}

int os_pipe_connect(OS_HANDLE pipe)
{
	return 0;
}

int os_pipe_disconnect(OS_HANDLE pipe)
{
	return 0;
}

int os_pipe_read(OS_HANDLE pipe, unsigned char* buf, size_t size)
{
	return 0;
}

int os_pipe_write(OS_HANDLE pipe, const unsigned char* buf, size_t size)
{
	return 0;
}

#endif
