#ifndef __MUTEX_H__
#define __MUTEX_H__

#include "os.h"
#include "LockSentry.h"

#define CS_MUTEX(x) LockSentry<Mutex> __LOCAL_AUTOLOCK_VAR__((x));

namespace os {
	
class Mutex {
public:
		
    Mutex();
    ~Mutex();
    
    bool lock(unsigned int iTimeoutMilliseconds = INFINITE);
    bool release();
    
private:
		
#if defined(OS_WINDOWS)
  
    HANDLE m_mutex;
    
#elif defined(OS_EMBOS)
    
    OS_RSEMA m_sema;
    
#else
    
    pthread_mutex_t m_mutex;
    pthread_mutexattr_t m_attr;
    
#endif
};
	
}

#endif

