#ifndef CRITICAL_SECTION_H
#define CRITICAL_SECTION_H

#include "os.h"
//#include "Event.h"
#include "LockSentry.h"
#include "Mutex.h"
//#include "Interlocked.h"

#if defined(_WIN32)
    #define USE_OUR_CRITICAL_SECTION
#endif

#define CS_LOCK(x) \
    LockSentry<CriticalSection> __LOCAL_AUTOLOCK_VAR__((x));
#define BEGIN_CRITICAL_SECTION(x) \
    { LockSentry<CriticalSection>  __LOCAL_AUTOLOCK_VAR__((x));
#define END_CRITICAL_SECTION \
    }

//namespace os {
  
#if !defined(USE_OUR_CRITICAL_SECTION) 
  
    typedef Mutex CriticalSection;
    
#else
    
    #define DEFAULT_SPIN_COUNT 4000

    class CriticalSection
    {
	public:
		
		CriticalSection(unsigned int iSpinCount=DEFAULT_SPIN_COUNT);
		CriticalSection(const CriticalSection& other); 
		~CriticalSection(); 
		void lock();
#if defined(USE_TRYLOCK)
		bool trylock();
#endif
		void release();
	private:
		void init(int iSpinCount);
        void cleanup();
		
#if defined(_WIN32)
		CRITICAL_SECTION m_section;
#else // use our own implementation
		int m_spinCount;
		int  m_lockCount;
		int  m_recurseCount;
		bool m_isUniprocessor;
		int m_threadId;
		Event m_event;
#endif
	}; // class CriticalSection
#endif
	   
#if defined(USE_OUR_CRITICAL_SECTION) 
    
inline CriticalSection::CriticalSection(unsigned int iSpinCount)
{
	init(iSpinCount); 
}

inline CriticalSection::CriticalSection(const CriticalSection& other)
{
	init(DEFAULT_SPIN_COUNT);
}

inline CriticalSection::~CriticalSection()
{
    cleanup();
}

inline void CriticalSection::lock()
{
#if defined(USE_TRYLOCK)
	if (trylock()) 
		return;
#endif

#if defined(_WIN32)
	EnterCriticalSection(&m_section);
#else
	unsigned int threadId = Thread::getCurrentThreadId();
	if (Interlocked::increment(m_lockCount) == 1)
    {
      // CriticalSection is unowned, let this thread own it once
		Interlocked::setValue(m_threadId, threadId);
		m_recurseCount = 1;
	}
    else if (m_threadId == threadId)
    {
         // CriticalSection is owned by this thread, own it again
         ++m_recurseCount;
    }
    else
    {
         // CriticalSection is owned by another thread
         // Wait for the owning thread to release the CriticalSection
		m_event.wait();
         // We got ownership of the Optex
		Interlocked::setValue(m_threadId, threadId); 
        m_recurseCount = 1;       
   }
#endif
}

#if defined(USE_TRYLOCK)

inline bool CriticalSection::trylock()
{
#if defined(_WIN32)
	return (FALSE != TryEnterCriticalSection(&m_section)); 
#else
	unsigned int threadId = Thread::getCurrentThreadId(); 
   // If the lock count is zero, the Optex is unowned and
   // this thread can become the owner of it now.
   bool weOwnTheMutex = false;
   s32 spinCount = m_spinCount;
   do {
	   weOwnTheMutex = (0 == Interlocked::setValueIfEquals(m_lockCount, 1, 0)); 
      if (weOwnTheMutex)
      {
         // We now own the Optex
		  Interlocked::setValue(m_threadId, threadId); // We own it
		  m_recurseCount = 1;       // We own it once
      }
      else
      {
         // Some thread owns the Optex
         if (m_threadId == threadId)
         {
             // We already own the Optex
			 Interlocked::increment(m_lockCount);
			 m_recurseCount++;         // We own it again
             weOwnTheMutex= true;  // Return that we own the Optex
         }
      }
   } while (!weOwnTheMutex && (spinCount-- > 0));
   // Return whether or not this thread owns the Optex
   return(weOwnTheMutex);
#endif
}
#endif

inline void CriticalSection::release()
{
#if defined(_WIN32)
	LeaveCriticalSection(&m_section);
#else
	if (--m_recurseCount > 0)
    {
        // We still own the Optex
		Interlocked::decrement(m_lockCount);
    }
    else
    {
        // We don't own the Optex
        Interlocked::setValue(m_threadId, 0);
        if (Interlocked::decrement(m_lockCount) > 0)
        {
            // Other threads are waiting, wake one of them
            m_event.set();
        }
    }
#endif
}

inline void CriticalSection::init(int iSpinCount)
{
#if defined(_WIN32)
#if defined(USE_TRYLOCK)
	InitializeCriticalSectionAndSpinCount(&m_section, iSpinCount);
#else
	InitializeCriticalSection(&m_section);
#endif
/*
	// check if the machine is UniProcessor
	SYSTEM_INFO sinf;
	GetSystemInfo(&sinf);
	m_isUniprocessor = (sinf.dwNumberOfProcessors == 1);
	if (m_isUniprocessor)
		m_spinCount = 0;
*/
#else // UNIX
	// :TBD: patch: we should identify multiprocessor status in Linux.
	m_spinCount = 1; 
	m_isUniprocessor = true;
#endif
}

inline void CriticalSection::cleanup()
{
#if defined(_WIN32)
    DeleteCriticalSection(&m_section);
#endif
}

#endif // USE_OUR_CRITICAL_SECTION 


//} //namespace

#endif //  __CRITICALSECTION_H_INCLUDED__

