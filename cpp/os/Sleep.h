#ifndef SLEEP_H
#define SLEEP_H

#include "os.h"

namespace os {

	static inline void Sleep(unsigned milliseconds)
	{
#if defined(OS_WINDOWS)

		::Sleep(milliseconds);

#elif defined(OS_LINUX)

		usleep(milliseconds * 1000);
		
#endif  
	}

}

#endif
