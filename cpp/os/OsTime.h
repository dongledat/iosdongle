#ifndef OS_TIME_H
#define OS_TIME_H

#include "os.h"
#include <time.h>
#if defined(OS_LINUX)
#include <sys/time.h>
#endif
#include <stddef.h>
#if defined(OS_IOS)
#include <mach/mach_time.h>
#include <mach/clock.h>
#include <mach/mach.h>
#endif

namespace os
{

#ifdef OS_WINDOWS
  #if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
    #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
  #else
    #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
  #endif

	struct timespec
	{
		time_t tv_sec;  // Seconds - >= 0
		long   tv_nsec; // Nanoseconds - [0, 999999999]
	};

	struct timezone
	{
		int  tz_minuteswest; /* minutes W of Greenwich */
		int  tz_dsttime;     /* type of dst correction */
	};

	// This function is reliable until 2038
	static inline int gettimeofday(struct timeval *tv, struct timezone *tz)
	{
		FILETIME ft;
		unsigned __int64 tmpres = 0;
		static int tzflag;

		if (NULL != tv)
		{
			GetSystemTimeAsFileTime(&ft);

			// The GetSystemTimeAsFileTime returns the number of 100 nanosecond 
			// intervals since Jan 1, 1601 in a structure. Copy the high bits to 
			// the 64 bit tmpres, shift it left by 32 then or in the low 32 bits.
			tmpres |= ft.dwHighDateTime;
			tmpres <<= 32;
			tmpres |= ft.dwLowDateTime;

			// Convert to microseconds by dividing by 10
			tmpres /= 10;  /*convert into microseconds*/

						   // The Unix epoch starts on Jan 1 1970.  Need to subtract the difference 
						   // in seconds from Jan 1 1601.
			tmpres -= DELTA_EPOCH_IN_MICROSECS;

			// Finally change microseconds to seconds and place in the seconds value. 
			// The modulus picks up the microseconds.
			tv->tv_sec = (long)(tmpres / 1000000UL);
			tv->tv_usec = (long)(tmpres % 1000000UL);
		}
#if 0
		if (NULL != tz)
		{
			if (!tzflag)
			{
				_tzset();
				tzflag++;
			}
			tz->tz_minuteswest = _timezone / 60;
			tz->tz_dsttime = _daylight;
		}
#endif
		return 0;
	}

#endif

class Time
{
private:

    static long long tsToUSeconds(timespec& ts) { return ts.tv_sec * ( long long )1000 * 1000 + ts.tv_nsec / ( long long ) 1000; }
    static long long tsToNSeconds(timespec& ts) { return ts.tv_sec * ( long long )1000 * 1000 * 1000 + ts.tv_nsec; }

public:

	static void getMonotonicTime(timespec& mts)
    {

#ifndef CLOCK_MONOTONIC_RAW
#if defined(__powerpc__) || defined(__ppc__) || defined(__PPC__)
#define CLOCK_MONOTONIC_RAW	CLOCK_MONOTONIC
#else
#define CLOCK_MONOTONIC_RAW	( 4 )
#endif
#endif

#if defined(OS_LINUX)

        clock_gettime(CLOCK_MONOTONIC_RAW, &mts);

#elif defined(OS_IOS)

        static mach_timebase_info_data_t timebase;
        uint64_t time;
        long long nsecs;

        if (timebase.denom == 0) {
            ( void )mach_timebase_info(&timebase);
        }

        time = mach_absolute_time();
        nsecs = (double)time * timebase.numer / timebase.denom;
        long long secToNsec = 1000 * 1000 * 1000;
        mts.tv_sec = nsecs / secToNsec;
        mts.tv_nsec = nsecs % secToNsec;

#endif
    }

    static void getRealTime(timespec& ts)
    {
#if defined(OS_LINUX)

        clock_gettime(CLOCK_REALTIME, &ts);

#elif defined(OS_WINDOWS)

		struct timeval tv;
		gettimeofday(&tv, NULL);
		ts.tv_sec = tv.tv_sec;
		ts.tv_nsec = tv.tv_usec * 1000;

#elif defined(OS_IOS)

        clock_serv_t cclock;
        mach_timespec_t mts;
        host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
        clock_get_time(cclock, &mts);
        mach_port_deallocate(mach_task_self(), cclock);
        ts.tv_sec = mts.tv_sec;
        ts.tv_nsec = mts.tv_nsec;      

#endif
    }

    static long long monotonicNSeconds()
    {
        timespec ts;
        getMonotonicTime(ts);
        return tsToNSeconds(ts);
    }

    static long long realNSeconds()
    {
        timespec ts;
        getRealTime(ts);
        return tsToNSeconds(ts);
    }

    static long long realUSeconds()
    {
        timespec ts;
        getRealTime(ts);
        return tsToUSeconds(ts) ;
    }
};

}

#endif // __OS_TIME_H__
