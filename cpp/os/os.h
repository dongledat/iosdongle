#ifndef OS_H
#define OS_H

typedef void* OS_HANDLE;

#if defined(_WIN32)

    #define OS_WINDOWS
    #define WINDOWS_THREADS
    #define RETVAL unsigned int
    #include <winsock2.h>
    #include <windows.h>
    #include <process.h> 

#elif defined(__IAR_SYSTEMS_ICC__)

    #define OS_EMBOS
    #define WINAPI
    #define RETVAL void*
    // TODO: use actual ntohs implementation / check byte order
    #define ntohs(s) ((((s) & 0xff) << 8) | ((s) >> 8))
    #define htons ntohs

#elif __linux__

    #define OS_LINUX
    #define WINAPI
    #define RETVAL void* 
    #include <pthread.h>
    #include <unistd.h>

#elif __APPLE__

#define OS_IOS
#include <unistd.h>


#elif defined(__ANDROID__)

#include <arpa/inet.h>

#else

    #error "Unknown OS"

#endif

#endif
