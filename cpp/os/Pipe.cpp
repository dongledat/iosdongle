#include "Pipe.h"
#include <stdio.h>

using namespace os;

#if defined(OS_WINDOWS)

static HANDLE pipe_create(const char* name, unsigned size)
{
    return CreateNamedPipe(
        TEXT("\\\\.\\pipe\\Pipe"),
        PIPE_ACCESS_DUPLEX, 
		PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
        1,
        size,
        size,
        NMPWAIT_USE_DEFAULT_WAIT,
        NULL);
}

static HANDLE pipe_open(const char* name)
{
    HANDLE handle;
    DWORD dwMode = PIPE_READMODE_MESSAGE; 

     handle = CreateFile(
         TEXT("\\\\.\\pipe\\Pipe"), 
         GENERIC_READ | GENERIC_WRITE, 
         0,
         NULL,
         OPEN_EXISTING,
         0,
         NULL);

    if (handle == INVALID_HANDLE_VALUE)
		return (HANDLE)0;

//	if (!WaitNamedPipe(TEXT("\\\\.\\pipe\\Pipe"), 0)) 
//		return (HANDLE)0;

	if (!SetNamedPipeHandleState( 
			handle,    // pipe handle 
			&dwMode,  // new pipe mode 
			NULL,     // don't set maximum bytes 
			NULL))    // don't set maximum time 

		return (HANDLE)0;

	return handle;
}

static int pipe_connect(HANDLE handle)
{
    return ConnectNamedPipe(handle, NULL);   // wait for someone to connect to the pipe
}

static int pipe_write(HANDLE handle, const unsigned char* buffer, size_t size)
{
    DWORD dwWritten;

    if (!WriteFile(
            handle,
            buffer,
            size,   // = length of string + terminating '\0' !!!
            &dwWritten,
            NULL))
	{
		return -1;
	}

	printf("%s, size=%d, bytes=%d\n", __FUNCTION__, size, dwWritten);

	return dwWritten;
}

static int pipe_read(HANDLE handle, unsigned char* buffer, size_t size)
{
    DWORD dwRead;
	BOOL success;

	while(1)
	{
		success = ReadFile(handle, buffer, size, &dwRead, NULL);

		if (!success && GetLastError() != ERROR_MORE_DATA)
		{
			break; 
		}
 
        /* do something with data in buffer */
		printf("%s, size=%d, bytes=%d\n", __FUNCTION__, size, dwRead);

		return dwRead;
    }

	return -1;
}

static int pipe_disconnect(HANDLE handle)
{
    return DisconnectNamedPipe(handle);
}

static int pipe_close(HANDLE handle)
{
    return CloseHandle(handle);
}

Pipe::Pipe(bool server, const char* name, unsigned size) :
    m_server(server),
    m_handle(server ? pipe_create(name, size) : pipe_open(name))
{
}

Pipe::~Pipe()
{
    pipe_disconnect(m_handle);
    pipe_close(m_handle);
}

bool Pipe::connect()
{
    return pipe_connect(m_handle) > 0;
}

bool Pipe::disconnect()
{
    return pipe_disconnect(m_handle) > 0;
}

int Pipe::read(unsigned char* buf, int size)
{
    return pipe_read(m_handle, buf, size);
}

int Pipe::write(const unsigned char* buf, int size)
{
    return pipe_write(m_handle, buf, size);
}

#elif defined(OS_EMBOS)

Pipe::Pipe(bool server, const char* name, unsigned size) :
    m_server(server),
    m_read(new Fifo(size)),
    m_write(new Fifo(size))
{
}

Pipe::Pipe(const Pipe& server) :
    m_server(false),
    m_read(server.m_write),
    m_write(server.m_read)
{
}

Pipe::~Pipe()
{
    if (m_server)
    {
        delete m_write;
        delete m_read;
    }  
}

bool Pipe::connect()
{
    return true;
}

bool Pipe::disconnect()
{
    return true;
}

int Pipe::read(unsigned char* buf, int size)
{
    return m_read->get(buf, size);
}

int Pipe::write(const unsigned char* buf, int size)
{
    return m_write->put(buf, size);
}

#else


#endif