#ifndef EVENT_H
#define EVENT_H

//#include <iostream>
#include "os.h"

//namespace {
	enum enum_WaitResult {
		wait_result_timeout,
		wait_result_signalled,
		wait_result_error
			
	}; 
	class EventSelector;

	// wrapper for an event (win only, for now)
	class Event
	{
	public:
		Event(bool manual_reset=true, const char* evtName = "");
		Event(const Event& ev) ;
		~Event();
		bool set() ;
		bool reset();
		
		enum_WaitResult wait(unsigned int iTimeoutMilliseconds=INFINITE);
		bool operator!() const;  
		
		//int timedWait(int milliseconds); 
	protected:
		bool m_isManual; 
#if defined(WINDOWS_THREADS)
		HANDLE m_eventHandle; 
#else
		pthread_mutex_t m_mutex;
		pthread_cond_t	m_condition;
		bool			m_isSignalled;
		EventSelector*  m_group;
#endif 
		
		friend class EventSelector; 
		friend class QueueDispatcher; 
#if defined (WINDOWS_THREADS)
		HANDLE getHandle()
		{
			return m_eventHandle; 
		}
#endif
	
		void init(const char* evtName = ""); 
	};

//} // namespace 

#endif

