#ifndef __PIPE_H__
#define __PIPE_H__

#include "os.h"
#include "Fifo.h"

namespace os {

class Pipe
{
public:

    Pipe(bool server, const char* name, unsigned size = 1024 * 16);
#ifdef OS_EMBOS
    Pipe(const Pipe &);
#endif
    ~Pipe();

    bool connect();
    bool disconnect();
    int read(unsigned char* buf, int size);
    int write(const unsigned char* buf, int size);

private:

    bool m_server;
  
#if defined OS_WINDOWS
  
    HANDLE m_handle;
    
#elif defined(OS_EMBOS)    
  
    Fifo* m_read; 
    Fifo* m_write;

#else

     
#endif    
    
};

} // namespace os

#endif
