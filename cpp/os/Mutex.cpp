#include "Mutex.h"
//#include <iostream>
//#include <time.h>

using namespace os;

Mutex::Mutex()
{
#if defined(OS_WINDOWS)
  
    m_mutex = CreateMutex(NULL, FALSE, NULL); 
    
#elif defined(OS_EMBOS)
    
    OS_CREATERSEMA(&m_sema);
    
#else
    
    CALL_PTHREAD(pthread_mutexattr_init)(&m_attr);
    CALL_PTHREAD(pthread_mutexattr_settype)(&m_attr, PTHREAD_MUTEX_RECURSIVE);
    CALL_PTHREAD(pthread_mutex_init)(&m_mutex, &m_attr);
    
#endif
}

Mutex::~Mutex()
{
#if defined(OS_WINDOWS)
  
    if (m_mutex != NULL)
        CloseHandle(m_mutex); 
    
#elif defined(OS_EMBOS)
    
    OS_DeleteRSema(&m_sema);

#else 
    
    if (m_mutex != NULL)
    {  
        CALL_PTHREAD(pthread_mutex_destroy)(&m_mutex);
        CALL_PTHREAD(pthread_mutexattr_destroy)(&m_attr);
    }
    
#endif
}

bool Mutex::lock(unsigned int iTimeoutMilliseconds)
{
#if defined(OS_WINDOWS)
  
    if (m_mutex == NULL)
        return false;
	
    if (WaitForSingleObject(m_mutex, iTimeoutMilliseconds) != WAIT_OBJECT_0)
        return false;
    
#elif defined(OS_EMBOS)
    
    OS_Use(&m_sema);
    
#else    
    
  #if !defined(NO_MUTEX_TIMEOUT)
    
    if (iTimeoutMilliseconds != INFINITE)
    {
        struct timespec to; 
        clock_gettime (CLOCK_REALTIME, &to);
        to.tv_sec  += (iTimeoutMilliseconds / 1000);
        to.tv_nsec += (iTimeoutMilliseconds % 1000) * 1000 * 1000;
        res = CALL_PTHREAD(pthread_mutex_timedlock)(&m_mutex, &to);
    }
    else
      
  #endif
      
    {
        res = CALL_PTHREAD(pthread_mutex_lock)(&m_mutex); 
    }

    if (res != 0) 
    {
        if(res != ETIMEDOUT)
        {
             std::cout << "Error locking mutex " << res << std::endl;
        }
        
	return false;
    }
    
#endif
	
    return true;
}
/*
 *
 */
bool Mutex::release()
{
#if defined(OS_WINDOWS)
  
    if (m_mutex == NULL)
        return false;
    
    ReleaseMutex(m_mutex);
    
#elif defined(OS_EMBOS)
    
    OS_Unuse(&m_sema);    
    
#else
    
    CALL_PTHREAD(pthread_mutex_unlock)(&m_mutex);
    
#endif
    
    return true;
}

