#ifndef NET_BUF_H
#define NET_BUF_H

#if defined(__cplusplus)
extern "C" {
#endif

enum { NET_BUF_DEFAULT_HEADER_SIZE = 32 };

/*! \struct net_buf_t
 *
 */
typedef struct 
{
    unsigned char* buffer;
	size_t buffer_len;
    unsigned char *data;
	size_t length;
} net_buf_t;

net_buf_t *net_buf_alloc(size_t buf_len);
void net_buf_free(net_buf_t *net_buf);
unsigned char *net_buf_put(net_buf_t* net_buf, size_t length);
void net_buf_trim(net_buf_t* net_buf, size_t length);
int net_buf_add_tail(net_buf_t* net_buf, const void *data, size_t data_len);
int net_buf_add_head(net_buf_t* net_buf, const void *data, size_t data_len);

#if defined(__cplusplus)
}
#endif

#endif