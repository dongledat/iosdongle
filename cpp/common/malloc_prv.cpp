#include "malloc_prv.h"
#include <stdint.h>

static void* _malloc_prv(size_t size)
{
    return new uint8_t[size];
}

static void _free_prv(void* ptr)
{
    delete [] ptr;
}

static void* (*g_alloc)(size_t) = _malloc_prv;
static void(*g_free)(void*) = _free_prv;

void set_alloc_prv(void* (*falloc)(size_t), void (*ffree)(void*))
{
    if (falloc)
        g_alloc = falloc;
    if (ffree)
        g_free = ffree;
}

void* malloc_prv(size_t size)
{
    return (*g_alloc)(size);
}

void free_prv(void* ptr)
{
    (*g_free)(ptr);
}

void *realloc_prv(void *ptr, size_t size)
{
	free_prv(ptr);
	return malloc_prv(size);
}
