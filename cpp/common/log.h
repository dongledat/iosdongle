#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include "os/os.h"

#if defined(DEBUG) || defined(_DEBUG)
    #define LOG(fmt, ...) printf("%s: " fmt "\n", __func__, ## __VA_ARGS__)
    #define LOG_ERROR(fmt, ...) printf("%s (%s:%d): ERROR - " fmt "\n", __func__, __FILE__, __LINE__, ## __VA_ARGS__)

#if defined(OS_EMBOS)
    #define DLOG LOG
#else
    #define DLOG(...) do {} while (0)
#endif

#else

    #define LOG(...) do {} while (0)
    #define DLOG(...) do {} while (0)
    #define LOG_ERROR(...) do {} while (0)

#endif

#endif
