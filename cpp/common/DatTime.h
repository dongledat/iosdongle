#ifndef COMMON_TIME_H
#define COMMON_TIME_H

#include <limits.h>

namespace common {

	class Time
	{
	public:

		Time(long long uSeconds) : m_uSeconds(uSeconds) {}

		static Time infinity()
		{
			return Time(LLONG_MAX);
		}

		static Time mSeconds(long long mSeconds)
		{
			return Time(mSeconds * (long long)1000);
		}

		static Time seconds(long long seconds)
		{
			return Time(seconds * (long long)1000 * 1000);
		}

		static Time uSeconds(long long uSeconds)
		{
			return Time(uSeconds);
		}

		static Time nSeconds(long long nSecodes)
		{
			return Time(nSecodes / (long long)1000);
		}

		Time operator - (const Time & other) const { return Time(m_uSeconds - other.m_uSeconds); }
		Time operator + (const Time & other) const { return Time(m_uSeconds + other.m_uSeconds); }
		void operator += (const Time & other) { m_uSeconds += other.m_uSeconds; }
		void operator -= (const Time & other) { m_uSeconds -= other.m_uSeconds; }

		bool operator > (const Time & other) const { return m_uSeconds > other.m_uSeconds; }
		bool operator >= (const Time & other) const { return m_uSeconds >= other.m_uSeconds; }
		bool operator < (const Time & other) const { return m_uSeconds < other.m_uSeconds; }
		bool operator <= (const Time & other) const { return m_uSeconds <= other.m_uSeconds; }
		bool operator == (const Time & other) const { return m_uSeconds == other.m_uSeconds; }
		bool operator != (const Time & other) const { return m_uSeconds != other.m_uSeconds; }

		long long uSeconds() const
		{
			return m_uSeconds;
		}

		long long mSeconds() const
		{
			return m_uSeconds / 1000;
		}

		long long seconds() const
		{
			return m_uSeconds / (1000 * 1000);
		}

		long long nSeconds() const
		{
			return m_uSeconds * 1000;
		}

	private:

		long long m_uSeconds;
	};

}

#if 0
inline std::ostream & operator << (std::ostream & os, const Time & t)
{
    struct tm * timeInfo;
    char buf[100];

    time_t timeInSeconds = t.seconds();
    timeInfo = localtime(&timeInSeconds);
    strftime(buf, sizeof(buf), "%d/%m/%y %H:%M:%S", timeInfo);
    os << buf;
    return os;
}

inline std::ostream & operator << (std::ostream & os, const struct timeval & tv)
{
    struct tm* timeInfo;
    char buf[100];

    time_t timeInSeconds = tv.tv_sec;
    timeInfo = gmtime(&timeInSeconds);
    strftime(buf, sizeof(buf), "%d/%m/%y %H:%M:%S,", timeInfo);
    os << buf << std::setw(3) << std::setfill( '0' ) << (tv.tv_usec / 1000);
    return os;
}

inline std::ostream & operator << (std::ostream & os, const timespec & ts)
{
    os << ts.tv_sec << "." << std::setw(6) << std::setfill('0') << (ts.tv_nsec / 1000);
	return os;
}
#endif

#endif // COMMON_TIME_H
