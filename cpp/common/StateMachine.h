#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__

#include <string.h>

namespace common {

class StateMachine;

struct TransitionContext
{
    TransitionContext( bool print ) : print( print ) {}

    bool printable() const { return print; }

private:

    bool print;
};

class StateMachineClass
{
    friend class StateMachine;

public:

    typedef bool ( * OnEvent )( void * instance, StateMachine * sm, void * data );

    struct Transition
    {
        Transition();

        OnEvent action;
        unsigned state;
        const char * currentStateName;
        const char * newStateName;
        const char * eventName;
        const TransitionContext * context;

        enum
        {
            STATE_NONE = ( unsigned ) -1
        };
    };

    struct MoveInto
    {
        MoveInto();
        OnEvent action;
        const char * stateName;
        bool print;
    };

    StateMachineClass(const char* name, unsigned states, unsigned events) :
            m_name( name ),
            m_numberOfStates( states ),
            m_numberOfEvents( events ),
            m_matrix( new Transition[ states * events ] ),
            m_moveInto( new MoveInto[ states ] ),
            m_class( *this )
    {
    }

    ~StateMachineClass()
    {
        delete [] m_matrix;
        delete [] m_moveInto;
    }

    unsigned getNumOfStates() const
    {
        return m_numberOfStates;
    }

    unsigned getNumOfEvents() const
    {
        return m_numberOfEvents;
    }

    const char* name() const
    {
        return m_name;
    }

    const TransitionContext * getTransitionContext( unsigned state, unsigned event ) const;

    bool InitTransition( unsigned state, const char * currentStateName, unsigned event, const char * eventName, unsigned newSate, const char * newStateName, OnEvent action, const TransitionContext * context );
    bool InitTransition( unsigned state, const char * currentStateName, unsigned event, const char * eventName, unsigned newSate, const char * newStateName, OnEvent action, bool print = true );
    bool InitMoveInto( unsigned state, const char * stateName, OnEvent action, bool print = true );

    #define INIT_TRANSITION(state,event,new_state,class,action,...) \
        m_class.InitTransition ( state, #state, event, #event, new_state, #new_state, static_cast<StateMachineClass::OnEvent> (&class::action##_), ##__VA_ARGS__ )

    #define INIT_MOVE_INTO(state,class,action,...) \
        m_class.InitMoveInto ( state, #state, static_cast<StateMachineClass::OnEvent> (&class::action##_), ##__VA_ARGS__ )

    #define DECL_TRANSITION(class_,action) \
        static bool action##_ (void * inst, ::StateMachine *, void * data) { return (( class_ * )inst)->action( data ); } \
        bool action (void * data)

    #define DECL_TRANSITION_IN_STATE(action) \
        static bool action##_ (void * inst, ::StateMachine *, void * data); \
        bool action (void * data)

    #define DECL_TRANSITION_IN_STATE_2(class_,state,action) \
        static bool action##_ (void * inst, ::StateMachine *, void * data) { return (( class_ * )inst)->state::action( data ); } \
        bool action (void * data)

    #define IMPL_TRANSITION_IN_STATE(class_,state,action) \
        bool state::action##_ (void * inst, ::StateMachine *, void * data) { return (( class_ * )inst)->state::action( data ); }

protected:

    StateMachineClass() : m_class( m_dummy ) {}

private:

    static const TransitionContext defaultPrintable;
    static const TransitionContext defaultNonPrintable;

    const char*         m_name;
    unsigned            m_numberOfStates;
    unsigned            m_numberOfEvents;
    Transition *        m_matrix;
    MoveInto *          m_moveInto;
    StateMachineClass & m_class;
    static StateMachineClass  m_dummy;
};

class StateMachine
{
    friend class StateMachineTransitionTable;

public:

    StateMachine( void * inst, StateMachineClass & smClass, unsigned state = 0 ) :
            m_inst( ( StateMachine * ) inst ),
            m_class( smClass ),
            m_state( state ),
            m_currentStateName( "" ),
            m_currentEventName( "" ),
            m_event( ( unsigned ) -1 ),
            m_nesting( 0 ),
            m_pending( false ),
            m_pendingEvent( ( unsigned ) -1 ),
            m_pendingData( NULL ),
            m_historyIdx( 0 )
    {
        memset( m_history, 0, sizeof( m_history ) );
    }

    struct Event
    {
        struct Data
        {
            virtual ~Data() {}
            virtual void * getData() = 0;
            virtual Data * copy() = 0;
        };

        typedef unsigned Type;
    };

    bool postEvent( Event::Type event )
    {
        return postEvent< int >( event, NULL );
    }

    template< class T > bool postEvent( Event::Type event, T * data );

    static bool nop( void * inst, StateMachine * stateMachine, void * )
    {
        return stateMachine->nop( NULL );
    }

    static bool nop2( void * inst, StateMachine * stateMachine, void * )
    {
        return stateMachine->nop2( NULL );
    }

    unsigned getState() const
    {
        return m_state;
    }

    const char * getStateName() const
    {
        return m_currentStateName;
    }

    unsigned getNumOfStates() const
    {
        return m_class.getNumOfStates();
    }

    unsigned getEvent() const
    {
        return m_event;
    }

    const char * getEventName() const
    {
        return m_currentEventName;
    }

    unsigned getNumOfEvents() const
    {
        return m_class.getNumOfEvents();
    }

    const TransitionContext * getTransitionContext( unsigned event ) const
    {
        return m_class.getTransitionContext( m_state, event );
    }

    void printHistory();

protected:

    bool nop( void * );
    bool nop2( void *) { return true; }

    void * m_inst;
    StateMachineClass & m_class;
    static StateMachineClass m_dummy;


    StateMachine() : m_class( m_dummy )
    {
    }

private:

    typedef struct
    {
        unsigned state;
        unsigned event;
    } StateEvent_t;

    enum
    {
        MAX_HISTORY = 10
    };

    unsigned                        m_state;
    const char *                    m_currentStateName;
    const char *                    m_currentEventName;
    Event::Type                     m_event;
    unsigned                        m_nesting;
    bool                            m_pending;
    Event::Type                     m_pendingEvent;
    Event::Data *                   m_pendingData;
    StateEvent_t                    m_history[ MAX_HISTORY ];
    unsigned                        m_historyIdx;

    bool _postEvent( Event::Type event, Event::Data * data );
};

template< class T >
inline bool StateMachine::postEvent( Event::Type event, T * data )
{
    struct DataOfType : public Event::Data
    {
        DataOfType( T & data ) :
                m_data( data )
        {
        }

        void * getData()
        {
            return &m_data;
        }

        Event::Data * copy()
        {
            return new DataOfType( m_data );
        }

        T m_data;
    };

    struct DataRefOfType : public Event::Data
    {
        DataRefOfType( T * data ) :
                m_data( data )
        {
        }

        void * getData()
        {
            return m_data;
        }

        Event::Data * copy()
        {
            if ( m_data )
                return new DataOfType( *m_data );
            else
                return new DataRefOfType( m_data );
        }

        T * m_data;
    };

    DataRefOfType dataOfType( data );
    return _postEvent( event, & dataOfType );
}

} // namespace common

#endif

