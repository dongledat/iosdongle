#ifndef CHECK_TYPE_H
#define CHECK_TYPE_H

#define CHECK_TYPE_SIZE_LE(type,size) typedef char type##le##size##_t [size - sizeof(type)]
#define CHECK_TYPE_SIZE_GE(type,size) typedef char type##ge##size##_t [sizeof(type) - size]
#define CHECK_TYPE_SIZE_EQ(type,size) CHECK_TYPE_SIZE_LE(type,size); CHECK_TYPE_SIZE_GE(type,size) 

#endif
