#ifndef MALLOC_PRV_H
#define MALLOC_PRV_H

#include "os/os.h"

#ifdef __cplusplus 
extern "C" {
#endif

void *malloc_prv(size_t size);

void *realloc_prv(void* ptr, size_t size);

void free_prv(void *ptr);

void set_alloc_prv(void* (*falloc)(size_t), void(*ffree)(void*));

#ifdef __cplusplus 
}
#endif

#endif

