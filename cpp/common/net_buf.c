#include <stdlib.h>
#include <string.h>
#include "net_buf.h"
#include "malloc_prv.h"

net_buf_t* net_buf_alloc(size_t buf_len)
{
	/* Reserve header space */
	buf_len += NET_BUF_DEFAULT_HEADER_SIZE;

    net_buf_t *net_buf = malloc_prv(buf_len + sizeof(*net_buf));
	if (!net_buf)
		return NULL;

	net_buf->buffer_len = buf_len;
	net_buf->buffer = (void *)(net_buf + 1);

	/* Start data after header space */
	net_buf->data = net_buf->buffer + NET_BUF_DEFAULT_HEADER_SIZE;
	net_buf->length = 0;

    return net_buf;
}

void net_buf_free(net_buf_t *net_buf)
{
	free_prv(net_buf);
}

static size_t net_buf_tail_space(net_buf_t *net_buf)
{
	unsigned char *end = net_buf->buffer + net_buf->buffer_len;
	return (size_t)(end - (net_buf->data + net_buf->length));
}

unsigned char *net_buf_put(net_buf_t* net_buf, size_t length)
{
	if (net_buf_tail_space(net_buf) < length)
		return NULL;

	/* Save current data end (before increasing length) */
	unsigned char *pos = net_buf->data + net_buf->length;

	/* Increase length */
	net_buf->length += length;

	return pos;
}

void net_buf_trim(net_buf_t* net_buf, size_t length)
{
	if (net_buf->length < length)
		return;

	net_buf->length -= length;
}

int net_buf_add_tail(net_buf_t* net_buf, const void *data, size_t data_len)
{
	unsigned char *data_pos = net_buf_put(net_buf, data_len);
	if (!data_pos)
		return -1;

    memcpy(data_pos, data, data_len);
    return 0;
}

int net_buf_add_head(net_buf_t* net_buf, const void *data, size_t data_len)
{
    if ((size_t)(net_buf->data - net_buf->buffer) < data_len)
        return -1;

    memcpy(net_buf->data - data_len, data, data_len);
    net_buf->length += data_len;
    net_buf->data -= data_len;

    return 0;
}
