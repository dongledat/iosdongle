#include "common/DatTime.h"
#include "os/OsTime.h"
#include "os/Sleep.h"

TEST(Common, Time)
{
    common::Time t0(os::Time::realUSeconds());
	os::Sleep(50);
    common::Time t1(os::Time::realUSeconds());
	CHECK(t1 > t0);
	
	CHECK(t1 - t0 > common::Time(40 * 1000));
}
