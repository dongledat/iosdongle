#include "common/StateMachine.h"

using namespace common;

class Test_StateMachine
{
public:

    class SampleStateMachine;

    class StateBase : public ::StateMachine
    {
    public:

        typedef enum
        {
            NONE_STATE = -1,
            STATE_0,
            STATE_1,
            STATE_2,
            MAX_STATE

        } States;

        typedef enum
        {
            NONE_EVENT = -1,
            EVENT_0,
            EVENT_1,
            EVENT_2,
            MAX_EVENT

        } Events;

        int _value;

    protected:

        StateBase( void * inst, StateMachineClass & smClass ) :
            ::StateMachine( inst, smClass, STATE_0 ),
            _value( 0 )
        {}

        StateBase() {}

        DECL_TRANSITION_IN_STATE_2 ( SampleStateMachine, StateBase, onEvent2 )
        {
            _value = 2;
            return true;
        }
    };

    class State0 : public virtual StateBase
    {
    protected:

        State0() {}

        DECL_TRANSITION_IN_STATE_2 ( SampleStateMachine, State0, onEvent0 )
        {
            _value = 0;
            return true;
        }

        DECL_TRANSITION_IN_STATE_2 ( SampleStateMachine, State0, onEvent1 )
        {
            _value = 1;
            return true;
        }
    };

    class State1 : public virtual StateBase
    {
    protected:

        State1() {}

        DECL_TRANSITION_IN_STATE_2 ( SampleStateMachine, State1, onEvent1 )
        {
            _value = 1;
            return true;
        }

        DECL_TRANSITION_IN_STATE_2 ( SampleStateMachine, State1, onEvent0 )
        {
            _value = 4;
            return true;
        }
    };

    class State2 : public virtual StateBase
    {
    protected:

        State2() {}

        DECL_TRANSITION_IN_STATE_2 ( SampleStateMachine, State2, onEvent1 )
        {
            _value = 3;
            postEvent( StateBase::EVENT_0 );
            return true;
        }
    };

    class SampleStateMachine : public State0, public State1, public State2
    {
    public:

        SampleStateMachine() :
            StateBase( this, _stateMachineClass ),
            State0(),
            State1(),
            State2(),
            _stateMachineClass( "Test", MAX_STATE, MAX_EVENT )
        {
             INIT_TRANSITION ( STATE_0, EVENT_0, STATE_0, SampleStateMachine, State0::onEvent0 );
             INIT_TRANSITION ( STATE_0, EVENT_1, STATE_1, SampleStateMachine, State0::onEvent1 );

             INIT_TRANSITION ( STATE_1, EVENT_0, STATE_0, SampleStateMachine, State1::onEvent0 );
             INIT_TRANSITION ( STATE_1, EVENT_1, STATE_1, SampleStateMachine, State1::onEvent1 );
             INIT_TRANSITION ( STATE_1, EVENT_2, STATE_2, SampleStateMachine, State1::onEvent2 );

             INIT_TRANSITION ( STATE_2, EVENT_2, STATE_2, SampleStateMachine, State2::onEvent2 );
             INIT_TRANSITION ( STATE_2, EVENT_1, STATE_1, SampleStateMachine, State2::onEvent1 );
        }

    private:

        StateMachineClass _stateMachineClass;
    };

public:

    static void test()
    {
        SampleStateMachine stateMachine;

        CHECK_EQUAL( stateMachine.getNumOfStates(), ( unsigned )SampleStateMachine::MAX_STATE );
        CHECK_EQUAL( stateMachine.getNumOfEvents(), ( unsigned )SampleStateMachine::MAX_EVENT );

        CHECK_EQUAL( stateMachine.getState(), ( unsigned )SampleStateMachine::STATE_0 );
        CHECK_EQUAL( stateMachine._value, 0 );

        stateMachine.postEvent( SampleStateMachine::EVENT_0 );
        CHECK_EQUAL( stateMachine.getState(), ( unsigned )SampleStateMachine::STATE_0 );
        CHECK_EQUAL( stateMachine._value, 0 );

        stateMachine.postEvent( SampleStateMachine::EVENT_2 );
        CHECK_EQUAL( stateMachine.getState(), ( unsigned )SampleStateMachine::STATE_0 );
        CHECK_EQUAL( stateMachine._value, 0 );

        stateMachine.postEvent( SampleStateMachine::EVENT_1 );
        CHECK_EQUAL( stateMachine.getState(), ( unsigned )SampleStateMachine::STATE_1 );
        CHECK_EQUAL( stateMachine._value, 1 );

        stateMachine.postEvent( SampleStateMachine::EVENT_2 );
        CHECK_EQUAL( stateMachine.getState(), ( unsigned )SampleStateMachine::STATE_2 );
        CHECK_EQUAL( stateMachine._value, 2 );

        stateMachine.postEvent( SampleStateMachine::EVENT_1 );
        CHECK_EQUAL( stateMachine.getState(), ( unsigned )SampleStateMachine::STATE_0 );
        CHECK_EQUAL( stateMachine._value, 4 );
    }
};

TEST(Common, StateMachine)
{
    Test_StateMachine::test();
}

