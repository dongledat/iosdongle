#include "common/net_buf.h"

TEST(Common, net_buf)
{
    net_buf_t *nb = net_buf_alloc(5);
    CHECK(nb != NULL);

	int res = net_buf_add_tail(nb, "123456", 6);
	CHECK(res != 0);

	char header[NET_BUF_DEFAULT_HEADER_SIZE + 1] = { 0 };
	res = net_buf_add_head(nb, header, sizeof(header));
	CHECK(res != 0);

	res = net_buf_add_head(nb, header, NET_BUF_DEFAULT_HEADER_SIZE);
	CHECK(res == 0);
	CHECK(nb->buffer == nb->data);

	net_buf_free(nb);
}
