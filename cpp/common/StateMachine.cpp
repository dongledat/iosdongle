#include "StateMachine.h"

using namespace common;

bool StateMachine::_postEvent( Event::Type event, Event::Data * data )
{
    if ( event > m_class.m_numberOfEvents ) {
//        TRACE_ERROR( ( "State machine class=%s - invalid event=%u", _class.name().c_str(), event ) );
        return false;
    }

    m_event = event;
    m_pending = true;

    m_history[ m_historyIdx ].state = m_state;
    m_history[ m_historyIdx ].event = event;

    m_historyIdx = ( m_historyIdx + 1 ) % MAX_HISTORY;

    if ( m_nesting > 1 ) {
//        TRACE_WARNING( ( "State machine class=%s - invalid nesting=%u; current state=%d, event=%d",
//             _class.name().c_str(), _nesting, _state, event ) );
        return false;
    }

    if ( m_nesting > 0 ) {
//        TRACE_DEBUG( ( "State machine class=%s - non-zero nesting=%u; current state=%d, event=%d",
//             _class.name().c_str(), _nesting, _state, event ) );
        m_pendingEvent = event;
        if (m_pendingData != NULL) 
        {
            delete m_pendingData;
            m_pendingData = data->copy();
        }
        return true;
    }

    while ( m_pending ) {
        unsigned index = m_state * m_class.m_numberOfEvents + event;

        m_nesting ++;
        m_pending = false;

        unsigned newState = m_class.m_matrix[ index ].state;
        const char * newStateName = m_class.m_matrix[ index ].newStateName;
        m_currentEventName = m_class.m_matrix[ index ].eventName;
        
        //
        // Don't change state in one of the following cases:
        //    - transition returns false
        //    - event is unhandled
        //
//        if ( _class._matrix[ index ].context->printable() )
//            TRACE_DEBUG( ( "State machine class=%s, instance=%p, state=%s, event=%s, new-state=%s",
//                _class.name().c_str(), this, _class._matrix[ index ].currentStateName, _currentEventName, newStateName ) );

        if ( m_class.m_matrix[ index ].action( m_inst, this, data ? data->getData() : NULL ) ) {
            if ( m_state != newState ) {
                m_class.m_moveInto[ newState ].action( m_inst, this, data ? data->getData() : NULL );
                m_state = newState;
                m_currentStateName = newStateName;
            }
        }

        m_nesting --;
        event = m_pendingEvent;
        data = m_pendingData;
    }

    return true;
}

bool StateMachine::nop( void * )
{
//    TRACE_WARNING( ( "State machine class=%s - unhandled transition, instance=%p, current state=%s, event=%d",
//                     _class.name().c_str(), _inst, getStateName(), _event ) );

    printHistory();

    return false;
}

void StateMachine::printHistory()
{
    int i = 0;
    int j = m_historyIdx;

//    TRACE_DEBUG(( "State machine history, instance=%p", this ));
//
//    for (; i < MAX_HISTORY; i++, j++ )
//    {
//        TRACE_DEBUG(( "state=%d, event=%d", _history[j % MAX_HISTORY].state, _history[j % MAX_HISTORY].event ));
//    }
}

const TransitionContext StateMachineClass::defaultPrintable( true );
const TransitionContext StateMachineClass::defaultNonPrintable( false );

StateMachineClass  StateMachine::m_dummy;
StateMachineClass  StateMachineClass::m_dummy;

StateMachineClass::Transition::Transition() :
    action( & StateMachine::nop ),
    state( STATE_NONE ),
    currentStateName( "none" ),
    newStateName( "none" ),
    eventName( "none" ),
    context( & defaultPrintable )
{
}

StateMachineClass::MoveInto::MoveInto() :
    action( & StateMachine::nop2 ),
    stateName( "none" ),
    print( false )
{
}

bool StateMachineClass::InitTransition(
    unsigned state,
    const char * currentStateName,
    unsigned event,
    const char * eventName,
    unsigned newState,
    const char * newStateName,
    OnEvent action,
    const TransitionContext * transitionContext )
{
    if ( event < m_numberOfEvents && state < m_numberOfStates ) {
        unsigned index = state * m_numberOfEvents + event;

        m_matrix[ index ].state = newState;
        m_matrix[ index ].action = action;
        m_matrix[ index ].currentStateName = currentStateName;
        m_matrix[ index ].newStateName = newStateName;
        m_matrix[ index ].eventName = eventName;
        m_matrix[ index ].context = transitionContext;
        return true;
    }
    else
        return false;
}

bool StateMachineClass::InitTransition( unsigned state, const char * currentStateName, unsigned event, const char * eventName, unsigned newState, const char * newStateName, OnEvent action, bool print )
{
    return InitTransition( state, currentStateName, event, eventName, newState, newStateName, action,
                           print ? & defaultPrintable : & defaultNonPrintable );
}

bool StateMachineClass::InitMoveInto( unsigned state, const char * stateName, OnEvent action, bool print )
{
    if ( state < m_numberOfStates ) {
        m_moveInto[ state ].action = action;
        m_moveInto[ state ].print = print;
        m_moveInto[ state ].stateName = stateName;
        return true;
    }
    else
        return false;
}

const TransitionContext * StateMachineClass::getTransitionContext( unsigned state, unsigned event ) const
{
    if ( state < m_numberOfStates && event < m_numberOfEvents )
        return NULL;

     const TransitionContext * context = m_matrix[ state * m_numberOfEvents + event ].context;

     return ( context == & defaultPrintable || context == & defaultNonPrintable ) ? NULL : context;
}
