#include "DongleSimulator.h"
#include "dat.h"
#include "CarSimulator.h"
#include <functional>
#include "pb_encode.h"
#include "pb_decode.h"
#include "DataSubscription.h"
#include "os\Sleep.h"
#include "os\OsTime.h"
#include "common\DatTime.h"
#include <iostream>

#define SERVER_BUF_SIZE (1000)

#define SOCKET_POLLING_TIMEOUT (50)

extern "C" int g_tls_enabled;
extern "C" int g_dtp_enabled;

DongleSimulator* DongleSimulator::g_DongleSimulatorInstance = nullptr;

DongleSimulator * DongleSimulator::get()
{
	if (!g_DongleSimulatorInstance) {
		g_DongleSimulatorInstance = new DongleSimulator();
	}

	return g_DongleSimulatorInstance;
}

DongleSimulator::DongleSimulator()
{
}


DongleSimulator::~DongleSimulator()
{
}

void DongleSimulator::start(unsigned short port, bool useTLS, bool useDTP, unsigned int timeoutInSec, bool useTCP, bool sendPerByte)
{
	m_networkProxyThread.setPort(port);
	m_networkProxyThread.setTimeout(timeoutInSec);
	m_networkProxyThread.setModeTCP(useTCP);
	m_networkProxyThread.setSendModePerByte(sendPerByte);

	setDTP(useDTP);
	setTLS(useTLS);

	m_networkProxyThread.start();
}

void DongleSimulator::setDTP(bool useDTP)
{
	g_dtp_enabled = useDTP ? 1 : 0;
}

void DongleSimulator::setTLS(bool useTLS)
{
	g_tls_enabled = useTLS ? 1 : 0;
}

void DongleSimulator::stop()
{
	m_networkProxyThread.stop(true);
	m_networkProxyThread.closeServer();
}

void DongleSimulator::wait()
{
	m_networkProxyThread.wait();
}

void DongleSimulator::setPort(unsigned short port)
{
	m_networkProxyThread.setPort(port);
}

void DongleSimulator::setTimeout(unsigned int timeoutInSec)
{
	m_networkProxyThread.setTimeout(timeoutInSec);
}

void DongleSimulator::setModeTCP(bool useTCP)
{
	m_networkProxyThread.setModeTCP(useTCP);
}

void DongleSimulator::setSendModePerByte(bool sendModePerByte)
{
	m_networkProxyThread.setSendModePerByte(sendModePerByte);
}

DongleSimulatorThread & DongleSimulator::getThread()
{
	return m_networkProxyThread;
}

DongleSimulatorThread::DongleSimulatorThread() :
	m_udpServer(nullptr),
	m_tcpServer(nullptr),
	m_lastKnownPeer({ 0 }),
	m_port(0),
	m_useTCP(false),
	m_sendPerByte(false),
	m_timeout(0)
{
}

int sim_dat_to_net_cb_c(const unsigned char* buffer, size_t length)
{
	//std::cout << "Sending " << length << " bytes" << std::endl;
	return DongleSimulator::get()->getThread().sim_dat_to_net_cb(buffer, length);
}

int sim_dat_to_net_cb_c_tcp(const unsigned char* buffer, size_t length)
{
	return DongleSimulator::get()->getThread().sim_dat_to_net_cb_tcp(buffer, length);
}

int DongleSimulatorThread::sim_dat_to_net_cb(const unsigned char* buffer, size_t length)
{
	int sent = m_udpServer->sendto(buffer, length, m_lastKnownPeer);
	return sent;
}

int DongleSimulatorThread::sim_dat_to_net_cb_tcp(const unsigned char* buffer, size_t length)
{
	// Send the response
	int sent = m_tcpConnectionSocket.send(buffer, length);
	return sent;
}

void DongleSimulatorThread::closeServer()
{
	if (m_useTCP) {
		if (m_tcpServer) {
			delete m_tcpServer;
			m_tcpServer = nullptr;
		}
	}
	else {
		if (m_udpServer) {
			delete m_udpServer;
			m_udpServer = nullptr;
		}
	}
}

void DongleSimulatorThread::setPort(unsigned short port)
{
	m_port = port;
}

void DongleSimulatorThread::setTimeout(unsigned int timeoutInSec)
{
	m_timeout = timeoutInSec;
	m_timeoutInUseconds = m_timeout * 1000 * 1000;
}

void DongleSimulatorThread::setModeTCP(bool useTCP)
{
	m_useTCP = useTCP;
}

void DongleSimulatorThread::setSendModePerByte(bool sendPerByte)
{
	m_sendPerByte = sendPerByte;
}

//
// ** General flow **:
//
// We "as texa" just got raw buffer from net.
// We pass it on to dat_from_net..
//
// dat_from_net will parse it, and send the request to the car, using dat_to_car_cb().
// Since this is the simulator, it will simply call sim_dat_to_car_cb.
//
// It will then return the response, which we need to send back.
//

void DongleSimulatorThread::forwardData(unsigned char * buffer, size_t length)
{
	if (m_sendPerByte) {
		for (unsigned int i = 0; i < length; ++i) {
			if (DAT_OK != dat_from_net(buffer + i, 1)) {
				//printf("dat_from_net returned error\n");
				//return -1;
			}
		}
	} else {
		if (DAT_OK != dat_from_net(buffer, length)) {
			//printf("dat_from_net returned error\n");
			//return -1;
		}
	}
}

unsigned int DongleSimulatorThread::_run_tcp()
{
	unsigned char buffer[SERVER_BUF_SIZE] = { 0 };
	int length = 0;

	// Initialize TCP server
	m_tcpServer = new net::TCPServer(m_port);
	if (-1 == m_tcpServer->getDescriptor()) {
		printf("dongle simulator failed to bind to UDP port");
		return -1;
	}

	common::Time startTime = os::Time::realUSeconds();
	while (1) {
		// Get connection from net
		m_tcpConnectionSocket = m_tcpServer->accept();

		// Got connection
		while (1) {
			length = m_tcpConnectionSocket.recvTimeoutInMS(buffer, SERVER_BUF_SIZE, SOCKET_POLLING_TIMEOUT);
			if (length < 0) {
				// Error - bye bye
				goto lblError;
			}
			else if (length == 0) {
				// No data yet
				common::Time currentTime(os::Time::realUSeconds());
				if (currentTime >(startTime + common::Time::uSeconds(m_timeoutInUseconds))) {
					goto lblError;
				}

				if (_state == thread_state_finish) {
					goto lblEnd;
				}
				continue;
			}

			forwardData(buffer, length);

		}

		if (_state == thread_state_finish) {
			goto lblEnd;
		}
	}

lblEnd:

	dat_done();
	return 0;

lblError:
	dat_done();
	return -1;
}

unsigned int DongleSimulatorThread::_run_udp()
{
	unsigned char buffer[SERVER_BUF_SIZE] = { 0 };
	int length = 0;

	// Initialize UDP server
	m_udpServer = new net::UdpServer(m_port);
	if (-1 == m_udpServer->getDescriptor()) {
		printf("dongle simulator failed to bind to UDP port");
		return -1;
	}
	m_udpServer->setTimeoutInMS(SOCKET_POLLING_TIMEOUT);

	common::Time startTime = os::Time::realUSeconds();
	while (1) {
		// Get buffer from net
		sockaddr_in peer = { 0 };
		length = m_udpServer->recvfrom(buffer, sizeof(buffer), peer);
		if (length < 0) {
			// Error - bye bye
			goto lblError;
		} else if (length == 0) {
			// No data yet
			common::Time currentTime(os::Time::realUSeconds());
			if (currentTime >(startTime + common::Time::uSeconds(m_timeoutInUseconds))) {
				goto lblError;
			}

			if (_state == thread_state_finish) {
				break;
			}

			continue;
		}

		startTime = os::Time::realUSeconds();

		// Multiple smartphones talking to this simulator would cause problems.
		// So we must rely on only one smartphone in each simulation. A bit nasty, but this is just a simulator, so whatever
		m_lastKnownPeer = peer;

		// Forward to dongle
		//std::cout << "Got " << length << " bytes" << std::endl;
		forwardData(buffer, length);

		if (_state == thread_state_finish) {
			break;
		}
	} 

	dat_done();
	return 0;

lblError:
	dat_done();
	return -1;
}

unsigned int DongleSimulatorThread::_run()
{
	unsigned int ret = 0;

	// Initialize dat context
	dat_context_t dat_context = { 0 };

	if (m_useTCP) {
		dat_context.dat_to_net_cb = sim_dat_to_net_cb_c_tcp;
	} else {
		dat_context.dat_to_net_cb = sim_dat_to_net_cb_c;
	}

	dat_context.dat_to_car_cb = sim_dat_to_car_cb;
	dat_init(&dat_context);

	if (m_useTCP) {
		 ret = _run_tcp();
	}
	else {
		ret = _run_udp();
	}

	dat_done();
	return ret;
}
