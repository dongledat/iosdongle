#pragma once

#include <map>
#include "os/Thread.h"
#include "dongle.pb.h"
#include <functional>

class DataSubscription : public os::Thread
{
public:
	DataSubscription(unsigned int transactionID, unsigned int urlCode, unsigned int timeout, std::function<unsigned int(const TDongleRsp*)> m_callback);

private:
	unsigned int m_urlCode;
	unsigned int m_timeout;

	unsigned int m_previousValue;
	unsigned int m_currentValue;

	unsigned int m_transactionID;

	std::function<unsigned int(const TDongleRsp*)> m_callback;

protected:

	virtual unsigned int _run();

	void randomPerformChange();
	unsigned int changeOccurred();
	void postChangeAsync();
	void saveNewState();
};

class SubscriptionManager
{
public:
	SubscriptionManager();
	void clear();
	unsigned int subscribe(unsigned int url_code, unsigned int timeout, unsigned int transactionID);
	unsigned int unsubscribe(unsigned int url_code);
	void setCallback(std::function<unsigned int(const TDongleRsp*)> function);

private:
	std::map<unsigned int, DataSubscription*> m_subscriptions;
	std::function<unsigned int(const TDongleRsp*)> m_callback;
};