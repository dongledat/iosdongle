
#define WIN32_LEAN_AND_MEAN
#include "DataSubscription.h"
#include "CarSimulatorSupportedDiagnostics.h"
#include "dat.h"

#include "pb.h"

SubscriptionManager g_DiagCodeSubscriptionManager;

void _getDataObject(int32_t UrlCode, TDongleRsp * rsp);
void getDataObject(const TDongleReq * req, TDongleRsp * rsp);
void subscribeObject(const TDongleReq * req, TDongleRsp * rsp);
void unsubscribeObject(const TDongleReq * req, TDongleRsp * rsp);
void fillJunk(const TDongleReq * req, TDongleRsp * rsp);

dat_result_e sim_dat_to_car_cb(const TDongleReq * req, TDongleRsp * rsp)
{
	switch (req->which_request) 
	{
	case (TDongleReq_GetDataObjectReq_tag):
		getDataObject(req, rsp);
		break;

	case (TDongleReq_SubscribeObjectReq_tag):
		subscribeObject(req, rsp);
		break;

	case (TDongleReq_UnsubscribeObjectReq_tag):
		unsubscribeObject(req, rsp);
		break;

	//PDU

	default:
		fillJunk(req, rsp);
		break;
	}

	rsp->id = req->id;
	rsp->has_id = true;

	return DAT_OK;
}

pb_bytes_array_t *pb_strdup(const char *s)
{
	int len = strlen(s) +1;
	pb_bytes_array_s *ret = (pb_bytes_array_s*)malloc(sizeof(pb_size_t) + len);
	ret->size = len;
	memcpy(ret->bytes, s, len);
	return ret;
}

void _getDataObject(int32_t UrlCode, TDongleRsp * rsp)
{
	switch (UrlCode) {
	case DIAG_FUELCONSUMPTION:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(5 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 5;
	elements[0].code = 0x0002001A;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00020018;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x00020019;
	elements[2].value.f = 0.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x00020009;
	elements[3].value.f = 0.0;
	elements[3].which_value = TDataElemenent_f_tag;
	elements[4].code = 0x0002001B;
	elements[4].value.f = 0.0;
	elements[4].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_LPGGASCONSUMPTION:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(5 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 5;
	elements[0].code = 0x00020022;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00020021;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x00020023;
	elements[2].value.f = 0.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x00020024;
	elements[3].value.f = 0.0;
	elements[3].which_value = TDataElemenent_f_tag;
	elements[4].code = 0x00020025;
	elements[4].value.f = 0.0;
	elements[4].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_CNGGASCONSUMPTION:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(5 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 5;
	elements[0].code = 0x0002001D;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x0002001C;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x0002001E;
	elements[2].value.f = 0.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x0002001F;
	elements[3].value.f = 0.0;
	elements[3].which_value = TDataElemenent_f_tag;
	elements[4].code = 0x00020020;
	elements[4].value.f = 0.0;
	elements[4].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_ODOMETER:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00020004;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_TOTALDAYMILEAGE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00020017;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_CRUISINGRANGE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(2 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 2;
	elements[0].code = 0x00020005;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00020016;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_FUELLEVEL:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(5 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 5;
	elements[0].code = 0x00020001;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00020012;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x00020002;
	elements[2].value.f = 0.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x00020014;
	elements[3].value.f = 0.0;
	elements[3].which_value = TDataElemenent_f_tag;
	elements[4].code = 0x00020015;
	elements[4].value.f = 0.0;
	elements[4].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_SERVICE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(6 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 6;
	elements[0].code = 0x00070008;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00070009;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x00070011;
	elements[2].value.f = 0.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x00070010;
	elements[3].value.f = 0.0;
	elements[3].which_value = TDataElemenent_f_tag;
	elements[4].code = 0x00070007;
	elements[4].value.f = 0.0;
	elements[4].which_value = TDataElemenent_f_tag;
	elements[5].code = 0x00070006;
	elements[5].value.f = 0.0;
	elements[5].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_OILSERVICE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(4 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 4;
	elements[0].code = 0x00070001;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00070000;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x0007000C;
	elements[2].value.f = 0.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x0007000D;
	elements[3].value.f = 0.0;
	elements[3].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_AVVEHICLESPEED:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00020011;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_DASHBOARDINDICATORS:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(47 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 47;
	elements[0].code = 0x00090004;
	elements[0].value.b = false;
	elements[0].which_value = TDataElemenent_b_tag;
	elements[1].code = 0x00010002;
	elements[1].value.b = false;
	elements[1].which_value = TDataElemenent_b_tag;
	elements[2].code = 0x00010007;
	elements[2].value.b = false;
	elements[2].which_value = TDataElemenent_b_tag;
	elements[3].code = 0x0001001E;
	elements[3].value.b = false;
	elements[3].which_value = TDataElemenent_b_tag;
	elements[4].code = 0x00090001;
	elements[4].value.b = false;
	elements[4].which_value = TDataElemenent_b_tag;
	elements[5].code = 0x00090000;
	elements[5].value.b = false;
	elements[5].which_value = TDataElemenent_b_tag;
	elements[6].code = 0x00010013;
	elements[6].value.b = false;
	elements[6].which_value = TDataElemenent_b_tag;
	elements[7].code = 0x00010014;
	elements[7].value.b = false;
	elements[7].which_value = TDataElemenent_b_tag;
	elements[8].code = 0x00090001;
	elements[8].value.b = false;
	elements[8].which_value = TDataElemenent_b_tag;
	elements[9].code = 0x00010005;
	elements[9].value.b = false;
	elements[9].which_value = TDataElemenent_b_tag;
	elements[10].code = 0x00010015;
	elements[10].value.b = false;
	elements[10].which_value = TDataElemenent_b_tag;
	elements[11].code = 0x00010011;
	elements[11].value.b = false;
	elements[11].which_value = TDataElemenent_b_tag;
	elements[12].code = 0x0001000F;
	elements[12].value.b = false;
	elements[12].which_value = TDataElemenent_b_tag;
	elements[13].code = 0x0009000F;
	elements[13].value.b = false;
	elements[13].which_value = TDataElemenent_b_tag;
	elements[14].code = 0x0001001F;
	elements[14].value.b = false;
	elements[14].which_value = TDataElemenent_b_tag;
	elements[15].code = 0x00010008;
	elements[15].value.b = false;
	elements[15].which_value = TDataElemenent_b_tag;
	elements[16].code = 0x00010016;
	elements[16].value.b = false;
	elements[16].which_value = TDataElemenent_b_tag;
	elements[17].code = 0x00090002;
	elements[17].value.b = false;
	elements[17].which_value = TDataElemenent_b_tag;
	elements[18].code = 0x00090003;
	elements[18].value.b = false;
	elements[18].which_value = TDataElemenent_b_tag;
	elements[19].code = 0x00010004;
	elements[19].value.b = false;
	elements[19].which_value = TDataElemenent_b_tag;
	elements[20].code = 0x00010009;
	elements[20].value.b = false;
	elements[20].which_value = TDataElemenent_b_tag;
	elements[21].code = 0x00010017;
	elements[21].value.b = false;
	elements[21].which_value = TDataElemenent_b_tag;
	elements[22].code = 0x00040004;
	elements[22].value.b = false;
	elements[22].which_value = TDataElemenent_b_tag;
	elements[23].code = 0x0001000E;
	elements[23].value.b = false;
	elements[23].which_value = TDataElemenent_b_tag;
	elements[24].code = 0x00010020;
	elements[24].value.b = false;
	elements[24].which_value = TDataElemenent_b_tag;
	elements[25].code = 0x00010010;
	elements[25].value.b = false;
	elements[25].which_value = TDataElemenent_b_tag;
	elements[26].code = 0x00010018;
	elements[26].value.b = false;
	elements[26].which_value = TDataElemenent_b_tag;
	elements[27].code = 0x00010019;
	elements[27].value.b = false;
	elements[27].which_value = TDataElemenent_b_tag;
	elements[28].code = 0x0001001A;
	elements[28].value.b = false;
	elements[28].which_value = TDataElemenent_b_tag;
	elements[29].code = 0x00010000;
	elements[29].value.b = false;
	elements[29].which_value = TDataElemenent_b_tag;
	elements[30].code = 0x0001001B;
	elements[30].value.b = false;
	elements[30].which_value = TDataElemenent_b_tag;
	elements[31].code = 0x0001000B;
	elements[31].value.b = false;
	elements[31].which_value = TDataElemenent_b_tag;
	elements[32].code = 0x00010003;
	elements[32].value.b = false;
	elements[32].which_value = TDataElemenent_b_tag;
	elements[33].code = 0x00010012;
	elements[33].value.b = false;
	elements[33].which_value = TDataElemenent_b_tag;
	elements[34].code = 0x0001000A;
	elements[34].value.b = false;
	elements[34].which_value = TDataElemenent_b_tag;
	elements[35].code = 0x00090005;
	elements[35].value.b = false;
	elements[35].which_value = TDataElemenent_b_tag;
	elements[36].code = 0x00090006;
	elements[36].value.b = false;
	elements[36].which_value = TDataElemenent_b_tag;
	elements[37].code = 0x00090007;
	elements[37].value.b = false;
	elements[37].which_value = TDataElemenent_b_tag;
	elements[38].code = 0x00090008;
	elements[38].value.b = false;
	elements[38].which_value = TDataElemenent_b_tag;
	elements[39].code = 0x00090009;
	elements[39].value.b = false;
	elements[39].which_value = TDataElemenent_b_tag;
	elements[40].code = 0x0009000A;
	elements[40].value.b = false;
	elements[40].which_value = TDataElemenent_b_tag;
	elements[41].code = 0x0009000B;
	elements[41].value.b = false;
	elements[41].which_value = TDataElemenent_b_tag;
	elements[42].code = 0x0009000C;
	elements[42].value.b = false;
	elements[42].which_value = TDataElemenent_b_tag;
	elements[43].code = 0x0009000D;
	elements[43].value.b = false;
	elements[43].which_value = TDataElemenent_b_tag;
	elements[44].code = 0x0009000E;
	elements[44].value.b = false;
	elements[44].which_value = TDataElemenent_b_tag;
	elements[45].code = 0x0001001C;
	elements[45].value.b = false;
	elements[45].which_value = TDataElemenent_b_tag;
	elements[46].code = 0x0001001D;
	elements[46].value.b = false;
	elements[46].which_value = TDataElemenent_b_tag;

	}
	break;
	case DIAG_ENGINETORQUE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00040015;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_EXTERIORTEMPERATURE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00020006;
	elements[0].value.f = -60.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_ENGINESPEED:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00040000;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_DRIVINGTIME:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x0002000A;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_COOLANTTEMPERATURE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00040001;
	elements[0].value.f = -60.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_BOOSTPRESSURE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(4 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 4;
	elements[0].code = 0x00040016;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00040018;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x0004001A;
	elements[2].value.f = 0.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x0004001C;
	elements[3].value.f = 0.0;
	elements[3].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_BAROMETRICPRESSURE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00020008;
	elements[0].value.f = 80.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_OILTEMPERATURE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x0004000F;
	elements[0].value.f = -60.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_BATTERYVOLTAGE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(2 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 2;
	elements[0].code = 0x00050001;
	elements[0].value.f = 5.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00050002;
	elements[1].value.f = 4.0;
	elements[1].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_TANKCAPACITY:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(3 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 3;
	elements[0].code = 0x0002000B;
	elements[0].value.f = 5.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x0002000D;
	elements[1].value.f = 4.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x0002000C;
	elements[2].value.f = 4.0;
	elements[2].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_VEHICLEIDENTIFICATIONNUMBER:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00060000;
	elements[0].value.s = pb_strdup("AAAAAAAAAA");
	elements[0].which_value = TDataElemenent_s_tag;

	}
	break;
	case DIAG_VEHICLESPEED:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00020000;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_ENGINELOAD:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00040005;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_ENGINETIME:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(3 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 3;
	elements[0].code = 0x00040011;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00040038;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x00040039;
	elements[2].value.f = 0.0;
	elements[2].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_EGR:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(5 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 5;
	elements[0].code = 0x0004001E;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x0004001F;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x00040020;
	elements[2].value.f = -100.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x00040021;
	elements[3].value.f = -40.0;
	elements[3].which_value = TDataElemenent_f_tag;
	elements[4].code = 0x00040022;
	elements[4].value.f = -40.0;
	elements[4].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_ALCOHOINFUEL:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00040023;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_INTAKEAIR:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(5 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 5;
	elements[0].code = 0x0004000E;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00040024;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x00040025;
	elements[2].value.f = 0.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x0004000C;
	elements[3].value.f = 0.0;
	elements[3].which_value = TDataElemenent_f_tag;
	elements[4].code = 0x0004000D;
	elements[4].value.f = -40.0;
	elements[4].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_WARMUPCYCLES:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x0002000E;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_THROTTLEPOSITION:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(2 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 2;
	elements[0].code = 0x00040026;
	elements[0].value.f = SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_ABSOLUTE;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00040027;
    elements[1].value.f = SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE;
	elements[1].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_ACCELERATORPEDALPOSITION:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00040009;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_FUELINJECTION:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(2 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 2;
	elements[0].code = 0x00040010;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00040028;
	elements[1].value.f = -40.0;
	elements[1].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_DISTANCETRAVELEDDTCCLEAR:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x0002000F;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_CATALYSTTEMPERATURE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(2 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 2;
	elements[0].code = 0x00040029;
	elements[0].value.f = -40.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x0004002A;
	elements[1].value.f = -40.0;
	elements[1].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_CHARGEAIRCOOLERTEMPERATURE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(2 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 2;
	elements[0].code = 0x0004002B;
	elements[0].value.f = -40.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x0004002C;
	elements[1].value.f = -40.0;
	elements[1].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_LAMBDAFUELMIXTURE:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(2 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 2;
	elements[0].code = 0x0004002D;
	elements[0].value.f = -100.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x0004002E;
	elements[1].value.f = -100.0;
	elements[1].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_LAMBDASENSOR:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(6 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 6;
	elements[0].code = 0x0004002F;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00040030;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;
	elements[2].code = 0x00040031;
	elements[2].value.f = -128.0;
	elements[2].which_value = TDataElemenent_f_tag;
	elements[3].code = 0x00040032;
	elements[3].value.f = -128.0;
	elements[3].which_value = TDataElemenent_f_tag;
	elements[4].code = 0x00040033;
	elements[4].value.f = 0.0;
	elements[4].which_value = TDataElemenent_f_tag;
	elements[5].code = 0x00040034;
	elements[5].value.f = 0.0;
	elements[5].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_FUELAIRRATIOCOMMANDED:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00040035;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_EVAPORATIVESYSTEM:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(2 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 2;

































	elements[0].code = 0x00040036;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;
	elements[1].code = 0x00040037;
	elements[1].value.f = 0.0;
	elements[1].which_value = TDataElemenent_f_tag;

	}
	break;
	case DIAG_IGNITIONCOUNTER:
	{                       auto& obj = rsp->response.GetDataObjectRsp.DataObject;
	TDataElemenent* elements = (TDataElemenent*)malloc(1 * sizeof(TDataElemenent));
	if (NULL == elements) {
		return;
	}
	obj.DataElement = elements;
	obj.DataElement_count = 1;
	elements[0].code = 0x00020010;
	elements[0].value.f = 0.0;
	elements[0].which_value = TDataElemenent_f_tag;

	}
	break;

default:
	return;
	}


	rsp->response.GetDataObjectRsp.DataObject.UrlCode = UrlCode;
	rsp->which_response = TDongleRsp_GetDataObjectRsp_tag;
}

void getDataObject(const TDongleReq * req, TDongleRsp * rsp)
{
	_getDataObject(req->request.GetDataObjectReq.UrlCode, rsp);
}


void subscribeObject(const TDongleReq * req, TDongleRsp * rsp)
{
	if (0 == g_DiagCodeSubscriptionManager.subscribe(req->request.SubscribeObjectReq.UrlCode, req->request.SubscribeObjectReq.Timeout, req->id)) {
		rsp->response.SubscribeObjectRsp.result = TSubscribeObjectRsp_EResult_OK;
	} else {
		rsp->response.SubscribeObjectRsp.result = TSubscribeObjectRsp_EResult_UrlCannotBeSubscribed;
	}

	rsp->which_response = TDongleRsp_SubscribeObjectRsp_tag;
}

void unsubscribeObject(const TDongleReq * req, TDongleRsp * rsp)
{
	if (0 == g_DiagCodeSubscriptionManager.unsubscribe(req->request.UnsubscribeObjectReq.ObjectId)) {
		rsp->response.UnsubscribeObjectRsp.result = TUnsubscribeObjectRsp_EResult_OK;
	} else {
		 rsp->response.UnsubscribeObjectRsp.result = TUnsubscribeObjectRsp_EResult_UrlCannotBeUnsubscribed;
	}

	rsp->which_response = TDongleRsp_UnsubscribeObjectRsp_tag;
}


/****************************************/
/**************** JUNK ******************/
/****************************************/

template <class T>
void default_junk(T * obj)
{
	memset(obj, 'A', sizeof(*obj) - 1);
}

void fillSerialNumberDeviceReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.SerialNumberDeviceRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_SerialNumberDeviceRsp_tag;
}

void fillBtMacAddressReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.BtMacAddressRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_BtMacAddressRsp_tag;
}

void fillHwVersionReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.HwVersionRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_HwVersionRsp_tag;
}

#define _SETSTRING(arr, str) \
	strncpy_s(arr, sizeof(arr), str, strlen(str) + 1)

void fillFwStatusReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.FwStatusRsp);
	//default_junk(obj);
	rsp->which_response = TDongleRsp_FwStatusRsp_tag;
	auto &resp = rsp->response.FwStatusRsp;

	_SETSTRING(resp.Ver_userApp.ver, "uapp v1");
	_SETSTRING(resp.Ver_serviceApp.ver, "serv v1");
	_SETSTRING(resp.Ver_uLoader.ver, "ldr v1");
	resp.ver_db = 1;
	resp.IDBrand = 12;
	resp.IDGroup = 13;
	resp.updateInProgress = 0;
}

 
void fillDevStatusReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.DiagStatusRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_DiagStatusRsp_tag;
	rsp->response.DiagStatusRsp.DiagnosticStatus = TDiagStatusRsp_EDiagnosticStatus_ConfiguredAndConnected;
}

void fillActivationStatusReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.ActivationStatusRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_ActivationStatusRsp_tag;
	_SETSTRING(rsp->response.ActivationStatusRsp.ActivationDate.Date, "1/1/1990");
	_SETSTRING(rsp->response.ActivationStatusRsp.ProductionDate.Date, "1/1/1990");
}

void fillSetProtocolModeReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.SetProtocolModeRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_SetProtocolModeRsp_tag;
	rsp->response.SetProtocolModeRsp.Result = TSetProtocolModeRsp_EResult_ok;
}

void fillOpenUpdatingSessionReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.OpenUpdatingSessionRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_OpenUpdatingSessionRsp_tag;
}

void fillDownloadDataBlockReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.DownloadDataBlockRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_DownloadDataBlockRsp_tag;
}

void fillCloseUpdatingSessionReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.CloseUpdatingSessionRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_CloseUpdatingSessionRsp_tag;
}

void fillGetAvailableDiagnosticParameterReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.GetAvailableDiagnosticParametersRsp);

    obj->IsSubscribed.size = 40;
    obj->UrlCode.size = 40;

	obj->IsSubscribed.bytes[0] = false;
	obj->UrlCode.bytes[0] = DIAG_FUELCONSUMPTION;

	obj->IsSubscribed.bytes[1] = false;
	obj->UrlCode.bytes[1] = DIAG_LPGGASCONSUMPTION;

	obj->IsSubscribed.bytes[2] = false;
	obj->UrlCode.bytes[2] = DIAG_CNGGASCONSUMPTION;

	obj->IsSubscribed.bytes[3] = false;
	obj->UrlCode.bytes[3] = DIAG_ODOMETER;

	obj->IsSubscribed.bytes[4] = false;
	obj->UrlCode.bytes[4] = DIAG_TOTALDAYMILEAGE;

	obj->IsSubscribed.bytes[5] = false;
	obj->UrlCode.bytes[5] = DIAG_CRUISINGRANGE;

	obj->IsSubscribed.bytes[6] = false;
	obj->UrlCode.bytes[6] = DIAG_FUELLEVEL;

	obj->IsSubscribed.bytes[7] = false;
	obj->UrlCode.bytes[7] = DIAG_SERVICE;

	obj->IsSubscribed.bytes[8] = false;
	obj->UrlCode.bytes[8] = DIAG_OILSERVICE;

	obj->IsSubscribed.bytes[9] = false;
	obj->UrlCode.bytes[9] = DIAG_AVVEHICLESPEED;

	obj->IsSubscribed.bytes[10] = false;
	obj->UrlCode.bytes[10] = DIAG_DASHBOARDINDICATORS;

	obj->IsSubscribed.bytes[11] = false;
	obj->UrlCode.bytes[11] = DIAG_ENGINETORQUE;

	obj->IsSubscribed.bytes[12] = false;
	obj->UrlCode.bytes[12] = DIAG_EXTERIORTEMPERATURE;

	obj->IsSubscribed.bytes[13] = false;
	obj->UrlCode.bytes[13] = DIAG_ENGINESPEED;

	obj->IsSubscribed.bytes[14] = false;
	obj->UrlCode.bytes[14] = DIAG_DRIVINGTIME;

	obj->IsSubscribed.bytes[15] = false;
	obj->UrlCode.bytes[15] = DIAG_COOLANTTEMPERATURE;

	obj->IsSubscribed.bytes[16] = false;
	obj->UrlCode.bytes[16] = DIAG_BOOSTPRESSURE;

	obj->IsSubscribed.bytes[17] = false;
	obj->UrlCode.bytes[17] = DIAG_BAROMETRICPRESSURE;

	obj->IsSubscribed.bytes[18] = false;
	obj->UrlCode.bytes[18] = DIAG_OILTEMPERATURE;

	obj->IsSubscribed.bytes[19] = false;
	obj->UrlCode.bytes[19] = DIAG_BATTERYVOLTAGE;

	obj->IsSubscribed.bytes[20] = false;
	obj->UrlCode.bytes[20] = DIAG_TANKCAPACITY;

	obj->IsSubscribed.bytes[21] = false;
	obj->UrlCode.bytes[21] = DIAG_VEHICLEIDENTIFICATIONNUMBER;

	obj->IsSubscribed.bytes[22] = false;
	obj->UrlCode.bytes[22] = DIAG_VEHICLESPEED;

	obj->IsSubscribed.bytes[23] = false;
	obj->UrlCode.bytes[23] = DIAG_ENGINELOAD;

	obj->IsSubscribed.bytes[24] = false;
	obj->UrlCode.bytes[24] = DIAG_ENGINETIME;

	obj->IsSubscribed.bytes[25] = false;
	obj->UrlCode.bytes[25] = DIAG_EGR;

	obj->IsSubscribed.bytes[26] = false;
	obj->UrlCode.bytes[26] = DIAG_ALCOHOINFUEL;

	obj->IsSubscribed.bytes[27] = false;
	obj->UrlCode.bytes[27] = DIAG_INTAKEAIR;

	obj->IsSubscribed.bytes[28] = false;
	obj->UrlCode.bytes[28] = DIAG_WARMUPCYCLES;

	obj->IsSubscribed.bytes[29] = false;
	obj->UrlCode.bytes[29] = DIAG_THROTTLEPOSITION;

	obj->IsSubscribed.bytes[30] = false;
	obj->UrlCode.bytes[30] = DIAG_ACCELERATORPEDALPOSITION;

	obj->IsSubscribed.bytes[31] = false;
	obj->UrlCode.bytes[31] = DIAG_FUELINJECTION;

	obj->IsSubscribed.bytes[32] = false;
	obj->UrlCode.bytes[32] = DIAG_DISTANCETRAVELEDDTCCLEAR;

	obj->IsSubscribed.bytes[33] = false;
	obj->UrlCode.bytes[33] = DIAG_CATALYSTTEMPERATURE;

	obj->IsSubscribed.bytes[34] = false;
	obj->UrlCode.bytes[34] = DIAG_CHARGEAIRCOOLERTEMPERATURE;

	obj->IsSubscribed.bytes[35] = false;
	obj->UrlCode.bytes[35] = DIAG_LAMBDAFUELMIXTURE;

	obj->IsSubscribed.bytes[36] = false;
	obj->UrlCode.bytes[36] = DIAG_LAMBDASENSOR;

	obj->IsSubscribed.bytes[37] = false;
	obj->UrlCode.bytes[37] = DIAG_FUELAIRRATIOCOMMANDED;

	obj->IsSubscribed.bytes[38] = false;
	obj->UrlCode.bytes[38] = DIAG_EVAPORATIVESYSTEM;

	obj->IsSubscribed.bytes[39] = false;
	obj->UrlCode.bytes[39] = DIAG_IGNITIONCOUNTER;

    rsp->which_response = TDongleRsp_GetAvailableDiagnosticParametersRsp_tag;
}

void fillAvailableDiagParametersAreChangedReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.AvailableDiagParametersAreChangedRsp);
	default_junk(obj);
	rsp->which_response = TDongleRsp_AvailableDiagParametersAreChangedRsp_tag;
}

void fillPDUReq(TDongleRsp * rsp)
{
	auto* obj = &(rsp->response.PDURsp);
	//default_junk(obj);
	rsp->response.PDURsp.Result = TPDURsp_EPDUResult_PDU_UNKNOWN_ERROR; // Because simulator doesn't support PDU
	rsp->which_response = TDongleRsp_PDURsp_tag;
}

void fillSRPKeyProof(TDongleRsp * rsp)
{
	//auto* obj = &(rsp->response.SRPKeyProof);
	//default_junk(obj);
	//rsp->which_response = TDongleRsp_SRPKeyProof_tag;
}

void fillJunk(const TDongleReq * req, TDongleRsp * rsp)
{
	switch (req->which_request)
	{
	case TDongleReq_SerialNumberDeviceReq_tag:
		fillSerialNumberDeviceReq(rsp);
		break;

	case TDongleReq_BtMacAddressReq_tag:
		fillBtMacAddressReq(rsp);
		break;

	case TDongleReq_HwVersionReq_tag:
		fillHwVersionReq(rsp);
		break;

	case TDongleReq_FwStatusReq_tag:
		fillFwStatusReq(rsp);
		break;

	case TDongleReq_DiagStatusReq_tag:
		fillDevStatusReq(rsp);
		break;

	case TDongleReq_ActivationStatusReq_tag:
		fillActivationStatusReq(rsp);
		break;

	case TDongleReq_SetProtocolModeReq_tag:
		fillSetProtocolModeReq(rsp);
		break;

	case TDongleReq_OpenUpdatingSessionReq_tag:
		fillOpenUpdatingSessionReq(rsp);
		break;

	case TDongleReq_DownloadDataBlockReq_tag:
		fillDownloadDataBlockReq(rsp);
		break;

	case TDongleReq_CloseUpdatingSessionReq_tag:
		fillCloseUpdatingSessionReq(rsp);
		break;

	case TDongleReq_GetAvailableDiagnosticParametersReq_tag:
		fillGetAvailableDiagnosticParameterReq(rsp);
		break;

	case TDongleReq_AvailableDiagParametersAreChangedReq_tag:
		fillAvailableDiagParametersAreChangedReq(rsp);
		break;

	case TDongleReq_PDUReq_tag:
		fillPDUReq(rsp);
		break;


	//case TDongleReq_SRPKeyProof_tag:
	//	fillSRPKeyProof(rsp);
	//	break;

		//case TDongleReq_ProtocolReq_tag:
		//	fillProtocolReq(rsp);
		//	break;

		//case TDongleReq_SRPUserBytesA_tag:
		//	fillSRPUserBytesA(rsp);
		//	break;

		//case TDongleReq_AuthenticationReq_tag:
		//	fillAuthenticationReq(rsp);
		//	break;

	default:
		break;
	}
}
