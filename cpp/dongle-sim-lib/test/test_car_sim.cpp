#include "../../ext/Cpputest/include/CppUTest/TestHarness.h"
#include "../../ext/Cpputest/include/CppUTest/MemoryLeakDetector.h"
#include "../../ext/Cpputest/include/CppUTest/TestMemoryAllocator.h"
#include "../../ext/Cpputest/include/CppUTest/PlatformSpecificFunctions.h"

#include <string.h>
#include <stdio.h>

#include "../CarSimulator.h"
#include "../DataSubscription.h"
#include "../CarSimulatorSupportedDiagnostics.h"
#include "dongle.pb.h"

#include <Windows.h>
#include <memory>
#include <functional>

#define REGISTER(f) registered_callback = f
#define UNREGISTER() registered_callback = NULL

typedef dat_result_e (*from_car_cb)(const TDongleRsp* resp);

static from_car_cb registered_callback = NULL;
static unsigned int callback_hit_count = 0;
static std::string* error_string;

extern SubscriptionManager g_DiagCodeSubscriptionManager;

#define TEST_SUBSCRIBE_SLEEP_TIME (1000)

dat_result_e test_car_sim_callback_dat_from_car(const TDongleRsp* resp)
{
	callback_hit_count++;
	if (registered_callback) {
		// DO NOT pass ownership on to callback
		return registered_callback(resp);
	}
	else {
		delete resp;
	}

	return DAT_ERROR;
}

void set_error_string(std::string * str) {
	if (error_string) {
		delete error_string;
	}

	error_string = str;
}

void clear_error_string() {
	if (error_string) {
		delete error_string;
	}
	error_string = nullptr;
}

void clean_globals() {
	g_DiagCodeSubscriptionManager.clear();
	clear_error_string();
	callback_hit_count = 0;
	g_DiagCodeSubscriptionManager.setCallback(std::bind(test_car_sim_callback_dat_from_car, std::placeholders::_1));
}

TEST_GROUP(CarSim)
{
	TEST_SETUP()
	{
		MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
		clean_globals();
	}

	TEST_TEARDOWN()
	{
		clean_globals();
		MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();
	}
};

TEST_GROUP(CarSim_Failers)
{
	TEST_SETUP()
	{
		clean_globals();
	}

	TEST_TEARDOWN()
	{
		clean_globals();
	}
};

TEST(CarSim, CarSimGetDataObject)
{

	TDongleReq r;
	r.which_request = TDongleReq_GetDataObjectReq_tag;
	r.request.GetDataObjectReq.UrlCode = DIAG_URL_CODE_THROTTLE_POSITION;
	r.id = 7080;
	r.has_id = true;

	TDongleRsp resp;

	sim_dat_to_car_cb(&r, &resp);

	CHECK_TEXT(resp.id == r.id, "ID do not match");
	CHECK_TEXT(resp.which_response == TDongleRsp_DataObjectRsp_tag, "response type does not match");
	CHECK_TEXT(resp.response.DataObjectRsp.UrlCode == r.request.GetDataObjectReq.UrlCode, "url code does not match");
	CHECK_TEXT(resp.response.DataObjectRsp.DataObject.DataElement_count == 2, "bad element count");

	CHECK_TEXT(resp.response.DataObjectRsp.DataObject.DataElement[0].code == DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "first code not match");
	CHECK_TEXT(resp.response.DataObjectRsp.DataObject.DataElement[0].value.u == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "1st value not as expected");


	CHECK_TEXT(resp.response.DataObjectRsp.DataObject.DataElement[1].code == DIAG_CODE_THROTTLE_POSITION_RELATIVE, "second code not match");
	CHECK_TEXT(resp.response.DataObjectRsp.DataObject.DataElement[1].value.u == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE, "2nd value not as expected");
}

static unsigned int last = 0;

dat_result_e SimpleSubscribeCallback(const TDongleRsp * resp)
{
	if (error_string) {
		// Already hit an error..
		return DAT_OK;
	}

	if (resp->which_response != TDongleRsp_DataObjectRsp_tag) {
		set_error_string(new std::string( "Got response of wrong type"));
		return DAT_ERROR;
	}

	if (resp->response.DataObjectRsp.UrlCode != DIAG_URL_CODE_THROTTLE_POSITION) {
		set_error_string(new std::string("Got response with wrong url code"));
		return DAT_ERROR;
	}

	if (resp->response.DataObjectRsp.DataObject.DataElement_count != 2) {
		set_error_string(new std::string("Got response with wrong amount of elements"));
		return DAT_ERROR;
	}

	if (resp->response.DataObjectRsp.DataObject.DataElement[0].code != DIAG_CODE_THROTTLE_POSITION_ABSOLUTE) {
		set_error_string(new std::string("Got response with wrong code for first element"));
		return DAT_ERROR;
	}

	if (resp->response.DataObjectRsp.DataObject.DataElement[1].code != DIAG_CODE_THROTTLE_POSITION_RELATIVE) {
		set_error_string(new std::string("Got response with wrong code for second element"));
		return DAT_ERROR;
	}

	if (resp->response.DataObjectRsp.DataObject.DataElement[1].value.u != 1338) {
		set_error_string(new std::string("Got response with wrong value for second element (not supposed to change)"));
		return DAT_ERROR;
	}

	if (!last) {
		last = resp->response.DataObjectRsp.DataObject.DataElement[0].value.u;
		return DAT_OK;
	}

	// This is not the first update we got - check to see the value actually changed
	if (resp->response.DataObjectRsp.DataObject.DataElement[0].value.u == last) {
		set_error_string(new std::string("Got response with the same value"));
		return DAT_ERROR;
	}
	else {
		//printf("New value: %d\n", resp->response.DataObjectRsp.DataObject.DataElement[0].value.u);
	}

	return DAT_OK;
}

TEST(CarSim, SimpleSubscribe)
{

	TDongleReq r;
	r.which_request = TDongleReq_SubscribeObjectReq_tag;
	r.request.SubscribeObjectReq.Timeout = 500;
	r.request.SubscribeObjectReq.UrlCode = DIAG_URL_CODE_THROTTLE_POSITION;

	r.id = 9898;
	r.has_id = true;

	last = 0;
	REGISTER(SimpleSubscribeCallback);
	TDongleRsp resp;
	sim_dat_to_car_cb(&r, &resp);

	CHECK_TEXT(resp.id == r.id, "ID do not match");
	CHECK_TEXT(resp.which_response == TDongleRsp_SubscribeObjectRsp_tag, "response type does not match");
	CHECK_TEXT(resp.response.SubscribeObjectRsp.result == TSubscribeObjectRsp_EResult_ok, "error in subscribing");

	// Let it run for a bit
	Sleep(TEST_SUBSCRIBE_SLEEP_TIME);
	UNREGISTER();

	// Check if there's an error
	if (error_string) {
		FAIL(error_string->c_str());
	}
}

TEST(CarSim, SimpleSubscribeAndUnsubscribe)
{

	TDongleReq r;
	r.which_request = TDongleReq_SubscribeObjectReq_tag;
	r.request.SubscribeObjectReq.Timeout = 500;
	r.request.SubscribeObjectReq.UrlCode = DIAG_URL_CODE_THROTTLE_POSITION;

	r.id = 9898;
	r.has_id = true;

	auto initial_hit_count = callback_hit_count;

	last = 0;
	REGISTER(SimpleSubscribeCallback);
	TDongleRsp resp;
	sim_dat_to_car_cb(&r, &resp);

	CHECK_TEXT(resp.id == r.id, "ID do not match");
	CHECK_TEXT(resp.which_response == TDongleRsp_SubscribeObjectRsp_tag, "response type does not match");
	CHECK_TEXT(resp.response.SubscribeObjectRsp.result == TSubscribeObjectRsp_EResult_ok, "error in subscribing");

	// Let it run for a bit
	Sleep(TEST_SUBSCRIBE_SLEEP_TIME);

	// Check if there's an error
	if (error_string) {
		FAIL(error_string->c_str());
	}

	// Now stop getting our callback, so even if updates occur - we don't do anything with them
	UNREGISTER();

	CHECK_TEXT(callback_hit_count > initial_hit_count, "Callback was not called");

	TDongleReq unsub;
	unsub.which_request = TDongleReq_UnsubscribeObjectReq_tag;
	unsub.request.UnsubscribeObjectReq.ObjectId = DIAG_URL_CODE_THROTTLE_POSITION;
	
	unsub.id = 4848;
	unsub.has_id = true;

	TDongleRsp resp2;
	sim_dat_to_car_cb(&unsub, &resp2);
	CHECK_TEXT(resp2.id == unsub.id, "ID do not match");
	CHECK_TEXT(resp2.which_response == TDongleRsp_UnsubscribeObjectRsp_tag, "response type does not match");
	CHECK_TEXT(resp2.response.SubscribeObjectRsp.result == TUnsubscribeObjectRsp_EResult_ok, "error in subscribing");
	
	// Now we are supposed to be unsubscribed, and the hit count should remain static
	auto hit_count = callback_hit_count;
	
	Sleep(TEST_SUBSCRIBE_SLEEP_TIME);

	CHECK_TEXT(hit_count == callback_hit_count, "Unsubscribe failed - callback was called while supposed to be unsubscribed");

}

/********************* Failers ****************************/
