#pragma once

#include "dat.h"
#include "dongle.pb.h"

dat_result_e sim_dat_to_car_cb(const TDongleReq * req, TDongleRsp * rsp);