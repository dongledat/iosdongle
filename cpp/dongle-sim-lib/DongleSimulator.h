#pragma once
#include "net/UdpServer.h"
#include "net/TCPServer.h"
#include "net/Net.h"
#include "os/Thread.h"
#include "common/net_buf.h"
#include "dongle.pb.h"
#include "dat.h"

#include <string>

class DongleSimulatorException 
{
public:
	explicit DongleSimulatorException(char * what) :
		m_what(what) {};

private:
	std::string m_what;
};

class DongleSimulatorThread : public os::Thread
{
public:
	DongleSimulatorThread();
	int sim_dat_to_net_cb(const unsigned char* buffer, size_t length);
	int sim_dat_to_net_cb_tcp(const unsigned char* buffer, size_t length);

	void closeServer();
	void setPort(unsigned short port);
	void setTimeout(unsigned int timeoutInSec);
	void setModeTCP(bool useTCP);
	void setSendModePerByte(bool sendPerByte);


	void forwardData(unsigned char * buffer, size_t length);
	unsigned int _run() override;
	unsigned int _run_udp();
	unsigned int _run_tcp();

private:
	bool m_useTCP;
	bool m_sendPerByte;

	net::UdpServer* m_udpServer;
	net::TCPServer* m_tcpServer;
	net::TCPSocket m_tcpConnectionSocket;

	sockaddr_in m_lastKnownPeer;
	unsigned short m_port;
	unsigned int m_timeout;
	unsigned int m_timeoutInUseconds;
};

class DongleSimulator
{
public:
	static DongleSimulator* get();
	~DongleSimulator();

	DongleSimulatorThread& getThread();

	void start(unsigned short port, bool useTLS, bool useDTP, unsigned int timeoutInSec = 0, bool useTCP = 0, bool sendPerByte = false);
	void stop();
	void wait();

	void setDTP(bool useDTP);
	void setTLS(bool useTLS);

	void setPort(unsigned short port);
	void setTimeout(unsigned int timeoutInSec);
	void setModeTCP(bool useTCP);
	void setSendModePerByte(bool sendModePerByte);


private:
	static DongleSimulator* g_DongleSimulatorInstance;

	DongleSimulator();
	DongleSimulatorThread m_networkProxyThread;
};