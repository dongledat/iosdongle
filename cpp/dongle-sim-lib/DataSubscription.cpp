#define WIN32_LEAN_AND_MEAN
#include "DataSubscription.h"
#include "CarSimulatorSupportedDiagnostics.h"
#include "dat.h"

#include "os/Thread.h"
#include <functional>
#include "pb.h"

//todo: use timeout instead
#define SLEEP_INTERVAL (10) //ms

SubscriptionManager::SubscriptionManager()
{
}

void SubscriptionManager::setCallback(std::function<unsigned int(const TDongleRsp*)> callback)
{
	m_callback = callback;
}

void SubscriptionManager::clear()
{
	for (auto& i : m_subscriptions) {
		i.second->kill();
		delete i.second;
	}

	m_subscriptions.clear();
}

unsigned int SubscriptionManager::subscribe(unsigned int url_code, unsigned int timeout, unsigned int transactionID)
{
	if (m_subscriptions.end() != m_subscriptions.find(url_code)) {
		return 1;
	}

	DataSubscription* s = new DataSubscription(transactionID, url_code, timeout, m_callback);
	s->start();

	m_subscriptions.emplace(url_code, s);
	return 0;
}

unsigned int SubscriptionManager::unsubscribe(unsigned int url_code)
{
	if (m_subscriptions.end() == m_subscriptions.find(url_code)) {
		return 1;
	}

	DataSubscription* sub = m_subscriptions.find(url_code)->second;
	sub->kill();
	delete sub;

	m_subscriptions.erase(url_code);
	return 0;
}

DataSubscription::DataSubscription(unsigned int transactionID, unsigned int urlCode, unsigned int timeout, std::function<unsigned int(const TDongleRsp*)> callback) :
	m_transactionID(transactionID),
	m_urlCode(urlCode),
	m_timeout(timeout),
	m_callback(callback)
{
}

unsigned int DataSubscription::_run()
{
	m_currentValue = DATA_SUBSCRIPTION_SIMULATION_BASE_VALUE;
	m_previousValue = m_currentValue;

	while (1) {
		Sleep(SLEEP_INTERVAL);

		randomPerformChange();

		if (changeOccurred()) {
			postChangeAsync();
			saveNewState();
		}
	}

	return 0;
}

void DataSubscription::randomPerformChange()
{
	if (0 == (rand() % 10)) {
		m_currentValue += DATA_SUBSCRIPTION_SIMULATION_VALUE_INCREMENTS;
	}
}

unsigned int DataSubscription::changeOccurred()
{
	if (m_currentValue != m_previousValue) {
		return 1;
	}

	return 0;
}

void _getDataObject(int32_t UrlCode, TDongleRsp * rsp);

void DataSubscription::postChangeAsync()
{
	TDongleRsp r = TDongleRsp_init_zero;

	_getDataObject(m_urlCode, &r);

	r.id = m_transactionID; //todo: Should ID be same as subscribe request's transaction ID?
	r.has_id = true;
	r.which_response = TDongleRsp_GetDataObjectRsp_tag;

	dat_from_car(&r);
}

void DataSubscription::saveNewState()
{
	m_previousValue = m_currentValue;
}
