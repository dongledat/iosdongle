import xmltodict
import pprint 
import sys

def Load(xml):
	fl = open(xml).readlines()

	dl = xmltodict.parse("".join(fl))
	ll = [[ol['@id'], ol['@url'], [], ol.get('Text', []), ol.get('Absolute', []), ol.get('Activity', [])] for ol in dl['Objects']['Object']]
	for l in ll:
		for v in range(3, 6):
			if type(l[v]) is not list:
				l[v] = [l[v]]
			l[2].extend([(v, lk['@id'], lk['@name'], 's = pb_strdup("AAAAAAAAAA")' if v == 3 else 'f = {}'.format(lk['@min']) if v == 4 else 'b = false', 
														'TDataElemenent_s_tag' if v == 3 else 'TDataElemenent_f_tag' if v == 4 else 'TDataElemenent_b_tag', ) 
										for lk in l[v]])
		l[:] = l[:3]
	pprint.pprint(ll)
	return ll


#"C:\Users\Avraham Yoffe\Documents\VW_DataPlug_2_0_ClientURLTranslationFIle\VW_DataPlug_2_0_ClientURLTranslationFIle\VW_DataPlug_2_0_DiagnosticDatacatalog_3_0.xml"



def PrinAvailable(res):
	print """
	    obj->IsSubscribed.size = {count};
	    obj->UrlCode.size = {count};

		""".format(count=len(res))
	off = 0
	for r in res:
		print """
		obj->IsSubscribed.bytes[{off}] = false;
		obj->UrlCode.bytes[{off}] = DIAG_{name};""".format(off=off, name=r[1].upper())
		off += 1
def PrintEnum(res):
	print '{'
	for r in res:
		print 'DIAG_{} = {},'.format(r[1].upper(), r[0])
	print '}'

def PrintSwitch(res):
	print 'switch (req->request.GetDataObjectReq.UrlCode) {'
	for r in res:
		print \
""" case DIAG_{name}:
	{{			
			auto& obj = rsp->response.GetDataObjectRsp.DataObject;
			TDataElemenent* elements = (TDataElemenent*)malloc({count} * sizeof(TDataElemenent));
			if (NULL == elements) {{
				return;
			}}
			obj.DataElement = elements;
			obj.DataElement_count = {count};""".format(name=r[1].upper(), count=len(r[2]))
		off = 0
		for er in r[2]:
			print \
"""			elements[{off}].code = {code};
			elements[{off}].value.{val};
			elements[{off}].which_value = {tp};""".format(off = off, code = er[1], val = er[3], tp = er[4])
			off += 1
		print """
	}
		break;"""
	print """
	default: 
		return;
	}"""


if __name__ == '__main__':
	res = Load(sys.argv[1])

	PrintSwitch(res)
	PrinAvailable(res)
	PrintEnum(res)

#pprint.pprint(res)