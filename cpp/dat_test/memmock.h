#ifndef _MEMMOCK_H_
#define _MEMMOCK_H_

#include <stdlib.h>

typedef int (*malloc_callback) (void* cb_param, void* caller, size_t size);
typedef int (*free_callback) (void* cb_param, void* caller, void * address);
typedef int (*realloc_callback) (void* cb_param, void* caller, void * address, size_t size);
typedef int (*calloc_callback) (void* cb_param, void* caller, size_t count, size_t size);

typedef enum {
	MOCK_MALLOC,
	MOCK_FREE,
	MOCK_REALLOC,
	MOCK_CALLOC,
	MOCK_NO_SUCH_FUNCTION
} MOCK_FUNCTION;

/* This should be a bitmask, cba */
typedef enum {
	MOCK_CALLBACK_DECISION_DEFAULT_MOCK = 0,
	MOCK_CALLBACK_DECISION_DONT_LOG = 1,
	MOCK_CALLBACK_DECISION_STOP_AFTER_CURRENT = 2,
	MOCK_CALLBACK_DECISION_DONT_MOCK = 4,
} MOCK_CALLBACK_DECISION;

typedef enum {
	MOCK_ACTION_FORWARD_TO_CPPUTEST,
	MOCK_ACTION_MOCK,
	MOCK_ACTION_CALL_ORIGINAL
} mock_action_type;

/* Macro to ensure correct ordering in cases where index is based on the enum values */
#define FOR_EACH_MOCK_FUNCTION(macro) \
	macro(MOCK_MALLOC) \
	macro(MOCK_FREE) \
	macro(MOCK_REALLOC) \
	macro(MOCK_CALLOC)

/* Startup functions ***
	memmock_X_start - start mocking the X function.

	Remarks:
		- When mocking, calls do not actually allocate/free memory, but just return some value.
		- Code duplication to enforce compiler evaluation of passed callback, to reduce chances to make an 
		  error with passing a wrong type of function pointer

	Parameters:
		count - amount of times to mock the function; 0 for infinite
		return_value - default value to return to caller
		cb - user-supplied callback. If non-null, will be called on each call with the received parameters.

	Returns:
		dat_mock_X_end - total amount of times mocked in this session.
*/
void memmock_malloc_start(size_t count, int return_value, malloc_callback cb, void* cb_param);
void memmock_free_start(size_t count, int return_value, free_callback cb, void* cb_param);
void memmock_realloc_start(size_t count, int return_value, realloc_callback cb, void* cb_param);
void memmock_calloc_start(size_t count, int return_value, calloc_callback cb, void* cb_param);

/*
	memmock_stop - stop mocking the X function.
	
	Parameters:
		function - Identifier of the function to stop mocking.

	Return value: 
		int - count of times the mock function was called, or -1 if error occurred
	
	Remarks:
		When not mocking, calls will be transfered to the unit test framework (CPPuTest) for coverage of actual heap use.
*/
int memmock_stop(MOCK_FUNCTION function);

void memmock_set_default_action(MOCK_FUNCTION function, mock_action_type action);
void memmock_set_default_action_all(mock_action_type action);
mock_action_type memmock_get_default_action(MOCK_FUNCTION function);

/*
	memmock_get_count - get count of calls to the mocked functions until now

	Parameters:
		function - Identifier of the function to query.

	Return value:
		int - count of times the mock function was called, or -1 if error occurred
*/
int memmock_get_count(MOCK_FUNCTION function);

/* Functions to override libc memory allocation functions */
#ifdef WIN32
#pragma warning( push )
#pragma warning( disable: 4273 28251 )
#endif
void* memmock_malloc(size_t size);
void memmock_free(void * address);
void* memmock_realloc(void * address, size_t size);
void* memmock_calloc(size_t count, size_t size);
#ifdef WIN32
#pragma warning( pop )
#endif

#endif /* _MEMMOCK_H_ */