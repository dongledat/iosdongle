#include <stdio.h>
#include <malloc.h>
#include <assert.h>

#include "../memmock.h"
#include "../memmock_fuzzy.h"

extern void perform_supermock_tests();
extern void perform_fuzzy_supermock_tests();
extern void finalize(); 

void simple_test() {

	void* a = malloc(15);
	assert(a);

	memmock_malloc_start(0, 0, NULL, NULL);

	unsigned int x = memmock_get_count(MOCK_MALLOC);
	assert(x == 0);

	a = malloc(15);
	assert(!a);

	x = memmock_get_count(MOCK_MALLOC);
	assert(x == 1);

	a = malloc(15);
	assert(!a);

	x = memmock_get_count(MOCK_MALLOC);
	assert(x == 2);

	a = malloc(15);
	assert(!a);

	x = memmock_end(MOCK_MALLOC);
	assert(x == 3);

	a = malloc(15);
	assert(a);

	memmock_malloc_start(0, 0, NULL, NULL);

	x = memmock_get_count(MOCK_MALLOC);
	assert(x == 0);

	a = malloc(15);
	assert(!a);

	x = memmock_get_count(MOCK_MALLOC);
	assert(x == 1);

	a = malloc(15);
	assert(!a);

	x = memmock_end(MOCK_MALLOC);
	assert(x == 2);

	printf("Simple test passed\n");
}


/***** Fuzzy *******/

typedef enum {
	ERR_SUCCESS,
	ERR_NO_MEMORY,

} ERROR;

int bar() {
	void* ptr1 = malloc(10);
	if (NULL == ptr1) {
		return ERR_NO_MEMORY;
	}

	void* ptr2 = malloc(10);
	if (NULL == ptr2) {
		return ERR_NO_MEMORY;
	}

	return ERR_SUCCESS;
}

int foo() {
	void* ptr1 = malloc(10);
	if (NULL == ptr1) {
		return ERR_NO_MEMORY;
	}

	void* ptr2 = malloc(10);
	if (NULL == ptr2) {
		return ERR_NO_MEMORY;
	}

	if (ERR_NO_MEMORY == bar()) {
		return ERR_NO_MEMORY;
	}

	void* ptr3 = malloc(10);
	if (NULL == ptr3) {
		return ERR_NO_MEMORY;
	}

	ptr3 = realloc(ptr3, 20);
	if (NULL == ptr3) {
		return ERR_NO_MEMORY;
	}

	void* ptr4 = calloc(10, 20);
	if (NULL == ptr4) {
		return ERR_NO_MEMORY;
	}

	return ERR_SUCCESS;
}


void fuzzy_test() {
	int count = 0;

	BEGIN_FUZZY_MOCK_LOOP(FUZZY_MOCK_ALL)

	auto result = foo();

	FUZZY_COMPLETE_ITERATION()

	assert(ERR_NO_MEMORY == result);
	count += 1;

	END_FUZZY_MOCK_LOOP()

	assert(count == 7);
	printf("Fuzzy test passed\n");
}

void simple_twoinarow_test() {
	void* a = malloc(15);
	assert(a);

	memmock_malloc_start(0, 0, NULL, NULL);

	auto x = memmock_get_count(MOCK_MALLOC);
	assert(x == 0);

	a = malloc(15);
	assert(!a);
	a = malloc(15);
	assert(!a);
	a = malloc(15);
	assert(!a);
	a = malloc(15);
	assert(!a);

	x = memmock_get_count(MOCK_MALLOC);
	assert(x == 4);

	a = malloc(15);
	assert(!a);

	x = memmock_get_count(MOCK_MALLOC);
	assert(x == 5);

	a = malloc(15);
	assert(!a);

	x = memmock_end(MOCK_MALLOC);
	assert(x == 6);

	a = malloc(15);
	assert(a);

	a = malloc(15);
	assert(a);

	printf("Simple Two-in-a-row test passed\n");
}

int footwo() {
	void* ptr1 = malloc(10);
	void* ptr2 = malloc(10);
	void* ptr3 = malloc(10);
	if (NULL == ptr1 || NULL == ptr2 || NULL == ptr3) {
		return ERR_NO_MEMORY;
	}
	return ERR_SUCCESS;
}

void fuzzy_twoinarow_test() {
	
	int count = 0;

	BEGIN_FUZZY_MOCK_LOOP(FUZZY_MOCK_ALL)

	auto result = footwo();

	FUZZY_COMPLETE_ITERATION()

	assert(ERR_NO_MEMORY == result);
	count += 1;

	END_FUZZY_MOCK_LOOP()

	assert(count == 3);

	printf("Fuzzy Two-in-a-row test passed\n");
}

void simple_loop_test() {
	memmock_malloc_start(0, 0, NULL, NULL);

	auto x = memmock_get_count(MOCK_MALLOC);
	assert(x == 0);

	void* a = malloc(10);

	for (int i = 0; i < 9; ++i) {
		x = memmock_get_count(MOCK_MALLOC);
		assert(x == i + 1);

		a = malloc(15);
		assert(!a);
	}


	x = memmock_get_count(MOCK_MALLOC);
	assert(x == 10);

	x = memmock_end(MOCK_MALLOC);
	assert(x == 10);

	printf("Simple loop test passed\n");
}

int fooloop() {
	int result = 0;

	void * a[10] = { 0 };
	result = 0;

	for (int i = 0; i < 10; ++i) {
		a[i] = malloc(10);
	}

	for (int i = 0; i < 10; ++i) {
		if (a[i] == NULL) {
			return ERR_NO_MEMORY;
		}
	}

	return ERR_SUCCESS;
}

void fuzzy_loop_test() {

	int count = 0;
	int result = 0;

	BEGIN_FUZZY_MOCK_LOOP(FUZZY_MOCK_ALL)

		result = fooloop();
	
	FUZZY_COMPLETE_ITERATION()
		auto fun = dat_fuzzy_mock_get_elapsed_iteration_count();
		assert(ERR_NO_MEMORY == result);
		count += 1;

	END_FUZZY_MOCK_LOOP()

	assert(count == 10); // Because there are 10 mallocs

	printf("Fuzzy loop test passed!\n");
}


void memmock_tests() {
	simple_test();
	fuzzy_test();

	simple_twoinarow_test();
	fuzzy_twoinarow_test();

	simple_loop_test();
	fuzzy_loop_test();
}