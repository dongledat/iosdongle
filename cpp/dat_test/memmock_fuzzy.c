#ifdef WIN32
#include <Windows.h>
#include <DbgHelp.h>
#else
#define FALSE (0)
#define TRUE (1)

#endif

#include <string.h>
#include <assert.h>

#include "memmock_fuzzy.h"
#include "memmock.h"

/*** Definitions ****/
#define STACK_TRACE_FRAME_COUNT (30)
#define STACK_TRACE_SKIP_FRAME_COUNT (5)

/*** Global variables ****/
static unsigned int g_previous_mock_counter = 0;
static int g_is_fuzzing = FALSE;
static int g_mocked_functions = 0;
static int g_reaped = 1;
static void* g_last_mock[STACK_TRACE_FRAME_COUNT + 1];

/*** Prototypes ****/
static int malloc_cb(void* cb_param, void* caller, size_t size);
static int free_cb(void* cb_param, void* caller, void * address);
static int realloc_cb(void* cb_param, void* caller, void * address, size_t size);
static int calloc_cb(void* cb_param, void* caller, size_t count, size_t size);
static int handle_callback(void* caller);
static unsigned int get_total_mock_count();
static void save_stack_trace();

extern int fuzzy_tracker_is_new_call(void* caller);
extern void fuzzy_tracker_log_caller(void* caller);
extern void fuzzy_tracker_clear_callers_list();
extern void fuzzy_tracker_clear_callers_current_counter();

#ifndef _DEBUG

void memmock_fuzzy_loop_start(int funcs_to_mock) {

}

void memmock_fuzzy_iteration_completed() {

}

int memmock_fuzzy_did_mock_happen() {
	return 0;
}

void memmock_fuzzy_loop_stop() {

}

unsigned int memmock_fuzzy_get_elapsed_iteration_count() {
	return 0;
}

#else

/*** Exported functions ****/
void memmock_fuzzy_loop_start(int funcs_to_mock) {
	assert(!g_is_fuzzing);
	assert(funcs_to_mock);

	if (funcs_to_mock & FUZZY_MOCK_MALLOC)
		memmock_malloc_start(0, 0, malloc_cb, NULL);

	if (funcs_to_mock & FUZZY_MOCK_FREE)
		memmock_free_start(0, 0, free_cb, NULL);

	if (funcs_to_mock & FUZZY_MOCK_REALLOC)
		memmock_realloc_start(0, 0, realloc_cb, NULL);

	if (funcs_to_mock & FUZZY_MOCK_CALLOC)
		memmock_calloc_start(0, 0, calloc_cb, NULL);

	g_previous_mock_counter = 0;
	g_is_fuzzing = TRUE;
	g_mocked_functions = funcs_to_mock;
	memset(g_last_mock, 0, sizeof(g_last_mock));
	g_reaped = 1;
}

void memmock_fuzzy_iteration_completed() {
	assert(g_is_fuzzing);

	unsigned int new_counter = get_total_mock_count();
	g_previous_mock_counter = new_counter;

	fuzzy_tracker_clear_callers_current_counter();
	g_reaped = 1;
}

int memmock_fuzzy_did_mock_happen() {
	assert(g_is_fuzzing);

	unsigned int new_counter = get_total_mock_count();
	if (new_counter == g_previous_mock_counter) {
		return FALSE;
	}
	assert(g_previous_mock_counter + 1 == new_counter);

	return TRUE;
}

void** memmock_fuzzy_get_last_mock() {
	return g_last_mock;
}

void memmock_fuzzy_loop_stop() {
	assert(g_is_fuzzing);

	if (g_mocked_functions & FUZZY_MOCK_MALLOC)
		memmock_stop(MOCK_MALLOC);

	if (g_mocked_functions & FUZZY_MOCK_FREE)
		memmock_stop(MOCK_FREE);
	
	if (g_mocked_functions & FUZZY_MOCK_REALLOC)
		memmock_stop(MOCK_REALLOC);

	if (g_mocked_functions & FUZZY_MOCK_CALLOC)
		memmock_stop(MOCK_CALLOC);

	fuzzy_tracker_clear_callers_list();
	g_previous_mock_counter = 0;
	g_is_fuzzing = FALSE;
	g_mocked_functions = 0;
	memset(g_last_mock, 0, sizeof(g_last_mock));

}

unsigned int memmock_fuzzy_get_elapsed_iteration_count() {
	return g_previous_mock_counter;
}

/*** Internal functions ****/
int malloc_cb(void* cb_param, void* caller, size_t size) {
	return handle_callback(caller);
}

int free_cb(void* cb_param, void* caller, void* address) {
	return handle_callback(caller);
}

int realloc_cb(void* cb_param, void* caller, void* address, size_t size) {
	return handle_callback(caller);
}

int calloc_cb(void* cb_param, void* caller, size_t count, size_t size) {
	return handle_callback(caller);
}

int handle_callback(void* caller) {
	int retval = 0;

	if (!g_reaped) {
		/* We already mocked a function in this iteration */
		return MOCK_CALLBACK_DECISION_DONT_MOCK;
	}

	if (fuzzy_tracker_is_new_call(caller)) {
		/* We're mocking */
		retval = MOCK_CALLBACK_DECISION_DEFAULT_MOCK;
		g_reaped = 0;
	} else {
		retval = MOCK_CALLBACK_DECISION_DONT_MOCK;
	}

	if (MOCK_CALLBACK_DECISION_DEFAULT_MOCK == retval) {
		/* We're mocking, let's save the stack trace */
		save_stack_trace();
	}

	fuzzy_tracker_log_caller(caller);
	return retval;
}

unsigned int get_total_mock_count() {
	unsigned int count = 0;

	assert(g_is_fuzzing);

	if (g_mocked_functions & FUZZY_MOCK_MALLOC)
		count += memmock_get_count(MOCK_MALLOC);

	if (g_mocked_functions & FUZZY_MOCK_FREE)
		count += memmock_get_count(MOCK_FREE);

	if (g_mocked_functions & FUZZY_MOCK_REALLOC)
		count += memmock_get_count(MOCK_REALLOC);

	if (g_mocked_functions & FUZZY_MOCK_CALLOC)
		count += memmock_get_count(MOCK_CALLOC);

	return count;
}

void save_stack_trace() {
#ifdef WIN32
	memset(g_last_mock, 0, sizeof(g_last_mock));
	CaptureStackBackTrace(STACK_TRACE_SKIP_FRAME_COUNT, STACK_TRACE_FRAME_COUNT, (PVOID*)&g_last_mock, NULL);
#endif
}

#endif /* _DEBUG */