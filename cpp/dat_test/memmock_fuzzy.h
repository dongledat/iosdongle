#ifndef _MEMMOCK_FUZZY_H_
#define _MEMMOCK_FUZZY_H_

/* 
	Important open issues:

	1.	Case coverage - since we mock once per caller, if the same line of code that
	    called malloc is called more than once, we will only catch it once:

		...
		for (int i = 0; i < arr_len; ++i) {
			arr[i] = malloc(512);
		}
		...

		We will treat all these mallocs the same, since they are (probably) called 
		from the same address in memory (except if the compiler performs loop unfolding).
		Thus only the first malloc will be mocked.

		An optional solution would be to save a map of <caller, max_times_called, current_times_called>.
		Every time malloc is called, we would increase current_times_called, until it surpasses
		max_times_called (until then we just NOT mock the function). Then we max_times_called++,
		and mock the current call.

	2. Code coverage - since we fail one malloc every iteration, we don't catch permutations of 
	   failed-mallocs (i.e. more than one malloc failing before the tested code realises and exits).
	   For example:

	   ...
	   auto p1 = malloc(32);
	   auto p2 = malloc(32);
	   auto p3 = malloc(32);

	   if (NULL == p1 && NULL == p3) {
			return ERR_NO_MEM;
	   }
	   ...

	   This is of course a silly case, but it demonstrates the point and there might be real life cases.

	3. Thread safety - there is none of that here.
	
*/

#include "memmock.h"

/* 
	Usage:
	=====
	
		BEGIN_FUZZY_MOCK_LOOP(FUZZY_MOCK_ALL)

		<code to test>

		FUZZY_COMPLETE_ITERATION()

		<post-iteration - check test condition>

		END_FUZZY_MOCK_LOOP()

	Example:
	=======

		BEGIN_FUZZY_MOCK_LOOP(FUZZY_MOCK_ALL)

		auto param = new std::vector(10);
		auto result = do_authentication(param)

		FUZZY_COMPLETE_ITERATION()

		assert(ERR_NO_MEM == result);

		END_FUZZY_MOCK_LOOP()
*/



/*
	Macros should be processed to:
	-------------------------------

	dat_fuzzy_mock_loop_start(FUZZY_MOCK_ALL);

	while (1 == 1) {
		auto result = foo();
		dat_fuzzy_mock_loop_iteration_completed();
		if (!dat_fuzzy_mock_loop_did_mock_happen()) {
			break;
		}

		assert(result == -1);
	}

	dat_fuzzy_mock_loop_stop();
*/
#define BEGIN_FUZZY_MOCK_LOOP(funcs_to_mock) \
	memmock_fuzzy_loop_start(funcs_to_mock); \
	while (1 == 1) {

#define FUZZY_COMPLETE_ITERATION() \
	if (!memmock_fuzzy_did_mock_happen()) { \
		/* Because if the iteration completed without a mock, we're done here - break out of the fuzzy loop */ \
		break; \
	} \
	memmock_fuzzy_iteration_completed();

#define END_FUZZY_MOCK_LOOP() \
	} \
	memmock_fuzzy_loop_stop();


void memmock_fuzzy_loop_start(int funcs_to_mock);
void memmock_fuzzy_iteration_completed();
int memmock_fuzzy_did_mock_happen();
void memmock_fuzzy_loop_stop();
unsigned int memmock_fuzzy_get_elapsed_iteration_count();
void** memmock_fuzzy_get_last_mock();

/* Should use bitmask... */
#define FUZZY_MOCK_MALLOC (1)
#define FUZZY_MOCK_FREE (2)
#define FUZZY_MOCK_REALLOC (4)
#define FUZZY_MOCK_CALLOC (8)
#define FUZZY_MOCK_ALLOCATIONS (FUZZY_MOCK_MALLOC | FUZZY_MOCK_REALLOC | FUZZY_MOCK_CALLOC)
#define FUZZY_MOCK_ALL (FUZZY_MOCK_ALLOCATIONS | FUZZY_MOCK_FREE)

#endif /* _MEMMOCK_FUZZY_H_ */