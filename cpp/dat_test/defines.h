#ifndef _DEFINES_H_
#define _DEFINES_H_

#define malloc memmock_malloc
#define free memmock_free
#define calloc memmock_calloc
#define realloc memmock_realloc

#endif /* _DEFINES_H_ */