#ifdef _DEBUG

#include <assert.h>
#include <set>
#include <map>

typedef struct {
	unsigned int max_called;
	unsigned int current_called;
} calls;

#ifdef WIN32

static std::set<unsigned int> callers;
static std::map<unsigned int, calls> callers_counter;


extern "C" int fuzzy_tracker_is_new_call(void* caller) {
	if (callers.end() == callers.find(reinterpret_cast<unsigned int>(caller))) {
		return 1;
	}

	auto c = callers_counter.find(reinterpret_cast<unsigned int>(caller));
	auto max_called = c->second.max_called;
	auto current_called = c->second.current_called;

	if (current_called >= max_called) {
		/* This is a new call - this is the first iteration where we got to this Nth call from the same caller (same line of code) */
		return 1;
	}

	/* We've already had an iteration which got to this number of calls from the same caller (same line of code) */
	return 0;
}

extern "C" void fuzzy_tracker_log_caller(void* caller) {
	if (callers.end() == callers.find(reinterpret_cast<unsigned int>(caller))) {
		callers.emplace(reinterpret_cast<unsigned int>(caller));
		calls test = { 1, 1 };
		callers_counter.emplace(reinterpret_cast<unsigned int>(caller), test);
		return;
	}

	auto c = callers_counter.find(reinterpret_cast<unsigned int>(caller));

	c->second.current_called++;
	if (c->second.current_called > c->second.max_called) {
		assert(c->second.current_called = c->second.max_called + 1);

		c->second.max_called = c->second.current_called;
	}
}

extern "C" void fuzzy_tracker_clear_callers_list() {
	callers.clear();
	callers_counter.clear();
}

extern "C" void fuzzy_tracker_clear_callers_current_counter() {
	for (auto& i : callers_counter) {
		i.second.current_called = 0;
	}
}

#elif 0

#include <vector>

static set<unsigned int> callers;

static vector<unsigned int> callers_counter_caller;
static vector<calls> callers_counter_calls_count;

unsigned int vector_index_of(vector<unsigned int> & v, unsigned int value) {
  unsigned int index = 0;
  for (vector<unsigned int>::iterator i = v.begin(); i != v.end(); ++i) {
    if (*i == value) {
      return index;
    }
    
    index++;
  }
  
  return -1;
}


extern "C" int fuzzy_tracker_is_new_call(void* caller) {
	if (callers.end() == callers.find(reinterpret_cast<unsigned int>(caller))) {
		return 1;
	}

        unsigned int index = vector_index_of(callers_counter_caller, (unsigned int)caller);
        if (-1 == index) {
          return 1;
        }
        
        unsigned int max_called = callers_counter_calls_count[index].max_called;
        unsigned int current_called = callers_counter_calls_count[index].current_called;

	if (current_called >= max_called) {
		/* This is a new call - this is the first iteration where we got to this Nth call from the same caller (same line of code) */
		return 1;
	}

	/* We've already had an iteration which got to this number of calls from the same caller (same line of code) */
	return 0;
}

extern "C" void fuzzy_tracker_log_caller(void* caller) {
	if (callers.end() == callers.find(reinterpret_cast<unsigned int>(caller))) {
		callers.insert(reinterpret_cast<unsigned int>(caller));
		calls test = { 1, 1 };
		callers_counter_caller.push_back(reinterpret_cast<unsigned int>(caller));
                callers_counter_calls_count.push_back(test);
		return;
	}

        unsigned int index = vector_index_of(callers_counter_caller, (unsigned int)caller);
        if (-1 == index) {
          return;
        }
        
        
	callers_counter_calls_count[index].current_called++;
	if (callers_counter_calls_count[index].current_called > callers_counter_calls_count[index].max_called) {
		assert(callers_counter_calls_count[index].current_called = callers_counter_calls_count[index].max_called + 1);

		callers_counter_calls_count[index].max_called = callers_counter_calls_count[index].current_called;
	}
}

extern "C" void fuzzy_tracker_clear_callers_list() {
	callers.clear();
        callers_counter_caller.clear();
	callers_counter_calls_count.clear();
}

extern "C" void fuzzy_tracker_clear_callers_current_counter() {
        for (int i = 0; i < callers_counter_calls_count.size(); ++i) {
          callers_counter_calls_count[i].current_called = 0;
        }
}

#else



extern "C" int fuzzy_tracker_is_new_call(void* caller) {
  return 0;
}

extern "C" void fuzzy_tracker_log_caller(void* caller) {
}

extern "C" void fuzzy_tracker_clear_callers_list() {
}

extern "C" void fuzzy_tracker_clear_callers_current_counter() {
}


#endif

#endif /* _DEBUG */