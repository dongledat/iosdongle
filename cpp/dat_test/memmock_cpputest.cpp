#pragma once

#include "memmock_cpputest.h"

MemmockFuzzyCPPUTest::MemmockFuzzyCPPUTest()
{
	memmock_fuzzy_loop_start(FUZZY_MOCK_ALL);
	m_started = true;
}

MemmockFuzzyCPPUTest::~MemmockFuzzyCPPUTest()
{
	if (m_started) {
		memmock_fuzzy_loop_stop();
		m_started = false;
	}
}

void MemmockFuzzyCPPUTest::doneMocking()
{
	/* Clean exit, test does not need to know about last mock */
	UtestShell::getCurrent()->setMockLocation(NULL);

	if (m_started) {
		memmock_fuzzy_loop_stop();
		m_started = false;
	}
}

bool MemmockFuzzyCPPUTest::didMock()
{
	return (0 != memmock_fuzzy_did_mock_happen());
}

void MemmockFuzzyCPPUTest::iterationCompleted()
{
	memmock_fuzzy_iteration_completed();
	UtestShell::getCurrent()->setMockLocation(memmock_fuzzy_get_last_mock());
}





/* Bug: mixing use of set_default_action_all and get_default_action (single).
	    I don't really care, probably won't be mixed anwyay */
MemmockCPPUTest::MemmockCPPUTest() :
	m_originalDefaultActionType(memmock_get_default_action(MOCK_MALLOC))
{
}

MemmockCPPUTest::~MemmockCPPUTest()
{
	memmock_set_default_action_all(m_originalDefaultActionType);
}

void MemmockCPPUTest::setDefaultAction(mock_action_type action_type)
{
	memmock_set_default_action_all(action_type);
}

void MemmockCPPUTest::startMock()
{
	memmock_malloc_start(0, NULL, NULL, NULL);
	memmock_free_start(0, NULL, NULL, NULL);
	memmock_realloc_start(0, NULL, NULL, NULL);
	memmock_calloc_start(0, NULL, NULL, NULL);
}

void MemmockCPPUTest::stopMock()
{
	memmock_stop(MOCK_MALLOC);
	memmock_stop(MOCK_FREE);
	memmock_stop(MOCK_REALLOC);
	memmock_stop(MOCK_CALLOC);
}

size_t MemmockCPPUTest::getCount(MOCK_FUNCTION function)
{
	return memmock_get_count(function);
}
