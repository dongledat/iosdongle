#include <assert.h>


#include "memmock.h"

#include "CppUTest\TestHarness_c.h"

#ifndef WIN32
void* _ReturnAddress() {
  return NULL;
}

#include <stdarg.h>
#define __crt_va_arg(a,b) va_arg(a,b)
#define __crt_va_start(a,b) va_start(a,b)
#endif

/*** Definitions ****/
typedef enum {
	MOCK_STATE_NOT_MOCKING,
	MOCK_STATE_MOCKING,
} MOCK_STATE;

typedef struct {
	MOCK_FUNCTION function;
	MOCK_STATE state;
	unsigned int mock_count;
	unsigned int max_mock_count;
	unsigned int return_value;
	void * mock_callback;
	void * callback_param;
	unsigned int debug_count;
	mock_action_type default_action;
} mock_info;

#define RESET_MOCK_ENTRY(function) \
	{	\
		function,				\
		MOCK_STATE_NOT_MOCKING,	\
		0,						\
		0,						\
		0,						\
		NULL,					\
		0,						\
		MOCK_ACTION_FORWARD_TO_CPPUTEST \
	}

typedef struct {
	mock_action_type action_type;
	int return_value;
} mock_action;

#define CREATE_MOCK_ENTRY(function) \
	RESET_MOCK_ENTRY(function),

#ifndef _DEBUG
void memmock_malloc_start(size_t count, int return_value, malloc_callback cb, void* cb_param) {

}

void memmock_free_start(size_t count, int return_value, free_callback cb, void* cb_param) {

}

void memmock_realloc_start(size_t count, int return_value, realloc_callback cb, void* cb_param) {

}

void memmock_calloc_start(size_t count, int return_value, calloc_callback cb, void* cb_param) {

}

int memmock_stop(MOCK_FUNCTION function) {
	return 0;
}

void memmock_set_default_action(MOCK_FUNCTION function, mock_action_type action)
{
}

int memmock_get_count(MOCK_FUNCTION function) {
	return 0;
}

#else


/*** Global variables ****/
static mock_info g_mock_info_db[] = {
	FOR_EACH_MOCK_FUNCTION(CREATE_MOCK_ENTRY)
};

/*** Prototypes ****/
static void mock_reset_mock_info(MOCK_FUNCTION function);
static void mock_begin(MOCK_FUNCTION function, int count, int return_value, void* cb, void* cb_param);
static mock_action register_mock_call(MOCK_FUNCTION function, void* caller, ...);

/*** Exported functions ****/
void memmock_malloc_start(size_t count, int return_value, malloc_callback cb, void* cb_param)
{
	mock_begin(MOCK_MALLOC, count, return_value, (void*)cb, cb_param);
}

void memmock_free_start(size_t count, int return_value, free_callback cb, void* cb_param)
{
	mock_begin(MOCK_FREE, count, return_value, (void*)cb, cb_param);
}

void memmock_realloc_start(size_t count, int return_value, realloc_callback cb, void* cb_param)
{
	mock_begin(MOCK_REALLOC, count, return_value, (void*)cb, cb_param);
}

void memmock_calloc_start(size_t count, int return_value, calloc_callback cb, void* cb_param)
{
	mock_begin(MOCK_CALLOC, count, return_value, (void*)cb, cb_param);
}

int memmock_stop(MOCK_FUNCTION function)
{
	int count = g_mock_info_db[function].mock_count;

	mock_reset_mock_info(function);
	return count;
}

void memmock_set_default_action(MOCK_FUNCTION function, mock_action_type action)
{
	g_mock_info_db[function].default_action = action;
}

void memmock_set_default_action_all (mock_action_type action)
{
	memmock_set_default_action(MOCK_MALLOC, action);
	memmock_set_default_action(MOCK_FREE, action);
	memmock_set_default_action(MOCK_CALLOC, action);
	memmock_set_default_action(MOCK_REALLOC, action);
}

mock_action_type memmock_get_default_action(MOCK_FUNCTION function)
{
	return g_mock_info_db[function].default_action;
}

int memmock_get_count(MOCK_FUNCTION function)
{
	assert(MOCK_STATE_MOCKING == g_mock_info_db[function].state);

	if (MOCK_STATE_MOCKING != g_mock_info_db[function].state) {
		return 0;
	}

	return g_mock_info_db[function].mock_count;
}

#include <stdio.h>

/*** Replacement functions ****/
void * memmock_malloc(size_t size) {
	g_mock_info_db[MOCK_MALLOC].debug_count++;
	mock_action action = register_mock_call(MOCK_MALLOC, _ReturnAddress(), size);
	if (MOCK_ACTION_FORWARD_TO_CPPUTEST == action.action_type) {
		return (void*)cpputest_malloc(size);
	} else if (MOCK_ACTION_CALL_ORIGINAL == action.action_type) {
		return malloc(size);
	}

	/* I'm mocking here! */
	return (void*)action.return_value;
}

void memmock_free(void * address) {
	g_mock_info_db[MOCK_FREE].debug_count++;
	mock_action action = register_mock_call(MOCK_FREE, _ReturnAddress(), address);
	if (MOCK_ACTION_FORWARD_TO_CPPUTEST == action.action_type) {
		cpputest_free(address);
		return;
	} else if (MOCK_ACTION_CALL_ORIGINAL == action.action_type) {
		free(address);
		return;
	}

	/* I'm mocking here! */
}

void* memmock_realloc(void * address, size_t size) {
	g_mock_info_db[MOCK_REALLOC].debug_count++;
	mock_action action = register_mock_call(MOCK_REALLOC, _ReturnAddress(), address, size);
	if (MOCK_ACTION_FORWARD_TO_CPPUTEST == action.action_type) {
		return (void*)cpputest_realloc(address, size);
	} else if (MOCK_ACTION_CALL_ORIGINAL == action.action_type) {
		return realloc(address, size);
	}

	/* I'm mocking here! */
	return (void*)action.return_value;
}

void * memmock_calloc(size_t count, size_t size) {
	g_mock_info_db[MOCK_CALLOC].debug_count++;
	mock_action action = register_mock_call(MOCK_CALLOC, _ReturnAddress(), count, size);
	if (MOCK_ACTION_FORWARD_TO_CPPUTEST == action.action_type) {
		return (void*)cpputest_calloc(count, size);
	} else if (MOCK_ACTION_CALL_ORIGINAL == action.action_type) {
		return calloc(count, size);
	}

	/* I'm mocking here! */
	return (void*)action.return_value;
}

/*** Internal functions ****/
void mock_reset_mock_info(MOCK_FUNCTION function) {
	g_mock_info_db[function].state = MOCK_STATE_NOT_MOCKING;
	g_mock_info_db[function].return_value = 0;
	g_mock_info_db[function].mock_count = 0;
	g_mock_info_db[function].mock_callback = NULL;
	g_mock_info_db[function].max_mock_count = 0;
}

void mock_begin(MOCK_FUNCTION function, int count, int return_value, void* cb, void* cb_param) {
	g_mock_info_db[function].return_value = return_value;
	g_mock_info_db[function].mock_callback = (void *)cb;
	g_mock_info_db[function].max_mock_count = count;
	g_mock_info_db[function].state = MOCK_STATE_MOCKING;
	g_mock_info_db[function].callback_param = cb_param;
}

typedef int(*single_arg_callback_template)(void * cb_param, void* caller, int arg1);
typedef int(*double_arg_callback_template)(void * cb_param, void* caller, int arg1, int arg2);

mock_action register_mock_call(MOCK_FUNCTION function, void* caller, ...) {
	/* This function is obviously horrible and limits mocking to functions with only one or two parameters,
	   also, the callback features was made purely for fun and has no real purpose at the moment. */
	int callback_decision = 0;
	va_list args;
	
	int args_count = 0;
	int arg1 = 0;
	int arg2 = 0;
	mock_action action = { g_mock_info_db[function].default_action, 0 };

	if (MOCK_STATE_MOCKING != g_mock_info_db[function].state) {
		/* We're not even in a mocking session */
		return action;
	}

	if (g_mock_info_db[function].max_mock_count &&
		(g_mock_info_db[function].mock_count == g_mock_info_db[function].max_mock_count)) {

		/* Reached max - do not mock */
		return action;
	}

	/* First consult the user-supplied callback */
	if (g_mock_info_db[function].mock_callback) {
		
#ifdef WIN32
              __crt_va_start(args, args_count);
#else
               va_start(args, caller);
#endif
		arg1 = __crt_va_arg(args, int);

		if (args_count > 1) {
			arg2 = __crt_va_arg(args, int);

			double_arg_callback_template fptr = (double_arg_callback_template)g_mock_info_db[function].mock_callback;
			callback_decision = fptr(g_mock_info_db[function].callback_param, caller, arg1, arg2);
		} else {

			single_arg_callback_template fptr = (single_arg_callback_template)g_mock_info_db[function].mock_callback;
			callback_decision = fptr(g_mock_info_db[function].callback_param, caller, arg1);
		}
	}

	if (callback_decision & MOCK_CALLBACK_DECISION_DONT_MOCK) {
		/* Not mocking */
		return action;
	}

	/* We're going to mock! */
	action.action_type = MOCK_ACTION_MOCK;
	action.return_value = g_mock_info_db[function].return_value;

	/* Log the call (Optionally - don't log) */
	if (!(callback_decision & MOCK_CALLBACK_DECISION_DONT_LOG)) {
		g_mock_info_db[function].mock_count++;
	}

	/* Optionally - stop mocking after this current mock */
	if (callback_decision & MOCK_CALLBACK_DECISION_STOP_AFTER_CURRENT) {
		mock_reset_mock_info(function);
	}

	return action;
}

#endif /* _DEBUG */