#pragma once

#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetector.h"
#include "CppUTest/TestMemoryAllocator.h"
#include "CppUTest/PlatformSpecificFunctions.h"

extern "C" {
	#include "memmock.h"
	#include "memmock_fuzzy.h"
}

/* Mocks all memory functions, for simpler implementation */
class MemmockFuzzyCPPUTest
{
public:
	MemmockFuzzyCPPUTest();
	~MemmockFuzzyCPPUTest();
	void doneMocking();
	bool didMock();
	void iterationCompleted();

private:
	bool m_started;
};

#define MEMMOCK_FUZZY_CPPUTEST_LOOP_START() \
	{ \
		MemmockFuzzyCPPUTest __m__; \
		while (1 == 1) {

#define MEMMOCK_FUZZY_CPPUTEST_COMPLETE_ITERATION() \
		if (!__m__.didMock()) { \
			/* Because if the iteration completed without a mock, we're done here - break out of the fuzzy loop */ \
			break; \
		} \
		__m__.iterationCompleted();

#define MEMMOCK_FUZZY_CPPUTEST_LOOP_END() \
		} \
		__m__.doneMocking(); \
	}



/* Mocks all memory functions, for simpler implementation */
class MemmockCPPUTest
{
public:
	MemmockCPPUTest();
	~MemmockCPPUTest();
	void setDefaultAction(mock_action_type action_type);
	void startMock();
	void stopMock();
	size_t getCount(MOCK_FUNCTION function);

private:
	mock_action_type m_originalDefaultActionType;
};