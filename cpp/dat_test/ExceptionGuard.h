#ifdef WIN32
#pragma once

#include <Windows.h>
#include <eh.h>
#include "CppUTest/UtestMacros.h"
#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetector.h"
#include "CppUTest/TestMemoryAllocator.h"
#include "CppUTest/PlatformSpecificFunctions.h"
#include "CppUTest/Symbol.h"

#include <string>

#pragma warning( push )
#pragma warning( disable: 4091 )
#include <DbgHelp.h>
#pragma warning( pop )

#pragma comment( lib, "DbgHelp.lib" )

class HWException {
public:
	HWException(std::string what) : m_what(what) {
	}

	const char* what() const {
		return m_what.c_str();
	}

private:
	std::string m_what;
};

#define TRACE_FRAMES (50)

class ExceptionGuard {
public:

	ExceptionGuard() {
		m_old_se_translator = _set_se_translator(ExceptionGuard::_se_translator);
		m_sym_initialized = 0 != SymInitialize(GetCurrentProcess(), NULL, TRUE);
	}

	~ExceptionGuard() {
		_set_se_translator(m_old_se_translator);
		if (m_sym_initialized) {
			SymCleanup(GetCurrentProcess());
		}
	}

	static void _se_translator(unsigned int code, struct _EXCEPTION_POINTERS* exc) {
		char buf[8192] = { 0 };
		unsigned int gle = 0;
		DWORD64 displacement = 0;
		PVOID bt[TRACE_FRAMES] = { nullptr };
		CaptureStackBackTrace(STACK_TRACE_FRAMES_TO_SKIP, STACK_TRACE_FRAMES_COUNT, bt, NULL);

		SimpleStringBuffer msg;
		Symbol s(exc->ExceptionRecord->ExceptionAddress);

		msg.add(	"\nCPU exception thrown\n" \
					"Code: 0x%08X\n" \
					"Address: 0x%08X (%s)\n" \
					"Stack Trace:\n",
			exc->ExceptionRecord->ExceptionCode,
			exc->ExceptionRecord->ExceptionAddress,
			s.toSimpleString().toString()); //so sorry

		msg.addStackTrace(bt);

		throw HWException(msg.toString());
	}

private:
	_se_translator_function m_old_se_translator;
	bool m_sym_initialized;
};
#else

class ExceptionGuard
{
};

#endif