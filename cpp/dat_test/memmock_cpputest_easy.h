#pragma once

#include <functional>
#include "ExceptionGuard.h"
#include "memmock_cpputest.h"

template <typename ReturnType, typename... Args>
class MemmockCPPUTestEasy
{
public:
	explicit MemmockCPPUTestEasy(std::function<ReturnType(Args...)> func, Args&& ... args);
	void setTester(std::function<void(ReturnType)> func);
	void performTests();

private:

	std::function<ReturnType(Args...)> m_funcToCheck;
	std::function<void(ReturnType)> m_tester;
};

template <typename ReturnType, typename ... Args>
MemmockCPPUTestEasy<ReturnType, Args...>::MemmockCPPUTestEasy(std::function<ReturnType(Args...)> func, Args&&... args) {
	m_funcToCheck = std::bind(func, args...);
}

template <typename ReturnType, typename ... Args>
void MemmockCPPUTestEasy<ReturnType, Args...>::setTester(std::function<void(ReturnType)> func) {
	m_tester = func;
}

template <typename ReturnType, typename ... Args>
void MemmockCPPUTestEasy<ReturnType, Args...>::performTests() {
	ExceptionGuard g;

	MEMMOCK_FUZZY_CPPUTEST_LOOP_START()

		ReturnType retval;

		try {
			retval = m_funcToCheck();
		} catch (...) {
			if (__m__.didMock()) {
				__m__.iterationCompleted();
				FAIL("Unhandled exception while running tested function. A mock occurred during execution, so it might be the cause\n");
			}
			else {
				FAIL("Unhandled exception while running tested function. No mock occurred during execution\n");
			}
		}

	MEMMOCK_FUZZY_CPPUTEST_COMPLETE_ITERATION()

		m_tester(retval);

	MEMMOCK_FUZZY_CPPUTEST_LOOP_END()
}

/* Partial class specialization for VOID return value in checked function */

template <typename... Args>
class MemmockCPPUTestEasy<void, Args...>
{
public:
	explicit MemmockCPPUTestEasy(std::function<void(Args...)> func, Args&& ... args);
	void setTester(std::function<void(void)> func);
	void performTests() const;

private:

	std::function<void(Args...)> m_funcToCheck;
	std::function<void(void)> m_tester;
};

template <typename ... Args>
MemmockCPPUTestEasy<void, Args...>::MemmockCPPUTestEasy(std::function<void(Args...)> func, Args&&... args) {
	m_funcToCheck = std::bind(func, args...);
}

template <typename ... Args>
void MemmockCPPUTestEasy<void, Args...>::setTester(std::function<void(void)> func) {
	m_tester = func;
}

template<typename ...Args>
void MemmockCPPUTestEasy<void, Args...>::performTests() const
{
	ExceptionGuard g;

	MEMMOCK_FUZZY_CPPUTEST_LOOP_START()

		m_funcToCheck();

	MEMMOCK_FUZZY_CPPUTEST_COMPLETE_ITERATION()

		m_tester();

	MEMMOCK_FUZZY_CPPUTEST_LOOP_END()
}
