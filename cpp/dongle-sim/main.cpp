#include "DongleSimulator.h"
#include <iostream>

#define SERVER_UDP_PORT (11115	)

static const bool useTLS = false;
static const bool useDTP = true;
static const unsigned int timeout = 50000;

void main(int argc, char * argv[])
{
	net::init();
	DongleSimulator* s = DongleSimulator::get();

	std::cout << "Dongle Simulator listening on UDP port " << SERVER_UDP_PORT << std::endl;
	std::cout << "TLS:  " << (useTLS ? "YES" : "NO") << std::endl;
	std::cout << "DTP:  " << (useDTP ? "YES" : "NO") << std::endl;
	std::cout << "Timeout :  " << (timeout) << "s" << std::endl;

	s->start(SERVER_UDP_PORT, useTLS, useDTP, timeout);
	s->wait();
}