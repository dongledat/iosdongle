LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

DONGLE_S := $(LOCAL_PATH)/../dongle-smart
DONGLE := $(LOCAL_PATH)/../dongle
MBEDTLS := $(LOCAL_PATH)/../../ext/mbedtls/include
CRYPTO := $(LOCAL_PATH)/../crypto
COMMON :=$(LOCAL_PATH)/../common

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../ $(DONGLE_S) $(DONGLE) $(MBEDTLS) $(CRYPTO) $(COMMON)
LOCAL_EXPORT_C_INCLUDES := $(DONGLE_S) $(DONGLE) $(MBEDTLS) $(CRYPTO) $(COMMON)

LOCAL_MODULE    := dongles

LOCAL_SRC_FILES := ConfigurationUpdateTransaction.cpp   Connection.cpp                       DataAggregator.cpp	\
	DataListener.cpp                     DataSubscriptionTransaction.cpp      DongleSocket.cpp	\
	DongleStatusListener.cpp             ITransaction.cpp                     NanopbRAIIUtilities.cpp	\
	PDUTransaction.cpp                   PinAuthenticationTransaction.cpp     SimpleTransaction.cpp	\
	TokenAuthenticationTransaction.cpp   TransactionManager.cpp               UpdateProgressListener.cpp \
	Streams.cpp SecureTunnel.cpp DTPStream.cpp

#LOCAL_CFLAGS += -DPB_ENABLE_MALLOC
LOCAL_SHARED_LIBRARIES += dongle
LOCAL_SHARED_LIBRARIES += mbedtls

#include $(BUILD_STATIC_LIBRARY)
include $(BUILD_SHARED_LIBRARY)