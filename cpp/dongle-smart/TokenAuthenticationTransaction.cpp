#include "TokenAuthenticationTransaction.h"

using namespace dat;

TokenAuthenticationTransaction::TokenAuthenticationTransaction(DongleSocket & dongleSocket, IDongleStatusListener & dongleStatusListener, TransactionID_t id) :
	m_dongleStatusListener(dongleStatusListener),
	ITransaction(dongleSocket, TransactionType_TokenAuth, id, true, true) // Expirable, and wait for reap
{
}

int TokenAuthenticationTransaction::onMessage(TDongleRsp & rsp)
{
	if (!isMessageTypeExpected(rsp.which_response)) {
		return -1;
	}

	return 0;
}

bool TokenAuthenticationTransaction::isMessageTypeExpected(unsigned int responseType) const
{
	return false;
}

void TokenAuthenticationTransaction::beginAuthentication()
{
}
