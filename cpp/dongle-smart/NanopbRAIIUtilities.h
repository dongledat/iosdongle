#ifndef _NANOPB_BYTES_H_
#define _NANOPB_BYTES_H_

#include "pb_common.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include <memory>

class NanopbBytesRAII
{
public:
	NanopbBytesRAII(const pb_byte_t * data, pb_size_t data_size);
	NanopbBytesRAII(pb_size_t data_size);
	void setData(const pb_byte_t * data, pb_size_t data_size);
	pb_bytes_array_t* get();
	pb_byte_t* getDataPointer();

private:
	void prepareStruct(pb_size_t data_size);

	std::unique_ptr<pb_bytes_array_t> m_bytes;
};

template <class NanoPBMsgType>
class NanopbDynamicMessageGuard
{
public:
	NanopbDynamicMessageGuard(const pb_field_t * m_nanopbFields, NanoPBMsgType * nanopbStruct);
	~NanopbDynamicMessageGuard();

private:
	NanoPBMsgType * m_nanopbStruct;
	const pb_field_t * m_nanopbFields;
};


template <class NanoPBMsgType>
NanopbDynamicMessageGuard<NanoPBMsgType>::NanopbDynamicMessageGuard(const pb_field_t * nanopbFields, NanoPBMsgType * nanopbStruct) :
	m_nanopbFields(nanopbFields),
	m_nanopbStruct(nanopbStruct)
{
}

template <class NanoPBMsgType>
NanopbDynamicMessageGuard<NanoPBMsgType>::~NanopbDynamicMessageGuard()
{
	try {
		if (nullptr != m_nanopbStruct) {
			if (nullptr != m_nanopbFields) {
				pb_release(m_nanopbFields, m_nanopbStruct);
			}

			delete m_nanopbStruct;
		}
	}
	catch (...) {
		// pass
	}
}

#endif /* _NANOPB_BYTES_H_ */