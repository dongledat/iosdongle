#include "IConnection.h"

namespace dat {

	class UpdateProgressListener : public IUpdateProgressListener {
	public:
		virtual void onUpdateStatus(UpdateStatus) override;

		bool m_isCalled_onUpdateStatus;

		void reset() {
			m_isCalled_onUpdateStatus = false;
		}

	private:
	};

}