#include "ConfigurationUpdateTransaction.h"
#include "NanopbRAIIUtilities.h"

using namespace dat;

ConfigurationUpdateTransaction::ConfigurationUpdateTransaction(DongleSocket & dongleSocket, IUpdateProgressListener & updateProgressListener, TransactionID_t id) :
	m_updateProgressListener(updateProgressListener),
	ITransaction(dongleSocket, TransactionType_ConfigurationUpdate, id)
{
}

int ConfigurationUpdateTransaction::onMessage(TDongleRsp & rsp)
{
	if (!isMessageTypeExpected(rsp.which_response)) {
		return -1;
	}

	switch (rsp.which_response) 
	{
	case TDongleRsp_OpenUpdatingSessionRsp_tag:
	{
		if (TOpenUpdatingSessionRsp_EResult_ok == rsp.response.OpenUpdatingSessionRsp.result) {
			m_stateMachine = CONF_UPDATE_READY_TO_SEND_CHUNK;
		}
		else {
			// Failed..
			m_stateMachine = CONF_UPDATE_FAILED_SIGNATURE;
			m_updateProgressListener.onUpdateStatus(IUpdateProgressListener::UPDATE_FAILED);
			m_isCompleted = true;
		}

		break;
	}

	case TDongleRsp_DownloadDataBlockRsp_tag:
	{
		if (TDownloadDataBlockRsp_EResult_ok == rsp.response.DownloadDataBlockRsp.Result) {
			m_stateMachine = CONF_UPDATE_READY_TO_SEND_CHUNK;
			m_updateProgressListener.onUpdateStatus(IUpdateProgressListener::UPDATE_CHUNK_SENT);
		}
		else {
			// Failed..
			m_stateMachine = CONF_UPDATE_FAILED_CHUNK;
			m_updateProgressListener.onUpdateStatus(IUpdateProgressListener::UPDATE_FAILED);
			m_isCompleted = true;
		}

		break;
	}

	case TDongleRsp_CloseUpdatingSessionRsp_tag:
	{
		if (TCloseUpdatingSessionRsp_EResult_ok == rsp.response.CloseUpdatingSessionRsp.Result) {
			m_stateMachine = CONF_UPDATE_COMPLETED;
			m_updateProgressListener.onUpdateStatus(IUpdateProgressListener::UPDATE_COMPLETE);
		}
		else {
			// Failed..
			m_stateMachine = CONF_UPDATE_FAILED_COMPLETE;
			m_updateProgressListener.onUpdateStatus(IUpdateProgressListener::UPDATE_FAILED);
			m_isCompleted = true;
		}

		break;
	}
	}

	return 0;
}

bool ConfigurationUpdateTransaction::isMessageTypeExpected(unsigned int responseType) const
{
	switch (responseType) {
		case TDongleRsp_OpenUpdatingSessionRsp_tag: return CONF_UPDATE_SENT_SIGNATURE == m_stateMachine;

		case TDongleRsp_DownloadDataBlockRsp_tag: return CONF_UPDATE_SENT_CHUNK == m_stateMachine;

		case TDongleRsp_CloseUpdatingSessionRsp_tag: return CONF_UPDATE_SENT_COMPLETE == m_stateMachine;

		default:
			return false;
	}
}

int ConfigurationUpdateTransaction::setSignature(const std::string & signature)
{
	if (m_stateMachine != CONF_UPDATE_NOT_STARTED) {
		return -1;
	}

	TDongleReq req = TDongleReq_init_zero;
	NanopbBytesRAII bytes(reinterpret_cast<const pb_byte_t*>(signature.c_str()), static_cast<pb_size_t>(signature.size()));

	req.request.OpenUpdatingSessionReq.Signature = bytes.get();
	sendMessage(req);

	m_stateMachine = CONF_UPDATE_SENT_SIGNATURE;
	m_updateProgressListener.onUpdateStatus(IUpdateProgressListener::UPDATE_STARTED);
	return 0;
}

int ConfigurationUpdateTransaction::setDataChunk(const unsigned char* chunk, size_t size)
{
	if (m_stateMachine != CONF_UPDATE_READY_TO_SEND_CHUNK) {
		return -1;
	}

	TDongleReq req = TDongleReq_init_zero;
	NanopbBytesRAII bytes(reinterpret_cast<const pb_byte_t*>(chunk), static_cast<pb_size_t>(size));

	req.request.DownloadDataBlockReq.BlockContent = bytes.get();
	sendMessage(req);

	m_stateMachine = CONF_UPDATE_SENT_CHUNK;
	return 0;
}

int ConfigurationUpdateTransaction::complete()
{
	if (m_stateMachine != CONF_UPDATE_READY_TO_SEND_CHUNK) {
		return -1;
	}

	TDongleReq req = TDongleReq_init_zero;
	req.which_request = TDongleReq_CloseUpdatingSessionReq_tag;
	//todo
	//req.request.CloseUpdatingSessionReq.Signature
	sendMessage(req);

	m_stateMachine = CONF_UPDATE_SENT_COMPLETE;
	return 0;
}

int ConfigurationUpdateTransaction::cancel()
{
	// Let the dongle know we're done
	(void)complete();

	// Basically we're saying we don't care about the dongle's response, we're done here
	m_stateMachine = CONF_UPDATE_CANCELLED;
	m_isCompleted = true;
	return 0;
}
