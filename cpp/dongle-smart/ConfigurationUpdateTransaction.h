#ifndef _CONFIGURATION_UPDATE_TRANSACTION_H_
#define _CONFIGURATION_UPDATE_TRANSACTION_H_

#include "ITransaction.h"

namespace dat {

	class ConfigurationUpdateTransaction : public ITransaction {

		typedef enum {
			CONF_UPDATE_NOT_STARTED = 0,

			CONF_UPDATE_SENT_SIGNATURE,
			
			CONF_UPDATE_SIGNATURE_ACK,
			CONF_UPDATE_READY_TO_SEND_CHUNK = CONF_UPDATE_SIGNATURE_ACK,

			CONF_UPDATE_SENT_CHUNK,
			CONF_UPDATE_CHUNK_ACK = CONF_UPDATE_READY_TO_SEND_CHUNK,

			CONF_UPDATE_SENT_COMPLETE,
			CONF_UPDATE_COMPLETE_ACK,
			CONF_UPDATE_COMPLETED = CONF_UPDATE_COMPLETE_ACK,
			
			CONF_UPDATE_CANCELLED,

			// Errors
			CONF_UPDATE_FAILED_SIGNATURE,
			CONF_UPDATE_FAILED_CHUNK,
			CONF_UPDATE_FAILED_COMPLETE
		} ConfUpdateStateMachine;

	public:
		ConfigurationUpdateTransaction(DongleSocket& dongleSocket, IUpdateProgressListener& updateProgressListener, TransactionID_t id);

		int onMessage(TDongleRsp & rsp) override;

		int setSignature(const std::string& signature);
		int setDataChunk(const unsigned char* chunk, size_t size);
		int complete();
		int cancel();

	private:
		bool isMessageTypeExpected(unsigned int responseType) const;

		ConfUpdateStateMachine m_stateMachine;
		IUpdateProgressListener& m_updateProgressListener;
	};

}

#endif /* _SIMPLE_TRANSACTION_H_ */