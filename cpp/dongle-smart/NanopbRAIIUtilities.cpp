#include "NanopbRAIIUtilities.h"

NanopbBytesRAII::NanopbBytesRAII(const pb_byte_t * data, pb_size_t data_size)
{
	prepareStruct(data_size);
	setData(data, data_size);
}

NanopbBytesRAII::NanopbBytesRAII(pb_size_t data_size)
{
	prepareStruct(data_size);
}

void NanopbBytesRAII::setData(const pb_byte_t * data, pb_size_t data_size)
{
	memcpy((&m_bytes->bytes), data, data_size);
}

pb_bytes_array_t * NanopbBytesRAII::get()
{
	return m_bytes.get();
}

pb_byte_t * NanopbBytesRAII::getDataPointer()
{
	return m_bytes.get()->bytes;
}

void NanopbBytesRAII::prepareStruct(pb_size_t data_size)
{
	auto struct_size = data_size + sizeof(m_bytes.get()->size);

	m_bytes.reset(reinterpret_cast<pb_bytes_array_t*>(new char[struct_size]));
	m_bytes->size = data_size;
}

