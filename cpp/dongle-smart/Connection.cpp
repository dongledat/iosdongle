#include "Connection.h"
#include <functional>
#include <memory>
#include "../crypto/dat_base64.h"
#include "NanopbRAIIUtilities.h"

using namespace dat;

Connection::Connection(IOutputStream& outputStream, IDongleStatusListener& dongleStatusListener, IDataListener& dataListener, IUpdateProgressListener& updateProgressListener, bool useTLS, bool useDTP) :
	IConnection(),
	m_dongleStatusListener(dongleStatusListener),
	m_dataListener(dataListener),
	m_updateProgressListener(updateProgressListener),
	m_isAuthenticatedByPIN(false),
	m_isAuthenticatedByToken(false),
	m_transactionManager(m_dataListener, m_updateProgressListener, m_dongleStatusListener, m_dongleSocket),
	m_rawNetStream(outputStream),
	m_useTLS(useTLS),
	m_useDTP(useDTP)
{
	// Con -> Dongle(PB) -> Network
	m_dongleSocket.setlowerLayerStream(&m_rawNetStream);

	if (m_useDTP) {
		// Con -> Dongle(PB) -> DTP -> Network
		m_dtpStream.setlowerLayerStream(&m_rawNetStream);
		m_dongleSocket.setlowerLayerStream(&m_dtpStream);
	}

	if (m_useTLS) {

		m_secureTunnel.setlowerLayerStream(&m_rawNetStream);

		if (m_useDTP) {
			// Either:
			// Con -> Dongle(PB) -> DTP -> TLS -> Network
			m_dtpStream.setlowerLayerStream(&m_secureTunnel);
		}
		else {
			// Or:
			// Con -> Dongle(PB) -> TLS -> Network
			m_dongleSocket.setlowerLayerStream(&m_secureTunnel);
		}
	}
}

void Connection::reset()
{
	m_dtpStream.reset();
	m_transactionManager.reset();
	m_isAuthenticatedByPIN = false;
	m_isAuthenticatedByToken = false;
	m_secureTunnel.reset();
}

/****************************************************************/
/************************ TX ************************************/
/****************************************************************/

void Connection::authenticateByPIN(const std::string& identity, const std::string& pin)
{

	if (m_isAuthenticatedByPIN) {
		// Already authenticated
        m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_PASS);
		return;
	}

	if (!m_useTLS) {
		//TODO: log? This authentication is meant to happen with TLS
		//TODO: Just for debug:
		m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_IN_PROGRESS);
		m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_PASS);
		m_isAuthenticatedByPIN = true;
		return;
	}

	m_secureTunnel.startPSKHandshake(pin, identity);

	switch (m_secureTunnel.getState()) {
		case SecureTunnel::TUNNEL_STATE_IN_PROGRESS:
			m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_IN_PROGRESS);
			break;

		default:
			m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_FAIL);
			break;
	}
}

void Connection::authenticateByToken(const std::string& token)
{
	if (m_isAuthenticatedByToken) {
		// Already authenticated
		return;
	}

	m_transactionManager.cleanupTransactions();

	TokenAuthenticationTransaction* transaction = m_transactionManager.newTokenAuthenticationTransaction();
	if (nullptr == transaction) {
		m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_TOKEN, IDongleStatusListener::AUTH_RESULT_FAIL);
		return;
	}

	transaction->beginAuthentication();
	m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_TOKEN, IDongleStatusListener::AUTH_RESULT_IN_PROGRESS);
	// Should be:
	// return;
	// And the rest would be handled by the transaction

	// TODO: temporary
	m_isAuthenticatedByToken = true;
	m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_TOKEN, IDongleStatusListener::AUTH_RESULT_PASS);
}

int Connection::subscribeObject(int url, int val)
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	DataSubscriptionTransaction* transaction = m_transactionManager.newDataSubscriptionTransaction();
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->subscribe(url, val);
	return transaction->getTransactionID();
}

int Connection::unsubscribeObject(int transactionID)
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	DataSubscriptionTransaction* transaction = reinterpret_cast<DataSubscriptionTransaction*>(m_transactionManager.findTransaction(transactionID, TransactionType_DataSubscription));
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->unsubscribe();
	return transactionID;
}

int Connection::getAvailableDiagParametersAsync()
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetAvailableDiagnosticParameter);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getAvailableDiagParameters();
	return transaction->getTransactionID();
}

int Connection::getDataObjectAsync(int url)
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetDataObject);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getDataObject(url);
	return transaction->getTransactionID();
}

int Connection::getDongleHWRevisionAsync()
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetHwVersion);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getHWVersion();
	return transaction->getTransactionID();
}

int Connection::getDongleSerialNumberAsync()
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetSerialNumberDevice);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getSerialNumber();
	return transaction->getTransactionID();
}

int Connection::getFirmwareStatusAsync()
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_GetFwStatus);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->getFirmwareStatus();
	return transaction->getTransactionID();
}

int Connection::startUpdate(const std::string& signature)
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	ConfigurationUpdateTransaction* transaction = m_transactionManager.newConfigurationUpdateTransaction();
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->setSignature(signature);
	return transaction->getTransactionID();
}

int Connection::sendUpdateChunk(int transactionID, const unsigned char* chunk, size_t size)
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	ConfigurationUpdateTransaction* transaction = reinterpret_cast<ConfigurationUpdateTransaction*>(
		m_transactionManager.findTransaction(transactionID, TransactionType_ConfigurationUpdate));

	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->setDataChunk(chunk, size);
	return transaction->getTransactionID();
}

int Connection::completeUpdate(int transactionID)
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	ConfigurationUpdateTransaction* transaction = reinterpret_cast<ConfigurationUpdateTransaction*>(
		m_transactionManager.findTransaction(transactionID, TransactionType_ConfigurationUpdate));

	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->complete();
	return transaction->getTransactionID();
}

int Connection::cancelUpdate(int transactionID)
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	ConfigurationUpdateTransaction* transaction = reinterpret_cast<ConfigurationUpdateTransaction*>(
		m_transactionManager.findTransaction(transactionID, TransactionType_ConfigurationUpdate));

	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->cancel();
	return transaction->getTransactionID();
}

int Connection::switchToServiceMode()
{	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	SimpleTransaction* transaction = m_transactionManager.newSimpleTransaction(TransactionType_SwitchToServiceMode);
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	transaction->switchToServiceMode();
	return transaction->getTransactionID();
}

int Connection::sendPDU(TPDUReq_EPDUJ1962Pin pins,
	IConnection::PDUProtocol protocol,
	int startingTimeOut,
	std::vector<TPDUCommunicationParameter> & commParameters,
	std::vector<TPDURequest> & pduRequests)
{
	if (!isSessionEstablished()) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	m_transactionManager.cleanupTransactions();
	PDUTransaction* transaction = m_transactionManager.newPDUTransaction();
	if (nullptr == transaction) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	if (ITransaction::OPERATION_ERROR == transaction->sendPDU(pins, protocol, startingTimeOut, commParameters, pduRequests)) {
		return ITransaction::INVALID_TRANSACTION_ID;
	}

	return transaction->getTransactionID();
}

/****************************************************************/
/************************ RX ************************************/
/****************************************************************/

// Called by java with data from dongle
void Connection::fromNet(const unsigned char* data, size_t length)
{
	m_rawNetStream.appendData(data, length);
	
	// We have two possible states to handle
	if (isSessionEstablished()) {
		onSessionUpdate();
	} else {
		onPreSessionUpdate();
	}
}

//TODO: Delete this
void dat::Connection::DebugSendRawData(std::string & data)
{
	m_secureTunnel.send(reinterpret_cast<const unsigned char *>(data.c_str()), data.size());
}

void Connection::onPreSessionUpdate()
{
	if (!m_useTLS) {
		//TODO: Log? unexpected..
		//		Because if we're not using TLS, the session is established by default
		return;
	}

	// The data will be used for TLS handshake
	while (m_rawNetStream.getSize()) {
		m_secureTunnel.continueHandshake();

		// Check the new state of the TLS handshake
		if (!m_secureTunnel.isCompleted()) {
			// Not yet completed
			return;
		}

		// TLS handshake has just completed
		switch (m_secureTunnel.getState()) {
		case SecureTunnel::TUNNEL_STATE_ESTABLISHED:
			// Notify high level that session has been established successfuly
			m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_PASS); //TODO: how do i know it's PIN? need to keep state of which authentication we're doing
			m_isAuthenticatedByPIN = true; //TODO: how do i know it's PIN? need to keep state of which authentication we're doing
			break;

		case SecureTunnel::TUNNEL_STATE_FAILED:
			// Notify high level that we failed to establish session
			m_dongleStatusListener.onAuthenticate(IDongleStatusListener::AUTH_METHOD_PIN, IDongleStatusListener::AUTH_RESULT_FAIL);
			break;
		}
	}

	// Check if the network data we have also includes app-layer data
	if ((m_secureTunnel.getState() == SecureTunnel::TUNNEL_STATE_ESTABLISHED) &&
		m_rawNetStream.getSize()) {
		onSessionUpdate();
	}
}

void Connection::onSessionUpdate()
{
	if (!m_dongleSocket.onSessionUpdate()) {
		// App layer (or lower) protocol error, reset
		m_dongleStatusListener.onDongleStatus(IDongleStatusListener::CONNECTIONRESET);
		reset();
		return;
	}

	while (m_dongleSocket.getMessageCount() > 0) {
		// Get one received message
		TDongleRsp* rsp = m_dongleSocket.getMessage();
		if (nullptr == rsp) {
			return;
		}

		// Will clear out the message when this function ends
		NanopbDynamicMessageGuard<TDongleRsp> g(TDongleRsp_fields, rsp);

		//m_transactionManager.cleanupTransactions();

		if (isRelatedToTransaction(*rsp)) {
			handleOutOfBandResponse(*rsp);
			return;
		}

		// Find the relevant ongoing transaction
		ITransaction* transaction = m_transactionManager.findTransaction(rsp->id);
		if (nullptr == transaction) {
			return;
		}
		
		handleTransactionMessage(*transaction, *rsp);
	}
}

void Connection::handleTransactionMessage(ITransaction & transaction, TDongleRsp & rsp)
{
	// Let transaction handle this message
	transaction.onMessage(rsp);

	// Extend expiration time
	transaction.extendExpiryTime();

	// Completed / expired transactions might change the state of our connection
	//monitorTransactionsState();

	// Cleans expired or completed transactions
	m_transactionManager.cleanupTransactions();
}


/*******************************************************************/
/************************ State ************************************/
/*******************************************************************/

bool Connection::isAuthenticatedByPIN() const
{
	return m_isAuthenticatedByPIN;
}

bool Connection::isAuthenticatedByToken() const
{
	return m_isAuthenticatedByToken;
}

bool Connection::isAuthenticated()
{
	return m_isAuthenticatedByPIN || m_isAuthenticatedByToken;
}

bool Connection::isSessionEstablished()
{
	if (m_useTLS) {
		return isAuthenticated();
	}
	else {
		return true;
	}
}

bool Connection::isRelatedToTransaction(TDongleRsp & rsp)
{
	if ((rsp.which_response == TDongleRsp_ErrorRsp_tag) &&
		!(rsp.has_id)) {
		// We got an error not related to any transaction; handle it
		return true;
	}

	if ((rsp.which_response == TDongleRsp_GetDataObjectRsp_tag) &&
		!(rsp.has_id)) {
		// We got a Data Object not related to a transaction, so it must be from subscription
		return true;
	}

	return false;
}

void Connection::notifyError(TDongleRsp & rsp)
{
	switch (rsp.response.ErrorRsp.Reason)
	{
	case TErrorRsp_EReason_MemoryAllocation:
		m_dongleStatusListener.onDongleStatus(IDongleStatusListener::DongleStatus::ERRORMEMORYPROBLEM);
		break;

	case TErrorRsp_EReason_ProtobufDecodeFailure:
		m_dongleStatusListener.onDongleStatus(IDongleStatusListener::DongleStatus::PROTOBUFDECODEERROR);
		break;

	default:
		m_dongleStatusListener.onDongleStatus(IDongleStatusListener::DongleStatus::UNKNOWNERRORREASON);
	}
}

void Connection::handleOutOfBandResponse(TDongleRsp & rsp)
{
	if (rsp.which_response == TDongleRsp_ErrorRsp_tag) {
		//TODO: Notify higher level
		notifyError(rsp);
		return;
	}

	if (rsp.which_response == TDongleRsp_GetDataObjectRsp_tag) {
		unsigned char buffer[4096]; //TODO: remove magic number
		pb_ostream_t ostream = pb_ostream_from_buffer(buffer, 4096);
		if (!pb_encode(&ostream, TDongleRsp_fields, &rsp)) {
			return;
		}
		std::string rspAsString(buffer, buffer + ostream.bytes_written);

		m_dataListener.onData(0, rsp.response.GetDataObjectRsp.DataObject, rspAsString);
	}
}

/**
* @brief Start Backend certificate update.
* @note New method.
*/
//void updateCertificate(Certificate certificate, IUpdateCertificateListener listener);
