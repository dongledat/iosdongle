#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_

#include <string>

namespace dat {

	class Configuration
	{
	public:

		// How long transactions (such as PDU, GetDataObject, etc.) will stay alive before timing out
		static const unsigned long long DEFAULT_TRANSACTION_EXPIRY_INTERVAL_IN_SECONDS = 10;

		// PDU parameters
		static const unsigned int MAX_REQ_PDU_SIZE = 256;
		static const unsigned int MAX_COMM_PARAMETERS_COUNT = 50;
		static const unsigned int MAX_PDU_REQUESTS_COUNT = 5;

		// Identity used for PSK authentication
		//static const std::string PSK_IDENTITY;


	};

	//const std::string Configuration::PSK_IDENTITY = "PSKIdentity";
}

#endif /* _CONFIGURATION_H_  */
