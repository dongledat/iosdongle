#include "ITransaction.h"

using namespace dat;


ITransaction::ITransaction(DongleSocket& socket, TransactionType transactionType, TransactionID_t id, bool expirable, bool waitForReap) :
	m_transactionID(id),
	m_startTime(os::Time::realUSeconds()),
	m_expiryTime(m_startTime.uSeconds() + DEFAULT_EXPIRY_INTERVAL_IN_USECONDS),
	m_transactionType(transactionType),
	m_isCompleted(false),
	m_dongleSocket(socket),
	m_isReaped(false),
	m_waitForReap(waitForReap),
	m_expirable(expirable)
{
}

bool ITransaction::isCompleted() const
{
	return m_isCompleted;
}

bool ITransaction::isExpired() const
{
	if (!m_expirable) {
		return false;
	}

	common::Time currentTime(os::Time::realUSeconds());
	if (currentTime >= m_expiryTime) {
		return true;
	}

	return false;
}

bool ITransaction::isReaped() const
{
	if (!m_isCompleted) {
		return false; // Can't be done before completing
	}

	if (!m_waitForReap) {
		return true; // No need to wait for reaping, so treat it as reaped
	}

	return m_isReaped;
}

void ITransaction::sendMessage(TDongleReq& msg)
{
	msg.id = m_transactionID;
	msg.has_id = true;

	m_dongleSocket.sendMessage(&msg);
}