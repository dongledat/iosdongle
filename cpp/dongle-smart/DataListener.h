#include "IConnection.h"

namespace dat {

	class DataListener : public IDataListener {
	public:
		virtual void onData(int respID, TDataObject & dataObject, std::string & rspAsString) override;
		virtual void onAvailableDiagParameters(int respID, const std::list<TDataCatalogEntry>& l, std::string & rspAsString) override;
		virtual void onDongleHWRevision(int respID, const std::string&) override;
		virtual void onDongleSerialNumber(int respID, const std::string&) override;
		virtual void onFirmwareStatus(int respID, const FWStatus& fwStatus) override;
		virtual void onPDUNotify(int respID, const std::string& pduRawDataResponse, bool success) override;

		bool m_isCalled_onData;
		bool m_isCalled_onAvailableDiagParameters;
		bool m_isCalled_onDongleHWRevision;
		bool m_isCalled_onDongleSerialNumber;
		bool m_isCalled_onFirmwareStatus;
		bool m_isCalled_onPDUResponse;

		void reset() {
			m_isCalled_onData = false;
			m_isCalled_onAvailableDiagParameters = false;
			m_isCalled_onDongleHWRevision = false;
			m_isCalled_onDongleSerialNumber = false;
			m_isCalled_onFirmwareStatus = false;
			m_isCalled_onPDUResponse = false;
		}

	private:
	};

}