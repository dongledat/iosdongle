#include "TransactionManager.h"

using namespace dat;

std::atomic<TransactionID_t> TransactionManager::s_transactionId(INITIAL_TRANSACTION_ID);

TransactionManager::TransactionManager(IDataListener & dataListener, IUpdateProgressListener& updateProgressListener, IDongleStatusListener& dongleStatusListener, DongleSocket & dongleSocket) :
	m_dataListener(dataListener),
	m_dongleSocket(dongleSocket),
	m_updateProgressListener(updateProgressListener),
	m_dongleStatusListener(dongleStatusListener)
{
}

TransactionManager::~TransactionManager()
{
	for (auto i = m_transactions.begin(); i != m_transactions.end();) {
		delete i->second;
		m_transactions.erase(i++);
	}
}

void TransactionManager::reset()
{
	m_transactions.clear();
}

TransactionID_t TransactionManager::reserveId()
{
	// Find unused ID, because ID global might wrap-around and run over existing transaction IDs (hypothetical)
	TransactionID_t newID = 0;
	do {
		newID = s_transactionId++;
	} while (m_transactions.end() != m_transactions.find(newID));

	return newID;	
}

SimpleTransaction * TransactionManager::newSimpleTransaction(TransactionType type)
{
	SimpleTransaction * transaction = new SimpleTransaction(m_dongleSocket, m_dataListener, type, this->reserveId());
	if (nullptr == transaction) {
		return nullptr;
	}

	m_transactions.emplace(transaction->getTransactionID(), transaction);
	return transaction;
}

DataSubscriptionTransaction * TransactionManager::newDataSubscriptionTransaction()
{
	DataSubscriptionTransaction * transaction = new DataSubscriptionTransaction(m_dongleSocket, m_dataListener, this->reserveId());
	if (nullptr == transaction) {
		return nullptr;
	}

	m_transactions.emplace(transaction->getTransactionID(), transaction);
	return transaction;
}

PDUTransaction * TransactionManager::newPDUTransaction()
{
	if (existsActiveTransaction(TransactionType_PDU)) {
		return nullptr;
	}

	PDUTransaction * transaction = new PDUTransaction(m_dongleSocket, m_dataListener, this->reserveId());
	if (nullptr == transaction) {
		return nullptr;
	}

	m_transactions.emplace(transaction->getTransactionID(), transaction);
	return transaction;
}

ConfigurationUpdateTransaction * TransactionManager::newConfigurationUpdateTransaction()
{
	if (existsActiveTransaction(TransactionType_ConfigurationUpdate)) {
		return nullptr;
	}

	ConfigurationUpdateTransaction * transaction = new ConfigurationUpdateTransaction(m_dongleSocket, m_updateProgressListener, this->reserveId());
	if (nullptr == transaction) {
		return nullptr;
	}

	m_transactions.emplace(transaction->getTransactionID(), transaction);
	return transaction;
}

PinAuthenticationTransaction * TransactionManager::newPinAuthenticationTransaction()
{
	if (existsActiveTransaction(TransactionType_PinAuth) ||
		existsActiveTransaction(TransactionType_TokenAuth)) {
		return nullptr;
	}
	
	PinAuthenticationTransaction * transaction = new PinAuthenticationTransaction(m_dongleSocket, m_dongleStatusListener, this->reserveId());
	if (nullptr == transaction) {
		return nullptr;
	}

	m_transactions.emplace(transaction->getTransactionID(), transaction);
	return transaction;
}

TokenAuthenticationTransaction * dat::TransactionManager::newTokenAuthenticationTransaction()
{
	if (existsActiveTransaction(TransactionType_PinAuth) ||
		existsActiveTransaction(TransactionType_TokenAuth)) {
		return nullptr;
	}

	TokenAuthenticationTransaction * transaction = new TokenAuthenticationTransaction(m_dongleSocket, m_dongleStatusListener, this->reserveId());
	if (nullptr == transaction) {
		return nullptr;
	}

	m_transactions.emplace(transaction->getTransactionID(), transaction);
	return transaction;
}

ITransaction * TransactionManager::findTransaction(TransactionID_t id)
{
	for (auto i = m_transactions.begin(); i != m_transactions.end(); ++i) {
		if (i->second->getTransactionID() == id) {
			return i->second;
		}
	}

	return nullptr;
}

ITransaction * TransactionManager::findTransaction(TransactionID_t id, TransactionType type)
{
	ITransaction* transaction = findTransaction(id);
	if (nullptr == transaction) {
		return nullptr;
	}

	if (transaction->getTransactionType() != type) {
		return nullptr;
	}

	return transaction;
}

void TransactionManager::cleanupTransactions()
{
	for (auto i = m_transactions.begin(); i != m_transactions.end();) {
		if (i->second->isReaped() ||
			i->second->isExpired()) {

			delete i->second;
			m_transactions.erase(i++);

		}
		else {
			++i;
		}
	}
}

unsigned int dat::TransactionManager::getTransactionCount()
{
	return m_transactions.size();
}

bool TransactionManager::existsActiveTransaction(TransactionType type)
{
	for (auto i = m_transactions.begin(); i != m_transactions.end(); ++i) {
		if (i->second->getTransactionType() != type) {
			continue;
		}

		if ((!i->second->isCompleted()) && (!i->second->isExpired())) {
			return true;
		}

		// Either completed, expired, or both - not active
	}

	return false;
}
