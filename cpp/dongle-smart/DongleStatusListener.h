#include "IConnection.h"

namespace dat {

	class DongleStatusListener : public IDongleStatusListener {
	public:
		DongleStatusListener();

		virtual void onDongleStatus(DongleStatus dongleStatus) override;
		virtual void onAuthenticate(AuthenticationMethod authenticationMethod, AuthenticationResult authenticationResult) override;

		bool m_isCalled_onDongleStatus;
		bool m_isCalled_onAuthenticate;

		AuthenticationMethod m_last_onAuthenticate_param_AuthenticationMethod;
		AuthenticationResult m_last_onAuthenticate_param_AuthenticationResult;
		DongleStatus m_last_onDongleStatus_param_dongleStatus;

		void reset() {
			m_isCalled_onDongleStatus = false;
			m_isCalled_onAuthenticate = false;
			m_last_onAuthenticate_param_AuthenticationMethod = AuthenticationMethod::AUTH_METHOD_NONE;
			m_last_onAuthenticate_param_AuthenticationResult = AuthenticationResult::AUTH_RESULT_NONE;
		}

	private:
	};

}