#ifndef _DTP_STREAM_H_
#define _DTP_STREAM_H_

#include "Streams.h"
#include "dtp.h"

namespace dat {

	class DTPStream : public IIOStream
	{
	public:
		DTPStream();
		DTPStream(IIOStream * lowerLayer);
		~DTPStream();

		void reset();

		// Set the lower level
		void setlowerLayerStream(IIOStream * lowerLayer);

		// Will wrap a buffer with DTP and send it over to the lower layer
		size_t send(const unsigned char* data, size_t length) override;

		// Will read serially byte after byte from the lower layer,
		// untill a single DTP message is retreived. That message will be returned in the output parameters,
		// with the size returning in as return value.
		size_t recv(unsigned char* buffer, size_t buffer_size) override;

	private:
		IIOStream * m_lowerLayer;
		dtp_t * m_dtp;
	};

}

#endif /* _DTP_STREAM_H_ */