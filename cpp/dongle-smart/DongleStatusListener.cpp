#include "DongleStatusListener.h"

using namespace dat;

DongleStatusListener::DongleStatusListener() {
	reset();
}

void DongleStatusListener::onDongleStatus(DongleStatus dongleStatus)
{
	m_isCalled_onDongleStatus = true;
	m_last_onDongleStatus_param_dongleStatus = dongleStatus;
}

void DongleStatusListener::onAuthenticate(AuthenticationMethod authenticationMethod, AuthenticationResult authenticationResult)
{
	m_isCalled_onAuthenticate = true;
	m_last_onAuthenticate_param_AuthenticationMethod = authenticationMethod;
	m_last_onAuthenticate_param_AuthenticationResult = authenticationResult;
}
