#include "DTPStream.h"
#include "dtp.h"
#include "dat.h"

//TODO: ????

using namespace dat;

DTPStream::DTPStream() :
	m_dtp(dtp_alloc(DTP_BUFFER_SIZE))
{
}

DTPStream::DTPStream(IIOStream * stream) :
	m_lowerLayer(stream),
	m_dtp(dtp_alloc(DTP_BUFFER_SIZE))
{
}

DTPStream::~DTPStream()
{
	if (m_dtp) {
		dtp_free(m_dtp);
	}
}

void DTPStream::reset()
{
	if (m_dtp) {
		dtp_free(m_dtp);
		m_dtp = nullptr;
	}
	
	m_lowerLayer = nullptr;
}

void DTPStream::setlowerLayerStream(IIOStream * stream)
{
	m_lowerLayer = stream;
}

size_t DTPStream::send(const unsigned char * data, size_t length)
{
	net_buf_t *net_buf = net_buf_alloc(length);
	if (!net_buf)
		return 0;

	
	if (0 != net_buf_add_tail(net_buf, (const void*)data, length))
		goto out;

	dtp_add_header(net_buf);

	m_lowerLayer->send(net_buf->data, net_buf->length);

out:
	net_buf_free(net_buf);

	return 0;
}

size_t DTPStream::recv(unsigned char * buffer, size_t buffer_size)
{
	unsigned char temp_buffer[DTP_BUFFER_SIZE];
	int temp_buffer_size = m_lowerLayer->recv(temp_buffer, DTP_BUFFER_SIZE);
	if (0 >= temp_buffer_size) {
		// no data OR error; propagate
		return temp_buffer_size;
	}

	dtp_from_net(m_dtp, temp_buffer, temp_buffer_size); // TODO: Return value?
	if (!m_dtp->done) {
		return 0;
	}

	int dtp_size = dtp_len(m_dtp);
	memcpy(buffer, dtp_data(m_dtp), dtp_size);
	
	// reset
	dtp_free(m_dtp);
	m_dtp = dtp_alloc(DTP_BUFFER_SIZE);
	
	return dtp_size;
}
