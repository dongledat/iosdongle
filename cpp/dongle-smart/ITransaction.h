#ifndef _ITRANSACTION_H_
#define _ITRANSACTION_H_

#include "dat.h"
#include "IConnection.h"
#include "DongleSocket.h"
#include "Configuration.h"

#include "../os/OsTime.h"
#include "../common/DatTime.h"

#include <functional>
#include <list>
#include <set>
#include <map>
#include <atomic>

namespace dat {
	
	typedef unsigned int TransactionID_t;

	typedef enum {

		TransactionType_GetSerialNumberDevice = 0,
		TransactionType_GetBtMacAddress,
		TransactionType_GetHwVersion,
		TransactionType_GetFwStatus,
		TransactionType_GetDevStatus,
		TransactionType_GetActivationStatus,
		TransactionType_GetSetProtocolMode,
		TransactionType_GetAvailableDiagnosticParameter,
		TransactionType_SwitchToServiceMode,
		TransactionType_SwitchToUserMode,
		//TransactionType_AvailableDiagParametersAreChanged, //todo
		//TransactionType_GetDir, //todo
		TransactionType_GetProtocol,
		TransactionType_GetDataObject,
		TransactionType_DataSubscription,
		TransactionType_ConfigurationUpdate,
		TransactionType_SimplePDU,
		TransactionType_PDU,
		TransactionType_PinAuth,
		TransactionType_TokenAuth

	} TransactionType;

	class ITransaction {
	public:

		// Init start & expiry times,
		ITransaction(DongleSocket& socket, TransactionType transactionType, TransactionID_t id, bool expirable = true, bool waitForReap = false);
		void sendMessage(TDongleReq& msg);
		
		// Getters
		bool isCompleted() const;
		bool isExpired() const;
		bool isReaped() const;
		TransactionID_t getTransactionID() const { return m_transactionID; }
		common::Time getStartTime() const { return m_startTime; }
		common::Time getExpiryTime() const { return m_expiryTime; }
		TransactionType getTransactionType() const { return m_transactionType; }

		// Setters
		void setReaped() { m_isReaped = true; }
		void setCompleted() { m_isCompleted = true; }
		void extendExpiryTime() { m_expiryTime = common::Time::uSeconds(os::Time::realUSeconds() + DEFAULT_EXPIRY_INTERVAL_IN_USECONDS); }

		// Virtual methods
		virtual int onMessage(TDongleRsp & rsp) = 0;

		// Constants
		static const unsigned int INVALID_TRANSACTION_ID = 0;
		static const unsigned long long DEFAULT_EXPIRY_INTERVAL_IN_USECONDS = Configuration::DEFAULT_TRANSACTION_EXPIRY_INTERVAL_IN_SECONDS * 1000 * 1000;
		static const int OPERATION_ERROR = -1;
	protected:
		bool m_isCompleted;
		bool m_isReaped;
		bool m_waitForReap;
		bool m_expirable;
		DongleSocket& m_dongleSocket;

	private:
		TransactionID_t m_transactionID;
		TransactionType m_transactionType;
		common::Time m_startTime;
		common::Time m_expiryTime;
	};


}
#endif /* _ITRANSACTION_H_ */