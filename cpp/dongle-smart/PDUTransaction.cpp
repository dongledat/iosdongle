#include "PDUTransaction.h"
#include "../crypto/dat_base64.h"
#include "NanopbRAIIUtilities.h"
#include "Configuration.h"
#include "../dongle/dat_config.h"

#ifdef __ANDROID__
extern "C" {
#include "../../ext/mbedtls/library/base64.c"
}
#endif //__ANDROID__

using namespace dat;

const unsigned int PDU_NOTIFY_SUCCESS = 0;

PDUTransaction::PDUTransaction(DongleSocket & socket, IDataListener & dataListener, TransactionID_t id) :
	m_dataListener(dataListener),
	ITransaction(socket, TransactionType_PDU, id)
{
}

int PDUTransaction::sendPDU(TPDUReq_EPDUJ1962Pin pins,
	IConnection::PDUProtocol protocol,
	int startingTimeOut,
	std::vector<TPDUCommunicationParameter> & commParameters,
	std::vector<TPDURequest> & pduRequests)
{
	TDongleReq req = TDongleReq_init_zero;
	req.which_request = TDongleReq_PDUReq_tag;
	auto & pdu = req.request.PDUReq;

	if (commParameters.size() > Configuration::MAX_COMM_PARAMETERS_COUNT) {
		return ITransaction::OPERATION_ERROR;
	}

	if (pduRequests.size() > Configuration::MAX_PDU_REQUESTS_COUNT) {
		return ITransaction::OPERATION_ERROR;
	}

	switch (protocol) {
		case IConnection::PDUProtocol::TP20:
			pdu.Protocol = TPDUReq_EProtocolType_VW2000LPonTP20;
			break;

		case IConnection::PDUProtocol::ISO14229:
			pdu.Protocol = TPDUReq_EProtocolType_ISOUDSonCAN;
			break;

		case IConnection::PDUProtocol::ISO15765:
			pdu.Protocol = TPDUReq_EProtocolType_ISOOBDonCAN;
			break;

		default:
			return ITransaction::OPERATION_ERROR;
	}

	pdu.Pins = pins;
	pdu.StartingTimeOut = startingTimeOut;

	for (int i = 0; i < commParameters.size(); ++i) {
		pdu.commParams[i] = commParameters[i];
	}
	pdu.commParams_count = commParameters.size();

	for (int i = 0; i < pduRequests.size(); ++i) {
		pdu.dynamicList[i] = pduRequests[i];
	}
	pdu.dynamicList_count = pduRequests.size();

	sendMessage(req);
	m_requestSent = true;
	return 0;
}

/*
typedef struct _TPDUReq {
TPDUReq_EPDUJ1962Pin Pins;
TPDUReq_EProtocolType Protocol;
int32_t StartingTimeOut;
int32_t nCommParams;
int32_t nPDU;
TPDUReq_dynamicList_t dynamicList;
*/
//
//int PDUTransaction::sendPDU(int canID, int canSpeed, int canSP, int canSJW, int canResponseTimeout, int tp20ChannelAddress, IConnection::PDUProtocol protocol, const std::string & reqPDU)
//{
//	TDongleReq req = TDongleReq_init_zero;	
//	// Calculate de-base64'd size
//	size_t buffer_size = 0;
//	if (0 != dat_base64_decode(nullptr, 0, &buffer_size, (const unsigned char*)(reqPDU.c_str()), reqPDU.size())) {
//		return -1; //todo
//	}
//
//	// Build nanopb pb_bytes_array, with De-base64'd size and actual raw (de-base64'd) data
//	NanopbBytesRAII bytes(static_cast<pb_size_t>(buffer_size));
//	if (0 != dat_base64_decode((unsigned char*)(bytes.getDataPointer()), buffer_size, nullptr, (const unsigned char*)(reqPDU.c_str()), reqPDU.size())) {
//		return -1; //todo
//	}
//
//	// Build request
//	req.which_request = TDongleReq_PDUReq_tag;
//	req.request.PDUReq.CanID = canID;
//	req.request.PDUReq.CanSpeed = canSpeed;
//	req.request.PDUReq.CanSP = canSP;
//	req.request.PDUReq.CanSJW = canSJW;
//	req.request.PDUReq.CanResponseTimeout = canResponseTimeout;
//	req.request.PDUReq.TP20ChannelAddress = tp20ChannelAddress;
//	req.request.PDUReq.PDUProtocol = protocol;
//	req.request.PDUReq.Data = bytes.get();
//	
//	sendMessage(req);
//	m_requestSent = true;
//	return 0;
//}

/************ The fragmented version ************/
//int PDUTransaction::onMessage(TDongleRsp & rsp)
//{
//	if (!isMessageTypeExpected(rsp.which_response)) {
//		return -1;
//	}
//
//	auto& pdu = rsp.response.PDURsp;
//
//	m_pduAggregator.onDataFragment(DataChunk(pdu.TransactionID,
//		pdu.TotalChunkCount,
//		pdu.ChunkIndex,
//		std::string(reinterpret_cast<char*>(&(pdu.Data->bytes)),
//					reinterpret_cast<char*>(&(pdu.Data->bytes) + pdu.Data->size))));
//
//	if (!m_pduAggregator.isCompleted(pdu.TransactionID)) {
//		// PDU is not fully aggregated yet
//		return 0;
//	}
//
//	// Completed PDU
//	m_dataListener.onPDUResponse(rsp.id, m_pduAggregator.popAggregatedData(pdu.TransactionID));
//	m_isCompleted = true;
//	return 0;
//}

int PDUTransaction::onMessage(TDongleRsp & rsp)
{
	if (!isMessageTypeExpected(rsp.which_response)) {
		return -1;
	}

	switch (rsp.which_response)
	{
	case TDongleRsp_PDURsp_tag:
		m_responseReceived = true;

		if (0 != rsp.response.PDURsp.Result) {
            // Failure
			m_isCompleted = true;
            m_dataListener.onPDUNotify(rsp.id, "", false);
            return -1;
        } 

		break;

	case TDongleRsp_PDUNotify_tag:
		auto & notify = rsp.response.PDUNotify;

		if (PDU_NOTIFY_SUCCESS != notify.ErrorCode) {
			//TODO: Notify high level through callback about failure
			m_isCompleted = true;
			return -1;
		}
		
		unsigned char buffer[DAT_MAX_PROTOBUF_MESSAGE_LEN];
		pb_ostream_t ostream = pb_ostream_from_buffer(buffer, DAT_MAX_PROTOBUF_MESSAGE_LEN);
		if (!pb_encode(&ostream, TDongleRsp_fields, &rsp)) {
			return -1;
		}
		std::string rspAsString(buffer, buffer + ostream.bytes_written);

        m_dataListener.onPDUNotify(rsp.id, rspAsString, true);
        m_isCompleted = true;
		break;
	}

	return 0;
}

bool PDUTransaction::isMessageTypeExpected(unsigned int responseType) const
{
	switch (responseType) 
	{
	case TDongleRsp_PDURsp_tag:
		return m_requestSent;

	case TDongleRsp_PDUNotify_tag:
		return m_responseReceived;

	default:
		return false;
	}
}
