#include "DongleSocket.h"
#include "pb_encode.h"
#include "pb_decode.h"
#include "pb_common.h"

const unsigned int REQUEST_MAX_BUFFER_SIZE = 4096;

using namespace dat;

DongleSocket::DongleSocket() :
	m_stream(nullptr)
{
}

void DongleSocket::setlowerLayerStream(IIOStream* stream)
{
	m_stream = stream;
}

bool DongleSocket::onSessionUpdate()
{
	unsigned char buffer[DAT_MAX_PROTOBUF_MESSAGE_LEN];
	
	if (!m_stream) {
		return false;
	}

	// Get protobuf message
	auto size = m_stream->recv(buffer, DAT_MAX_PROTOBUF_MESSAGE_LEN);
	if (IStream::STREAM_ERROR == size) {
		//TODO: log
		return false;
	} else if (0 == size) {
		// No data ready
		return true;
	}

	// Deserialize the message
	TDongleRsp* rsp = new TDongleRsp;
	pb_istream_t stream = pb_istream_from_buffer(buffer, size);
	if (!pb_decode(&stream, TDongleRsp_fields, rsp)) {
		// Error in decoding
		return false;
	}

	m_messages.emplace(rsp);
	return true;
}

TDongleRsp* DongleSocket::getMessage()
{
	if (m_messages.size() == 0) {
		return nullptr;
	}

	auto rsp = m_messages.front();
	m_messages.pop();
	return rsp;
}

//todo: bool return value
void DongleSocket::sendMessage(TDongleReq * msg)
{
	unsigned char buffer[REQUEST_MAX_BUFFER_SIZE];

	// Serialize the request
	pb_ostream_t ostream = pb_ostream_from_buffer(buffer, REQUEST_MAX_BUFFER_SIZE);
	if (!pb_encode(&ostream, TDongleReq_fields, msg))
		return;

	m_stream->send(buffer, ostream.bytes_written);
}

unsigned int DongleSocket::getMessageCount()
{
	return m_messages.size();
}
