#ifndef _STREAMS_H_
#define _STREAMS_H_

#include <vector>
#include <stdlib.h>

namespace dat {

	class IStream
	{
	public:

		static const int STREAM_ERROR = -1;
	};
	
	class IOutputStream : public IStream
	{
	public:

		virtual size_t send(const unsigned char* data, size_t length) = 0;
	};

	class IInputStream : public IStream
	{
	public:

		virtual size_t recv(unsigned char* buffer, size_t buffer_size) = 0;
	};

	class IIOStream : public IOutputStream, public IInputStream
	{
	};

	// A stream that uses a buffer to store the input, and a different stream to send the output
	// We will use it to store data incoming from high level, which is actually network data,
	// and also to read it in a different location in our code 
	class InputBufferedStream : public IIOStream
	{
	public:
		explicit InputBufferedStream(IOutputStream & outputStream);

		// Send onward
		size_t send(const unsigned char* data, size_t length) override;
		// Read from recv buffer
		size_t recv(unsigned char* buffer, size_t buffer_size) override;

		// Update recv buffer with data
		void appendData(const unsigned char* data, size_t length);

		// Get current amount of bytes in the buffer
		size_t getSize();

	private:
		std::vector<unsigned char> m_buffer;
		IOutputStream& m_outputStream;
	};

}

#endif /* _STREAMS_H_ */