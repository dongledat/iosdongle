#ifndef _DONGLE_SOCKET_H_
#define _DONGLE_SOCKET_H_

#include "IConnection.h"
#include <queue>
#include <memory>

namespace dat {

	class DongleSocket {
	public:

		DongleSocket();
		void setlowerLayerStream(IIOStream* stream);

		// Returns true for success, false for error
		bool onSessionUpdate();
		
		TDongleRsp* getMessage();
		void sendMessage(TDongleReq* msg);
		unsigned int getMessageCount();

	private:
		IIOStream * m_stream;
		std::queue<TDongleRsp*> m_messages;
	};

}

#endif /* _DONGLE_SOCKET_H_ */