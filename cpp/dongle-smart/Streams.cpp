#pragma warning(disable:4996) 

#include "Streams.h"

#include <algorithm>

using namespace dat;

InputBufferedStream::InputBufferedStream(IOutputStream & outputStream) : m_outputStream(outputStream)
{
}

size_t InputBufferedStream::send(const unsigned char * data, size_t length)
{
	return m_outputStream.send(data, length);
}

size_t InputBufferedStream::recv(unsigned char * buffer, size_t buffer_size)
{
	auto toRead = std::min(m_buffer.size(), buffer_size);

	// Pop from buffer
	std::copy(m_buffer.begin(), m_buffer.begin() + toRead, buffer);
	m_buffer.erase(m_buffer.begin(), m_buffer.begin() + toRead);

	return toRead;
}

void InputBufferedStream::appendData(const unsigned char * data, size_t length)
{
	// Insert into buffer
	m_buffer.insert(m_buffer.end(), data, data + length);
}

size_t InputBufferedStream::getSize()
{
	return m_buffer.size();
}
