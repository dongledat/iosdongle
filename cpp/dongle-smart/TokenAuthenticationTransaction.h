#ifndef _TOKEN_AUTHENTICATION_TRANSACTION_H_
#define _TOKEN_AUTHENTICATION_TRANSACTION_H_

#include "ITransaction.h"

namespace dat {

	class TokenAuthenticationTransaction : public ITransaction {
	public:
		TokenAuthenticationTransaction(DongleSocket& dongleSocket, IDongleStatusListener& dongleStatusListener, TransactionID_t id);

		int onMessage(TDongleRsp & rsp) override;

		void beginAuthentication();

		IDongleStatusListener::AuthenticationResult getResult() { return m_result; }

	private:
		bool isMessageTypeExpected(unsigned int responseType)  const;

		IDongleStatusListener::AuthenticationResult m_result;
		IDongleStatusListener& m_dongleStatusListener;
	};

}

#endif /* _SIMPLE_TRANSACTION_H_ */
