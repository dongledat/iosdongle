#include "CppUTest/TestHarness.h"
#include "net/Net.h"
#include "net/UdpClient.h"
#include "os/Thread.h"
#include "os/Sleep.h"
#include "net_buf.h"
#include "dtp.h"
#include "dongle-sim-lib/DongleSimulator.h"
#include "dongle-sim-lib/CarSimulatorSupportedDiagnostics.h"
#include "dongle-smart\IConnection.h"
#include "dongle-smart\Connection.h"
#include "dongle-smart\DongleStatusListener.h"
#include "dongle-smart\DataListener.h"
#include "dongle-smart\UpdateProgressListener.h"
#include "dongle-smart\DataAggregator.h"
#include "dongle-sim-lib\DataSubscription.h"

#include "log.h"

#define SERVER_UDP_PORT (11111)
#define CLIENT_UDP_SRC_PORT (13372)
#define BUFFER_SIZE (1000)
#define JAVA_STUBS

#define UDP_TIMEOUT (40)
#define SIMULATOR_TIMEOUT (40)

#define TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL (DIAG_VEHICLESPEED)
#define TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_ELEMENT_COUNT (1)
#define TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_FIRST_ELEMENT_CODE (0x00020000)
#define TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_SECOND_ELEMENT_CODE (1)
#define TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_FIRST_ELEMENT_TYPE (TDataElemenent_f_tag)
#define TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_SECOND_ELEMENT_TYPE (TDataElemenent_f_tag)

class MyTestOutputStream : public dat::IOutputStream
{

public:
	MyTestOutputStream(const net::UdpClient & client) :
		m_client(client) {
	}

	virtual size_t send(const unsigned char* data, size_t length) {
		return m_client.send(reinterpret_cast<const char*>(data), length);
	}

private:
	net::UdpClient m_client;
};

static dat::DongleStatusListener dongleStatusListener;
static dat::DataListener dataListener;
static dat::UpdateProgressListener updateProgressListener;
static MyTestOutputStream* outputStream;
static DongleSimulator * simulator;
static dat::Connection* con;
static net::UdpClient* smartphoneUdp;

extern SubscriptionManager g_DiagCodeSubscriptionManager;

extern "C" int g_tls_enabled;
extern "C" int g_dtp_enabled;

static size_t recvSessionData(net::UdpClient *udpClient, unsigned char *raw_buf, unsigned int size, TDongleRsp *rsp)
{
#ifdef DAT_ENABLE_DTP
	if (g_dtp_enabled) {
		char buf[0x1000];
		dtp_t *dtp = dtp_alloc(BUFFER_SIZE);
		if (!dtp)
			FAIL("DTP allocation failed");

		while (!dtp->done) {
			int received = udpClient->recv(buf, sizeof(buf));
			if (received <= 0) {
				if (received == -1) {
					int x = WSAGetLastError();
					if (WSAETIMEDOUT == x) {
						FAIL("Timeout while waiting for data from the Dongle");
					}
					else {
						FAIL("Socket error while waiting for data from the Dongle");
					}
				}
				else {
					FAIL("Empty packet received from Dongle");
				}
			}

			dtp_from_net(dtp, buf, received);
		}

		if (!rsp)
			FAIL("Invalid response");

		pb_istream_t stream = pb_istream_from_buffer(dtp_data(dtp), dtp_len(dtp));
		if (!pb_decode(&stream, TDongleRsp_fields, rsp)) {
			FAIL("pb_decode failed on response");
		}

		size_t out_len = dtp->net_buf->length;
		if (size < out_len)
			FAIL("raw buffer is too small");

		memcpy(raw_buf, dtp->net_buf->data, out_len);
		dtp_free(dtp);
		return out_len;
	}
#endif

	int received = udpClient->recv((char *)raw_buf, size);
	if (received <= 0)
		FAIL("recv failed");

	pb_istream_t stream = pb_istream_from_buffer(raw_buf, received);
	if (!pb_decode(&stream, TDongleRsp_fields, rsp)) {
		FAIL("pb_decode failed on response");
	}
	return received;
}

TEST_GROUP(Smart)
{

	TEST_SETUP()
	{
		MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
		auto hack  = DongleSimulator::get();
		MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();

		net::init();

		// [DONGLE] Start simulator in new thread, let it initialize
		simulator = DongleSimulator::get();
		simulator->start(SERVER_UDP_PORT, false, true, SIMULATOR_TIMEOUT, false, false);
		
		os::Sleep(500);

		// [SMARTPHONE] Initialize connection, bind to src port, set target port as the simulator's server port
		smartphoneUdp = new net::UdpClient("127.0.0.1", SERVER_UDP_PORT, UDP_TIMEOUT);
		smartphoneUdp->bindSrcPort(CLIENT_UDP_SRC_PORT);
		outputStream = new MyTestOutputStream(*smartphoneUdp);
		con = new dat::Connection(*outputStream, dongleStatusListener, dataListener, updateProgressListener, false, true);


#ifdef JAVA_STUBS
		dongleStatusListener.reset();
		dataListener.reset();
		updateProgressListener.reset();
#endif

	}

	TEST_TEARDOWN()
	{
		simulator->stop();
		delete con;
		delete outputStream;
		delete smartphoneUdp;
		g_DiagCodeSubscriptionManager.clear();
		
		dat_done();
		net::done();
		//MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();
	}

};

TEST_GROUP(Smart_NoDongle)
{

	TEST_SETUP()
	{
		MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
		net::init();

		// [SMARTPHONE] Initialize connection, bind to src port, set target port as the simulator's server port
		smartphoneUdp = new net::UdpClient("127.0.0.1", SERVER_UDP_PORT);
		smartphoneUdp->bindSrcPort(CLIENT_UDP_SRC_PORT);
		outputStream = new MyTestOutputStream(*smartphoneUdp);
		con = new dat::Connection(*outputStream, dongleStatusListener, dataListener, updateProgressListener, false, true);

#ifdef JAVA_STUBS
		dongleStatusListener.reset();
		dataListener.reset();
		updateProgressListener.reset();
#endif
	}

	TEST_TEARDOWN()
	{
		delete con;
		delete outputStream;
		delete smartphoneUdp;

		net::done();
		MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();
	}
};

TEST_GROUP(DataAggregator)
{
};

TEST(Smart, GetDongleSerialNumber)
{
    // [SMARTPHONE] Request something from the dongle
    int transaction_id = con->getDongleSerialNumberAsync();

    // Now a UDP request is sent to the dongle simulator
    // Now the dongle simulator sends a UDP response

    // [SMARTPHONE] Get the raw response from the net
	TDongleRsp rsp = TDongleRsp_init_zero;
	unsigned char raw_buf[BUFFER_SIZE];
	size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

    CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
    CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
    CHECK_TEXT(rsp.which_response == TDongleRsp_SerialNumberDeviceRsp_tag, "Response type wrong");

    dataListener.reset();

    // [SMARTPHONE] Pass the data for further handling
	con->fromNet(raw_buf, buf_len);

#ifdef JAVA_STUBS
    // [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
    CHECK_TEXT(dataListener.m_isCalled_onDongleSerialNumber, "JAVA callback not called");
#endif
}


TEST(Smart, GetDongleSerialNumber_NO_DTP)
{
	simulator->setDTP(false);
	delete con;
	con = new dat::Connection(*outputStream, dongleStatusListener, dataListener, updateProgressListener, false, false);

	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->getDongleSerialNumberAsync();

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// [SMARTPHONE] Get the raw response from the net
	TDongleRsp rsp = TDongleRsp_init_zero;
	unsigned char raw_buf[BUFFER_SIZE];
	size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

	CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
	CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
	CHECK_TEXT(rsp.which_response == TDongleRsp_SerialNumberDeviceRsp_tag, "Response type wrong");

	dataListener.reset();

	// [SMARTPHONE] Pass the data for further handling
	con->fromNet(raw_buf, buf_len);

#ifdef JAVA_STUBS
	// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
	CHECK_TEXT(dataListener.m_isCalled_onDongleSerialNumber, "JAVA callback not called");
#endif
}

TEST(Smart, GetFirmwareStatus)
{
    // [SMARTPHONE] Request something from the dongle
    int transaction_id = con->getFirmwareStatusAsync();

    // Now a UDP request is sent to the dongle simulator
    // Now the dongle simulator sends a UDP response

    // [SMARTPHONE] Get the raw response from the net
	TDongleRsp rsp = TDongleRsp_init_zero;
	unsigned char raw_buf[BUFFER_SIZE];
	size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

    CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
    CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
    CHECK_TEXT(rsp.which_response == TDongleRsp_FwStatusRsp_tag, "Response type wrong");

    dataListener.reset();

    // [SMARTPHONE] Pass the data for further handling
	con->fromNet(raw_buf, buf_len);

#ifdef JAVA_STUBS
    // [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
    CHECK_TEXT(dataListener.m_isCalled_onFirmwareStatus, "JAVA callback not called");
#endif
}

TEST(Smart, GetDongleHWRevision)
{
	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->getDongleHWRevisionAsync();

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// [SMARTPHONE] Get the raw response from the net
	TDongleRsp rsp = TDongleRsp_init_zero;
	unsigned char raw_buf[BUFFER_SIZE];
	size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

	// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
	CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
	CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
	CHECK_TEXT(rsp.which_response == TDongleRsp_HwVersionRsp_tag, "Response type wrong");
	dataListener.reset();

	//

	// [SMARTPHONE] Pass the data for further handling
	con->fromNet(raw_buf, buf_len);

#ifdef JAVA_STUBS
	// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
	CHECK_TEXT(dataListener.m_isCalled_onDongleHWRevision, "JAVA callback not called");
#endif
}

TEST(Smart, SmartGetAvaialbleDiagParams)
{
	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->getAvailableDiagParametersAsync();

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// [SMARTPHONE] Get the raw response from the net
	TDongleRsp rsp = TDongleRsp_init_zero;
	unsigned char raw_buf[BUFFER_SIZE];
	size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);


	// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
	CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
	CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
	CHECK_TEXT(rsp.which_response == TDongleRsp_GetAvailableDiagnosticParametersRsp_tag, "response type wrong");

	dataListener.reset();

	// [SMARTPHONE] Pass the data for further handling
	con->fromNet(raw_buf, buf_len);

    //LOG("\nAmount of available diagnostic parameters: %d\n", rsp.response.GetAvailableDiagnosticParametersRsp.UrlCode.size);
    CHECK(rsp.response.GetAvailableDiagnosticParametersRsp.UrlCode.size == rsp.response.GetAvailableDiagnosticParametersRsp.IsSubscribed.size);

#ifdef JAVA_STUBS
	// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
	CHECK_TEXT(dataListener.m_isCalled_onAvailableDiagParameters, "JAVA callback not called");
#endif
}

TEST(Smart, SmartMultipleSimpleRequests)
{
	int first_trans_id = 0;

	for (int i = 0; i < 10; ++i) {
		// [SMARTPHONE] Request something from the dongle
		int transaction_id = con->getDongleHWRevisionAsync();

		if (i == 0) {
			first_trans_id = transaction_id;
		}
		else {
			CHECK_TEXT(i +  first_trans_id == transaction_id, "Transaction ID static");
		}

		// Now a UDP request is sent to the dongle simulator
		// Now the dongle simulator sends a UDP response

		// [SMARTPHONE] Get the raw response from the net
		TDongleRsp rsp = TDongleRsp_init_zero;
		unsigned char raw_buf[BUFFER_SIZE];
		size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);


		// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
		CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
		CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
		CHECK_TEXT(rsp.which_response == TDongleRsp_HwVersionRsp_tag, "response type wrong");

		dataListener.reset();

		// [SMARTPHONE] Pass the data for further handling
		con->fromNet(raw_buf, buf_len);

#ifdef JAVA_STUBS
		// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
		CHECK_TEXT(dataListener.m_isCalled_onDongleHWRevision, "JAVA callback not called");
#endif
	}
}

TEST(Smart, SmartGetDataObject_VIN)
{
	TDongleRsp rsp = TDongleRsp_init_zero;
	int transaction_id = con->getDataObjectAsync(DIAG_VEHICLEIDENTIFICATIONNUMBER);

	unsigned char raw_buf[BUFFER_SIZE];
	size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

	CHECK_TEXT(rsp.which_response == TDongleRsp_GetDataObjectRsp_tag, "response type wrong");

	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.UrlCode == DIAG_VEHICLEIDENTIFICATIONNUMBER, "wrong url code");
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement_count == 1, "expected 1 element");
	
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].which_value == TDataElemenent_s_tag, "expected TDataElemenent_s_tag");
	
	//printf("%.*s\n", rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.s->size, rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.s->bytes);
		
	pb_release(TDongleRsp_fields, &rsp);

	dataListener.reset();

	// [SMARTPHONE] Pass the data for further handling
	con->fromNet((const unsigned char*)raw_buf, buf_len);

}

TEST(Smart, SmartGetDataObject)
{
	
	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->getDataObjectAsync(DIAG_URL_CODE_THROTTLE_POSITION);

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// [SMARTPHONE] Get the raw response from the net
	TDongleRsp rsp = TDongleRsp_init_zero;
	unsigned char raw_buf[BUFFER_SIZE];
	size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

	// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
	CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
	CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");

	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.UrlCode == DIAG_URL_CODE_THROTTLE_POSITION, "wrong url code");
	CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement_count > 0, "bad element count");
	//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].code == DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "expected DIAG_CODE_THROTTLE_POSITION_ABSOLUTE");
	//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].code == DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected DIAG_CODE_THROTTLE_POSITION_RELATIVE");
	//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].which_value == TDataElemenent_f_tag, "expected TDataElemenent_f_tag");
	//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].which_value == TDataElemenent_f_tag, "expected TDataElemenent_f_tag");
	//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.i == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "expected SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_ABSOLUTE");
	//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].value.i == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE");

	pb_release(TDongleRsp_fields, &rsp);

	dataListener.reset();

	// [SMARTPHONE] Pass the data for further handling
	con->fromNet(raw_buf, buf_len);

#ifdef JAVA_STUBS
	// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
	CHECK_TEXT(dataListener.m_isCalled_onData, "JAVA callback not called");
#endif
}

TEST(Smart, SmartSubscribe)
{
	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->subscribeObject(DIAG_URL_CODE_THROTTLE_POSITION, 200);
	bool got_sub_response = false;

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// Get 10 updates
	for (int i = 0; i < 5; ++i) {

		// [SMARTPHONE] Get the raw response from the net
		TDongleRsp rsp = TDongleRsp_init_zero;
		unsigned char raw_buf[BUFFER_SIZE];
		size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

		// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
		if (TDongleRsp_SubscribeObjectRsp_tag == rsp.which_response) {
			if (got_sub_response) {
				FAIL("Got subscription response twice");
			}

			CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
			CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
			CHECK_TEXT(rsp.response.SubscribeObjectRsp.result == TSubscribeObjectRsp_EResult_OK, "Subscribe error");
			got_sub_response = true;
            continue;
		}

		CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
		CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
		CHECK_TEXT(rsp.which_response == TDongleRsp_GetDataObjectRsp_tag, "response type wrong");

		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.UrlCode == DIAG_URL_CODE_THROTTLE_POSITION, "wrong url code");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement_count > 0, "bad element count");
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].code == DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "expected DIAG_CODE_THROTTLE_POSITION_ABSOLUTE");
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].code == DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected DIAG_CODE_THROTTLE_POSITION_RELATIVE");
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].which_value == TDataElemenent_f_tag, "expected TDataElemenent_f_tag");
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].which_value == TDataElemenent_f_tag, "expected TDataElemenent_f_tag");
		/*
		auto expected_value = DATA_SUBSCRIPTION_SIMULATION_BASE_VALUE +
			(DATA_SUBSCRIPTION_SIMULATION_VALUE_INCREMENTS * i);
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.i == expected_value, "updated value not as expected");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].value.i == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE");
		*/
		pb_release(TDongleRsp_fields, &rsp);

		dataListener.reset();

		// [SMARTPHONE] Pass the data for further handling
		con->fromNet(raw_buf, buf_len);

#ifdef JAVA_STUBS
		// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
		CHECK_TEXT(dataListener.m_isCalled_onData, "JAVA callback not called");
#endif

	}

	con->unsubscribeObject(transaction_id);
}


IGNORE_TEST(Smart, SmartSubscribe_TEXA_Simulator)
{
    // [SMARTPHONE] Request something from the dongle
    int transaction_id = con->subscribeObject(TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL, 200);
    bool got_sub_response = false;

    // Now a UDP request is sent to the dongle simulator
    // Now the dongle simulator sends a UDP response

    // Get 10 updates
    for (int i = 0; i < 5; ++i) {

        // [SMARTPHONE] Get the raw response from the net
        TDongleRsp rsp = TDongleRsp_init_zero;
        unsigned char raw_buf[BUFFER_SIZE];
        size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

        // [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
        if (TDongleRsp_SubscribeObjectRsp_tag == rsp.which_response) {
            if (got_sub_response) {
                FAIL("Got subscription response twice");
            }

            CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
            CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
            CHECK_TEXT(rsp.response.SubscribeObjectRsp.result == TSubscribeObjectRsp_EResult_OK, "Subscribe error");
            got_sub_response = true;

			LOG("\nGot subscription successful response\n");
			continue;
        }

        CHECK_TEXT(!rsp.has_id, "Response doesnt have transaction id");
        CHECK_TEXT(rsp.which_response == TDongleRsp_GetDataObjectRsp_tag, "response type wrong");

        CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.UrlCode == TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL, "wrong url code");
        CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement_count == TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_ELEMENT_COUNT, "bad element count");
        CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].code == TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_FIRST_ELEMENT_CODE, "expected ");
        //CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].code == TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_SECOND_ELEMENT_CODE, "expected ");
        CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].which_value == TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_FIRST_ELEMENT_TYPE, "expected TDataElemenent_f_tag");
        //CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].which_value == TEXA_SIMULATOR_SUPPORTED_DIAGNOSTIC_URL_SECOND_ELEMENT_TYPE, "expected TDataElemenent_f_tag");
        /*
        auto expected_value = DATA_SUBSCRIPTION_SIMULATION_BASE_VALUE +
        (DATA_SUBSCRIPTION_SIMULATION_VALUE_INCREMENTS * i);
        CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.i == expected_value, "updated value not as expected");
        CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].value.i == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE");
        */
		LOG("\nGot data object response; Element value: %.6f\n", rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.f);
		pb_release(TDongleRsp_fields, &rsp);

        dataListener.reset();

        // [SMARTPHONE] Pass the data for further handling
        con->fromNet(raw_buf, buf_len);

#ifdef JAVA_STUBS
        // [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
        //CHECK_TEXT(dataListener.m_isCalled_onData, "JAVA callback not called");
#endif

    }

    con->unsubscribeObject(transaction_id);
}


TEST(Smart, SmartSubscribeUnsubscribe)
{
	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->subscribeObject(DIAG_URL_CODE_THROTTLE_POSITION, 200);
	bool got_sub_response = false;

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// Get 10 updates
	for (int i = 0; i < 5; ++i) {

		// [SMARTPHONE] Get the raw response from the net
		TDongleRsp rsp = TDongleRsp_init_zero;
		unsigned char raw_buf[BUFFER_SIZE];
		size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

		// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
		if (TDongleRsp_SubscribeObjectRsp_tag == rsp.which_response) {
			if (got_sub_response) {
				FAIL("Got subscription response twice");
			}

			CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
			CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
			CHECK_TEXT(rsp.response.SubscribeObjectRsp.result == TSubscribeObjectRsp_EResult_OK, "Subscribe error");
			got_sub_response = true;
            continue;
		}

		CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
		CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
		CHECK_TEXT(rsp.which_response == TDongleRsp_GetDataObjectRsp_tag, "response type wrong");

		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.UrlCode == DIAG_URL_CODE_THROTTLE_POSITION, "wrong url code");
		CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement_count > 0, "bad element count");
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].code == DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "expected DIAG_CODE_THROTTLE_POSITION_ABSOLUTE");
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].code == DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected DIAG_CODE_THROTTLE_POSITION_RELATIVE");
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].which_value == TDataElemenent_f_tag, "expected TDataElemenent_f_tag");
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].which_value == TDataElemenent_f_tag, "expected TDataElemenent_f_tag");
		/*auto expected_value = DATA_SUBSCRIPTION_SIMULATION_BASE_VALUE +
			(DATA_SUBSCRIPTION_SIMULATION_VALUE_INCREMENTS * i);
		*/
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[0].value.i == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_ABSOLUTE, "updated value not as expected");
		//CHECK_TEXT(rsp.response.GetDataObjectRsp.DataObject.DataElement[1].value.i == SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE, "expected SIMULATED_VALUE_DIAG_CODE_THROTTLE_POSITION_RELATIVE");

		pb_release(TDongleRsp_fields, &rsp);

		dataListener.reset();

		// [SMARTPHONE] Pass the data for further handling
		con->fromNet(raw_buf, buf_len);

#ifdef JAVA_STUBS
		// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
		CHECK_TEXT(dataListener.m_isCalled_onData, "JAVA callback not called");
#endif
	}

	auto uns_transaction_id = con->unsubscribeObject(transaction_id);
	CHECK(transaction_id == uns_transaction_id);

	// [SMARTPHONE] Get the raw response from the net
	TDongleRsp rsp = TDongleRsp_init_zero;
	unsigned char raw_buf[BUFFER_SIZE];
	size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

	// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
	CHECK_TEXT(rsp.which_response == TDongleRsp_UnsubscribeObjectRsp_tag, "response type wrong");
	CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
	CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
	CHECK_TEXT(rsp.response.UnsubscribeObjectRsp.result == TUnsubscribeObjectRsp_EResult_OK, "Unsubscribe error");
}

TEST(Smart_NoDongle, CheckExpiry)
{
	int transaction_id = con->getDongleHWRevisionAsync();
	auto& trans = con->getTransactions();

	if (trans.end() == trans.find(transaction_id)) {
		FAIL("Our request wasn't saved in the transaction mapping")
	}

	os::Sleep(200);

	int transaction_id2 = con->getDongleHWRevisionAsync();

	if (trans.end() == trans.find(transaction_id)) {
		FAIL("Our request expired too soon (?)")
	}
	if (trans.end() == trans.find(transaction_id2)) {
		FAIL("Our request wasn't saved in the transaction mapping")
	}

	os::Sleep(1000 * (dat::Configuration::DEFAULT_TRANSACTION_EXPIRY_INTERVAL_IN_SECONDS + 2));

	// This should clean up both transactions
	int transaction_id3 = con->getDongleHWRevisionAsync();

	if (trans.end() == trans.find(transaction_id3)) {
		FAIL("New request not saved")
	}

	if (trans.end() != trans.find(transaction_id)) {
		FAIL("Our old request hasn't expired")
	}
	if (trans.end() != trans.find(transaction_id2)) {
		FAIL("Our old request hasn't expired")
	}
}

TEST(Smart_NoDongle, CheckSubscribeNoExpiry)
{
	auto transaction_id = con->subscribeObject(DIAG_URL_CODE_THROTTLE_POSITION, 100);
	auto& trans = con->getTransactions();

	if (trans.end() == trans.find(transaction_id)) {
		FAIL("Our 1st request wasn't saved in the transaction mapping")
	}

	os::Sleep(2000);

	// This should NOT clean up the first transaction
	int transaction_id2 = con->getDongleHWRevisionAsync();

	if (trans.end() == trans.find(transaction_id)) {
		FAIL("Our subscribe transaction expired, even though it shouldn't have")
	}

	if (trans.end() == trans.find(transaction_id2)) {
		FAIL("Our 2nd request wasn't saved in the transaction mapping")
	}
}

TEST(DataAggregator, SimpleAggregator)
{
	dat::DataAggregator a;

	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");
	a.onDataFragment(dat::DataChunk(5, 0, 3, "aaa"));
	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");
	CHECK_TEXT(!a.isCompleted(3), "Should not be completed");
	a.onDataFragment(dat::DataChunk(5, 1, 3, "bbb"));
	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");
	a.onDataFragment(dat::DataChunk(5, 2, 3, "ccc"));

	CHECK_TEXT(a.isCompleted(5), "Should be completed");
	CHECK_TEXT(!a.isCompleted(3), "Should not be completed");

	std::string result = a.popAggregatedData(5);
	std::string bad_result = a.popAggregatedData(1);

	CHECK_TEXT(0 == strcmp("", bad_result.c_str()), "Wrong output");
	CHECK_TEXT(0 == strcmp("aaabbbccc", result.c_str()), "Wrong output");

	result = a.popAggregatedData(5);

	CHECK_TEXT(0 == strcmp("", result.c_str()), "Wrong output");
}

TEST(DataAggregator, SimpleAggregatorMixedOrder)
{
	dat::DataAggregator a;

	a.onDataFragment(dat::DataChunk(5, 0, 3, "aaa"));
	a.onDataFragment(dat::DataChunk(5, 2, 3, "ccc"));
	a.onDataFragment(dat::DataChunk(5, 1, 3, "bbb"));

	CHECK_TEXT(a.isCompleted(5), "Should be completed");
	CHECK_TEXT(!a.isCompleted(1), "Should not be completed");

	std::string result = a.popAggregatedData(5);
	std::string bad_result = a.popAggregatedData(1);

	CHECK_TEXT(0 == strcmp("", bad_result.c_str()), "Wrong output");
	CHECK_TEXT(0 == strcmp("aaabbbccc", result.c_str()), "Wrong output");

	result = a.popAggregatedData(5);

	CHECK_TEXT(0 == strcmp("", result.c_str()), "Wrong output");
}

TEST(DataAggregator, WrongIndex)
{
	dat::DataAggregator a;

	a.onDataFragment(dat::DataChunk(5, 0, 3, "aaa"));
	a.onDataFragment(dat::DataChunk(5, 2, 3, "ccc"));
	a.onDataFragment(dat::DataChunk(5, 5, 3, "bbb")); // wrong index
	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");

	std::string result = a.popAggregatedData(5);
	CHECK_TEXT(0 == strcmp("", result.c_str()), "Wrong output");

	a.onDataFragment(dat::DataChunk(5, 1, 4, "bbb")); // wrong count
	CHECK_TEXT(!a.isCompleted(5), "Should not be completed");

	result = a.popAggregatedData(5);
	CHECK_TEXT(0 == strcmp("", result.c_str()), "Wrong output");
}

TEST(DataAggregator, AggregatorPopping)
{
	dat::DataAggregator a;

	CHECK_TEXT(0 == a.getDataCount(), "Should be zero");

	a.onDataFragment(dat::DataChunk(5, 0, 3, "aaa"));

	CHECK_TEXT(1 == a.getDataCount(), "Should be 1");

	a.onDataFragment(dat::DataChunk(5, 2, 3, "ccc"));

	CHECK_TEXT(1 == a.getDataCount(), "Should be 1");

	a.onDataFragment(dat::DataChunk(5, 1, 3, "bbb"));

	CHECK_TEXT(a.isCompleted(5), "Should be completed");
	CHECK_TEXT(1 == a.getDataCount(), "Should be 1");

	std::string result = a.popAggregatedData(5);
	CHECK_TEXT(0 == a.getDataCount(), "Should be 0");

	CHECK_TEXT(0 == strcmp("aaabbbccc", result.c_str()), "Wrong output");
}

/*


pRequest->ID = 2;
pRequest->Pins = NS_PDU::J1962_6_14;
pRequest->Protocol = NS_PDU::ISOUDSonCAN;
pRequest->StartingTimeOut = 1;
pRequest->reserved = 0;
pCommunicationParams[0].ID = NS_PDU::CP_Baudrate;
pCommunicationParams[0].Value = 500000;
pCommunicationParams[1].ID = NS_PDU::CP_BitSamplePoint;
pCommunicationParams[1].Value = 75;
pCommunicationParams[2].ID = NS_PDU::CP_SyncJumpWidth;
pCommunicationParams[2].Value = 24;
pCommunicationParams[3].ID = NS_PDU::CP_CanPhysReqId;
pCommunicationParams[3].Value = 0x714;
pCommunicationParams[4].ID = NS_PDU::CP_CanRespUSDTId;
pCommunicationParams[4].Value = 0x77e;

pPDURequest[0].ContinueIfFail = 1;
pPDURequest[0].CP_P2Max = 50000;
pPDURequest[0].CP_RepeatReqCountApp = 2;
pPDURequest[0].index = 4;
pPDURequest[0].DataFieldLenght = 4;
pPDURequest[0].DataField[0] = 0x03;
pPDURequest[0].DataField[1] = 0x22;
pPDURequest[0].DataField[2] = 0xF4;
pPDURequest[0].DataField[3] = 0x0D;

pPDURequest[1].ContinueIfFail = 1;
pPDURequest[1].CP_P2Max = 50000;
pPDURequest[1].CP_RepeatReqCountApp = 5;
pPDURequest[1].index = 11;
pPDURequest[1].DataFieldLenght = 4;
pPDURequest[1].DataField[0] = 0x03;
pPDURequest[1].DataField[1] = 0x22;
pPDURequest[1].DataField[2] = 0xF4;
pPDURequest[1].DataField[3] = 0x0C;

pPDURequest[2].ContinueIfFail = 1;
pPDURequest[2].CP_P2Max = 50000;
pPDURequest[2].CP_RepeatReqCountApp = 5;
pPDURequest[2].index = 11;
pPDURequest[2].DataFieldLenght = 4;
pPDURequest[2].DataField[0] = 0x03;
pPDURequest[2].DataField[1] = 0x22;
pPDURequest[2].DataField[2] = 0x22;
pPDURequest[2].DataField[3] = 0x99;

pPDURequest[3].ContinueIfFail = 1;
pPDURequest[3].CP_P2Max = 50000;
pPDURequest[3].CP_RepeatReqCountApp = 5;
pPDURequest[3].index = 11;
pPDURequest[3].DataFieldLenght = 4;
pPDURequest[3].DataField[0] = 0x03;
pPDURequest[3].DataField[1] = 0x22;
pPDURequest[3].DataField[2] = 0x22;
pPDURequest[3].DataField[3] = 0x03;


*/

TEST(Smart, PDU)
{
	//pb_byte_t requestDataField[] = "abcdef";

	std::vector<TPDUCommunicationParameter> commParameters;
	// ID, Value
	commParameters.push_back(TPDUCommunicationParameter{ 0, 500000 });
	commParameters.push_back(TPDUCommunicationParameter{ 1, 75 });
	commParameters.push_back(TPDUCommunicationParameter{ 2, 24 });
	commParameters.push_back(TPDUCommunicationParameter{ 109, 0x714 });
	commParameters.push_back(TPDUCommunicationParameter{ 112, 0x77e });


	std::vector<TPDURequest> pduRequests;
	pduRequests.push_back(TPDURequest{ 4, 50000, 2, 1, 4, "\x03\x22\xF4\x0D" });
	pduRequests.push_back(TPDURequest{ 11, 50000, 5, 1, 4, "\x03\x22\xF4\x0C" });
	pduRequests.push_back(TPDURequest{ 11, 50000, 5, 1, 4, "\x03\x22\x22\x99" });
	pduRequests.push_back(TPDURequest{ 11, 50000, 5, 1, 4, "\x03\x22\x22\x03" });

	// [SMARTPHONE] Request something from the dongle
	int transaction_id = con->sendPDU(TPDUReq_EPDUJ1962Pin_J1962_6_14,
		dat::IConnection::PDUProtocol::ISO14229,
		1, // Starting timeout
		commParameters,
		pduRequests
		);

	CHECK_TEXT(transaction_id != dat::ITransaction::OPERATION_ERROR, "Connection::sendPDU failed");

	auto & transactions = con->getTransactions();
	dat::PDUTransaction * transaction = reinterpret_cast<dat::PDUTransaction*>(transactions.find(transaction_id)->second);
	CHECK_TEXT(transaction->isRequestSent(), "Y no sent?");

	bool gotPDUResponse = false;

	// Now a UDP request is sent to the dongle simulator
	// Now the dongle simulator sends a UDP response

	// Get 10 updates
	for (int i = 0; i < 2; ++i) {

		// [SMARTPHONE] Get the raw response from the net
		TDongleRsp rsp = TDongleRsp_init_zero;
		unsigned char raw_buf[BUFFER_SIZE];
		size_t buf_len = recvSessionData(smartphoneUdp, raw_buf, sizeof(raw_buf), &rsp);

		CHECK_TEXT(rsp.which_response != TDongleRsp_ErrorRsp_tag, "Got error response");

		// [SMARTPHONE] Deserialize the response - this is just for testing the validity of the response
		if (TDongleRsp_PDURsp_tag == rsp.which_response) {
			if (gotPDUResponse) {
				FAIL("Got PDU response twice");
			}

			CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
			CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
			//TODO: Uncomment, as this is the correct way to check result
			//CHECK_TEXT(rsp.response.PDURsp.Result == TPDURsp_EPDUResult_PDU_IS_OK, "PDU error");
			CHECK_TEXT(rsp.response.PDURsp.Result == 0, "PDU error");
			gotPDUResponse = true;

			// Feed connection
			con->fromNet(raw_buf, buf_len);

			CHECK_TEXT(!transaction->isCompleted(), "Completed?");
			CHECK_TEXT(transaction->isRequestSent(), "Y no sent?");
			CHECK_TEXT(transaction->isResponseReceived(), "Y no response?");

#ifdef JAVA_STUBS
			CHECK_TEXT(!dataListener.m_isCalled_onPDUResponse, "JAVA callback called? why?");
#endif

			pb_release(TDongleRsp_fields, &rsp);
			continue;
		}

		CHECK_TEXT(rsp.has_id, "Response doesnt have transaction id");
		CHECK_TEXT(rsp.id == transaction_id, "Transaction ID not equal");
		CHECK_TEXT(rsp.which_response == TDongleRsp_PDUNotify_tag, "response type wrong");

		pb_release(TDongleRsp_fields, &rsp);

		dataListener.reset();

		// [SMARTPHONE] Pass the data for further handling
		con->fromNet(raw_buf, buf_len);

		CHECK_TEXT(transaction->isCompleted(), "Not Completed?");
		CHECK_TEXT(transaction->isResponseReceived(), "Y no response?");
		CHECK_TEXT(transaction->isRequestSent(), "Y no sent?");

#ifdef JAVA_STUBS
		// [SMARTPHONE-JAVA] Check the data listeners to see they were actually called - just stubs
		CHECK_TEXT(dataListener.m_isCalled_onPDUResponse, "JAVA callback not called");
#endif

	}
}

