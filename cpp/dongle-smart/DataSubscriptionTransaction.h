#ifndef _DATA_SUBSCRIPTION_TRANSACTION_H_
#define _DATA_SUBSCRIPTION_TRANSACTION_H_

#include "ITransaction.h"

namespace dat {

	typedef enum {
		Subscription_ResponsePending = 0,
		Subscription_Success,
		Subscription_Error
	} SubscriptionResult;

	class DataSubscriptionTransaction : public ITransaction {
	public:
		DataSubscriptionTransaction(DongleSocket& dongleSocket, IDataListener& dataListener, TransactionID_t id);
		int subscribe(int url, int val);
		int unsubscribe();
		int onMessage(TDongleRsp & rsp) override;

	private:
		bool isMessageTypeExpected(unsigned int responseType)  const;

		bool m_isSubscribed;
		SubscriptionResult m_subscriptionStatus;
		IDataListener& m_dataListener;

		int m_url;
		int m_timeout;
	};

}

#endif /* _SIMPLE_TRANSACTION_H_ */