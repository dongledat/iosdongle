#ifndef _PDU_TRANACTION_H_
#define _PDU_TRANSACTION_H_

#include "ITransaction.h"
#include "DataAggregator.h"
#include "IConnection.h"

#include <vector>

namespace dat {
	
	class PDUTransaction : public ITransaction {
	public:
		PDUTransaction(DongleSocket& socket, IDataListener& dataListener, TransactionID_t id);
		int sendPDU(TPDUReq_EPDUJ1962Pin pins,
			IConnection::PDUProtocol protocol,
			int startingTimeOut,
			std::vector<TPDUCommunicationParameter> & commParameters,
			std::vector<TPDURequest> & pduRequests);

		int onMessage(TDongleRsp & rsp) override;

		bool isRequestSent() { return m_requestSent; };
		bool isResponseReceived() { return m_responseReceived; };

	private:
		bool isMessageTypeExpected(unsigned int responseType) const;

		bool m_requestSent; // When PDUReq sent
		bool m_responseReceived; // When PDURsp received
		//DataAggregator m_pduAggregator;
		IDataListener& m_dataListener;
	};

}

#endif /* _SIMPLE_TRANSACTION_H_ */
