#ifndef _SECURE_TUNNEL_H_
#define _SECURE_TUNNEL_H_

#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "Streams.h"

#include <string>

namespace dat {
	class SecureTunnel : public IIOStream {
	public:

		enum SecureTunnelSetupState {
			TUNNEL_STATE_NONE,
			TUNNEL_STATE_READY,
			TUNNEL_STATE_IN_PROGRESS,
			
			// These are considered "Completed"
			TUNNEL_STATE_INIT_ERROR,
			TUNNEL_STATE_FAILED,
			TUNNEL_STATE_ESTABLISHED
		};

		SecureTunnel();
		SecureTunnel(IIOStream* lowerLayer);
		void startPSKHandshake(std::string PSK, std::string identity);

		// The stream's send method
		// Uses mbedtls_ssl_write to write data securely through the tunnel
		size_t send(const unsigned char* data, size_t length) override;

		// The stream's recv method
		// Uses mbedtls_ssl_read to read data securely through the tunnel
		size_t recv(unsigned char* buffer, size_t buffer_size) override;

		// Event, callback when new data is ready for continuing the handshake phase
		void continueHandshake();

		// Management methods
		void init();
		void setlowerLayerStream(IIOStream* lowerLayer);
		void reset();
		

		// Read the raw data, save in buffer, pass on to handshake_step
		SecureTunnelSetupState getState();
		bool isEstablished();
		bool isCompleted();

		// Callbacks for mbedtls; Will use the SecureTunnel context to access the lower layer and send/recv the buffer
		static int sendRaw(void *ctx, const unsigned char *buf, size_t len);
		static int recvRaw(void *ctx, unsigned char *buf, size_t len);

	private:
		bool m_isTunnelSetup;
		IIOStream * m_stream;
		SecureTunnelSetupState m_state;

		mbedtls_ssl_context m_ssl;
		mbedtls_ssl_config m_conf;
		mbedtls_entropy_context m_entropy;
		mbedtls_ctr_drbg_context m_ctr_drbg;
	};
}

#endif /* _SECURE_TUNNEL_H_ */