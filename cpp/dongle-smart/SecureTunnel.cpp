#include "SecureTunnel.h"
using namespace dat;

#define TLS_BUFFER_SIZE (0x4096)

const char *pers = "ssl_client2";

dat::SecureTunnel::SecureTunnel() :
	m_ssl({ 0 }),
	m_state(TUNNEL_STATE_INIT_ERROR)
{
	init();
}

SecureTunnel::SecureTunnel(IIOStream * lowerLayer) :
	m_stream(lowerLayer),
	m_ssl({ 0 }),
	m_state(TUNNEL_STATE_INIT_ERROR)
{
	init();
}

void SecureTunnel::init()
{
	// Initialize mbedtls ssl context
	mbedtls_ssl_init(&m_ssl);
	mbedtls_ssl_config_init(&m_conf);
	mbedtls_ctr_drbg_init(&m_ctr_drbg);

	// Initialize the RNG and the session data
	mbedtls_entropy_init(&m_entropy);
	if (0 != mbedtls_ctr_drbg_seed(&m_ctr_drbg, mbedtls_entropy_func, &m_entropy,
		(const unsigned char *)pers,
		strlen(pers)))
	{
		return;
	}

	// Setup
	if (0 != mbedtls_ssl_config_defaults(	&m_conf,
											MBEDTLS_SSL_IS_CLIENT,
											MBEDTLS_SSL_TRANSPORT_STREAM,
											MBEDTLS_SSL_PRESET_DEFAULT))
	{
		return;
	}

#if defined(MBEDTLS_SSL_ENCRYPT_THEN_MAC)
	mbedtls_ssl_conf_encrypt_then_mac(&m_conf, MBEDTLS_SSL_ETM_ENABLED);
#endif

	if (0 != mbedtls_ssl_setup(&m_ssl, &m_conf))
	{
		return;
	}

	mbedtls_ssl_conf_rng(&m_conf, mbedtls_ctr_drbg_random, &m_ctr_drbg);
	mbedtls_ssl_set_bio(&m_ssl, this, SecureTunnel::sendRaw, SecureTunnel::recvRaw, NULL);

	m_state = TUNNEL_STATE_READY;
}

void SecureTunnel::setlowerLayerStream(IIOStream * lowerLayer)
{
	m_stream = lowerLayer;
}

void SecureTunnel::reset()
{
	mbedtls_ssl_close_notify(&m_ssl);
	mbedtls_ssl_session_reset(&m_ssl);
	m_isTunnelSetup = false;
	m_state = TUNNEL_STATE_NONE;
}

void SecureTunnel::startPSKHandshake(std::string PSK, std::string identity)
{
	reset();

	if (PSK.size() == 0 || identity.size() == 0)
	{
		m_state = TUNNEL_STATE_INIT_ERROR;
		return;
	}

	if (0 != mbedtls_ssl_conf_psk(&m_conf,
		(const unsigned char *)PSK.c_str(), PSK.size(),
		(const unsigned char *)identity.c_str(), identity.size()))
	{
		m_state = TUNNEL_STATE_INIT_ERROR;
		return;
	}

	m_state = TUNNEL_STATE_IN_PROGRESS;
	switch (mbedtls_ssl_handshake(&m_ssl))
	{
	case 0:
		// Finished - makes no sense
		m_state = TUNNEL_STATE_ESTABLISHED;
		break;

	case MBEDTLS_ERR_SSL_WANT_READ:
	case MBEDTLS_ERR_SSL_WANT_WRITE:
		break;

	default:
		m_state = TUNNEL_STATE_FAILED;
	}
}

void SecureTunnel::continueHandshake()
{
	if (isCompleted()) {
		// Handshake done, nothing to do
		return;
	}
		
	// Get data from network stream
	switch (mbedtls_ssl_handshake(&m_ssl)) {
	case MBEDTLS_ERR_SSL_WANT_READ:
		// Need to wait for another net update
		break;

	case MBEDTLS_ERR_SSL_WANT_WRITE:
		// ???????????????
		//__debugbreak();
		break;

	case 0:
		// Is this it?
		m_state = TUNNEL_STATE_ESTABLISHED;
		break;

	default:
		// The bad
		m_state = TUNNEL_STATE_FAILED;
	}

}

SecureTunnel::SecureTunnelSetupState SecureTunnel::getState()
{
	return m_state;
}

bool SecureTunnel::isEstablished()
{
	return TUNNEL_STATE_ESTABLISHED == m_state;
}

bool SecureTunnel::isCompleted()
{
	return	(TUNNEL_STATE_FAILED == m_state) ||
			(TUNNEL_STATE_ESTABLISHED == m_state);
}

#define max(a,b) (a>b?a:b)

size_t SecureTunnel::send(const unsigned char * data, size_t length)
{
	//TODO: verify we get less than MAX_CONTENT_LENGTH
	return mbedtls_ssl_write(&m_ssl, data, length);
}

size_t SecureTunnel::recv(unsigned char * buffer, size_t buffer_size)
{
	int ret = mbedtls_ssl_read(&m_ssl, buffer, buffer_size);
	switch (ret) {
	case 0:
		// EOF?
		m_state = TUNNEL_STATE_FAILED;
		return InputBufferedStream::STREAM_ERROR;

	case MBEDTLS_ERR_SSL_WANT_READ:
		// We don't have enough, so nothing read
		return 0;

	case MBEDTLS_ERR_SSL_WANT_WRITE:
		// Not sure
		return 0;
	}
	
	if (ret < 0) {
		// Error - ssl unusable
		m_state = TUNNEL_STATE_FAILED;
		return InputBufferedStream::STREAM_ERROR;
	}
	else {
		// Managed to read app-level data
		return ret;
	}
}

int SecureTunnel::sendRaw(void * ctx, const unsigned char * buf, size_t len)
{
	SecureTunnel* tunnel = reinterpret_cast<SecureTunnel*>(ctx);
	
	if (nullptr == tunnel->m_stream) {
		return MBEDTLS_ERR_SSL_INTERNAL_ERROR;
	}

	//TODO: error values?
	return tunnel->m_stream->send(buf, len);
}

int SecureTunnel::recvRaw(void * ctx, unsigned char * buf, size_t len)
{
	SecureTunnel* tunnel = reinterpret_cast<SecureTunnel*>(ctx);

	if (nullptr == tunnel->m_stream) {
		return MBEDTLS_ERR_SSL_INTERNAL_ERROR;
	}

	int read = tunnel->m_stream->recv(buf, len);
	switch (read) {
	case 0:
		return MBEDTLS_ERR_SSL_WANT_READ;
	case -1:
		return MBEDTLS_ERR_SSL_INTERNAL_ERROR;
	default:
		return read;
	}
}
