#include "DataAggregator.h"

using namespace dat;

DataChunk::DataChunk(unsigned int dataElementID, unsigned int chunkIndex, unsigned int totalCount, std::string && data) :
	m_dataElementID(dataElementID),
	m_chunkIndex(chunkIndex),
	m_totalCount(totalCount),
	m_data(data)
{
}

DataChunk::DataChunk(DataChunk && other) :
	m_dataElementID(other.m_dataElementID),
	m_chunkIndex(other.m_chunkIndex),
	m_totalCount(other.m_totalCount),
	m_data(std::move(other.m_data))
{
}

DataElement::DataElement(DataChunk && firstChunk)
{
	m_expectedChunkCount = firstChunk.getTotalCount();
	m_dataElementID = firstChunk.getDataElementID();
	m_dataChunks.emplace(firstChunk.getChunkIndex(), firstChunk.popData());
}

void DataElement::addChunk(DataChunk && chunk)
{
	if (this->isComplete()) {
		//todo: exception
		return;
	}

	// Verify the chunk's metadata matches ours
	if ((m_expectedChunkCount != chunk.getTotalCount()) ||
		(m_dataElementID != chunk.getDataElementID())) {
		//todo: exception
		return;
	}

	// Verify the chunk's index is inside the range of indexes expected
	if (m_expectedChunkCount <= chunk.getChunkIndex()) {
		//todo: exception
		return;
	}


	// Verify we don't have this chunk's index already
	if (m_dataChunks.end() != m_dataChunks.find(chunk.getChunkIndex())) {
		//todo: exception
		return;
	}

	// Map guaranetees order
	m_dataChunks.emplace(chunk.getChunkIndex(), chunk.popData());
}

bool DataElement::isComplete()
{
	return m_dataChunks.size() == m_expectedChunkCount;
}

unsigned int DataElement::getDataElementID()
{
	return m_dataElementID;
}

std::string DataElement::popCompletedData()
{
	if (!isComplete()) {
		return "";
	}

	std::string output;

	for (auto chunk = m_dataChunks.begin(); chunk != m_dataChunks.end(); )
	{
		output += std::move(chunk->second);
		m_dataChunks.erase(chunk++);
	}

	return output;
}

DataAggregator::DataAggregator()
{
}

void DataAggregator::onDataFragment(DataChunk && chunk)
{
	auto i = m_dataElements.find(chunk.getDataElementID());
	if (m_dataElements.end() == i) {
		m_dataElements.emplace(chunk.getDataElementID(), DataElement(std::forward<DataChunk>(chunk)));
	}
	else {
		i->second.addChunk(std::forward<DataChunk>(chunk));
	}
}

bool DataAggregator::isCompleted(unsigned int transactionID)
{
	auto i = m_dataElements.find(transactionID);

	if (m_dataElements.end() == i) {
		return false;
	}

	return i->second.isComplete();
}

std::string DataAggregator::popAggregatedData(unsigned int transactionID)
{
	if (!isCompleted(transactionID)) {
		return "";
	}

	auto i = m_dataElements.find(transactionID);
	auto pduData = i->second.popCompletedData();

	m_dataElements.erase(i->first);
	return pduData;
}

unsigned int DataAggregator::getDataCount()
{
	return m_dataElements.size();
}
