#include <string.h>

#include "dat.h"
#ifdef DAT_ENABLE_TLS
#include "dat_tls.h"
#endif
#include "dat_handle_request.h"
#include "dtp.h"
#include "log.h"
#include "malloc_prv.h"

/* TODO: Horrible, ugly, disgusting, foul globals! */
static dat_context_t g_dat = { 0 };
static dat_context_t* dat = NULL;

#ifdef DAT_ENABLE_TLS
static dat_tls_ctx* dat_tls = NULL;
int g_tls_enabled = 1;
#else
int g_tls_enabled = 0;
#endif

#ifdef DAT_ENABLE_DTP
dtp_t *dtp_req = NULL;
int g_dtp_enabled = 1;
#else
int g_dtp_enabled = 0;
#endif

/**
* @fn void _reset_connection_state();
* @brief Resets the state regarding the current connection
*/
void _reset_connection_state();

/**
* @fn void _terminate_connection();
* @brief Terminates the current connection, while notifying the client and resetting the state
*/
void _terminate_connection();

/**
* @fn void _notify_client_error(TErrorRsp_EReason reason);
* @brief Sends an error message to the client
* @param reason The encountered error
*/
void _notify_client_error(TErrorRsp_EReason reason);

/**
* @fn void _on_protocol_error()
* @brief Handles situation when an error in the DAT protocol was identified (protobuf).
*/
void _on_protocol_error();

/**
* @fn dat_result_e _send_response_without_dtp(const TDongleRsp* resp)
* @brief Sends a serialized response from the dongle to the connected client. No DTP wrapping.
* @param resp Response object to send
*/
dat_result_e _send_response_without_dtp(const TDongleRsp* resp);

/**
* @fn dat_result_e _send_response_with_dtp(const TDongleRsp* resp)
* @brief Sends a serialized, DTP-wrapped response from the dongle to the connected client.
* @param resp Response object to send
*/
dat_result_e _send_response_with_dtp(const TDongleRsp* resp);

/**
* @fn void _recv_session_data(unsigned char * buffer, unsigned int size)
* @brief Called when session-layer data (i.e. DTP/Protobuf - to be determined) is ready.
* @param buffer Pointer to session data buffer
* @param size Buffer length in bytes
*
* If using TLS, this function will be called after the TLS layer has finished, i.e. - the plaintext.
* Else, just straight from the wire.
*/
dat_result_e _recv_session_data(const unsigned char * buffer, unsigned int size);

/**
* @fn dat_result_e _recv_session_data(unsigned char * buffer, unsigned int size)
* @brief Sends session data over the underlying layer (i.e. TLS if enabled)
* @param buffer Pointer to session data buffer
* @param size Buffer length in bytes
*/
dat_result_e _send_session_data(const unsigned char * buffer, unsigned int size);

/* TODO: currently global, can't make this function arguments because the API with TEXA doesn't have contexts in functions dat_from_net dat_from_car */
#ifdef DAT_ENABLE_TLS
char g_tls_psk[] = { DAT_PSK_SECRET };
char g_tls_psk_identity[] = DAT_PSK_IDENTITY;
#endif

/************ Public functions **********************************/
dat_result_e dat_init(dat_context_t* context)
{
    /* Copy the whole context */
    memcpy(&g_dat, context, sizeof(g_dat));

    set_alloc_prv(context->p_malloc, context->p_free);

    /* Initialize txcm with our context */
    dat_txcm_init_context(&g_dat);
    dat = &g_dat;

#ifdef DAT_ENABLE_TLS
	if (g_tls_enabled) {
		dat_tls = malloc_prv(sizeof(dat_tls_ctx));
		if (NULL == dat_tls) {
			return DAT_ERROR;
		}

		if (DAT_TLS_ERR_OK != dat_tls_init(dat_tls, _recv_session_data, dat->dat_to_net_cb)) {
			return DAT_ERROR;
		}

		if (DAT_TLS_ERR_OK != dat_tls_set_psk(dat_tls, g_tls_psk, sizeof(g_tls_psk), g_tls_psk_identity, sizeof(g_tls_psk_identity) - 1)) {
			return DAT_ERROR;
		}
	}
#endif

    DLOG("Init completed");
    return DAT_OK;
}

dat_result_e dat_done()
{
#ifdef DAT_ENABLE_TLS
	if (dat_tls) {
		dat_tls_deinit(dat_tls);
		free_prv(dat_tls);
		dat_tls = NULL;
	}
#endif

#ifdef DAT_ENABLE_DTP
	if (dtp_req) {
		dtp_free(dtp_req);
		dtp_req = NULL;
	}
#endif

    return DAT_OK;
}

/* TODO: This has to be linked to a dat context, and also to dat tls */
dat_result_e dat_from_net(const unsigned char* buffer, size_t length)
{
	dat_result_e res = DAT_OK;

#ifdef DAT_ENABLE_TLS
	if (g_tls_enabled) {
		res = dat_tls_from_net(dat_tls, buffer, length);
		if (DAT_TLS_ERR_OK == res) {
			return DAT_OK;
		}

		if (DAT_TLS_ERR_HIGH_LEVEL_FAILURE == res) {
			_on_protocol_error();
			return DAT_ERROR;
		} else {
			/*  TLS error means we can't communicate with the other side.
			It's unlikely that we can recover from it, so just drop the connection */
			_terminate_connection();
			return DAT_ERROR;
		}

		return DAT_OK;
	}
#endif

	if (DAT_OK != _recv_session_data(buffer, length)) {
		_on_protocol_error();
		return DAT_ERROR;
	}

	return DAT_OK;
}

/* TODO: This has to be linked to a dat context, and also to dat tls */
dat_result_e dat_from_car(const TDongleRsp* resp)
{

#ifdef DAT_ENABLE_DTP
	if (g_dtp_enabled) {
		return _send_response_with_dtp(resp);
	}
#endif
	
	return _send_response_without_dtp(resp);
}

void dat_connect()
{
	/* We just got a BT connection */

	//TODO: add statemachine code
}

/* TODO: This has to be linked to a dat context, and also to dat tls */
void dat_disconnect()
{
	/* The BT connection just disconnected */
	_reset_connection_state();

	//TODO: add statemachine reset
}

/************ Internal functions **********************************/

/************ Connection management ************/
void _terminate_connection()
{
	/*TODO: Call TEXA's bluetooth disconnect */
	_reset_connection_state();
	/* Now we're ready for new TLS connections */
}

void _reset_connection_state()
{
#ifdef DAT_ENABLE_TLS
	/* This TLS session has ended */
	(void)dat_tls_reset(dat_tls);
#endif

#ifdef DAT_ENABLE_DTP
	/* Flush the DTP buffer */
	(void)dtp_free(dtp_req);
	dtp_req = NULL;
#endif

	/*TODO: Set state to indicate we don't have a bluetooth connection */
}

/************ Response sending ************/
dat_result_e _send_response_without_dtp(const TDongleRsp* resp)
{
	/* TODO: how do we know this is enough? we happens if it's not enough? */
	unsigned char buffer[DAT_MAX_PROTOBUF_MESSAGE_LEN];
	pb_ostream_t ostream;

	/* Serialize the response */
	memset(buffer, 0, sizeof(buffer));
	ostream = pb_ostream_from_buffer(buffer, DAT_MAX_PROTOBUF_MESSAGE_LEN);
	if (!pb_encode(&ostream, TDongleRsp_fields, resp)) {
		printf("pb_encode failed on async car data object send");
		return DAT_ERROR;
	}

	/* Send the ready serialized protobuf */
	return _send_session_data((const unsigned char*)&buffer, ostream.bytes_written);
}

#ifdef DAT_ENABLE_DTP
dat_result_e _send_response_with_dtp(const TDongleRsp* resp)
{
	dat_result_e result = DAT_ERROR;
	unsigned char *buffer = NULL;
	pb_ostream_t ostream;
	net_buf_t *net_buf = NULL;
	
	net_buf = net_buf_alloc(DAT_MAX_PROTOBUF_MESSAGE_LEN);
	if (!net_buf) {
		return DAT_ERROR;
	}

	/* Reserve space */
	buffer = net_buf_put(net_buf, DAT_MAX_PROTOBUF_MESSAGE_LEN);
	if (!buffer) {
		goto out;
	}
	memset(buffer, 0, sizeof(buffer));

	/* Serialize the response */
	ostream = pb_ostream_from_buffer(buffer, DAT_MAX_PROTOBUF_MESSAGE_LEN);
	if (!pb_encode(&ostream, TDongleRsp_fields, resp)) {
		printf("pb_encode failed on async car data object send");
		goto out;
	}

	/* Finalize the netbuf */
	net_buf_trim(net_buf, DAT_MAX_PROTOBUF_MESSAGE_LEN - ostream.bytes_written);
	if (DTP_ERROR == dtp_add_header(net_buf)) {
		printf("dtp_add_header failed");
		goto out;
	}

	//LOG("which_response=%d, length=%d", resp->which_response, net_buf->length);

	/* Send the ready DTP message with serialized protobuf */
	result = _send_session_data(net_buf->data, net_buf->length);

out:
	if (net_buf) {
		net_buf_free(net_buf);
	}

	return result;
}
#endif

void _notify_client_error(TErrorRsp_EReason reason)
{
	TDongleRsp rsp = TDongleRsp_init_zero;

	rsp.has_id = 0;
	rsp.which_response = TDongleRsp_ErrorRsp_tag;
	rsp.response.ErrorRsp.Reason = reason;

	dat_from_car(&rsp);
}

void _on_protocol_error()
{
#ifdef DAT_TERMINATE_ON_BAD_PROTOBUF
	_terminate_connection();
#else
	_notify_client_error(TErrorRsp_EReason_ProtobufDecodeFailure);
#endif
}

/************ Network transfer ************/

/* TODO: This has to be linked to a dat context, and also to dat tls */
dat_result_e _send_session_data(const unsigned char * buffer, unsigned int size)
{
#ifdef DAT_ENABLE_TLS
	/* TODO:  Need to check retval, like WANT_WRITE, WANT_READ, errors, etc.... */
	if (g_tls_enabled) {
		/* TODO: what if we want to write too much? exceed MAX_CONTENT_LEN? keep recalling while getting DAT_WANT_WRITE, or....???? */
		if (DAT_TLS_ERR_OK != dat_tls_send(dat_tls, buffer, size))
		{
			return DAT_ERROR;
		}

		return DAT_OK;
	} 
#endif

	if (size != dat->dat_to_net_cb(buffer, size))
	{
		return DAT_ERROR;
	}

	return DAT_OK;

}

/* TODO: This has to be linked to a dat context, and also to dat tls */
dat_result_e _recv_session_data(const unsigned char * buffer, unsigned int size)
{
	return dat_build_request(dat, buffer, size);
}
