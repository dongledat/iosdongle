#ifndef DTP_H
#define DTP_H

#include <stdbool.h>
#include "net_buf.h"
#include "dat.h"

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct
{
	net_buf_t *net_buf;
	bool done;

} dtp_t;

typedef void* dtp_handle_t;

typedef enum { DTP_ERROR = -1, DTP_OK = 0 } dtp_result_e;

dtp_t *dtp_alloc(unsigned short size);
void dtp_free(dtp_t *dtp);
unsigned char *dtp_data(dtp_t *dtp);
size_t dtp_len(dtp_t *dtp);
dtp_result_e dtp_from_net(dtp_t *dtp, const void *buf, unsigned size);
dtp_result_e dtp_add_header(net_buf_t* net_buf);

#if defined(__cplusplus)
}
#endif

#endif /* DTP_H */
