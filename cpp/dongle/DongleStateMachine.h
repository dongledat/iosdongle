#ifndef DONGLE_STATE_MACINE_H
#define DONGLE_STATE_MACINE_H

#include "StateMachine.h"
#include "dat_config.h"

class DongleStateMachine;

class DongleStateBase : public common::StateMachine
{
public:

    typedef enum
    {
        NONE_STATE = -1,
        STATE_0,
        MAX_STATE

    } States;

    typedef enum
    {
        NONE_EVENT = -1,
        EVENT_0,
        MAX_EVENT

    } Events;

protected:

    DongleStateBase(void* inst, common::StateMachineClass& smClass) :
        common::StateMachine(inst, smClass, STATE_0)
    {}

    DongleStateBase() {}
};

class DongleState0 : public virtual DongleStateBase
{
protected:

    DongleState0() {}
};


class DongleStateMachine : public DongleState0/*, public State1, public State2*/
{
public:

    DongleStateMachine() :
        DongleStateBase(this, m_stateMachineClass),
        DongleState0(),
        m_stateMachineClass("Dongle", MAX_STATE, MAX_EVENT)
    {
    //	INIT_TRANSITION(STATE_0, EVENT_0, STATE_0, SampleStateMachine, State0::onEvent0);
    //	INIT_TRANSITION(STATE_0, EVENT_1, STATE_1, SampleStateMachine, State0::onEvent1);
    }

private:

    common::StateMachineClass m_stateMachineClass;
};

#endif
