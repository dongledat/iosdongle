/* pb_helpers.c: Common support functions

*/

#ifndef PB_HELPERS
#define PB_HELPERS

#include "pb.h"
#include "dongle.pb.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAXIMIM_PROTO_BUFFER (1024)

void dat_pb_init();
TDongleReq dat_pb_create_request(int32_t requestType);
size_t dat_pb_encode(TDongleReq req, uint8_t** buffer);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif

