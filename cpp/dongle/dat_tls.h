/*
	DAT TLS library, wrapper around MBEDTLS SSL library, with the async paradigm of 
	receiving data from an external source that has access to the network, and calling back
	whenever actual app-layer data can be made out of it.
	
	=============================================

	The library's mode of operation:

	1. Initialize context
	2. Feed it with network data, until:
		a. Error occurs (handshake failure / data failure)
		   In case of error, caller will get a return value different than DAT_TLS_ERR_OK,
		   and he must reset the DAT TLS context before re-using it.
	3. TLS library will callback the user when app-layer data is available
	4. Finalize context

	=============================================

	Internally, the data fed to our library is transferred to mbedtls in the following fashion:

	1. We are called with network data buffer
		2. We save the pointer to the buffer, for mbedtls' use
		3. We keep calling mbedtls until the buffer is fully fed to it
			4. mbedtls calls our callback repeatedly
				- Which in turn reads from the network data buffer
				- And also keeps track of how much of the buffer has been read
	5. Our function finishes, returns to caller which can then free the network data buffer

	A possible failure here would have been if mbedtls does not require to read the entirety of the network data buffer.
	That way, we will have unused data which will be overwritten next time we are called with network data buffer.
	However, "unused data" is still something that mbedtls must be fed, so our workaround is to make
	sure we use the entirety of the data (even by calling mbedtls_ssl_read right after finishing handshake), 
	otherwise reporting failure.

	
	Another possible failure here would be if mbedtls fails to read the entirety of the network data buffer.
	For example, if the mbedtls buffers are full. Doesn't make much sense, so todo:
	TODO: find a case where that's possible
*/

#ifndef _DAT_TLS_H_
#define _DAT_TLS_H_

#include "dat.h"
#include "mbedtls/ssl.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"

#ifdef DAT_ENABLE_TLS

/**
* @enum dat_tls_error_t
* @brief Result codes returned by library APIs
*/
typedef enum {
	DAT_TLS_ERR_OK = 0,
	DAT_TLS_ERR_ERROR,
	DAT_TLS_ERR_INVALID_PARAM,
	DAT_TLS_ERR_READ_ERROR,
	DAT_TLS_ERR_SEND_ERROR,
	DAT_TLS_ERR_CLIENT_RECONNECT,
	DAT_TLS_ERR_MBEDTLS_INIT_ERROR,
	DAT_TLS_ERR_HANDSHAKE_FAILED,
	DAT_TLS_ERR_SSL_RESET_FAILURE,
	DAT_TLS_ERR_HANDSHAKE_NOT_COMPLETED,
	DAT_TLS_ERR_HIGH_LEVEL_FAILURE
} dat_tls_error_t;

/**
* @enum dat_tls_state_t
* @brief Possible states for the TLS session
*/
typedef enum {
	DAT_TLS_STATE_NONE,
	DAT_TLS_STATE_HANDSHAKE_IN_PROGRESS,
	DAT_TLS_STATE_HANDSHAKE_FAILED,
	DAT_TLS_STATE_SESSION_ESTABLISHED
} dat_tls_state_t;

/**
* @fn dat_result_e (*on_app_data_ready_cb_t)(unsigned char * buffer, unsigned int size);
* @brief Callback function that receives app-layer data
* @param buffer Buffer of app-layer data ready to be used
* @param size Size of buffer
* @return Status code
*/
typedef dat_result_e (*on_app_data_ready_cb_t)(const unsigned char * buffer, unsigned int size);

/**
* @fn unsigned int (*data_send_cb_t)(const unsigned char * buffer, unsigned int size)
* @brief Callback function for sending data to the wire
* @param buffer Buffer of TLS-layer data ready to be used
* @param size Size of buffer
* @return Count of bytes sent, or -1 if failed
*/
typedef int (*data_send_cb_t)(const unsigned char * buffer, unsigned int size);

/**
* @struct dat_tls_ctx
* @brief TLS session context, takes care of both the handshake and the established session itself.
*
* The struct defines a network buffer for storing incoming network data, so mbedtls may read from it. //TODO: maybe remove netbuff
*/
typedef struct {
	mbedtls_ssl_context ssl;
	mbedtls_ssl_config conf;
	mbedtls_entropy_context entropy;
	mbedtls_ctr_drbg_context ctr_drbg;

	on_app_data_ready_cb_t on_app_data_ready_cb;
	data_send_cb_t send_cb;

#ifdef DAT_TLS_NETBUF
	unsigned char net_buffer[DAT_TLS_BUFFER_SIZE];
	unsigned int net_buffer_size;
#else
	const unsigned char * netbuf;
	unsigned int netbuf_remaining_size;
#endif
	
	dat_tls_state_t state;
} dat_tls_ctx;

dat_tls_ctx* dat_tls_new();

/**
* @fn dat_tls_error_t dat_tls_init(dat_tls_ctx * ctx*, on_app_data_ready_cb_t on_data_cb*, data_send_cb_t send_cb*);
* @brief Initializes a TLS context struct.
* @param ctx Pointer to the specific TLS session to initialize
* @param on_app_data_ready_cb Callback to a function that will be called when app-layer data is ready to be read
* @param send_cb Callback to a function that will be called to send tls records data over the wire
*
* Call dat_tls_deinit on the context when it is no longer needed.
*/
dat_tls_error_t dat_tls_init(dat_tls_ctx * ctx, on_app_data_ready_cb_t on_app_data_ready_cb, data_send_cb_t send_cb);

/**
* @fn dat_tls_error_t dat_tls_deinit(dat_tls_ctx * ctx*)
* @brief Cleans up a TLS context struct.
* @param ctx Pointer to the specific TLS session to cleanup
*/
dat_tls_error_t dat_tls_deinit(dat_tls_ctx * ctx);

/**
* @fn dat_tls_error_t dat_tls_set_psk(dat_tls_ctx * ctx*, char* psk*, unsigned int psk_len*, char* psk_identity*, unsigned int psk_identity_len*)
* @brief Sets up the parameters for PSK algorithm.
* @param ctx Pointer to the specific TLS session
* @param psk PSK secret buffer
* @param psk_len Size of PSK secret buffer
* @param psk_identity PSK identity buffer
* @param psk_identity_len Size of PSK identity
*/
dat_tls_error_t dat_tls_set_psk(dat_tls_ctx * ctx, char* psk, unsigned int psk_len, char* psk_identity, unsigned int psk_identity_len);

/**
* @fn dat_tls_state_t dat_tls_get_state(dat_tls_ctx * ctx)
* @brief Returns the state of the TLS session.
* @param ctx Pointer to the specific TLS session
*/
dat_tls_state_t dat_tls_get_state(dat_tls_ctx * ctx);

/**
* @fn dat_tls_error_t dat_tls_reset(dat_tls_ctx * ctx*)
* @brief Resets the TLS context.
* @param ctx Pointer to the specific TLS session
*
* After this function is called, the same context will be in the state of waiting for a client TLS session for a handshake.
*/
dat_tls_error_t dat_tls_reset(dat_tls_ctx * ctx);

/**
* @fn dat_tls_error_t dat_tls_from_net(dat_tls_ctx * ctx*, unsigned char * buffer*, unsigned int size*)
* @brief Feeds the TLS buffer with raw bytes from network.
* @param ctx Pointer to the specific TLS session
* @param buffer Buffer of raw bytes from network, treated as TLS records data
* @param size Size of buffer
*
* This data will be used depending on the TLS state.
* - If handshake is ongoing, the data will be used for the handshake process
* - If the TLS session is already established, the data will be processed as app-layer session data,
*	and ctx->on_app_data_ready_cb may be called.
*/
dat_tls_error_t dat_tls_from_net(dat_tls_ctx * ctx, const unsigned char * buffer, unsigned int size);

/**
* @fn dat_tls_error_t dat_tls_send(dat_tls_ctx * ctx*, unsigned char * buffer*, unsigned int size*)
* @brief Sends app-layer, plaintext data over the TLS session.
* @param ctx Pointer to the specific TLS session
* @param buffer Buffer of app-layer data to be encrypted and sent over the TLS session.
* @param size Size of buffer
*/
dat_tls_error_t dat_tls_send(dat_tls_ctx * ctx, const unsigned char * buffer, unsigned int size);


#endif /* DAT_ENABLE_TLS */
#endif /* _DAT_TLS_H_ */
