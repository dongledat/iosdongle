#include "dat_tls.h"
#include <string.h>
#include <assert.h>
#include <malloc_prv.h>

#ifdef DAT_ENABLE_TLS

#define min(a,b) ((a)<(b)?a:b)

/* Just for safety */
#define MAX_ATTEMPT_COUNT_FOR_FEEDING_DATA_TO_MBEDTLS (100)

/* 
	Internal function. Calling mbedtls to continue with the handshake phase,
	and react accordingly (return value & state change).
*/
static dat_tls_error_t _perform_hs_step(dat_tls_ctx * ctx);

/*
	Internal function. Calling mbedtls to try and receive ready app-layer data,
	and react accordingly (return the actual data and error code if an error ocurred).
*/
static dat_tls_error_t _on_data(dat_tls_ctx * ctx);

static dat_tls_error_t _dat_tls_feed_data_to_mbedtls(dat_tls_ctx * ctx);

/*
	Internal function. 
	A wrapper that simply saves a pointer to the netbuf and its size.
	Note that it only saves the pointer, so it must be used before control flow is returned to the owner.
*/
static dat_tls_error_t _dat_tls_netbuf_store(dat_tls_ctx * ctx, const unsigned char * buffer, unsigned int size);

/*
	Internal function. Checks if mbedtls has read the entirety of the network buffer.
*/
static int _dat_tls_netbuf_is_exhausted(dat_tls_ctx * ctx);

/*
	Internal function, to be called by mbedtls (registered as callback). 
	A wrapper around our net buffer to read from the "network".
	Keeps track using the context of how much mbedtls has read from this buffer.
*/
static int _dat_tls_netbuf_read(void * _ctx, unsigned char *buf, size_t len);

/*
	Internal function, to be called by mbedtls (registered as callback).
	Just a wrapper around the user-supplied callback to send raw data over the wire.
*/
static int _tls_send(void * _ctx, const unsigned char *buf, size_t len);

/*	Default identity; TODO: move this to some config file */
static char* g_psk_identity = "PSKIdentity";
/*	TODO: what is this? */
const char *pers = "dat_tls_server";

/************ Public functions **********************************/
dat_tls_error_t dat_tls_init(dat_tls_ctx * ctx, on_app_data_ready_cb_t on_app_data_ready_cb, data_send_cb_t send_cb)
{
	if ((NULL == ctx) || (NULL == on_app_data_ready_cb) || (NULL == send_cb)) {
		return DAT_TLS_ERR_INVALID_PARAM;
	}

	/* Init ssl connection */
	mbedtls_ssl_init(&(ctx->ssl));
	mbedtls_ssl_config_init(&(ctx->conf));
	mbedtls_ctr_drbg_init(&(ctx->ctr_drbg));
	mbedtls_entropy_init(&(ctx->entropy));

	/* Initialize the RNG and the session data */
	if (0 != mbedtls_ctr_drbg_seed(	&(ctx->ctr_drbg), 
									mbedtls_entropy_func, 
									&(ctx->entropy),
									(const unsigned char *)pers,
									strlen(pers)))
	{
		return DAT_TLS_ERR_MBEDTLS_INIT_ERROR;
	}

	mbedtls_ssl_conf_rng(&(ctx->conf), mbedtls_ctr_drbg_random, &(ctx->ctr_drbg));

	/* Setup mbedtls ssl structures */
	if (0 != mbedtls_ssl_config_defaults(	&(ctx->conf),
											MBEDTLS_SSL_IS_SERVER,
											MBEDTLS_SSL_TRANSPORT_STREAM,
											MBEDTLS_SSL_PRESET_DEFAULT))
	{
		return DAT_TLS_ERR_MBEDTLS_INIT_ERROR;
	}

#if defined(MBEDTLS_SSL_ENCRYPT_THEN_MAC)
	mbedtls_ssl_conf_encrypt_then_mac(&(ctx->conf), MBEDTLS_SSL_ETM_ENABLED);
#endif

	/* Will cause mbedtls to use our _tls_send, _dat_tls_netbuf_read functions to send to the net / read from net buffer */
	mbedtls_ssl_set_bio(&(ctx->ssl), ctx, _tls_send, _dat_tls_netbuf_read, NULL);

	/* Finalize SSL setup */
	if (0 != mbedtls_ssl_setup(&(ctx->ssl), &(ctx->conf)))
	{
		return DAT_TLS_ERR_MBEDTLS_INIT_ERROR;
	}

	/* Setup data buffer */
	ctx->netbuf = NULL;
	ctx->netbuf_remaining_size = 0;

	/* API to users */
	ctx->on_app_data_ready_cb = on_app_data_ready_cb;
	ctx->send_cb = send_cb;
	ctx->state = DAT_TLS_STATE_NONE;

	return DAT_TLS_ERR_OK;
}

dat_tls_error_t dat_tls_set_psk(dat_tls_ctx * ctx, char* psk, unsigned int psk_len, char* psk_identity, unsigned int psk_identity_len)
{
	if ((NULL == ctx) || (NULL == psk) || (NULL == psk_identity) || (0 == psk_len) || (0 == psk_identity_len)) {
		return DAT_TLS_ERR_INVALID_PARAM;
	}

	if (0 != mbedtls_ssl_conf_psk(&(ctx->conf),
		(const unsigned char *)psk, psk_len,
		(const unsigned char *)psk_identity, psk_identity_len))
	{
		return DAT_TLS_ERR_MBEDTLS_INIT_ERROR;
	}

	return DAT_TLS_ERR_OK;
}

dat_tls_state_t dat_tls_get_state(dat_tls_ctx * ctx)
{
	return ctx->state;
}

dat_tls_error_t dat_tls_reset(dat_tls_ctx * ctx)
{
	if (NULL == ctx) {
		return DAT_TLS_ERR_INVALID_PARAM;
	}

	if (0 != mbedtls_ssl_session_reset(&(ctx->ssl))) {
		return DAT_TLS_ERR_SSL_RESET_FAILURE;
	}

	return DAT_TLS_ERR_OK;
}

dat_tls_error_t dat_tls_deinit(dat_tls_ctx * ctx)
{
	if (NULL == ctx) {
		return DAT_TLS_ERR_INVALID_PARAM;
	}

	mbedtls_ssl_free(&(ctx->ssl));
	mbedtls_ctr_drbg_free(&(ctx->ctr_drbg));
	mbedtls_ssl_config_free(&(ctx->conf));
	mbedtls_entropy_free(&(ctx->entropy));

	return DAT_TLS_ERR_OK;
}

dat_tls_error_t dat_tls_from_net(dat_tls_ctx * ctx, const unsigned char * buffer, unsigned int size)
{
	dat_tls_error_t err = DAT_TLS_ERR_ERROR;
	unsigned int last_buf_size = 0;
	unsigned int attempt_count = 0;

	if ((NULL == ctx) || (NULL == buffer)) {
		return DAT_TLS_ERR_INVALID_PARAM;
	}

	_dat_tls_netbuf_store(ctx, buffer, size);

	do {
		err = _dat_tls_feed_data_to_mbedtls(ctx);

		/*  We do this just for safety. Mbedtls is always supposed to read data from us,
			but just in case something went wrong and it won't read data, we don't want to
			get stuck in an infinite loop */
		if (ctx->netbuf_remaining_size == last_buf_size) {
			attempt_count++;
		}	else {
			attempt_count = 0;
			last_buf_size = ctx->netbuf_remaining_size;
		}

	} while (	(!_dat_tls_netbuf_is_exhausted(ctx)) && 
				(DAT_TLS_ERR_OK == err) && 
				(attempt_count < MAX_ATTEMPT_COUNT_FOR_FEEDING_DATA_TO_MBEDTLS));

	return err;
}

dat_tls_error_t _dat_tls_feed_data_to_mbedtls(dat_tls_ctx * ctx)
{
	dat_tls_error_t err = DAT_TLS_ERR_ERROR;

	if (MBEDTLS_SSL_HANDSHAKE_OVER != ctx->ssl.state) {
		/* Still in handshake */
		err = _perform_hs_step(ctx);
	} else {
		/* Read app-layer data */
		err = _on_data(ctx);
	}

	return err;
}

dat_tls_error_t dat_tls_send(dat_tls_ctx * ctx, const unsigned char * buffer, unsigned int size)
{
	if ((NULL == ctx) || (NULL == buffer)) {
		return DAT_TLS_ERR_INVALID_PARAM;
	}

	if (MBEDTLS_SSL_HANDSHAKE_OVER != ctx->ssl.state) {
		/* Handshake not yet completed, can't send app-layer data over non-established session */
		return DAT_TLS_ERR_HANDSHAKE_NOT_COMPLETED;
	}

	switch (mbedtls_ssl_write(&(ctx->ssl), buffer, size)) 
	{
	case MBEDTLS_ERR_SSL_WANT_READ:
	case MBEDTLS_ERR_SSL_WANT_WRITE:
		return DAT_TLS_ERR_OK;

	default:
		//TODO: log?
		return DAT_TLS_ERR_SEND_ERROR;
	}
}

/************ Internal functions **********************************/
dat_tls_error_t _perform_hs_step(dat_tls_ctx * ctx)
{
	if (NULL == ctx) {
		return DAT_TLS_ERR_INVALID_PARAM;
	}

	/* Update state according to handshake step progression */
	switch (mbedtls_ssl_handshake(&(ctx->ssl)))
	{
	case 0:
		/* Success, handshake finished */
		ctx->state = DAT_TLS_STATE_SESSION_ESTABLISHED;
		return DAT_TLS_ERR_OK;

	case MBEDTLS_ERR_SSL_WANT_READ:
	case MBEDTLS_ERR_SSL_WANT_WRITE:
		ctx->state = DAT_TLS_STATE_HANDSHAKE_IN_PROGRESS;
		return DAT_TLS_ERR_OK;

	default:
		ctx->state = DAT_TLS_STATE_HANDSHAKE_FAILED;
		return DAT_TLS_ERR_HANDSHAKE_FAILED;
	}
}

dat_tls_error_t _on_data(dat_tls_ctx * ctx)
{
	unsigned char buffer[DAT_TLS_BUFFER_SIZE];
	int mbedtls_ret = 0;
		
	if (NULL == ctx) {
		return DAT_TLS_ERR_INVALID_PARAM;
	}

	mbedtls_ret = mbedtls_ssl_read(&(ctx->ssl), buffer, DAT_TLS_BUFFER_SIZE);
	switch (mbedtls_ret)
	{
	case MBEDTLS_ERR_SSL_WANT_READ:
	case MBEDTLS_ERR_SSL_WANT_WRITE:
		/* Not enough data */
		return DAT_TLS_ERR_OK;

	case MBEDTLS_ERR_SSL_CLIENT_RECONNECT:
		/* Not supported */
		return DAT_TLS_ERR_CLIENT_RECONNECT;
	}

	if (mbedtls_ret < 0) {
		return DAT_TLS_ERR_READ_ERROR;
	} 

	/* Got session data, calling callback */
	if (DAT_OK != ctx->on_app_data_ready_cb(buffer, mbedtls_ret)) {
		return DAT_TLS_ERR_HIGH_LEVEL_FAILURE;
	} else {
		return DAT_TLS_ERR_OK;
	}
}

dat_tls_error_t _dat_tls_netbuf_store(dat_tls_ctx * ctx, const unsigned char * buffer, unsigned int size)
{
	ctx->netbuf = buffer;
	ctx->netbuf_remaining_size = size;
	return DAT_TLS_ERR_OK;
}

int _dat_tls_netbuf_is_exhausted(dat_tls_ctx * ctx)
{
	return (0 == ctx->netbuf_remaining_size) ? 1 : 0;
}

/*
* Will be called by mbedtls
*/
static int _dat_tls_netbuf_read(void * _ctx, unsigned char *buf, size_t len)
{
	dat_tls_ctx * ctx = (dat_tls_ctx*)_ctx;
	unsigned int to_read = min(len, ctx->netbuf_remaining_size);

	if ((NULL == _ctx) || (NULL == buf)) {
		return MBEDTLS_ERR_SSL_BAD_INPUT_DATA;
	}

	if (0 == len) {
		/* Not sure if this can happen, but anyway */
		return 0;
	}

	if (0 == ctx->netbuf_remaining_size) {
		/* Nothing left for mbedtls, our buffer is exhausted */
		return MBEDTLS_ERR_SSL_WANT_READ;
	}

	/* Copy to mbedtls */
	memcpy(buf, ctx->netbuf, to_read);

	/* Shift buffer */
	ctx->netbuf = ctx->netbuf + to_read;
	ctx->netbuf_remaining_size -= to_read;

	return to_read;
}

/*
* Will be called by mbedtls
*/
static int _tls_send(void * _ctx, const unsigned char *buf, size_t len)
{
	if ((NULL == _ctx) || (NULL == buf)) {
		return MBEDTLS_ERR_SSL_BAD_INPUT_DATA;
	}

	dat_tls_ctx * ctx = (dat_tls_ctx*)_ctx;
	/*TODO: handle errors? */
	return ctx->send_cb(buf, len);
}

#endif /* DAT_ENABLE_TLS */
