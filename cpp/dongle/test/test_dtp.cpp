#include "dtp.h"

TEST(Dongle, dtp)
{
    /* Big enough buffer */
    dtp_t *dtp = dtp_alloc(40);
    CHECK(dtp != NULL);

    /* Prepare network buffer */
    net_buf_t* nb = net_buf_alloc(100);
    CHECK(nb != NULL);

    /* Set initial data to net buffer */
    const int packet_len = 10;
    unsigned char s[] = "0123456789abcdefgh";
    int res = net_buf_add_tail(nb, s, packet_len);
    CHECK(res == 0);

    /* Build transport packet */
    res = dtp_add_header(nb);
    CHECK(res == DTP_OK);
    int data_len = nb->length;
    CHECK(data_len == packet_len + 4);

    /* Put only a part transport packet */
    res = dtp_from_net(dtp, nb->data, 6);
    CHECK(res == DTP_OK);
    CHECK(!dtp->done);

    /* Put the remaining part of transport packet */
    res = dtp_from_net(dtp, nb->data + 6, 8);
    CHECK(res == DTP_OK);
    CHECK(dtp->done);

	net_buf_free(nb);
	dtp_free(dtp);
}
