#ifndef TOKEN_MANAGER_H
#define TOKEN_MANAGER_H

#define SERIAL_NUMBER_LENGTH    16
#define VIN_LENGTH              17
#define MAC_ADDRESSLENGTH       6
#define RANDOM_NUMBER_LENGTH    8

#ifndef __cplusplus
extern "C" {
#endif

int token_generate(IN const char* serial_number, IN int security_level, IN int profile_id, IN const char* vin, IN const char* bt_mac, IN const char* random, OUT char* token_buffer, IN OUT int* token_buffer_size);
int token_validate(const char token, const char* bt_mac);

#ifndef __cplusplus
}
#endif

#endif