#include "dat_txcm_pdu.h"
#include "stdint.h"
#include <txcm/txcm_cmnd_def.hpp>

extern "C" TXCM_PDU_REFERENCE dat_txcm_pdu_getReferences(uint16_t nCommParams,  uint16_t nPDU){
	TXCM_PDU_REFERENCE out;
	TXCM_CMN_helper cmnHelper;

	TXCM_PDU_PAYLOAD_REQUEST_helper pduHelper(&cmnHelper);
	out.req = (TXCM_PDU_PAYLOAD_REQUEST*)pduHelper.Build(nCommParams,nPDU);
	out.pComPar = pduHelper.GetCommunicationParamsArray(out.req);
	out.pPDURequests = pduHelper.GetPDURequestArray(out.req);
	return out;
}

extern "C" void dat_txcm_pdu_removeRequest(TXCM_PDU_PAYLOAD_REQUEST* req){
	TXCM_CMN_helper cmnHelper;
	
	cmnHelper._delete((uint8_t *)req);
}