#include "dat_ecc.h"
#include "dat_ecc_stm32.h"
#include <assert.h>

/******************************************************************************/
/********** Parameters for Elliptic Curve                              ********/
/******************************************************************************/
typedef struct {
    const unsigned char P_a[DAT_ECC_KEY_LENGTH];
    const unsigned char P_b[DAT_ECC_KEY_LENGTH];
    const unsigned char P_p[DAT_ECC_KEY_LENGTH];
    const unsigned char P_n[DAT_ECC_KEY_LENGTH];
    const unsigned char P_Gx[DAT_ECC_KEY_LENGTH];
    const unsigned char P_Gy[DAT_ECC_KEY_LENGTH];
} ec_params;

static const ec_params g_ec_params = {
#if defined(ECC_KEY_LENGTH_224)
        {
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFE
        },
        {
            0xB4, 0x05, 0x0A, 0x85, 0x0C, 0x04, 0xB3, 0xAB, 0xF5, 0x41, 0x32, 0x56,
            0x50, 0x44, 0xB0, 0xB7, 0xD7, 0xBF, 0xD8, 0xBA, 0x27, 0x0B, 0x39, 0x43,
            0x23, 0x55, 0xFF, 0xB4,
        },
        {
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x01,
        },
        {
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
            0xFF, 0xFF, 0x16, 0xA2, 0xE0, 0xB8, 0xF0, 0x3E, 0x13, 0xDD, 0x29, 0x45,
            0x5C, 0x5C, 0x2A, 0x3D
        },
        {
            0xB7, 0x0E, 0x0C, 0xBD, 0x6B, 0xB4, 0xBF, 0x7F, 0x32, 0x13, 0x90, 0xB9,
            0x4A, 0x03, 0xC1, 0xD3, 0x56, 0xC2, 0x11, 0x22, 0x34, 0x32, 0x80, 0xD6,
            0x11, 0x5C, 0x1D, 0x21
        },
        {
            0xbd, 0x37, 0x63, 0x88, 0xb5, 0xf7, 0x23, 0xfb, 0x4c, 0x22, 0xdf, 0xe6,
            0xcd, 0x43, 0x75, 0xa0, 0x5a, 0x07, 0x47, 0x64, 0x44, 0xd5, 0x81, 0x99,
            0x85, 0x00, 0x7e, 0x34
        }
#elif defined(ECC_KEY_LENGTH_256)
        /* A          */
        {
            0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC
        },
        /* B          */
        {
            0x5A, 0xC6, 0x35, 0xD8, 0xAA, 0x3A, 0x93, 0xE7, 0xB3, 0xEB, 0xBD, 0x55, 
            0x76, 0x98, 0x86, 0xBC, 0x65, 0x1D, 0x06, 0xB0, 0xCC, 0x53, 0xB0, 0xF6, 
            0x3B, 0xCE, 0x3C, 0x3E, 0x27, 0xD2, 0x60, 0x4B
        },
        /* Prime */
        {
            0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 
            0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
        },
        /* Order */
        {
            0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 
            0xFF, 0xFF, 0xFF, 0xFF, 0xBC, 0xE6, 0xFA, 0xAD, 0xA7, 0x17, 0x9E, 0x84, 
            0xF3, 0xB9, 0xCA, 0xC2, 0xFC, 0x63, 0x25, 0x51
        },
        /* Gx         */
        {
            0x6B, 0x17, 0xD1, 0xF2, 0xE1, 0x2C, 0x42, 0x47, 0xF8, 0xBC, 0xE6, 0xE5, 
            0x63, 0xA4, 0x40, 0xF2, 0x77, 0x03, 0x7D, 0x81, 0x2D, 0xEB, 0x33, 0xA0,
            0xF4, 0xA1, 0x39, 0x45, 0xD8, 0x98, 0xC2, 0x96
        },
        /* Gy         */
        {
            0x4F, 0xE3, 0x42, 0xE2, 0xFE, 0x1A, 0x7F, 0x9B, 0x8E, 0xE7, 0xEB, 0x4A, 
            0x7C, 0x0F, 0x9E, 0x16, 0x2B, 0xCE, 0x33, 0x57, 0x6B, 0x31, 0x5E, 0xCE, 
            0xCB, 0xB6, 0x40, 0x68, 0x37, 0xBF, 0x51, 0xF5
        }
#endif        
};

//#define EC_PARAM_INDEX_EC_P224 (0)
#define EC_PARAM_INDEX_EC_P256 (0)

/**** Private function prototypes ************************************/

static int _is_null_buffer(unsigned char* buffer, unsigned int size);
static DAT_CRYPTO_RESULT _init_curve(EC_stt* curve, membuf_stt* buffer);

/**** Globals *******************************************************/

/**** Exported functions *********************************************/
//TODO: Add functions for init/deinit of this module (for cleaning of curve, etc)

DAT_CRYPTO_RESULT dat_ecc_init(dat_ecc_ctx_t* ecc)
{
	if (DAT_CRYPTO_SUCCESS != dat_crypto_stm32_rng_init(&ecc->rng)) {
		return DAT_CRYPTO_FAILURE;
	}

    if (NULL == ecc) {
        return DAT_CRYPTO_FAILURE;
    }

    /* Init crypto buffer (and optionally: memory buffer) */
    ecc->crypto_buffer.mSize = DAT_ECC_STM32_MEMBUF_BUFFER_SIZE;
    ecc->crypto_buffer.mUsed = 0;
    ecc->crypto_buffer.pmBuf = (uint8_t*)&ecc->mem_buffer;
    
    if (DAT_CRYPTO_SUCCESS != _init_curve(&ecc->curve_state, &ecc->crypto_buffer)) {
        dat_ecc_deinit(ecc);
        return DAT_CRYPTO_FAILURE;
    }

    ecc->curve_init = 1;

    return DAT_CRYPTO_SUCCESS;
}

DAT_CRYPTO_RESULT dat_ecc_deinit(dat_ecc_ctx_t* ecc)
{
    if (NULL == ecc) {
        return DAT_CRYPTO_FAILURE;
    }

	dat_crypto_stm32_rng_deinit(&ecc->rng);

    if (ecc->curve_init) {
        if (ECC_SUCCESS != ECCfreeEC(&(ecc->curve_state), &(ecc->crypto_buffer))) {
            return DAT_CRYPTO_FAILURE;
        }
    }

    return DAT_CRYPTO_SUCCESS;
}

DAT_CRYPTO_RESULT dat_ecc_generate_keys(dat_ecc_ctx_t* ecc, unsigned char* private_key, unsigned char* public_key_x, unsigned char* public_key_y)
{
    DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
    unsigned int ecc_status = ECC_ERR_BAD_OPERATION;
    int o_sign_size = 0;

    /*	Structure that will contain the public key, note that the public key
        is just a point on the curve, hence the name ECpoint_stt */
    ECpoint_stt* ecc_public_key = NULL;

    /*	Private Key Structure */
    ECCprivKey_stt* ecc_private_key = NULL;

    if ((NULL == ecc) || (NULL == private_key) || (NULL == public_key_x) || (NULL == public_key_y)) {
        return DAT_CRYPTO_FAILURE;
    }

    /*	Generate key values */
    ecc_status = ECCinitPoint(&ecc_public_key, &ecc->curve_state, &ecc->crypto_buffer);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }

    ecc_status = ECCinitPrivKey(&ecc_private_key, &ecc->curve_state, &ecc->crypto_buffer);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }

    ecc_status = ECCkeyGen(ecc_private_key, ecc_public_key, &ecc->rng, &ecc->curve_state, &ecc->crypto_buffer);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }

    /*	Extract key values */
    ecc_status = ECCgetPrivKeyValue(ecc_private_key, private_key, &o_sign_size);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }

    ecc_status = ECCgetPointCoordinate(ecc_public_key, E_ECC_POINT_COORDINATE_X, public_key_x, &o_sign_size);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }

    ecc_status = ECCgetPointCoordinate(ecc_public_key, E_ECC_POINT_COORDINATE_Y, public_key_y, &o_sign_size);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }

    status = DAT_CRYPTO_SUCCESS;

lblCleanup:

    /* Free keys - best effort */
    if (ecc_private_key) {
        (void)ECCfreePrivKey(&ecc_private_key, &ecc->crypto_buffer);
    }

    if (ecc_public_key) {
        (void)ECCfreePoint(&ecc_public_key, &ecc->crypto_buffer);
    }

    return status;
}

DAT_CRYPTO_RESULT dat_ecc_calculate_secret_point(dat_ecc_ctx_t* ecc, unsigned char* private_key, unsigned char* public_key_x, unsigned char* public_key_y, unsigned char* output)
{
    DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
    unsigned int ecc_status = ECC_ERR_BAD_OPERATION;
        
    ECCprivKey_stt* ecc_private_key = NULL;
    ECpoint_stt* ecc_public_key = NULL;
    ECpoint_stt* secret_key = NULL;
    int o_size = 0;  

    /* Verify parameters */
    if ((NULL == ecc) || (NULL == private_key) || (NULL == public_key_x) || (NULL == public_key_y) || (NULL == output)) {
        return DAT_CRYPTO_FAILURE;
    }

    if ((1 == _is_null_buffer(private_key, DAT_ECC_KEY_LENGTH)) ||
        (1 == _is_null_buffer(public_key_x, DAT_ECC_KEY_LENGTH)) ||
        (1 == _is_null_buffer(public_key_y, DAT_ECC_KEY_LENGTH))) {
        return DAT_CRYPTO_FAILURE;
    }

    /* Init Private Key */
    ecc_status = ECCinitPrivKey(&ecc_private_key, &ecc->curve_state, &ecc->crypto_buffer);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }

    ecc_status = ECCsetPrivKeyValue(ecc_private_key, private_key, DAT_ECC_KEY_LENGTH);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }
    
    /* Init Public Key */
    ecc_status = ECCinitPoint(&ecc_public_key, &ecc->curve_state, &ecc->crypto_buffer);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }
    
    ecc_status = ECCsetPointCoordinate(ecc_public_key, E_ECC_POINT_COORDINATE_X, public_key_x, DAT_ECC_KEY_LENGTH);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }
    
    ecc_status = ECCsetPointCoordinate(ecc_public_key, E_ECC_POINT_COORDINATE_Y, public_key_y, DAT_ECC_KEY_LENGTH);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }
    
    ecc_status = ECCvalidatePubKey(ecc_public_key, &ecc->curve_state, &ecc->crypto_buffer);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }
    
    /* Init Secret Key */
    ecc_status = ECCinitPoint(&secret_key, &ecc->curve_state, &ecc->crypto_buffer);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }
    
    /* Generate secret by multiplying their public with our private */
    ecc_status = ECCscalarMul(ecc_public_key, ecc_private_key, secret_key, &ecc->curve_state, &ecc->crypto_buffer);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }
    
    /* Get secret coordinates */ 
    ecc_status = ECCgetPointCoordinate(secret_key, E_ECC_POINT_COORDINATE_X, output, &o_size);
    if (ECC_SUCCESS != ecc_status) {
        goto lblCleanup;
    }
    
    // TODO ADD dat_ecc_hkdf_sign

    status = DAT_CRYPTO_SUCCESS;

lblCleanup:

    /* Free keys - best effort */
    if (ecc_private_key) {
        ECCfreePrivKey(&ecc_private_key, &ecc->crypto_buffer);
    }

    if (ecc_public_key) {
        ECCfreePoint(&ecc_public_key, &ecc->crypto_buffer);
    }

    if (secret_key) {
        ECCfreePoint(&secret_key, &ecc->crypto_buffer);
    }

    return status;
}

DAT_CRYPTO_RESULT _init_curve(EC_stt* curve, membuf_stt* buffer)
{
    curve->pmA = (unsigned char*)&g_ec_params.P_a;
    curve->pmB = (unsigned char*)&g_ec_params.P_b;
    curve->pmP = (unsigned char*)&g_ec_params.P_p;
    curve->pmN = (unsigned char*)&g_ec_params.P_n;
    curve->pmGx = (unsigned char*)&g_ec_params.P_Gx;
    curve->pmGy = (unsigned char*)&g_ec_params.P_Gy;

    curve->mAsize = sizeof(g_ec_params.P_a);
    curve->mBsize = sizeof(g_ec_params.P_b);
    curve->mNsize = sizeof(g_ec_params.P_n);
    curve->mPsize = sizeof(g_ec_params.P_p);
    curve->mGxsize = sizeof(g_ec_params.P_Gx);
    curve->mGysize = sizeof(g_ec_params.P_Gy);

    /* Init EC Paramaters */
    if (ECC_SUCCESS != ECCinitEC(curve, buffer)) {
        return DAT_CRYPTO_FAILURE;
    }

    return DAT_CRYPTO_SUCCESS;
}

int _is_null_buffer(unsigned char* buffer, unsigned int size)
{
    for (unsigned int i = 0; i < size; ++i) {
        if ('\0' != buffer[i]) {
            return 0;
        }
    }

    return 1;
}

