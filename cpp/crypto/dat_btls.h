#ifndef _DAT_BTLS_H_
#define _DAT_BTLS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "stdlib.h"
#include "dat_aes.h"
#include "dat_ecc.h"

struct ProtocolVersion {
	uint8_t major, minor;
};

enum BtlsState {
	initialized = 1,
	handshake_done = 2,
	application = 3,
	error = 4
};

enum ContentType {
	change_cipher_spec = 20, 
	alert =21, 
	handshake = 22,
	application_data = 23
};

enum HandshakeType {
	hello_request = 0, 
	client_hello = 1, 
	server_hello = 2,
	certificate = 11, 
	server_key_exchange = 12,
	/*certificate_request = 13,*/ 
	server_hello_done = 14,
	/*certificate_verify = 15,*/ 
	client_key_exchange = 16,
	finished = 20
};

enum EncryptionType {
	ecc_regular = 0,
	ecc_usepin = 1
};


struct block_ciphered {
	uint8_t* content;
	uint8_t* sha256_mac;
	uint8_t* padding;
	uint32_t padding_length;
} GenericBlockCipher;


struct SSLCiphertext {
	enum ContentType type;
	struct ProtocolVersion version;
	uint16_t length;
	struct uint8_t* fragment;
};

struct IVState {
	char encryption_iv[16];
	char decryption_iv[16];

};

typedef struct
{
	unsigned char private_key[DAT_ECC_KEY_LENGTH];
	unsigned char public_key_x[DAT_ECC_KEY_LENGTH];
	unsigned char public_key_y[DAT_ECC_KEY_LENGTH];
} ecc_keys;

typedef struct
{
	unsigned char public_key_x[DAT_ECC_KEY_LENGTH];
	unsigned char public_key_y[DAT_ECC_KEY_LENGTH];
} ecc_keys_public;

typedef struct 
{
	struct IVState iv_state;
	enum BtlsState state;
	unsigned char buffer[4096];
	int buffer_size;
	char secret[DAT_AES_KEY_SIZE];
	struct ProtocolVersion version;
	mbedtls_entropy_context entropy;
	mbedtls_ctr_drbg_context ctr_drbg;
	ecc_keys keys;
	ecc_keys_public remote_keys;
	unsigned char shared_salt[12];
	enum EncryptionType encryption_type;
	int is_verified;
} btls_ctx;



struct {
	uint32_t gmt_unix_time;
	unsigned char random_bytes[28];
} btls_handshake_hello;

DAT_CRYPTO_RESULT init_btls(btls_ctx * ctx);
void release_btls(btls_ctx * ctx);

DAT_CRYPTO_RESULT load_btls_keys(btls_ctx* ctx, ecc_keys keys);
DAT_CRYPTO_RESULT generate_btls_keys(btls_ctx* ctx);
DAT_CRYPTO_RESULT decode_btls_packet(btls_ctx* ctx, unsigned char* fragment, int fragment_size);
DAT_CRYPTO_RESULT build_btls_packet_application(btls_ctx* ctx, unsigned char* content, int content_size);
DAT_CRYPTO_RESULT build_btls_packet_handshake_hello(btls_ctx* ctx, int is_client);
DAT_CRYPTO_RESULT build_btls_packet_handshake_keyexchange(btls_ctx* ctx, int is_client);
/*
int encrypt_cipher_text(byte* secretkey, byte* input_buffer, unsigned int input_size, byte* output_buffer, unsigned int* output_size);

int decrypt_cipher_text(byte* secretkey, byte* input_buffer, unsigned int input_size, byte* output_buffer, unsigned int* output_size);
*/
#ifdef __cplusplus
}
#endif

#endif