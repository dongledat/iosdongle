#if 0
#ifdef DAT_CRYPTO_MBEDTLS

/*
 * Secure Remote Password 6a implementation based on mbedtls.
 *
 * Copyright (c) 2015 Dieter Wimberger
 * https://github.com/dwimberger/mbedtls-csrp
 * 
 * Derived from:
 * Copyright (c) 2010 Tom Cocagne. All rights reserved.
 * https://github.com/cocagne/csrp
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Tom Cocagne, Dieter Wimberger
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

#include <time.h>

#include <stdlib.h>
#include <string.h>


#include "dat_srp_mbedtls_common.h"

int g_initialized = 0;
mbedtls_entropy_context entropy_ctx;
mbedtls_ctr_drbg_context ctr_drbg_ctx;
mbedtls_mpi * RR;

/* All constants here were pulled from Appendix A of RFC 5054 */
struct NGHex global_Ng_constants[] = {
 { /* 512 */
  "D66AAFE8E245F9AC245A199F62CE61AB8FA90A4D80C71CD2ADFD0B9DA163B29F2A34AFBDB3B"
  "1B5D0102559CE63D8B6E86B0AA59C14E79D4AA62D1748E4249DF3",
  "2"
 },
 { /* 768 */
   "B344C7C4F8C495031BB4E04FF8F84EE95008163940B9558276744D91F7CC9F402653BE7147F"
   "00F576B93754BCDDF71B636F2099E6FFF90E79575F3D0DE694AFF737D9BE9713CEF8D837ADA"
   "6380B1093E94B6A529A8C6C2BE33E0867C60C3262B",
   "2"
 },
 { /* 1024 */
   "EEAF0AB9ADB38DD69C33F80AFA8FC5E86072618775FF3C0B9EA2314C9C256576D674DF7496"
   "EA81D3383B4813D692C6E0E0D5D8E250B98BE48E495C1D6089DAD15DC7D7B46154D6B6CE8E"
   "F4AD69B15D4982559B297BCF1885C529F566660E57EC68EDBC3C05726CC02FD4CBF4976EAA"
   "9AFD5138FE8376435B9FC61D2FC0EB06E3",
   "2"
 },
 { /* 2048 */
   "AC6BDB41324A9A9BF166DE5E1389582FAF72B6651987EE07FC3192943DB56050A37329CBB4"
   "A099ED8193E0757767A13DD52312AB4B03310DCD7F48A9DA04FD50E8083969EDB767B0CF60"
   "95179A163AB3661A05FBD5FAAAE82918A9962F0B93B855F97993EC975EEAA80D740ADBF4FF"
   "747359D041D5C33EA71D281E446B14773BCA97B43A23FB801676BD207A436C6481F1D2B907"
   "8717461A5B9D32E688F87748544523B524B0D57D5EA77A2775D2ECFA032CFBDBF52FB37861"
   "60279004E57AE6AF874E7303CE53299CCC041C7BC308D82A5698F3A8D0C38271AE35F8E9DB"
   "FBB694B5C803D89F7AE435DE236D525F54759B65E372FCD68EF20FA7111F9E4AFF73",
   "2"
 },
 { /* 4096 */
   "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
   "8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
   "302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
   "A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
   "49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
   "FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
   "670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
   "180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
   "3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
   "04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
   "B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
   "1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
   "BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
   "E0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B26"
   "99C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB"
   "04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2"
   "233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127"
   "D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934063199"
   "FFFFFFFFFFFFFFFF",
   "5"
 },
 { /* 8192 */
   "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E08"
   "8A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B"
   "302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9"
   "A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE6"
   "49286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8"
   "FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D"
   "670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C"
   "180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF695581718"
   "3995497CEA956AE515D2261898FA051015728E5A8AAAC42DAD33170D"
   "04507A33A85521ABDF1CBA64ECFB850458DBEF0A8AEA71575D060C7D"
   "B3970F85A6E1E4C7ABF5AE8CDB0933D71E8C94E04A25619DCEE3D226"
   "1AD2EE6BF12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
   "BBE117577A615D6C770988C0BAD946E208E24FA074E5AB3143DB5BFC"
   "E0FD108E4B82D120A92108011A723C12A787E6D788719A10BDBA5B26"
   "99C327186AF4E23C1A946834B6150BDA2583E9CA2AD44CE8DBBBC2DB"
   "04DE8EF92E8EFC141FBECAA6287C59474E6BC05D99B2964FA090C3A2"
   "233BA186515BE7ED1F612970CEE2D7AFB81BDD762170481CD0069127"
   "D5B05AA993B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934028492"
   "36C3FAB4D27C7026C1D4DCB2602646DEC9751E763DBA37BDF8FF9406"
   "AD9E530EE5DB382F413001AEB06A53ED9027D831179727B0865A8918"
   "DA3EDBEBCF9B14ED44CE6CBACED4BB1BDB7F1447E6CC254B33205151"
   "2BD7AF426FB8F401378CD2BF5983CA01C64B92ECF032EA15D1721D03"
   "F482D7CE6E74FEF6D55E702F46980C82B5A84031900B1C9E59E7C97F"
   "BEC7E8F323A97A7E36CC88BE0F1D45B7FF585AC54BD407B22B4154AA"
   "CC8F6D7EBF48E1D814CC5ED20F8037E0A79715EEF29BE32806A1D58B"
   "B7C5DA76F550AA3D8A1FBFF0EB19CCB1A313D55CDA56C9EC2EF29632"
   "387FE8D76E3C0468043E8F663F4860EE12BF2D5B0B7474D6E694F91E"
   "6DBE115974A3926F12FEE5E438777CB6A932DF8CD8BEC4D073B931BA"
   "3BC832B68D9DD300741FA7BF8AFC47ED2576F6936BA424663AAB639C"
   "5AE4F5683423B4742BF1C978238F16CBE39D652DE3FDB8BEFC848AD9"
   "22222E04A4037C0713EB57A81A23F0C73473FC646CEA306B4BCBC886"
   "2F8385DDFA9D4B7FA2C087E879683303ED5BDD3A062B3CF5B3A278A6"
   "6D2A13F83F44F82DDF310EE074AB6A364597E899A0255DC164F31CC5"
   "0846851DF9AB48195DED7EA1B1D510BD7EE74D73FAF36BC31ECFA268"
   "359046F4EB879F924009438B481C6CD7889A002ED5EE382BC9190DA6"
   "FC026E479558E4475677E9AA9E3050E2765694DFC81F56E880B96E71"
   "60C980DD98EDD3DFFFFFFFFFFFFFFFFF",
   "13"
 },
 {0,0} /* null sentinel */
};


NGConstant * new_ng(SRP_NGType ng_type, const char * n_hex, const char * g_hex)
{
    NGConstant * ng   = (NGConstant *) malloc( sizeof(NGConstant) );

    if (!ng)
        return 0;

    ng->N = (mbedtls_mpi *) malloc(sizeof(mbedtls_mpi));
    ng->g = (mbedtls_mpi *) malloc(sizeof(mbedtls_mpi));
    mbedtls_mpi_init(ng->N);
    mbedtls_mpi_init(ng->g);

    if( !ng || !ng->N || !ng->g )
       return 0;

    if ( ng_type != SRP_NG_CUSTOM )
    {
        n_hex = global_Ng_constants[ ng_type ].n_hex;
        g_hex = global_Ng_constants[ ng_type ].g_hex;
    }

    mbedtls_mpi_read_string( ng->N, 16, n_hex);
    mbedtls_mpi_read_string( ng->g, 16, g_hex);

    return ng;
}

void delete_ng(NGConstant * ng)
{
	if (!ng) {
		return;
	}
	
	if (ng->N) {
		mbedtls_mpi_free(ng->N);
		free(ng->N);
	}

	if (ng->g) {
		mbedtls_mpi_free( ng->g );
   		free(ng->g);
	}

	free(ng);
}

void hash_init(SRP_HashAlgorithm alg, HashCTX *c)
{
    switch (alg)
    {
      case SRP_SHA1  : mbedtls_sha1_init( &c->sha );
      case SRP_SHA256: mbedtls_sha256_init( &c->sha256 );
      case SRP_SHA512: mbedtls_sha512_init( &c->sha512 );
      default:
        return;
    };
}

void hash_start(SRP_HashAlgorithm alg, HashCTX *c)
{
    switch (alg)
    {
      case SRP_SHA1  : mbedtls_sha1_starts( &c->sha );
      case SRP_SHA224: mbedtls_sha256_starts( &c->sha256, 1 );
      case SRP_SHA256: mbedtls_sha256_starts( &c->sha256, 0 );
      case SRP_SHA384: mbedtls_sha512_starts( &c->sha512, 1 );
      case SRP_SHA512: mbedtls_sha512_starts( &c->sha512, 0 );
      default:
        return;
    };
}

void hash_update(SRP_HashAlgorithm alg, HashCTX *c, const unsigned char *data, size_t len)
{
    switch (alg)
    {
      case SRP_SHA1  : mbedtls_sha1_update( &c->sha, data, len ); break;
      case SRP_SHA224: mbedtls_sha256_update( &c->sha256, data, len ); break;
      case SRP_SHA256: mbedtls_sha256_update( &c->sha256, data, len ); break;
      case SRP_SHA384: mbedtls_sha512_update( &c->sha512, data, len ); break;
      case SRP_SHA512: mbedtls_sha512_update( &c->sha512, data, len ); break;
      default:
        return;
    };
}

void hash_final(SRP_HashAlgorithm alg, HashCTX *c, unsigned char *md)
{
    switch (alg)
    {
      case SRP_SHA1  : mbedtls_sha1_finish( &c->sha, md ); break;
      case SRP_SHA224: mbedtls_sha256_finish( &c->sha256, md ); break;
      case SRP_SHA256: mbedtls_sha256_finish( &c->sha256, md ); break;
      case SRP_SHA384: mbedtls_sha512_finish( &c->sha512, md ); break;
      case SRP_SHA512: mbedtls_sha512_finish( &c->sha512, md ); break;
      default:
        return;
    };
}

void hash(SRP_HashAlgorithm alg, const unsigned char *d, size_t n, unsigned char *md)
{
    switch (alg)
    {
      case SRP_SHA1  : mbedtls_sha1( d, n, md ); break;
      case SRP_SHA224: mbedtls_sha256( d, n, md, 1); break;
      case SRP_SHA256: mbedtls_sha256( d, n, md, 0); break;
      case SRP_SHA384: mbedtls_sha512( d, n, md, 1 ); break;
      case SRP_SHA512: mbedtls_sha512( d, n, md, 0 ); break;
      default:
        return;
    };
}

int hash_length(SRP_HashAlgorithm alg)
{
    switch (alg)
    {
      case SRP_SHA1  : return SHA1_DIGEST_LENGTH;
      case SRP_SHA224: return SHA224_DIGEST_LENGTH;
      case SRP_SHA256: return SHA256_DIGEST_LENGTH;
      case SRP_SHA384: return SHA384_DIGEST_LENGTH;
      case SRP_SHA512: return SHA512_DIGEST_LENGTH;
      default:
        return -1;
    };
}

BIGNUM * H_nn(SRP_HashAlgorithm alg, const BIGNUM * n1, const BIGNUM * n2)
{
    unsigned char   buff[ SHA512_DIGEST_LENGTH ];
    int             len_n1 = mbedtls_mpi_size(n1);
    int             len_n2 = mbedtls_mpi_size(n2);
    int             nbytes = len_n1 + len_n2;
    unsigned char * bin    = (unsigned char *) malloc( nbytes );
    if (!bin)
       return 0;

    mbedtls_mpi_write_binary( n1, bin, len_n1 );
    mbedtls_mpi_write_binary( n2, bin+len_n1, len_n2 );
    hash( alg, bin, nbytes, buff );
    free(bin);
    BIGNUM * bn;
    bn = (mbedtls_mpi *) malloc(sizeof(mbedtls_mpi));
    mbedtls_mpi_init(bn);
    mbedtls_mpi_read_binary( bn, buff, hash_length(alg) );
    return bn;
}

BIGNUM * H_ns(SRP_HashAlgorithm alg, const BIGNUM * n, const unsigned char * bytes, int len_bytes)
{
    unsigned char   buff[ SHA512_DIGEST_LENGTH ];
    int             len_n  = mbedtls_mpi_size(n);
    int             nbytes = len_n + len_bytes;
    unsigned char * bin    = (unsigned char *) malloc( nbytes );
    if (!bin)
       return 0;
    mbedtls_mpi_write_binary( n, bin, len_n );
    memcpy( bin + len_n, bytes, len_bytes );
    hash( alg, bin, nbytes, buff );
    free(bin);

	BIGNUM * bn;
	bn = (mbedtls_mpi *) malloc(sizeof(mbedtls_mpi));
    mbedtls_mpi_init(bn);
    mbedtls_mpi_read_binary( bn, buff, hash_length(alg) );
    return bn;
}


BIGNUM * calculate_x(SRP_HashAlgorithm alg, const BIGNUM * salt, const unsigned char * password, int password_len)
{
    unsigned char ucp_hash[SHA512_DIGEST_LENGTH];
    HashCTX       ctx;

    hash_init( alg, &ctx );

    hash_update( alg, &ctx, password, password_len );

    hash_final( alg, &ctx, ucp_hash );

    return H_ns( alg, salt, ucp_hash, hash_length(alg) );
}

void update_hash_n(SRP_HashAlgorithm alg, HashCTX *ctx, const BIGNUM * n)
{
    unsigned long len = mbedtls_mpi_size(n);
    unsigned char * n_bytes = (unsigned char *) malloc( len );
    if (!n_bytes)
       return;
    mbedtls_mpi_write_binary( n, n_bytes, len );
    hash_update(alg, ctx, n_bytes, len);
    free(n_bytes);
}

void hash_num(SRP_HashAlgorithm alg, const BIGNUM * n, unsigned char * dest)
{
    int             nbytes = mbedtls_mpi_size(n);
    unsigned char * bin    = (unsigned char *) malloc( nbytes );
    if(!bin)
       return;
    mbedtls_mpi_write_binary( n, bin, nbytes );
    hash( alg, bin, nbytes, dest );
    free(bin);
}

void calculate_M( SRP_HashAlgorithm alg, NGConstant *ng, unsigned char * dest,
                         const BIGNUM * A, const BIGNUM * B, const unsigned char * K )
{
    unsigned char H_N[ SHA512_DIGEST_LENGTH ];
    unsigned char H_g[ SHA512_DIGEST_LENGTH ];
    unsigned char H_I[ SHA512_DIGEST_LENGTH ];
    unsigned char H_xor[ SHA512_DIGEST_LENGTH ];
    HashCTX       ctx;
    int           i = 0;
    int           hash_len = hash_length(alg);

    hash_num( alg, ng->N, H_N );
    hash_num( alg, ng->g, H_g );


    for (i=0; i < hash_len; i++ )
        H_xor[i] = H_N[i] ^ H_g[i];

    hash_init( alg, &ctx );

    hash_update( alg, &ctx, H_xor, hash_len );
    hash_update( alg, &ctx, H_I,   hash_len );
    update_hash_n( alg, &ctx, A );
    update_hash_n( alg, &ctx, B );
    hash_update( alg, &ctx, K, hash_len );

    hash_final( alg, &ctx, dest );
}

void calculate_H_AMK(SRP_HashAlgorithm alg, unsigned char *dest, const BIGNUM * A, const unsigned char * M, const unsigned char * K)
{
    HashCTX ctx;

    hash_init( alg, &ctx );

    update_hash_n( alg, &ctx, A );
    hash_update( alg, &ctx, M, hash_length(alg) );
    hash_update( alg, &ctx, K, hash_length(alg) );

    hash_final( alg, &ctx, dest );
}


void init_random()
{
    if (g_initialized)
        return;

     mbedtls_entropy_init( &entropy_ctx );
     mbedtls_ctr_drbg_init( &ctr_drbg_ctx );

     unsigned char hotBits[128] = {
    82, 42, 71, 87, 124, 241, 30, 1, 54, 239, 240, 121, 89, 9, 151, 11, 60,
    226, 142, 47, 115, 157, 100, 126, 242, 132, 46, 12, 56, 197, 194, 76,
    198, 122, 90, 241, 255, 43, 120, 209, 69, 21, 195, 212, 100, 251, 18,
    111, 30, 238, 24, 199, 238, 236, 138, 225, 45, 15, 42, 83, 114, 132,
    165, 141, 32, 185, 167, 100, 131, 23, 236, 9, 11, 51, 130, 136, 97, 161,
    36, 174, 129, 234, 2, 54, 119, 184, 70, 103, 118, 109, 122, 15, 24, 23,
    166, 203, 102, 160, 77, 100, 17, 4, 132, 138, 215, 204, 109, 245, 122,
    9, 184, 89, 70, 247, 125, 97, 213, 240, 85, 243, 91, 226, 127, 64, 136,
    37, 154, 232
};
     
	 mbedtls_ctr_drbg_seed(
     	&ctr_drbg_ctx,
     	mbedtls_entropy_func,
     	&entropy_ctx,
        hotBits,
        128
    );

    RR = (mbedtls_mpi *) malloc(sizeof(mbedtls_mpi));
    mbedtls_mpi_init(RR);
    g_initialized = 1;
}

void srp_teardown()
{
	/* Reset SRP module state, clean up used memory */
	if (RR) {
		if (RR->p) {
			free(RR->p);
		}

		free(RR);
	}

	RR = NULL;
	g_initialized = 0;
}

/***********************************************************************************************************
 *
 *  Exported Functions
 *
 ***********************************************************************************************************/

void srp_random_seed( const unsigned char * random_data, int data_length )
{
    g_initialized = 1;


   if( mbedtls_ctr_drbg_seed( &ctr_drbg_ctx, mbedtls_entropy_func, &entropy_ctx,
                           (const unsigned char *) random_data,
                           data_length )  != 0 )
	{
    	return;
    }

}

#endif /* DAT_CRYPTO_MBEDTLS */
#endif