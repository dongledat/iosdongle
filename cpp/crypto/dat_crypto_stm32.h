#ifndef _DAT_CRYPTO_STM32_H_
#define _DAT_CRYPTO_STM32_H_

#include "dat_crypto_common.h"

DAT_CRYPTO_RESULT dat_crypto_stm32_init();
DAT_CRYPTO_RESULT dat_crypto_stm32_deinit();
DAT_CRYPTO_RESULT dat_crypto_stm32_rng_init(RNGstate_stt* rng);
DAT_CRYPTO_RESULT dat_crypto_stm32_rng_deinit(RNGstate_stt* rng);

#endif /* _DAT_CRYPTO_STM32_H_ */
