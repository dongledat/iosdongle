/*! \file dat_ecc.h */

#ifndef _DAT_ECC_H_
#define _DAT_ECC_H_

#include "dat_crypto_common.h"
#if defined(DAT_CRYPTO_STM32)
#include "dat_ecc_stm32.h"
#elif defined (DAT_CRYPTO_MBEDTLS)
#include "dat_ecc_mbedtls.h"
#endif

// #define ECC_KEY_LENGTH_224
#define ECC_KEY_LENGTH_256

/** 
 *  DAT_ECC_KEY_LENGTH
 *	@brief The length, in bytes, of the private key / public X coord / public Y coordinate.
 */
#if defined(ECC_KEY_LENGTH_256)
#define DAT_ECC_KEY_LENGTH	(32)
#define DAT_ECC_ALGORITHM MBEDTLS_ECP_DP_SECP256R1
#elif defined(ECC_KEY_LENGTH_224)
#define DAT_ECC_ALGORITHM MBEDTLS_ECP_DP_SECP224R1
#define DAT_ECC_KEY_LENGTH	(28)
#endif

/** 
 *  DAT_ECC_SECRET_KEY_LENGTH
 *	@brief The length, in bytes, of the secret key calculated by the API 
 */
#define DAT_ECC_SECRET_KEY_LENGTH (32)

#if defined(__cplusplus)
extern "C" {
#endif

/** 
 *  @fn DAT_CRYPTO_RESULT dat_ecc_init(dat_ecc_ctx_t* ecc)
 *	@brief Initialize an ECC key agreement context.
 *	@param ecc Pointer to ECC context to intialize.
 *  @note It is assumed that @b Crypto_DeInit is called prior to @ref dat_ecc_init.
 *
 *	The context must be initialized prior to being passed to other ECC functions.
 */
DAT_CRYPTO_RESULT dat_ecc_init(dat_ecc_ctx_t* ecc);

/** 
 *  @fn DAT_CRYPTO_RESULT dat_ecc_deinit(dat_ecc_ctx_t* ecc)
 *	@brief Finalize an ECC key agreement context.
 *	@param ecc Pointer to ECC context to finalize.
 *	
 *	Finalize the context when it is no longer required.
 */
DAT_CRYPTO_RESULT dat_ecc_deinit(dat_ecc_ctx_t* ecc);

/** 
 *  @fn DAT_CRYPTO_RESULT dat_ecc_generate_keys(dat_ecc_ctx_t* ecc, unsigned char* private_key, unsigned char* public_key_x, unsigned char* public_key_y)
 *	@brief Generate a random private and public ECC key pair.
 *	@param[in] ecc Pointer to ECC context. Must be initialized with a call to dat_ecc_init.
 *	@param[out] private_key Buffer to receive the new private key.
 *	@param[out] public_key_x Buffer to receive the new public key's X coordinate.
 *	@param[out] public_key_y Buffer to receive the new public key's Y coordinate.
 *  @note It is assumed that keys buffers are allocated by the caller. The size of each buffer is dictated by @ref DAT_ECC_KEY_LENGTH.
 */
DAT_CRYPTO_RESULT dat_ecc_generate_keys(dat_ecc_ctx_t* ecc, unsigned char* private_key, unsigned char* public_key_x, unsigned char* public_key_y);

/** 
 *  @fn DAT_CRYPTO_RESULT dat_ecc_calculate_secret_point(dat_ecc_ctx_t* ecc, unsigned char* private_key, unsigned char* public_key_x, unsigned char* public_key_y, unsigned char* output)
 *	@brief Calculates a secret for symmetric encryption between entities A and B.
 *	@param[in] ecc Pointer to the context. Must be initialized with a call to dat_ecc_init.
 *	@param[in] private_key Entity A's private key.
 *	@param[in] public_key_x Entity B's public key's X coordinate.
 *	@param[in] public_key_y Entity B's public key's Y coordinate.
 *	@param[out] secret Buffer to receive the secret key. Bufferof size @ref DAT_ECC_SECRET_KEY_LENGTH has to be allocated by the caller.
 *  @note Entity A (usually) refers to the caller, whereas entity B (usually) refers to the other party.
 */
DAT_CRYPTO_RESULT dat_ecc_calculate_secret_point(dat_ecc_ctx_t* ecc, unsigned char* private_key, unsigned char* public_key_x, unsigned char* public_key_y, unsigned char* secret);

/**
 * @fn dat_ecc_convert_pem_to_bin(const char *pem, const int pem_size, unsigned char *privkey, int *privkey_size, unsigned char *pubkey, int *pubkey_size);
 * @param[in] pem
 * @param[in] pem_size
 * @param[out] privkey 
 * @param[in][out] privkey_size, should >= 64 bytes
 * @param[out] pubkey
 * @param[in][out] pubkey_size, should >= 64 bytes
 *
 * Extracts the public and private keys from an EC PEM file.
 * The input PEM file can either be a private or public PEM file.
 * The function will set (priv|pub)key_size to the length of the relevant key,
 * if one exists. The input buffers sizes (also specified by
 * (priv|pub)key_size) must be large enough.
 * The function returns 0 on success or a negative value on error.
 *
 * This API has a few limitations/assumptions:
 * - The PEM file isn't password protected
 * - The parameters are known in advance and are not extracted
 *
 * Since this will not be run in an embedded platform, this API should probably
 * not be used at all. Instead, openssl command line tools can be used instead
 * to extract the keys.
 */
DAT_CRYPTO_RESULT dat_ecc_convert_pem_to_bin(const char *pem, const int pem_size, unsigned char *privkey, int *privkey_size, unsigned char *pubkey, int *pubkey_size);

#if defined(__cplusplus)
}
#endif

#endif
