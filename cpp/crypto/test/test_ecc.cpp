#if 0
#include "crypto/dat_ecc.h"

#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetector.h"
#include "CppUTest/TestMemoryAllocator.h"
#include "CppUTest/PlatformSpecificFunctions.h"

#include <string.h>
#include <stdio.h>

TEST_GROUP(Crypto_ECC)
{

};

TEST_GROUP(Crypto_ECC_Failers)
{

};

static uint8_t null_buffer[DAT_ECC_KEY_LENGTH] = { 0 };

/***** Correct scenarios *****/
void perform_complete_ecc_dh_with_assertions();

IGNORE_TEST(Crypto_ECC, ECC)
{
	perform_complete_ecc_dh_with_assertions();
}

IGNORE_TEST(Crypto_ECC, MultipleECC)
{
	for (int i = 0; i < 20; ++i) {
		perform_complete_ecc_dh_with_assertions();
	}
}


void perform_complete_ecc_dh_with_assertions()
{
	DAT_CRYPTO_RESULT status;
	dat_ecc_ctx_t ecc_A = { 0 };
	dat_ecc_ctx_t ecc_B = { 0 };

	uint8_t priv_key_A[DAT_ECC_KEY_LENGTH] = { 0 };
	uint8_t priv_key_B[DAT_ECC_KEY_LENGTH] = { 0 };
	uint8_t public_key_x_A[DAT_ECC_KEY_LENGTH] = { 0 };
	uint8_t public_key_y_A[DAT_ECC_KEY_LENGTH] = { 0 };
	uint8_t public_key_x_B[DAT_ECC_KEY_LENGTH] = { 0 };
	uint8_t public_key_y_B[DAT_ECC_KEY_LENGTH] = { 0 };

	uint8_t secret_key[DAT_ECC_SECRET_KEY_LENGTH] = { 0 };
	uint8_t secret_key2[DAT_ECC_SECRET_KEY_LENGTH] = { 0 };

	if (DAT_CRYPTO_SUCCESS != dat_ecc_init(&ecc_A)) {
		FAIL("dat_ecc_init failure");
	}

	if (DAT_CRYPTO_SUCCESS != dat_ecc_init(&ecc_B)) {
		FAIL("dat_ecc_init failure");
	}

	// Generating Keys - A
	status = dat_ecc_generate_keys(&ecc_A, priv_key_A, public_key_x_A, public_key_y_A);
	LONGS_EQUAL_TEXT(status, 0, "Key generation failed");

	if (0 == memcmp(null_buffer, priv_key_A, DAT_ECC_KEY_LENGTH)) {
		FAIL("priv_key_A is NULL buffer");
	}
	if (0 == memcmp(null_buffer, public_key_x_A, DAT_ECC_KEY_LENGTH)) {
		FAIL("public_key_x_A is NULL buffer");
	}
	if (0 == memcmp(null_buffer, public_key_y_A, DAT_ECC_KEY_LENGTH)) {
		FAIL("public_key_y_A is NULL buffer");
	}

	// Generating Keys - B
	status = dat_ecc_generate_keys(&ecc_B, priv_key_B, public_key_x_B, public_key_y_B);
	LONGS_EQUAL_TEXT(status, 0, "Key generation failed");

	if (0 == memcmp(null_buffer, priv_key_B, DAT_ECC_KEY_LENGTH)) {
		FAIL("priv_key_B is NULL buffer");
	}
	if (0 == memcmp(null_buffer, public_key_x_B, DAT_ECC_KEY_LENGTH)) {
		FAIL("public_key_x_B is NULL buffer");
	}
	if (0 == memcmp(null_buffer, public_key_y_B, DAT_ECC_KEY_LENGTH)) {
		FAIL("public_key_y_B is NULL buffer");
	}

	// Verify keys are different
	int memcmp_status = memcmp(priv_key_A, priv_key_B, DAT_ECC_KEY_LENGTH);
	CHECK_TEXT(memcmp_status != 0, "Keys are same");

	// Generating secret & salt - A
	status = dat_ecc_calculate_secret_point(&ecc_A, priv_key_A, public_key_x_B, public_key_y_B, secret_key);
	LONGS_EQUAL_TEXT(status, 0, "Secret 1 Failed to create");
	if (0 == memcmp(null_buffer, secret_key, DAT_ECC_SECRET_KEY_LENGTH)) {
		FAIL("secret_key is NULL buffer");
	}

	// Generate secret B with salt from A
	status = dat_ecc_calculate_secret_point(&ecc_B, priv_key_B, public_key_x_A, public_key_y_A, secret_key2);
	LONGS_EQUAL_TEXT(status, 0, "Secret 2 Failed to create");
	if (0 == memcmp(null_buffer, secret_key2, DAT_ECC_SECRET_KEY_LENGTH)) {
		FAIL("secret_key2 is NULL buffer");
	}

	// Verify secrets match
	memcmp_status = memcmp(secret_key, secret_key2, DAT_ECC_SECRET_KEY_LENGTH);
	CHECK_TEXT(memcmp_status == 0, "Secrets do not match");

	status = dat_ecc_deinit(&ecc_A);
	LONGS_EQUAL_TEXT(status, 0, "ecc_A failed to deinit");

	status = dat_ecc_deinit(&ecc_B);
	LONGS_EQUAL_TEXT(status, 0, "ecc_B failed to deinit");
}

/***** Failers *****/

IGNORE_TEST(Crypto_ECC_Failers, NullPointers)
{
	DAT_CRYPTO_RESULT status;
	dat_ecc_ctx_t ecc_A = { 0 };

	uint8_t priv_key_A[DAT_ECC_KEY_LENGTH] = { 0 };
	uint8_t public_key_x_A[DAT_ECC_KEY_LENGTH] = { 0 };
	uint8_t public_key_y_A[DAT_ECC_KEY_LENGTH] = { 0 };

	uint8_t secret_key[DAT_ECC_SECRET_KEY_LENGTH] = { 0 };

	if (DAT_CRYPTO_SUCCESS != dat_ecc_init(&ecc_A)) {
		FAIL("dat_ecc_init failure");
	}

	// Try passing nulls
	status = dat_ecc_generate_keys(&ecc_A, NULL, public_key_x_A, public_key_y_A);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted NULL pointer");

	status = dat_ecc_generate_keys(&ecc_A, priv_key_A, NULL, public_key_y_A);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted NULL pointer");

	status = dat_ecc_generate_keys(&ecc_A, priv_key_A, public_key_x_A, NULL);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted NULL pointer");

	// Generate actual keys
	status = dat_ecc_generate_keys(&ecc_A, priv_key_A, public_key_x_A, public_key_y_A);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS == status, "dat_ecc_generate_keys with valid pointers failed");

	// Try passing nulls
	status = dat_ecc_calculate_secret_point(&ecc_A, NULL, public_key_x_A, public_key_y_A, secret_key);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted NULL pointer");

	status = dat_ecc_calculate_secret_point(&ecc_A, priv_key_A, NULL, public_key_y_A, secret_key);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted NULL pointer");

	status = dat_ecc_calculate_secret_point(&ecc_A, priv_key_A, public_key_x_A, NULL, secret_key);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted NULL pointer");

	status = dat_ecc_calculate_secret_point(&ecc_A, priv_key_A, public_key_x_A, public_key_y_A, NULL);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted NULL pointer");

	status = dat_ecc_deinit(&ecc_A);
	LONGS_EQUAL_TEXT(status, 0, "ecc_A failed to deinit");

}

IGNORE_TEST(Crypto_ECC_Failers, EmptyBuffers)
{
	DAT_CRYPTO_RESULT status;
	dat_ecc_ctx_t ecc_A = { 0 };

	uint8_t priv_key_A[DAT_ECC_KEY_LENGTH] = { 0 };
	uint8_t public_key_x_A[DAT_ECC_KEY_LENGTH] = { 0 };
	uint8_t public_key_y_A[DAT_ECC_KEY_LENGTH] = { 0 };

	uint8_t secret_key[DAT_ECC_SECRET_KEY_LENGTH] = { 0 };

	if (DAT_CRYPTO_SUCCESS != dat_ecc_init(&ecc_A)) {
		FAIL("dat_ecc_init failure");
	}

	// Generate keys
	status = dat_ecc_generate_keys(&ecc_A, priv_key_A, public_key_x_A, public_key_y_A);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS == status, "dat_ecc_generate_keys with valid pointers failed");

	// Try to generate secret with null private key / public key coordinate
	status = dat_ecc_calculate_secret_point(&ecc_A, null_buffer, public_key_x_A, public_key_y_A, secret_key);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted empty buffer as private key");

	status = dat_ecc_calculate_secret_point(&ecc_A, priv_key_A, null_buffer, public_key_y_A, secret_key);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted empty buffer as public key x");

	status = dat_ecc_calculate_secret_point(&ecc_A, priv_key_A, public_key_x_A, null_buffer, secret_key);
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat ECC accepted empty buffer as public key y");

	status = dat_ecc_deinit(&ecc_A);
	LONGS_EQUAL_TEXT(status, 0, "ecc_A failed to deinit");
}

//
//TEST(Crypto, ECCMocking)
//{
//  
//  BEGIN_FUZZY_MOCK_LOOP(FUZZY_MOCK_ALL)
//
//    unsigned int ret = perform_ECC();
//   
//  FUZZY_COMPLETE_ITERATION() 
//    
//    if (0 == ret) {
//            FAIL("Failed a malloc but ECC succeeded");
//    }
//
//  END_FUZZY_MOCK_LOOP()
//  
//}
//
//unsigned int perform_ECC()
//{
//	DAT_CRYPTO_RESULT status;
//	dat_ecc_ctx h = { 0 };
//	if (DAT_CRYPTO_SUCCESS != dat_ecc_init(&h)) {
//		return 1;
//	}
//
//	//CHECK_TEXT(h != NULL, "Init failed");
//
//	uint8_t null_buffer[DAT_ECC_KEY_LENGTH] = { 0 };
//
//	uint8_t priv_key_A[DAT_ECC_KEY_LENGTH] = { 0 };
//	uint8_t priv_key_B[DAT_ECC_KEY_LENGTH] = { 0 };
//	uint8_t public_key_x_A[DAT_ECC_KEY_LENGTH] = { 0 };
//	uint8_t public_key_y_A[DAT_ECC_KEY_LENGTH] = { 0 };
//	uint8_t public_key_x_B[DAT_ECC_KEY_LENGTH] = { 0 };
//	uint8_t public_key_y_B[DAT_ECC_KEY_LENGTH] = { 0 };
//
//	// Generating Keys
//	status = dat_ecc_generate_keys(&h, priv_key_A, public_key_x_A, public_key_y_A);
//	LONGS_EQUAL_TEXT(status, 0, "Key generation failed");
//
//	if (0 == memcmp(null_buffer, priv_key_A, DAT_ECC_KEY_LENGTH)) {
//		return 1;
//	}
//	if (0 == memcmp(null_buffer, public_key_x_A, DAT_ECC_KEY_LENGTH)) {
//		return 1;
//	}
//	if (0 == memcmp(null_buffer, public_key_y_A, DAT_ECC_KEY_LENGTH)) {
//		return 1;
//	}
//
//	status = dat_ecc_generate_keys(&h, priv_key_B, public_key_x_B, public_key_y_B);
//
//	if (0 == memcmp(null_buffer, priv_key_B, DAT_ECC_KEY_LENGTH)) {
//		return 1;
//	}
//	if (0 == memcmp(null_buffer, public_key_x_B, DAT_ECC_KEY_LENGTH)) {
//		return 1;
//	}
//	if (0 == memcmp(null_buffer, public_key_y_B, DAT_ECC_KEY_LENGTH)) {
//		return 1;
//	}
//
//	int memcmp_status = memcmp(priv_key_A, priv_key_B, DAT_ECC_KEY_LENGTH);
//	CHECK_TEXT(memcmp_status != 0, "Keys are same");
//
//	uint8_t salt[DAT_ECC_KEY_LENGTH] = { 0 };
//	uint8_t secret_key[DAT_ECC_SECRET_KEY_SIZE] = { 0 };
//	uint8_t secret_key2[DAT_ECC_SECRET_KEY_SIZE] = { 0 };
//	
//	// Generating secret
//	status = dat_ecc_calculate_secret(&h, priv_key_A, public_key_x_B, public_key_y_B, salt, secret_key, TRUE);
//	LONGS_EQUAL_TEXT(status, 0, "Secret 1 Failed to create");
//	if (0 == memcmp(null_buffer, secret_key, DAT_ECC_SECRET_KEY_SIZE)) {
//		return 1;
//	}
//
//	// Generate secret 2 with same salt
//	status = dat_ecc_calculate_secret(&h, priv_key_B, public_key_x_A, public_key_y_A, salt, secret_key2, FALSE);
//	LONGS_EQUAL_TEXT(status, 0, "Secret 2 Failed to create");
//	if (0 == memcmp(null_buffer, secret_key2, DAT_ECC_SECRET_KEY_SIZE)) {
//		return 1;
//	}
//
//	memcmp_status = memcmp(secret_key, secret_key2, DAT_ECC_SECRET_KEY_SIZE);
//	if (memcmp_status != 0) {
//          return 1;
//        }
//        
//        return 0;
//}


TEST(Crypto_ECC, ConvertPEMToBin)
{
    static const char pub_pem[] =
        "-----BEGIN PUBLIC KEY-----\n"
        "ME4wEAYHKoZIzj0CAQYFK4EEACEDOgAErlM4yS+W7GwPBjY/kJGQrZFF6RWgghuQ\n"
        "pb35yNNXnTNljeSlcVCmkCJFGbLz5jrxAYngDkzKwk4=\n"
        "-----END PUBLIC KEY-----\n";

    int pub_pem_size = sizeof(pub_pem);
    unsigned char pubkey[64];
    int pubkey_size = sizeof(pubkey);

    CHECK(DAT_CRYPTO_SUCCESS == dat_ecc_convert_pem_to_bin(pub_pem, pub_pem_size, NULL, NULL, pubkey, &pubkey_size));
}
#endif