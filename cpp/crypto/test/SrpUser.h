#pragma once

#include "../dat_srp_mbedtls_common.h"

#include <vector>

typedef unsigned char byte;
typedef std::vector<byte> Buffer;


class SRPUserFail {
};

class SrpUser {
public:
	
	SrpUser(SRP_HashAlgorithm algo, Buffer& pass, Buffer& n_hex, Buffer& g_hex);
	SrpUser(SRP_HashAlgorithm algo, SRP_NGType ngtype, Buffer& pass);
	~SrpUser();

	// Requires:	.
	// Calculates:	.
	// Stores:		s
	// Discards:	.
	// Verifies:	.
	// Returns:		.
	void setSalt(Buffer& salt);

	// Requires:	s
	// Calculates:	a, A
	// Stores:		a, A
	// Discards:	.
	// Verifies:	.
	// Returns:		A
	Buffer getA();

	// Requires:	.
	// Calculates:	.
	// Stores:		B, u
	// Discards:	.
	// Verifies:	.
	// Returns:		.
	void setBu(Buffer B, Buffer u);

	// Requires:	a, A, B, u
	// Calculates:	x, S, K
	// Stores:		K
	// Discards:	x, S, a, u
	// Verifies:	.
	// Returns:		.
	void calculateKey();

	// Requires:	A, B, K
	// Calculates:	M
	// Stores:		M
	// Discards:	B
	// Verifies:	.
	// Returns:		M
	Buffer getM();

	// Requires:	A, M, K
	// Calculates:	selfHAMK
	// Stores:		.
	// Discards:	A, M, selfHAMK
	// Verifies:	selfHAMK == HAMK
	// Returns:		.
	void setHAMK(Buffer HAMK);

	// Requires:	K
	// Calculates:	.
	// Stores:		.
	// Discards:	.
	// Verifies:	.
	// Returns:		K
	Buffer getSessionKey();

	void clear();

private:
	SRPUser* m_user;
};
