#pragma once

#include "../dat_srp_mbedtls_common.h"

#include <vector>
#include <tuple>

typedef std::vector<byte> Buffer;

class SRPVerFail {
};

class SrpVerifier {
public:

	SrpVerifier(SRP_HashAlgorithm alg, Buffer& pass, Buffer& n_hex, Buffer& g_hex);
	SrpVerifier(SRP_HashAlgorithm alg, SRP_NGType ngtype, Buffer& pass);
	~SrpVerifier();

	// Requires:	.
	// Calculates:	s, v, x
	// Stores:		v
	// Discards:	s, x
	// Verifies:	.
	// Returns:		s
	Buffer getSalt();
	
	// Requires:	.
	// Calculates:	.
	// Stores:		A
	// Discards:	.
	// Verifies:	A % N != 0
	// Returns:		.
	void setA(Buffer A);

	// Requires:	v
	// Calculates:	b, B, u
	// Stores:		b, B, u
	// Discards:	.
	// Verifies:	.
	// Returns:		B, u
	std::tuple<Buffer, Buffer> getBu();	

	// Requires:	A, v, u, b
	// Calculates:	S, K
	// Stores:		K
	// Discards:	S, b
	// Verifies:	.
	// Returns:		.
	void calculateKey();

	// Requires:	A, B, K
	// Calculates:	M
	// Stores:		M
	// Discards:	B
	// Verifies:	selfM == M
	// Returns:		.
	void setM(Buffer M);

	// Requires:	A, M, K
	// Calculates:	HAMK
	// Stores:		.
	// Discards:	A, M, HAMK
	// Verifies:	.
	// Returns:		.
	Buffer getHAMK();

	// Requires:	K
	// Calculates:	.
	// Stores:		.
	// Discards:	.
	// Verifies:	.
	// Returns:		K
	Buffer getSessionKey();

	void clear();

private:
	SRPVerifier* m_ver;
};