#if 0
#define CRC16
#include "crypto/crc/dat_crc.h"
#include "crypto/chisqr/dat_chisqr.h"
#include "CppUTest/TestHarness.h"

TEST_GROUP(Crypto_CRC)
{
};

TEST_GROUP(Crypto_CRC_Failers)
{
};

//#defined TABLE_SIZE(t) 

template <typename T,unsigned S>
inline unsigned arraysize(const T (&v)[S]) { return S; }

TEST(Crypto_CRC, CRC)
{
	int a[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	double prob[16] = { 0 };

	crcInit();

	size_t array_size = arraysize(a);

	for (size_t i = 0; i < 1000000; i++, a[0]++) 
	{
		crc index = crcFast((const unsigned char*)a, sizeof(a));
		index /= 4096;
		prob[index] += 1; 
	}

	/*
	 * Check that CRC distribution is uniform
	 */
	int uni = chiIsUniform(prob, arraysize(prob), 0.05);
	CHECK(uni);
}

#endif