

#include "crypto/dat_btls.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "CppUTest/TestHarness.h"
TEST_GROUP(Crypto_Btls)
{
};

TEST_GROUP(Crypto_Btls_Failers)
{
};

void test_encrypt_decrypt(int size);
void test_iv();

const char* plain_text = "HELLO WORLD-1234567890-HELLO WORLD-1234567890-HELLO WORLD-1234567890-HELLO WORLD-1234567890-HELLO WORLD-1234567890";
//uint8_t secretkey[32];

TEST(Crypto_Btls, BTLS)
{
	btls_ctx ctx_dongle;
	btls_ctx ctx_smartphone;
	init_btls(&ctx_dongle);
	init_btls(&ctx_smartphone);
	DAT_CRYPTO_RESULT status;

	// Load keys
	generate_btls_keys(&ctx_dongle);
	generate_btls_keys(&ctx_smartphone);

	// dongle send hello
	status = build_btls_packet_handshake_hello(&ctx_dongle, 1);
	CHECK(status == DAT_CRYPTO_SUCCESS)

	// smartphone get hello
	status = decode_btls_packet(&ctx_smartphone, ctx_dongle.buffer, ctx_dongle.buffer_size);
	CHECK(status == DAT_CRYPTO_SUCCESS)

	// smartphone send hello
	status = build_btls_packet_handshake_hello(&ctx_smartphone, 0);
	CHECK(status == DAT_CRYPTO_SUCCESS)

	// dongle get hello
	status = decode_btls_packet(&ctx_dongle, ctx_smartphone.buffer, ctx_smartphone.buffer_size);
	CHECK(status == DAT_CRYPTO_SUCCESS)

	CHECK(memcmp(ctx_smartphone.iv_state.encryption_iv, ctx_dongle.iv_state.decryption_iv, 16) == 0);
	CHECK(memcmp(ctx_smartphone.iv_state.decryption_iv, ctx_dongle.iv_state.encryption_iv, 16) == 0);

	// dongle send public
	status = build_btls_packet_handshake_keyexchange(&ctx_dongle, 1);
	CHECK(status == DAT_CRYPTO_SUCCESS)

	// smartphone get public
	status = decode_btls_packet(&ctx_smartphone, ctx_dongle.buffer, ctx_dongle.buffer_size);
	CHECK(status == DAT_CRYPTO_SUCCESS)

	// smartphone send public
	status = build_btls_packet_handshake_keyexchange(&ctx_smartphone, 0);
	CHECK(status == DAT_CRYPTO_SUCCESS)

	// dongle get public
	status = decode_btls_packet(&ctx_dongle, ctx_smartphone.buffer, ctx_smartphone.buffer_size);
	CHECK(status == DAT_CRYPTO_SUCCESS)	

	status = build_btls_packet_application(&ctx_dongle, (unsigned char*)"Hello", 5);
	CHECK(status == DAT_CRYPTO_SUCCESS)
	status = decode_btls_packet(&ctx_smartphone, ctx_dongle.buffer, ctx_dongle.buffer_size);
	CHECK(status == DAT_CRYPTO_SUCCESS)


	
	

	release_btls(&ctx_dongle);
};