#include "crypto/dat_aes.h"

#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetector.h"
#include "CppUTest/TestMemoryAllocator.h"
#include "CppUTest/PlatformSpecificFunctions.h"

#include <string.h>
#include <stdio.h>

TEST_GROUP(Crypto_AES)
{

};

TEST_GROUP(Crypto_AES_Failers)
{

};


unsigned char key128[] = {
	0x33, 0x2d, 0x67, 0x5f, 0xdd, 0xbd, 0x99, 0x8e, 0x95, 0x84, 0xd, 0xc3, 0x2a, 0x62, 0x53, 0x72
};

unsigned char key224[] = {
	0xef, 0x5, 0x9f, 0x4a, 0x5f, 0x3, 0xab, 0x18, 0xc4, 0xc8, 0xf8, 0xee, 0xc9, 0x31, 0xe1, 0x78, 0x68, 0x66, 0xbc, 0xc4, 0x82, 0xb7, 0x99, 0x17, 0x5e, 0xa3, 0xe5, 0x51
};

unsigned char key256[] = {
	0x96, 0x32, 0xe, 0xf2, 0xda, 0xc8, 0xe9, 0x95, 0x66, 0x46, 0xb6, 0x38, 0x62, 0x4, 0x15, 0x99, 0x83, 0xef, 0xde, 0xcc, 0xdd, 0xb6, 0xd9, 0x8d, 0x8c, 0xe6, 0x81, 0x69, 0xf1, 0xc5, 0x96, 0xe3
};

unsigned char plaintext[] = "The quick brown fox jumps over the lazy dog!!!!";

unsigned char g_iv[16] = { 0x99, 0xf1, 0x32, 0x55, 0x11, 0x55, 0x65, 0x28, 0x19, 0xEE, 0xCC, 0x39, 0x95, 0x01, 0xD2, 0xA3 };

#define TEST_TEXT_SIZE (sizeof(plaintext))

/* Explanation:
	MBEDTLS does not support some of the keys tested, and also does not support encrypting data
	which size is not a multiple of 16. 
	Thus the dat_aes_mbdetls lib also fails in those areas.
	Until that's fixed, we can't perform these tests on mbedtls aes
	*/
#ifndef DAT_CRYPTO_MBEDTLS

/***** Correct scenarios *****/

TEST(Crypto_AES, AES128)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
	unsigned char ciphertext[TEST_TEXT_SIZE] = { 0 };
	unsigned char deciphered_text[TEST_TEXT_SIZE] = { 0 };
	
	unsigned char iv[16] = { 0 };
	memcpy(iv, g_iv, 16);

	status = dat_aes_encrypt_message( key128, sizeof(key128), plaintext, sizeof(plaintext), iv, ciphertext);
	if (0 == memcmp(ciphertext, plaintext, sizeof(plaintext))) {
		FAIL("Plaintext == ciphertext");
	}

	LONGS_EQUAL_TEXT(DAT_CRYPTO_SUCCESS, status, "Failed to encrypt data");

	status = dat_aes_decrypt_message( key128, sizeof(key128), ciphertext, sizeof(plaintext), iv, deciphered_text);

	LONGS_EQUAL_TEXT(DAT_CRYPTO_SUCCESS, status, "Failed to decrypt data");

	if (0 != memcmp(deciphered_text, plaintext, sizeof(plaintext))) {
		FAIL("Deciphered text different from original plaintext");
	}
}


TEST(Crypto_AES, AES256)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
	unsigned char ciphertext[TEST_TEXT_SIZE] = { 0 };
	unsigned char deciphered_text[TEST_TEXT_SIZE] = { 0 };

	unsigned char iv[16] = { 0 };
	memcpy(iv, g_iv, 16);

	status = dat_aes_encrypt_message( key256, sizeof(key256), plaintext, sizeof(plaintext), iv, ciphertext);
	if (0 == memcmp(ciphertext, plaintext, sizeof(plaintext))) {
		FAIL("Plaintext == ciphertext");
	}

	LONGS_EQUAL_TEXT(DAT_CRYPTO_SUCCESS, status, "Failed to encrypt data");

	// Using same IV
	memcpy(iv, g_iv, 16);

	status = dat_aes_decrypt_message( key256, sizeof(key256), ciphertext, sizeof(plaintext), iv, deciphered_text);

	LONGS_EQUAL_TEXT(DAT_CRYPTO_SUCCESS, status, "Failed to decrypt data");

	if (0 != memcmp(deciphered_text, plaintext, sizeof(plaintext))) {
		FAIL("Deciphered text different from original plaintext");
	}
}


TEST(Crypto_AES, AES256_LongMessage)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
	unsigned char long_text[] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec euismod consectetur neque vel finibus. Suspendisse ac auctor augue, ornare hendrerit lectus. Ut molestie ex et risus eleifend, vitae imperdiet massa facilisis. Morbi non gravida tortor. Pellentesque fringilla luctus magna ut iaculis. Cras sed est vulputate, vestibulum magna ut, aliquet mi. Sed feugiat lectus diam, a dignissim ex rhoncus ac. Nulla facilisi. Aliquam ut finibus nulla, ac tempus turpis. Aliquam et ante ligula. Suspendisse laoreet sed neque efficitur feugiat. Nunc dignissim, sapien sed porta condimentum, velit dui dapibus orci, non molestie dui nunc et quam. Nunc commodo felis ante, in fringilla leo rhoncus ac. Phasellus tempor finibus tellus, ac finibus lorem tristique quis. Etiam lacinia et libero non convallis. Aenean sed ante interdum, sollicitudin turpis vitae, lobortis lorem. Nullam auctor libero leo, sed bibendum eros gravida quis. Nam purus metus, dignissim id sollicitudin nec, mattis ut lacus. Nam elit erat, hendrerit nec mi vel, vestibulum dignissim arcu. Donec id volutp!";
    unsigned char ciphertext[sizeof(long_text)] = { 0 };
	unsigned char deciphered_text[sizeof(long_text)] = { 0 };

	unsigned char iv[16] = { 0 };
	memcpy(iv, g_iv, 16);

	status = dat_aes_encrypt_message( key256, sizeof(key256), long_text, sizeof(long_text), iv, ciphertext);
	if (0 == memcmp(ciphertext, plaintext, sizeof(plaintext))) {
		FAIL("Plaintext == ciphertext");
	}

	LONGS_EQUAL_TEXT(DAT_CRYPTO_SUCCESS, status, "Failed to encrypt data");
	
	// Using same IV
	memcpy(iv, g_iv, 16);
	status = dat_aes_decrypt_message( key256, sizeof(key256), ciphertext, sizeof(long_text), iv, deciphered_text);

	LONGS_EQUAL_TEXT(DAT_CRYPTO_SUCCESS, status, "Failed to decrypt data");

	if (0 != memcmp(deciphered_text, long_text, sizeof(long_text))) {
		FAIL("Deciphered text different from original plaintext");
	}
}

/***** Failers *****/
TEST(Crypto_AES_Failers, AES256_ShortMessage)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
	unsigned char short_text[] = "abcdefg";
	unsigned char ciphertext[sizeof(short_text)] = { 0 };

	unsigned char iv[16] = { 0 };
	memcpy(iv, g_iv, 16);

	status = dat_aes_encrypt_message( key256, sizeof(key256), short_text, sizeof(short_text), iv, ciphertext);
	if (0 == memcmp(ciphertext, plaintext, sizeof(plaintext))) {
		FAIL("Plaintext == ciphertext");
	}
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat AES accepted message smaller than 16 bytes");
}

TEST(Crypto_AES_Failers, AES224)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
	unsigned char ciphertext[TEST_TEXT_SIZE] = { 0 };
        
	unsigned char iv[16] = { 0 };
	memcpy(iv, g_iv, 16);

	status = dat_aes_encrypt_message( key224, sizeof(key224), plaintext, sizeof(plaintext), iv, ciphertext);
	if (0 == memcmp(ciphertext, plaintext, sizeof(plaintext))) {
		FAIL("Plaintext == ciphertext");
	}
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat AES accepted 224-bit key");
}

TEST(Crypto_AES_Failers, AESEncryptKeySizeZero)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
	unsigned char ciphertext[TEST_TEXT_SIZE] = { 0 };

	unsigned char iv[16] = { 0 };
	memcpy(iv, g_iv, 16);

	status = dat_aes_encrypt_message( key256, 0, plaintext, sizeof(plaintext), iv, ciphertext);
	if (0 == memcmp(ciphertext, plaintext, sizeof(plaintext))) {
		FAIL("Plaintext == ciphertext");
	}
	CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat AES accepted zero-sized key");
}

TEST(Crypto_AES_Failers, AESDecryptKeySizeZero)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
	unsigned char ciphertext[TEST_TEXT_SIZE] = { 0 };
	unsigned char deciphered_text[TEST_TEXT_SIZE] = { 0 };

	unsigned char iv[16] = { 0 };
	memcpy(iv, g_iv, 16);

	status = dat_aes_encrypt_message( key256, sizeof(key256), plaintext, sizeof(plaintext), iv, ciphertext);
	if (0 == memcmp(ciphertext, plaintext, sizeof(plaintext))) {
		FAIL("Plaintext == ciphertext");
	}

	LONGS_EQUAL_TEXT(DAT_CRYPTO_SUCCESS, status, "Failed to encrypt data");

	status = dat_aes_decrypt_message( key256, 0, ciphertext, sizeof(plaintext), iv, deciphered_text);
    CHECK_TEXT(DAT_CRYPTO_SUCCESS != status, "Dat AES accepted zero-sized key");
}
#endif
