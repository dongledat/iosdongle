#if 0
#ifdef DAT_CRYPTO_MBEDTLS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "CppUTest/TestHarness.h"
#include "CppUTest/MemoryLeakDetector.h"
#include "CppUTest/TestMemoryAllocator.h"
#include "CppUTest/PlatformSpecificFunctions.h"

#include "../../dat_test/memmock_cpputest.h"
#include "../../dat_test/memmock_cpputest_easy.h"

#include "SrpVerifier.h"
#include "SrpUser.h"
#include "../dat_srp_mbedtls_user.h"
#include "../dat_srp_mbedtls_verifier.h"
#include "../dat_srp_mbedtls_common.h"


TEST_GROUP(Crypto_SRP)
{
};

TEST_GROUP(Crypto_SRP_Failers)
{
};

/* Test parameters */
#define TEST_HASH      SRP_SHA256
#define TEST_NG        SRP_NG_1024

bool perform_srp_auth();

#define TRACK_MEMORY() \
	m.setDefaultAction(MOCK_ACTION_FORWARD_TO_CPPUTEST)

#define DO_NOT_TRACK_MEMORY() \
	m.setDefaultAction(MOCK_ACTION_CALL_ORIGINAL)

TEST(Crypto_SRP, SRPMultiple)
{
	for (int i = 0; i < 3; ++i) {
		bool res = perform_srp_auth();
		CHECK_TEXT(res, "SRP failed");
	}
}

/*	Only tracking (with cpputest) during user-side code.
	As a result, we can monitor the user's memory leaks and peak memory usage */
TEST(Crypto_SRP, SRP_NG_UserMemCheck)
{
	MemmockCPPUTest m;
	SET_TEST_MEMORY_THRESHOLD(15000);

	std::string str_pass = "testpass";
	Buffer password(str_pass.begin(), str_pass.end());

	DO_NOT_TRACK_MEMORY();
	SrpVerifier verifier(TEST_HASH, TEST_NG, password);
	TRACK_MEMORY();
	SrpUser user(TEST_HASH, TEST_NG, password);

	// Transfer salt
	DO_NOT_TRACK_MEMORY();
	Buffer s = verifier.getSalt();
	TRACK_MEMORY();
	user.setSalt(s);

	// Transfer A
	TRACK_MEMORY();
	Buffer A = user.getA();
	DO_NOT_TRACK_MEMORY();
	verifier.setA(std::move(A));

	// Transfer B, u
	DO_NOT_TRACK_MEMORY();
	auto Bu = verifier.getBu();
	TRACK_MEMORY();
	user.setBu(std::get<0>(Bu), std::get<1>(Bu));

	TRACK_MEMORY();
	user.calculateKey();
	DO_NOT_TRACK_MEMORY();
	verifier.calculateKey();

	// Transfer M
	TRACK_MEMORY();
	Buffer M = user.getM();
	DO_NOT_TRACK_MEMORY();
	verifier.setM(M);

	// Transfer HAMK
	DO_NOT_TRACK_MEMORY();
	Buffer HAMK = verifier.getHAMK();
	TRACK_MEMORY();
	user.setHAMK(HAMK);

	//Buffer verKey = verifier.getSessionKey();

	// This is the awful part
	DO_NOT_TRACK_MEMORY();
	srp_teardown();

	DO_NOT_TRACK_MEMORY();
	verifier.clear();

	TRACK_MEMORY();
	user.clear();
}

/*	Only tracking (with cpputest) during verifier-side code.
	As a result, we can monitor the verifier's memory leaks and peak memory usage */
TEST(Crypto_SRP, SRP_NG_VerMemCheck)
{
	MemmockCPPUTest m;
	SET_TEST_MEMORY_THRESHOLD(10000);

	std::string str_pass = "testpass";
	Buffer password(str_pass.begin(), str_pass.end());

	TRACK_MEMORY();
	SrpVerifier verifier(TEST_HASH, TEST_NG, password);

	DO_NOT_TRACK_MEMORY();
	SrpUser user(TEST_HASH, TEST_NG, password);

	// Transfer salt
	TRACK_MEMORY();
	Buffer s = verifier.getSalt();
	DO_NOT_TRACK_MEMORY();
	user.setSalt(s);

	// Transfer A
	DO_NOT_TRACK_MEMORY();
	Buffer A = user.getA();
	TRACK_MEMORY();
	verifier.setA(std::move(A));

	// Transfer B, u
	TRACK_MEMORY();
	auto Bu = verifier.getBu();
	DO_NOT_TRACK_MEMORY();
	user.setBu(std::get<0>(Bu), std::get<1>(Bu));

	DO_NOT_TRACK_MEMORY();
	user.calculateKey();
	TRACK_MEMORY();
	verifier.calculateKey();

	// Transfer M
	DO_NOT_TRACK_MEMORY();
	Buffer M = user.getM();
	TRACK_MEMORY();
	verifier.setM(M);

	// Transfer HAMK
	TRACK_MEMORY();
	Buffer HAMK = verifier.getHAMK();
	DO_NOT_TRACK_MEMORY();
	user.setHAMK(HAMK);

	//Buffer verKey = verifier.getSessionKey();

	// This is the awful part
	TRACK_MEMORY();
	srp_teardown();

	TRACK_MEMORY();
	verifier.clear();

	DO_NOT_TRACK_MEMORY();
	user.clear();
}

/*	Perform the SRP authentication in a loop, each time failing a different memory allocation
	function. If the srp succeeds, we consider this a failure */
IGNORE_TEST(Crypto_SRP_Failers, SRPMockMemoryAllocations)
{
	MemmockCPPUTestEasy<bool> m(perform_srp_auth);

	/*	perform_srp_auth should return true if succeeded */
	m.setTester([](bool res) {
		CHECK_TEXT(!res, "Mocked but SRP still suceeded");
	});

	m.performTests();
}

/*	Create a new srp user context in a loop, each time failing a different memory allocation
	function. If the user context is created, we consider this a failure */
IGNORE_TEST(Crypto_SRP_Failers, SRP_NewUser_MockMemoryAllocations)
{
	MemmockCPPUTestEasy<SRPUser *> m(std::bind(srp_user_new,
		TEST_HASH,
		SRP_NG_1024,
		reinterpret_cast<byte*>("testpass"),
		sizeof("testpass"),
		reinterpret_cast<byte*>(NULL),
		reinterpret_cast<byte*>(NULL)));

	/*	perform_srp_auth should return true if succeeded */
	m.setTester([](SRPUser * user) {
		CHECK_TEXT(!user, "Performed mock but returned SRPUser is not NULL");
	});

	m.performTests();
}

bool perform_srp_auth()
{
	std::string str_pass = "testpass";
	/*std::string str_n = test_n_hex;
	std::string str_g = test_g_hex;
	*/
	Buffer password(str_pass.begin(), str_pass.end());
	//Buffer n(str_n.begin(), str_n.end());
	//Buffer g(str_g.begin(), str_g.end());

	SrpVerifier verifier(TEST_HASH, TEST_NG, password);
	SrpUser user(TEST_HASH, TEST_NG, password);

	// Transfer salt
	Buffer s = verifier.getSalt();
	user.setSalt(s);

	// Transfer A
	Buffer A = user.getA();
	verifier.setA(std::move(A));

	// Transfer B, u
	auto Bu = verifier.getBu();
	user.setBu(std::get<0>(Bu), std::get<1>(Bu));

	user.calculateKey();
	verifier.calculateKey();

	// Transfer M
	Buffer M = user.getM();
	verifier.setM(M);

	// Transfer HAMK
	Buffer HAMK = verifier.getHAMK();
	user.setHAMK(HAMK);

	//Buffer verKey = verifier.getSessionKey();

	// This is the awful part
	srp_teardown();

	verifier.clear();
	user.clear();

	return true;
}

#endif
#endif