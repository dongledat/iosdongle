#if 0
#ifdef DAT_CRYPTO_MBEDTLS

#include "SrpVerifier.h"

extern "C" {
#include "../dat_srp_mbedtls_verifier.h"
#include "../../dat_test/memmock.h"
#include "../../dat_test/memmock_fuzzy.h"
}

//todo: find last usage of ng, and discard it then


#define CHECK_SRP_FAIL(res) \
	if (0 != res) { \
		throw SRPVerFail(); \
}

#define CHECK_ALLOC_FAIL(res) \
	if (0 == res) { \
		throw SRPVerFail(); \
}

SrpVerifier::SrpVerifier(SRP_HashAlgorithm algo, Buffer& pass, Buffer& n_hex, Buffer& g_hex)
{

	m_ver = srp_verifier_new(
		algo,
		SRP_NG_CUSTOM,
		&(pass[0]),
		pass.size(),
		&(n_hex[0]),
		&(g_hex[0]));
	CHECK_ALLOC_FAIL(m_ver);
}

SrpVerifier::SrpVerifier(SRP_HashAlgorithm alg, SRP_NGType ngtype, Buffer & pass)
{

	m_ver = srp_verifier_new(
		alg,
		ngtype,
		&(pass[0]),
		pass.size(),
		NULL,
		NULL);
	CHECK_ALLOC_FAIL(m_ver);
}

SrpVerifier::~SrpVerifier()
{
	clear();
}

// Requires:	pass
// Calculates:	s, v, x
// Stores:		v
// Discards:	s, x, pass
// Verifies:	.
// Returns:		s
Buffer SrpVerifier::getSalt()
{
	SRP_RESULT res = SRP_ERROR;

	byte* s;
	int s_len;

	res = srp_verifier_calculate_s_v(m_ver);
	CHECK_SRP_FAIL(res);

	res = srp_verifier_get_salt(
		m_ver,
		&s,
		&s_len);
	CHECK_SRP_FAIL(res);

	/*  "Send" the data away from this machine.
		So the data may be freed on this machine */
	Buffer ret_s(s, s + s_len);
	memmock_free(s);
	return ret_s;
}

// Requires:	.
// Calculates:	.
// Stores:		A
// Discards:	.
// Verifies:	A % N != 0
// Returns:		.
void SrpVerifier::setA(Buffer A)
{
	SRP_RESULT res = SRP_ERROR;
	
	res = srp_verifier_set_A(
		m_ver,
		&(A[0]),
		A.size());
	CHECK_SRP_FAIL(res);
}

// Requires:	v
// Calculates:	b, B, u
// Stores:		b, B, u
// Discards:	.
// Verifies:	.
// Returns:		B, u
std::tuple<Buffer, Buffer> SrpVerifier::getBu()
{
	SRP_RESULT res = SRP_ERROR;

	byte * B;
	int B_len;
	byte * u;
	int u_len;

	res = srp_verifier_calculate_B_u(m_ver);
	CHECK_SRP_FAIL(res);

	res = srp_verifier_get_B_u(
		m_ver,
		&B,
		&B_len,
		&u,
		&u_len);
	CHECK_SRP_FAIL(res);

	/*  "Send" the data away from this machine.
	So the data may be freed on this machine */
	auto Bu = std::make_tuple(	Buffer(B, B + B_len),
								Buffer(u, u + u_len));
	memmock_free(B);
	memmock_free(u);
	return Bu;
}

// Requires:	A, v, u, b
// Calculates:	S, K
// Stores:		K
// Discards:	S, b
// Verifies:	.
// Returns:		.
void SrpVerifier::calculateKey()
{
	SRP_RESULT res = SRP_ERROR;

	res = srp_verifier_calculate_secret(m_ver);
	CHECK_SRP_FAIL(res);

	res = srp_verifier_calculate_key(m_ver);
	CHECK_SRP_FAIL(res);
}

// Requires:	A, B, K
// Calculates:	M
// Stores:		M
// Discards:	B
// Verifies:	selfM == M
// Returns:		.
void SrpVerifier::setM(Buffer M)
{
	SRP_RESULT res = SRP_ERROR;

	res = srp_verifier_calculate_M(m_ver);
	CHECK_SRP_FAIL(res);

	res = srp_verifier_verify_M(
		m_ver,
		&(M[0]),
		M.size());
	CHECK_SRP_FAIL(res);
}

// Requires:	A, M, K
// Calculates:	HAMK
// Stores:		.
// Discards:	A, M, HAMK
// Verifies:	.
// Returns:		.
Buffer SrpVerifier::getHAMK()
{
	SRP_RESULT res = SRP_ERROR;

	byte * HAMK;
	int HAMK_len;

	res = srp_verifier_calculate_HAMK(m_ver);
	CHECK_SRP_FAIL(res);

	res = srp_verifier_get_HAMK(
		m_ver,
		&HAMK,
		&HAMK_len);
	CHECK_SRP_FAIL(res);
	

	/*  "Send" the data away from this machine.
	So the data may be freed on this machine */
	Buffer HAMK_result(HAMK, HAMK + HAMK_len);
	memmock_free(HAMK);
	return HAMK_result;
}

// Requires:	K
// Calculates:	.
// Stores:		.
// Discards:	.
// Verifies:	.
// Returns:		K
Buffer SrpVerifier::getSessionKey()
{
	SRP_RESULT res = SRP_ERROR;

	byte * K;
	int K_len;

	res = srp_verifier_get_K(
		m_ver,
		&K,
		&K_len);
	CHECK_SRP_FAIL(res);

	/*  This data (the key) will be used as long as the session lives.
		We don't care, for us - the SRP has finished.
		So we free it */
	Buffer ret_K(K, K + K_len);
	memmock_free(K);
	return ret_K;
}

void SrpVerifier::clear()
{
	(void)srp_verifier_delete(m_ver);
	srp_teardown();
	m_ver = NULL;
}

#endif
#endif