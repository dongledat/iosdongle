#if 0
#ifdef DAT_CRYPTO_MBEDTLS

#undef malloc
#undef free

#include "SrpUser.h"
#include "../dat_srp_mbedtls_user.h"

extern "C" {
#include "../../dat_test/memmock.h"
#include "../../dat_test/memmock_fuzzy.h"
}


#define CHECK_SRP_FAIL(res) \
	if (0 != res) { \
		throw SRPUserFail(); \
}

#define CHECK_ALLOC_FAIL(res) \
	if (0 == res) { \
		throw SRPUserFail(); \
}

SrpUser::SrpUser(SRP_HashAlgorithm algo, Buffer& pass, Buffer& n_hex, Buffer& g_hex)
{

	m_user = srp_user_new(
		algo,
		SRP_NG_CUSTOM,
		&(pass[0]),
		pass.size(),
		&(n_hex[0]),
		&(g_hex[0]));
	CHECK_ALLOC_FAIL(m_user);
}

SrpUser::SrpUser(SRP_HashAlgorithm algo, SRP_NGType ngtype, Buffer& pass)
{

	m_user = srp_user_new(
		algo,
		ngtype,
		&(pass[0]),
		pass.size(),
		NULL,
		NULL);
	CHECK_ALLOC_FAIL(m_user);
}

SrpUser::~SrpUser()
{
	clear();
}

// Requires:	p
// Calculates:	.
// Stores:		s
// Discards:	.
// Verifies:	.
// Returns:		.
void SrpUser::setSalt(Buffer& salt)
{
	SRP_RESULT res = SRP_ERROR;

	res = srp_user_set_salt(
		m_user,
		&(salt[0]),
		salt.size());

	CHECK_SRP_FAIL(res);
}

// Requires:	.
// Calculates:	a, A
// Stores:		a, A
// Discards:	.
// Verifies:	.
// Returns:		A
Buffer SrpUser::getA()
{
	SRP_RESULT res = SRP_ERROR;

	byte* A;
	int A_len;

	res = srp_user_calculate_A(
		m_user);
	CHECK_SRP_FAIL(res);

	res = srp_user_get_A(
		m_user,
		&A,
		&A_len);
	CHECK_SRP_FAIL(res);


	/*  "Send" the data away from this machine.
	So the data may be freed on this machine */
	Buffer A_result(A, A + A_len);
	memmock_free(A);
	return A_result;
}

// Requires:	.
// Calculates:	.
// Stores:		B, u
// Discards:	.
// Verifies:	.
// Returns:		.
void SrpUser::setBu(Buffer B, Buffer u)
{
	SRP_RESULT res = SRP_ERROR;

	res = srp_user_set_B_u(
		m_user,
		&(B[0]),
		B.size(),
		&(u[0]),
		u.size());
	CHECK_SRP_FAIL(res);
}

// Requires:	a, B, u, x
// Calculates:	S, K
// Stores:		K
// Discards:	S, a, u, x
// Verifies:	.
// Returns:		.
void SrpUser::calculateKey()
{
	SRP_RESULT res = SRP_ERROR;

	res = srp_user_calculate_x(m_user);
	CHECK_SRP_FAIL(res);

	res = srp_user_calculate_secret(m_user);
	CHECK_SRP_FAIL(res);

	res = srp_user_calculate_key(m_user);
	CHECK_SRP_FAIL(res);
}

// Requires:	A, B, K
// Calculates:	M
// Stores:		M
// Discards:	B
// Verifies:	.
// Returns:		M
Buffer SrpUser::getM()
{
	SRP_RESULT res = SRP_ERROR;

	byte* M;
	int M_len;

	res = srp_user_calculate_M(
		m_user);
	CHECK_SRP_FAIL(res);

	res = srp_user_get_M(
		m_user,
		&M,
		&M_len);
	CHECK_SRP_FAIL(res);

	return Buffer(M, M + M_len);
}

// Requires:	A, M, K
// Calculates:	selfHAMK
// Stores:		.
// Discards:	A, M, selfHAMK
// Verifies:	selfHAMK == HAMK
// Returns:		.
void SrpUser::setHAMK(Buffer HAMK)
{
	SRP_RESULT res = SRP_ERROR;

	res = srp_user_calculate_HAMK(
		m_user);
	CHECK_SRP_FAIL(res);

	res = srp_user_verify_HAMK(
		m_user,
		&(HAMK[0]),
		HAMK.size());
	CHECK_SRP_FAIL(res);
}

// Requires:	K
// Calculates:	.
// Stores:		.
// Discards:	.
// Verifies:	.
// Returns:		K
Buffer SrpUser::getSessionKey()
{
	SRP_RESULT res = SRP_ERROR;

	byte* K;
	int K_len;

	res = srp_user_get_K(
		m_user,
		&K,
		&K_len);
	CHECK_SRP_FAIL(res);

	return Buffer(K, K + K_len);
}

void SrpUser::clear()
{
	(void)srp_user_delete(m_user);
	srp_teardown();
	m_user = NULL;
}

#endif
#endif