#include "crypto\dat_chisqr.h"
#include "CppUTest/TestHarness.h"

TEST_GROUP(Crypto_Chisqr)
{
};

TEST_GROUP(Crypto_Chisqr_Failers)
{
};

IGNORE_TEST(Crypto_Chisqr, chisqr)
{
    double dset1[] = { 199809., 200665., 199607., 200270., 199649. };
    double dset2[] = { 522573., 244456., 139979., 71531.,  21461. };
    double dset3[] = { 200000., 200000., 200000., 200000., 200000. };
	int uni = false;
 
//    uni = chiIsUniform(dset1, 5, 0.05);
//	CHECK(uni);
    uni = chiIsUniform(dset2, 5, 0.05);
	CHECK(!uni);
    uni = chiIsUniform(dset3, 5, 0.05);
	CHECK(!uni);
}