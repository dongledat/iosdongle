#if 0
#include "dat_ecc.h"

DAT_CRYPTO_RESULT dat_ecc_init(dat_ecc_ctx_t* ecc)
{
	int mbedtls_status = MBEDTLS_FAILURE;

	if (NULL == ecc) {
		return DAT_CRYPTO_FAILURE;
	}

    mbedtls_entropy_init(&ecc->entropy);

	mbedtls_status = mbedtls_ctr_drbg_seed(&ecc->ctr_drbg, mbedtls_entropy_func, &ecc->entropy, (const unsigned char *) "RANDOM_GEN", 10);
    if (MBEDTLS_SUCCESS != mbedtls_status) {
		return DAT_CRYPTO_FAILURE;
    }

	return DAT_CRYPTO_SUCCESS;
}

DAT_CRYPTO_RESULT dat_ecc_deinit(dat_ecc_ctx_t* ecc)
{
	if (NULL == ecc) {
		return DAT_CRYPTO_FAILURE;
	}

    mbedtls_ctr_drbg_free(&ecc->ctr_drbg);
    mbedtls_entropy_free(&ecc->entropy);

    return DAT_CRYPTO_SUCCESS;
}

DAT_CRYPTO_RESULT dat_ecc_generate_keys(dat_ecc_ctx_t * ecc, unsigned char * private_key, unsigned char * public_key_x, unsigned char * public_key_y)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
    int mbedtls_status = MBEDTLS_FAILURE;
	mbedtls_ecp_keypair* ecp = NULL;
	mbedtls_pk_context key = { 0 };

	if ((NULL == ecc) || (NULL == private_key) || (NULL == public_key_x) || (NULL == public_key_y)) {
		return DAT_CRYPTO_FAILURE;
	}

    memset(&key, 0, sizeof(key));

    mbedtls_status = mbedtls_pk_setup(&key, mbedtls_pk_info_from_type(MBEDTLS_PK_ECKEY));
    if (MBEDTLS_SUCCESS != status) {
		return DAT_CRYPTO_FAILURE;
	}

	mbedtls_status = mbedtls_ecp_gen_key(DAT_ECC_ALGORITHM, mbedtls_pk_ec(key), mbedtls_ctr_drbg_random, &ecc->ctr_drbg);
    if (MBEDTLS_SUCCESS != status) {
		goto lblCleanup;
	}

	ecp = mbedtls_pk_ec(key);
	if (NULL == ecp) {
		goto lblCleanup;
	}
	
    mbedtls_status = mbedtls_mpi_write_binary(&ecp->Q.X, public_key_x, DAT_ECC_KEY_LENGTH);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup;
	}
	
    mbedtls_status = mbedtls_mpi_write_binary(&ecp->Q.Y, public_key_y, DAT_ECC_KEY_LENGTH);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup;
	}
	
    mbedtls_status = mbedtls_mpi_write_binary(&ecp->d, private_key, DAT_ECC_KEY_LENGTH);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup;
	}

	status = DAT_CRYPTO_SUCCESS;

lblCleanup:
	
	mbedtls_pk_free(&key);
	return status;
}

DAT_CRYPTO_RESULT dat_ecc_calculate_secret_point(dat_ecc_ctx_t* ecc, unsigned char* private_key, unsigned char* public_key_x, unsigned char* public_key_y, unsigned char* output)
{
	DAT_CRYPTO_RESULT status = DAT_CRYPTO_FAILURE;
    int mbedtls_status = MBEDTLS_FAILURE;

    mbedtls_ecp_group grp = { 0 };
	mbedtls_pk_context key = { 0 };
	mbedtls_ecp_point ecp = { 0 };
	mbedtls_mpi private_mpi = { 0 };
	mbedtls_mpi secret_mpi = { 0 };

	const mbedtls_md_info_t* md = NULL;

	if ((NULL == ecc) || (NULL == private_key) || (NULL == public_key_x) || (NULL == public_key_y) || (NULL == output)/* || (NULL == salt)*/) {
		return DAT_CRYPTO_FAILURE;
	}

#if 0
    /* Generate Salt */
    if (create_salt)
    {
        status = mbedtls_ctr_drbg_random(&ecc->ctr_drbg, salt, 16);
        if (MBEDTLS_SUCCESS != status) {
            //todo!!!!
        }
    }
#endif

    /* Init parameters*/
    mbedtls_ecp_group_init(&grp);
	
    status = mbedtls_pk_setup(&key, mbedtls_pk_info_from_type(MBEDTLS_PK_ECKEY));
    if (MBEDTLS_SUCCESS != status) {
		goto lblCleanup1;
    }

    status = mbedtls_ecp_group_load(&grp, MBEDTLS_ECP_DP_SECP224R1);
    if (MBEDTLS_SUCCESS != status) {
		goto lblCleanup2;
	}

    mbedtls_ecp_point_init(&ecp);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup3;
	}

    mbedtls_mpi_init(&private_mpi);
    mbedtls_mpi_init(&secret_mpi);

    mbedtls_status = mbedtls_mpi_read_binary(&ecp.X, public_key_x, DAT_ECC_KEY_LENGTH);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup4;
	}
	
    mbedtls_status = mbedtls_mpi_read_binary(&ecp.Y, public_key_y, DAT_ECC_KEY_LENGTH);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup4;
	}
	
    mbedtls_status = mbedtls_mpi_read_binary(&private_mpi, private_key, DAT_ECC_KEY_LENGTH);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup4;
	}
	
    mbedtls_status = mbedtls_mpi_lset(&ecp.Z, 1);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup5;
	}
	
    status = mbedtls_ecdh_compute_shared(&grp, &secret_mpi, &ecp, &private_mpi, mbedtls_ctr_drbg_random, &ecc->ctr_drbg);
    if (MBEDTLS_SUCCESS != status) {
		goto lblCleanup6;
	}

    mbedtls_status = mbedtls_mpi_write_binary(&secret_mpi, output, DAT_ECC_KEY_LENGTH);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		goto lblCleanup6;
	}
	
    md = mbedtls_md_info_from_type(MBEDTLS_MD_SHA256);
	if (NULL == md) {
		goto lblCleanup6;
	}

#if 0
    mbedtls_status = mbedtls_hkdf(md, salt, 16, secret_key, DAT_ECC_KEY_LENGTH, NULL, 0, output, DAT_ECC_SECRET_KEY_LENGTH);
	if (MBEDTLS_SUCCESS != mbedtls_status) {
		//TODO!!
	}
#endif

	status = DAT_CRYPTO_SUCCESS;

lblCleanup6:
	mbedtls_mpi_free(&secret_mpi);
lblCleanup5:
	mbedtls_mpi_free(&private_mpi);
lblCleanup4:
	mbedtls_ecp_point_free(&ecp);
lblCleanup3:
	mbedtls_ecp_group_free(&grp);
lblCleanup2:
	mbedtls_pk_free(&key);
lblCleanup1:

    return status;
}

DAT_CRYPTO_RESULT dat_ecc_convert_pem_to_bin(const char *pem, const int pem_size,
    unsigned char *privkey, int *privkey_size,
    unsigned char *pubkey, int *pubkey_size)
{
    mbedtls_pk_context pk;
    mbedtls_ecp_keypair *ec;
    size_t len;
    int ret;

    mbedtls_pk_init(&pk);

    /* The PEM buffer might be a public or private, so try both */
    ret = mbedtls_pk_parse_public_key(&pk, pem, pem_size);
    if (ret != 0) {
        ret = mbedtls_pk_parse_key(&pk, pem, pem_size, NULL, 0);
        if (ret != 0)
            goto out;
    }

    /* Only EC is supported */
    if (mbedtls_pk_get_type(&pk) != MBEDTLS_PK_ECKEY) {
        ret = -1;
        goto out;
    }

    ec = mbedtls_pk_ec(pk);

    /* Extract public key */
    if (pubkey) {
        ret = mbedtls_ecp_point_write_binary(&ec->grp, &ec->Q,
            MBEDTLS_ECP_PF_UNCOMPRESSED,
            &len, pubkey, *pubkey_size);
        if (ret != 0) {
            *pubkey_size = -1;
            goto out;
        }

        *pubkey_size = len;
    }

    /* Extract private key */
    if (privkey) {
        int n = (int)mbedtls_mpi_size(&ec->d);

        if (n > *privkey_size) {
            ret = -1;
            *privkey_size = -1;
            goto out;
        }

        ret = mbedtls_mpi_write_binary(&ec->d, privkey, n);
        if (ret != 0) {
            *privkey_size = -1;
            goto out;
        }

        *privkey_size = n;
    }

out:
    mbedtls_pk_free(&pk);
    return (ret == 0) ? DAT_CRYPTO_SUCCESS : DAT_CRYPTO_FAILURE;
}

#ifdef PEM2BIN_EXAMPLE

static void dump_key(unsigned char *key, int sz)
{
    int i;

    for (i = 0; i < sz; i++) {
        if (i % 15 == 0)
            printf("    ");

        printf("%02x", key[i]);
        if (i != sz - 1)
            printf(":");

        if (i % 15 == 14 || i == sz - 1)
            printf("\n");
    }
}

int main(int argc, char *argv[])
{
    FILE *f;
    struct stat st;
    char *pem = NULL;
    int size;
    int ret;
    unsigned char privkey[128], pubkey[128];
    int privkey_size = sizeof(privkey);
    int pubkey_size = sizeof(pubkey);

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <PEM file>\n", argv[0]);
        return 1;
    }

    if (stat(argv[1], &st) < 0) {
        perror("stat");
        return errno;
    }

    size = st.st_size + 1; // NULL terminated
    pem = malloc(st.st_size + 1);
    if (!pem) {
        perror("malloc");
        return errno;
    }

    f = fopen(argv[1], "r");
    if (!f) {
        perror("fopen");
        ret = errno;
        goto out;
    }

    if (fread(pem, 1, st.st_size, f) != st.st_size) {
        fprintf(stderr, "Failed to read PEM file\n");
        ret = 1;
        goto out;
    }
    pem[st.st_size] = 0;

    ret = convert_pem_to_bin(pem, size,
        privkey, &privkey_size,
        pubkey, &pubkey_size);
    if (ret) {
        fprintf(stderr, "Failed to extract keys: %#x", ret);
        goto out;
    }

    if (privkey_size > 0) {
        int i;
        printf("private:\n");
        dump_key(privkey, privkey_size);
    }
    if (pubkey_size > 0) {
        int i;
        printf("public:\n");
        dump_key(pubkey, pubkey_size);
    }


out:
    free(pem);
    return ret;
}

#endif
#endif