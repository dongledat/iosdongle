#ifndef _DAT_CRYPTO_COMMON_H_
#define _DAT_CRYPTO_COMMON_H_

typedef enum {
	DAT_CRYPTO_SUCCESS = 0,
	DAT_CRYPTO_FAILURE,
} DAT_CRYPTO_RESULT;

#ifdef DAT_CRYPTO_STM32

#include "crypto.h"
#include "dat_crypto_stm32.h"

#elif defined DAT_CRYPTO_MBEDTLS

#include "mbedtls/error.h"
#include "mbedtls/pk.h"
#include "mbedtls/ecp.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/ecdh.h"

#define MBEDTLS_FAILURE (-1)
#define MBEDTLS_SUCCESS (0)

#elif defined DAT_CRYPTO_WOLFSSL

#endif

#endif /* _DAT_CRYPTO_COMMON_H_ */
