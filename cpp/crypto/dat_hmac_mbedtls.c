#include "dat_hmac.h"
#include "mbedtls/md.h"

int hmac(unsigned char* input, unsigned input_length, unsigned char* key, unsigned key_length, unsigned char* output)
{
	int status;
	mbedtls_md_context_t ctx;

	status = mbedtls_md_setup(&ctx, mbedtls_md_info_from_type(MBEDTLS_MD_SHA256), 1);
	status = mbedtls_md_hmac_starts(&ctx, key, key_length);
	status = mbedtls_md_hmac_update(&ctx, input, input_length);
	status = mbedtls_md_hmac_finish(&ctx, output);
	mbedtls_md_free(&ctx);

	return status;
}
