#ifndef _SRP_USER_H_
#define _SRP_USER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "dat_srp_mbedtls_common.h"

SRPUser* srp_user_new(
	SRP_HashAlgorithm algo,
	SRP_NGType ngtype,
	byte * pass,
	int pass_len,
	byte * n_hex,
	byte * g_hex);

SRP_RESULT srp_user_set_salt(
	SRPUser* user,
	byte * salt,
	int salt_len);

SRP_RESULT srp_user_calculate_x(
	SRPUser* user);

SRP_RESULT srp_user_calculate_A(
	SRPUser* user);

SRP_RESULT srp_user_get_A(
	SRPUser* user,
	byte ** A,
	int * A_len);

SRP_RESULT srp_user_set_B_u(
	SRPUser* user,
	byte * B,
	int B_len,
	byte * u,
	int u_len);

SRP_RESULT srp_user_calculate_secret(
	SRPUser * user);

SRP_RESULT srp_user_calculate_key(
	SRPUser* user);

SRP_RESULT srp_user_calculate_M(
	SRPUser* user);

SRP_RESULT srp_user_get_M(
	SRPUser* user,
	byte ** M,
	int * M_len);

SRP_RESULT srp_user_calculate_HAMK(
	SRPUser* user);

SRP_RESULT srp_user_verify_HAMK(
	SRPUser* user,
	byte * HAMK,
	int HAMK_len);

SRP_RESULT srp_user_get_K(
	SRPUser* user,
	byte ** K,
	int * K_len);

SRP_RESULT srp_user_delete(
	SRPUser* user);


#ifdef __cplusplus
}
#endif

#endif /* Include Guard */