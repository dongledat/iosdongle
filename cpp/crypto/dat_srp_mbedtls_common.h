#ifndef _SRP_COMMON_H_
#define _SRP_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif
/*
 * Secure Remote Password 6a implementation based on mbedtls.
 *
 * Copyright (c) 2015 Dieter Wimberger
 * https://github.com/dwimberger/mbedtls-csrp
 * 
 * Derived from:
 * Copyright (c) 2010 Tom Cocagne. All rights reserved.
 * https://github.com/cocagne/csrp
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Tom Cocagne, Dieter Wimberger
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/* 
 * 
 * Purpose:       This is a direct implementation of the Secure Remote Password
 *                Protocol version 6a as described by 
 *                http://srp.stanford.edu/design.html
 * 
 * Author:        tom.cocagne@gmail.com (Tom Cocagne), Dieter Wimberger
 * 
 * Dependencies:  mbedtls
 * 
 * Usage:         Refer to test_srp.c for a demonstration
 * 
 * Notes:
 *    This library allows multiple combinations of hashing algorithms and 
 *    prime number constants. For authentication to succeed, the hash and
 *    prime number constants must match between 
 *    srp_create_salted_verification_key(), srp_user_new(),
 *    and srp_verifier_new(). A recommended approach is to determine the
 *    desired level of security for an application and globally define the
 *    hash and prime number constants to the predetermined values.
 * 
 *    As one might suspect, more bits means more security. As one might also
 *    suspect, more bits also means more processing time. The test_srp.c 
 *    program can be easily modified to profile various combinations of 
 *    hash & prime number pairings.
 */

#include "mbedtls/bignum.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/sha1.h"
#include "mbedtls/sha256.h"
#include "mbedtls/sha512.h"

typedef unsigned char byte;

typedef enum {
	SRP_SUCCESS = 0,
	SRP_ERROR = 1
} SRP_RESULT;

#define SHA1_DIGEST_LENGTH 20
#define SHA224_DIGEST_LENGTH 224
#define SHA256_DIGEST_LENGTH 256
#define SHA384_DIGEST_LENGTH 384
#define SHA512_DIGEST_LENGTH 512
#define BIGNUM	mbedtls_mpi

typedef struct
{
	BIGNUM     *N;
	BIGNUM     *g;
} NGConstant;

struct NGHex
{
	const char * n_hex;
	const char * g_hex;
};

typedef enum
{
	SRP_NG_512,
	SRP_NG_768,
    SRP_NG_1024,
    SRP_NG_2048,
    SRP_NG_4096,
    SRP_NG_8192,
    SRP_NG_CUSTOM
} SRP_NGType;

typedef enum 
{
    SRP_SHA1, 
    SRP_SHA224, 
    SRP_SHA256,
    SRP_SHA384, 
    SRP_SHA512
} SRP_HashAlgorithm;

typedef union
{
	mbedtls_sha1_context   sha;
	mbedtls_sha256_context sha256;
	mbedtls_sha512_context sha512;
} HashCTX;

typedef struct
{
	SRP_HashAlgorithm hash_alg;
	NGConstant* ng;

	char* pass;
	int pass_len;

	BIGNUM* s;
	BIGNUM* v;

	BIGNUM* A;

	BIGNUM* b;
	BIGNUM* B;
	BIGNUM* u;

	BIGNUM* S;
	byte* K;
	
	byte* M;
	byte* HAMK;

	int                   authenticated;

} SRPVerifier;

typedef struct
{
	SRP_HashAlgorithm hash_alg;
	NGConstant* ng;

	char* pass;
	int pass_len;

	BIGNUM* s;

	BIGNUM* x;

	BIGNUM* a;
	BIGNUM* A;

	BIGNUM* B;
	BIGNUM* u;
	
	BIGNUM* S;
	byte* K;

	byte* M;
	byte* HAMK;

	int                   authenticated;

} SRPUser;

/* This library will automatically seed the mbedtls random number generator.
 *
 * The random data should include at least as many bits of entropy as the
 * largest hash function used by the application. So, for example, if a
 * 512-bit hash function is used, the random data requies at least 512
 * bits of entropy.
 * 
 * Passing a null pointer to this function will cause this library to skip
 * seeding the random number generator.
 * 
 * Notes: 
 *    * This function is optional on Windows & Linux and mandatory on all
 *      other platforms.
 */
void srp_random_seed( const unsigned char * random_data, int data_length );

/*
 * This function will free global resources used by the SRP module and reset the global state.
 * There's no need to call this function after every authentication;
 * Call it only when you're done with SRP completely.
 */
void srp_teardown();

/***** Functions for use by srp_user and srp_verifier ***************************/
void init_random();
void calculate_H_AMK(SRP_HashAlgorithm alg, unsigned char *dest, const BIGNUM * A, const unsigned char * M, const unsigned char * K);
void calculate_M(SRP_HashAlgorithm alg, NGConstant *ng, unsigned char * dest, const BIGNUM * A, const BIGNUM * B, const unsigned char * K );
void hash_num(SRP_HashAlgorithm alg, const BIGNUM * n, unsigned char * dest);
void update_hash_n(SRP_HashAlgorithm alg, HashCTX *ctx, const BIGNUM * n);
BIGNUM * H_ns(SRP_HashAlgorithm alg, const BIGNUM * n, const unsigned char * bytes, int len_bytes);
BIGNUM * H_nn(SRP_HashAlgorithm alg, const BIGNUM * n1, const BIGNUM * n2);
int hash_length(SRP_HashAlgorithm alg);
void hash(SRP_HashAlgorithm alg, const unsigned char *d, size_t n, unsigned char *md);
void hash_final(SRP_HashAlgorithm alg, HashCTX *c, unsigned char *md);
void hash_update(SRP_HashAlgorithm alg, HashCTX *c, const unsigned char *data, size_t len);
void hash_start(SRP_HashAlgorithm alg, HashCTX *c);
void hash_init(SRP_HashAlgorithm alg, HashCTX *c);
void delete_ng(NGConstant * ng);
NGConstant * new_ng(SRP_NGType ng_type, const char * n_hex, const char * g_hex);
BIGNUM * calculate_x(SRP_HashAlgorithm alg, const BIGNUM * salt, const unsigned char * password, int password_len);
/*******************************************************************************/



#ifdef __cplusplus
}
#endif

#endif /* Include Guard */
