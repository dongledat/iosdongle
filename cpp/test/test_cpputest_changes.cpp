#ifdef TEST_CPPUTEST
#include "CppUTest/TestHarness.h"


extern "C" void* memmock_malloc(size_t size);
extern "C" void memmock_free(void * address);
extern "C" void* memmock_realloc(void * address, size_t size);
extern "C" void* memmock_calloc(size_t count, size_t size);

TEST_GROUP(DatTest)
{
};

//#define FAILING_TESTS


#ifdef FAILING_TESTS
TEST(DatTest, Malloc_TestOverrideMemThresholdAndExceedIt)
{
	SET_TEST_MEMORY_THRESHOLD(5);
	void* x = memmock_malloc(10);
	memmock_free(x);
};

TEST(DatTest, Calloc_TestOverrideMemThresholdAndExceedIt)
{
	SET_TEST_MEMORY_THRESHOLD(20);
	void* x = memmock_calloc(10, 10);
	memmock_free(x);
};

TEST(DatTest, MallocRealloc_Exceed)
{
	void* x = memmock_malloc(500);
	x = memmock_realloc(x, 997);
	memmock_free(x);
};

TEST(DatTest, MallocRealloc_Exceed_Leak)
{
	void* x = memmock_malloc(500);
	x = memmock_realloc(x, 997);
};

TEST(DatTest, MallocRealloc_NoExceed_Leak)
{
	void* x = memmock_malloc(500);
	x = memmock_realloc(x, 996);
};

#else

TEST(DatTest, Malloc_TestOverrideMemThresholdButDontExceedIt)
{
	SET_TEST_MEMORY_THRESHOLD(20);
	void* x = memmock_malloc(10);
	memmock_free(x);
};


TEST(DatTest, Calloc_TestOverrideMemThresholdButDontExceedIt)
{
	SET_TEST_MEMORY_THRESHOLD(50);
	void* x = memmock_calloc(10, 2);
	memmock_free(x);
};

TEST(DatTest, MallocRealloc_DontExceed1)
{
	void* x = memmock_malloc(500);
	x = memmock_realloc(x, 995);
	memmock_free(x);
};

TEST(DatTest, MallocRealloc_DontExceed2)
{
	void* x = memmock_malloc(500);
	x = memmock_realloc(x, 0);
	memmock_free(x);
};

TEST(DatTest, MallocRealloc_DontExceed3)
{
	void* x = memmock_malloc(500);
	x = memmock_realloc(x, 996);
	memmock_free(x);
};

#endif /* FAILING_TESTS*/

#endif