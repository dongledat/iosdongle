#include "CppUTest/TestHarness.h"
#include "net/Net.h"
#include "os/Thread.h"
#include "os/Sleep.h"
#include "dongle-sim-lib/CarSimulatorSupportedDiagnostics.h"

#include "dongle-sim-lib\DataSubscription.h"
#include "dongle-smart\IConnection.h"
#include "dongle-smart\Connection.h"
#include "dongle-smart\DongleStatusListener.h"
#include "dongle-smart\DataListener.h"
#include "dongle-smart\UpdateProgressListener.h"
#include "dongle-sim-lib/DongleSimulator.h"
#include "net/UdpClient.h"
#include "dongle-smart\DataAggregator.h"

#include "log.h"

#define SERVER_UDP_PORT (11111)
#define SERVER_TCP_PORT (22222)
#define CLIENT_UDP_SRC_PORT (13372)
#define CLIENT_BUFFER_SIZE (4096)

#define JAVA_STUBS

#define TEST_IDENTITY ("PSKIdentity")
#define TEST_PIN ("\xde\xad\xbe\xef")

#define TEST_MAX_TIME_IN_SECONDS (5)
#define TEST_MAX_TIME_IN_USECONDS (TEST_MAX_TIME_IN_SECONDS * (long long)1000 * 1000)
#define TCP_CLIENT_RECV_TIMEOUT (1)
#define SIMULATOR_TIMEOUT_IN_SEC (5)

static DongleSimulator * simulator;
extern SubscriptionManager g_DiagCodeSubscriptionManager;

static dat::DongleStatusListener dongleStatusListener;
static dat::DataListener dataListener;
static dat::UpdateProgressListener updateProgressListener;

static net::TCPSocket* clientSocket;
static dat::Connection* con;

class MyTestTCPOutputStream : public dat::IOutputStream
{

public:
	MyTestTCPOutputStream(const net::TCPSocket & client) :
		m_client(client) {
	}

	virtual size_t send(const unsigned char* data, size_t length) {
		return m_client.send(data, length);
	}

private:
	net::TCPSocket m_client;
};

static MyTestTCPOutputStream* stream;

extern "C" int g_dtp_enabled;

TEST_GROUP(TLS_TCP)
{
	TEST_SETUP()
	{
		MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
		auto hack = DongleSimulator::get();
		MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();

		net::init();

		// [DONGLE] Start simulator in new thread, let it initialize
		simulator = DongleSimulator::get();
		simulator->start(SERVER_TCP_PORT, true, false, SIMULATOR_TIMEOUT_IN_SEC, true, false);
		os::Sleep(200);

		// [SMARTPHONE] Connect to the simulator using TCP
		sockaddr_in addr = { 0 };
		addr.sin_family = AF_INET;
		addr.sin_port = htons(SERVER_TCP_PORT);
		addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

		// This stream will be used to send on the client TCP connection
		clientSocket = new net::TCPSocket(&addr, sizeof(addr));
		if (INVALID_SOCKET == clientSocket->getDescriptor()) {
			FAIL("TCP Connection to TLS socket failed");
		}
		stream = new MyTestTCPOutputStream(*clientSocket);

		// Actual connection over network simulation (TCP instead of BT)
		con = new dat::Connection(*stream, dongleStatusListener, dataListener, updateProgressListener, true, true);

		g_dtp_enabled = 1;

#ifdef JAVA_STUBS
		dongleStatusListener.reset();
		dataListener.reset();
		updateProgressListener.reset();
#endif
	}

	TEST_TEARDOWN()
	{
		simulator->stop();
		delete con;
		delete stream;
		clientSocket->close();
		delete clientSocket;
		g_DiagCodeSubscriptionManager.clear();
		dat_done();

		net::done();
		//MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();
	}
};


TEST(TLS_TCP, TLS_ESTABLISHED_CONNECTION_INVALID_DATA_RECORD)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];

	con->authenticateByPIN(TEST_IDENTITY, TEST_PIN);
	CHECK_TEXT(dongleStatusListener.m_isCalled_onAuthenticate &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	dongleStatusListener.reset();

	common::Time startTime = os::Time::realUSeconds();
	while (true) {
		// Get data from net, pass it to connection
		auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
		if (0 >= count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		con->fromNet(buffer, count);

		// Check if authentication finished
		if (dongleStatusListener.m_isCalled_onAuthenticate) {
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_PASS, "PIN Auth failed");
			break;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}

	// We authenticated by PIN, so we have an established TLS session
	// Now to send malformed TLS data, by simply accessing the network layer
	for (int i = 0; i < 1024; ++i) {
		buffer[i] = 'A';
	}
	stream->send(buffer, 1024);
	Sleep(200);

	// Get dongle's TLS alert
	auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
	if (0 >= count) {
		FAIL("Timeout / empty data before handshake finished");
	}
	con->fromNet(buffer, count);
	Sleep(200);

	// Make sure connection handled it properly
	CHECK_TEXT(dongleStatusListener.m_isCalled_onDongleStatus, "onDongleStatus not called");
	CHECK_TEXT(dongleStatusListener.m_last_onDongleStatus_param_dongleStatus == dat::IDongleStatusListener::CONNECTIONRESET, "Connection not reseet?");
}

TEST(TLS_TCP, TCP_WrongPSKIdentity)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];

	con->authenticateByPIN("ThisIsWrongPSKIdentity", TEST_PIN);
	CHECK_TEXT(dongleStatusListener.m_isCalled_onAuthenticate &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	dongleStatusListener.reset();

	common::Time startTime = os::Time::realUSeconds();
	while (true) {
		// Get data from net, pass it to connection
		auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
		//printf("\nCount %d\n", count);
		if (0 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		con->fromNet(buffer, count);

		// Check if authentication finished
		if (dongleStatusListener.m_isCalled_onAuthenticate) {
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_FAIL, "PIN Auth didn't fail even though we used wrong PSK");
			break;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}
}


TEST(TLS_TCP, TCP_WrongPSKSecret)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];

	con->authenticateByPIN(TEST_IDENTITY, "ThisIsWrongPSKSecret");
	CHECK_TEXT(dongleStatusListener.m_isCalled_onAuthenticate &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	dongleStatusListener.reset();

	common::Time startTime = os::Time::realUSeconds();
	while (true) {
		// Get data from net, pass it to connection
		auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
		//printf("\nCount %d\n", count);
		if (0 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		//Sleep(50); //TODO: Without this, the test breaks, because for some reason we don't get the alert message of the failure
		con->fromNet(buffer, count);

		// Check if authentication finished
		if (dongleStatusListener.m_isCalled_onAuthenticate) {
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_FAIL, "PIN Auth didn't fail even though we used wrong PSK");
			break;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}
}

TEST(TLS_TCP, SimpleTCP)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];

	con->authenticateByPIN(TEST_IDENTITY, TEST_PIN);
	CHECK_TEXT(dongleStatusListener.m_isCalled_onAuthenticate &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	dongleStatusListener.reset();

	common::Time startTime = os::Time::realUSeconds();
	while (true) {
		// Get data from net, pass it to connection
		auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
		if (0 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		con->fromNet(buffer, count);

		// Check if authentication finished
		if (dongleStatusListener.m_isCalled_onAuthenticate) {
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_PASS, "PIN Auth failed");
			break;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}

	// TLS established

	int transactionID = con->getDongleSerialNumberAsync();
	CHECK_TEXT(dat::ITransaction::INVALID_TRANSACTION_ID != transactionID, "Invalid transaction ID");

	// Get data from net, pass it to connection
	auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
	if (0 == count) {
		FAIL("Timeout / empty data before handshake finished");
	}

	con->fromNet(buffer, count);

	// Check if authentication finished
	CHECK_TEXT(dataListener.m_isCalled_onDongleSerialNumber, "not called m_isCalled_onFirmwareStatus");
}



TEST(TLS_TCP, SimpleTCP_Recv_Per_Byte)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];

	con->authenticateByPIN(TEST_IDENTITY, TEST_PIN);
	CHECK_TEXT(dongleStatusListener.m_isCalled_onAuthenticate &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	dongleStatusListener.reset();

	common::Time startTime = os::Time::realUSeconds();
	while (true) {
		// Get data from net, pass it to connection
		auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
		if (0 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		// TEST: let's feed it one byte at a time, to see if it works
		for (int i = 0; i < count; ++i) {
			con->fromNet(buffer + i, 1);
		}

		// Check if authentication finished
		if (dongleStatusListener.m_isCalled_onAuthenticate) {
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_PASS, "PIN Auth failed");
			break;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}

	// TLS established

	int transactionID = con->getDongleSerialNumberAsync();
	CHECK_TEXT(dat::ITransaction::INVALID_TRANSACTION_ID != transactionID, "Invalid transaction ID");

	// Get data from net, pass it to connection
	auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, 500);
	if (0 == count) {
		FAIL("Timeout / empty data before handshake finished");
	}

	// TEST: let's feed it one byte at a time, to see if it works
	for (int i = 0; i < count; ++i) {
		con->fromNet(buffer + i, 1);
	}

	// Check if authentication finished
	CHECK_TEXT(dataListener.m_isCalled_onDongleSerialNumber, "not called m_isCalled_onFirmwareStatus");
}

TEST(TLS_TCP, SimpleTCP_Send_Per_Byte)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];

	simulator->setSendModePerByte(true);

	con->authenticateByPIN(TEST_IDENTITY, TEST_PIN);
	CHECK_TEXT(dongleStatusListener.m_isCalled_onAuthenticate &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	dongleStatusListener.reset();

	common::Time startTime = os::Time::realUSeconds();
	while (true) {
		// Get data from net, pass it to connection
		auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
		if (0 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		con->fromNet(buffer, count);

		// Check if authentication finished
		if (dongleStatusListener.m_isCalled_onAuthenticate) {
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_PASS, "PIN Auth failed");
			break;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}

	// TLS established

	int transactionID = con->getDongleSerialNumberAsync();
	CHECK_TEXT(dat::ITransaction::INVALID_TRANSACTION_ID != transactionID, "Invalid transaction ID");

	// Get data from net, pass it to connection
	auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, 500);
	if (0 == count) {
		FAIL("Timeout / empty data before handshake finished");
	}

	con->fromNet(buffer, count);

	// Check if authentication finished
	CHECK_TEXT(dataListener.m_isCalled_onDongleSerialNumber, "not called m_isCalled_onFirmwareStatus");
}


TEST(TLS_TCP, SimpleTCP_Over_DTP)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];

	simulator->setDTP(true);

	con->authenticateByPIN(TEST_IDENTITY, TEST_PIN);
	CHECK_TEXT(dongleStatusListener.m_isCalled_onAuthenticate &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	dongleStatusListener.reset();

	common::Time startTime = os::Time::realUSeconds();
	while (true) {
		// Get data from net, pass it to connection
		auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
		if (0 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		con->fromNet(buffer, count);

		// Check if authentication finished
		if (dongleStatusListener.m_isCalled_onAuthenticate) {
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_PASS, "PIN Auth failed");
			break;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}

	// TLS established

	int transactionID = con->getDongleSerialNumberAsync();
	CHECK_TEXT(dat::ITransaction::INVALID_TRANSACTION_ID != transactionID, "Invalid transaction ID");

	// Get data from net, pass it to connection
	auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
	if (0 == count) {
		FAIL("Timeout / empty data before handshake finished");
	}

	con->fromNet(buffer, count);

	// Check if authentication finished
	CHECK_TEXT(dataListener.m_isCalled_onDongleSerialNumber, "not called m_isCalled_onFirmwareStatus");
}


//
//TEST(TLS, DataLengths)
//{
//
//	/*
//		Over a TLS
//	*/
//	unsigned char buffer[CLIENT_BUFFER_SIZE];
//
//	con->authenticateByPIN(TEST_IDENTITY, TEST_PIN);
//	CHECK_TEXT(dongleStatusListener.m_isCalled_onAuthenticate &&
//		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
//		dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
//		"fail at psk handshake init");
//
//	dongleStatusListener.reset();
//	//TODO: This loop should have a timeout
//	common::Time startTime = os::Time::realUSeconds();
//
//	while (true) {
//		// Get data from net, pass it to connection
//		auto count = clientSocket->recvTimeout(buffer, CLIENT_BUFFER_SIZE, TCP_CLIENT_RECV_TIMEOUT);
//		if (0 == count) {
//			FAIL("Timeout / empty data before handshake finished");
//		}
//
//		con->fromNet(buffer, count);
//
//		// Check if authentication finished
//		if (dongleStatusListener.m_isCalled_onAuthenticate) {
//			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
//			CHECK_TEXT(dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_PASS, "PIN Auth failed");
//			break;
//		}
//		else {
//			// Not finished
//		}
//
//		common::Time currentTime(os::Time::realUSeconds());
//		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
//			FAIL("Test time limit exceeded");
//		}
//	}
//
//	// TLS established
//	char STATIC_A_BUFFER[0x2000];
//	memset(STATIC_A_BUFFER, 'A', 0x2000);
//
//	char * dataPtr = nullptr;
//	unsigned int dataSize = 0;
//
//	/*std::vector<std::string> testCases = {
//		"AAAA",
//	};*/
//
//	//for (int i = 1; i < 1024; i+=200) {
//
//	//	std::string testCase(STATIC_A_BUFFER, i);
//
//
//	//	con->DebugSendRawData(testCase);
//	//	Sleep(50);
//	//	dat_debug_get_last_session_data_test_deleteme(&dataPtr, &dataSize);
//
//	//	CHECK_TEXT(testCase.size() == dataSize, "Dongle received wrong size of data");
//	//	CHECK_TEXT(0 == memcmp(testCase.c_str(), dataPtr, dataSize), "Dongle received wrong size of data");
//	//}
//
//	/*std::string testCase(STATIC_A_BUFFER, 0x1025);
//
//	con->DebugSendRawData(testCase);
//	Sleep(50);
//	std::string lastData(dataPtr, dataSize);
//
//	dat_debug_get_last_session_data_test_deleteme(&dataPtr, &dataSize);
//
//	CHECK_TEXT(lastData.size() == dataSize, "A big buffer (1 over MAX_CONTENT_LEN) was accepted");
//	CHECK_TEXT(0 == memcmp(lastData.c_str(), dataPtr, dataSize), "A big buffer (1 over MAX_CONTENT_LEN) was accepted");*/
//	//todo: memcmp
//}

class MyTestOutputStreamUDP : public dat::IOutputStream
{

public:
	MyTestOutputStreamUDP(const net::UdpClient & client) :
		m_client(client) {
	}

	size_t send(const unsigned char* data, size_t length) {
		return m_client.send(reinterpret_cast<const char*>(data), length);
	}

private:
	net::UdpClient m_client;
};

DongleSimulator* _simulator;


void uncompleteTLSProcess(dat::Connection & con,
	dat::DongleStatusListener& status,
	net::UdpClient & udp,
	std::string identity,
	std::string pin,
	size_t stopCount)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];
	size_t totalCount = 0;
	common::Time startTime = os::Time::realUSeconds();

	con.authenticateByPIN(identity, pin);
	CHECK_TEXT(status.m_isCalled_onAuthenticate &&
		status.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		status.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	status.reset();

	while (true) {
		// Get data from net, pass it to connection
		auto count = udp.recv((char *)&buffer, CLIENT_BUFFER_SIZE);
		if (0 == count || -1 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		con.fromNet(buffer, count);

		// Check if authentication finished
		if (status.m_isCalled_onAuthenticate) {
			FAIL("Authentication finished, but test is supposed to get to hanging state")
		}
		else {
			// Not finished
		}

		totalCount += count;
		if (totalCount >= stopCount) {
			// We have enough to leave a hanging state at server side
			break;
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}
}

void completeTLSProcess(dat::Connection & con,
	dat::DongleStatusListener& status,
	net::UdpClient & udp,
	std::string identity,
	std::string pin)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];
	common::Time startTime = os::Time::realUSeconds();

	con.authenticateByPIN(identity, pin);
	CHECK_TEXT(status.m_isCalled_onAuthenticate &&
		status.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		status.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	status.reset();

	while (true) {
		// Get data from net, pass it to connection
		auto count = udp.recv((char *)&buffer, CLIENT_BUFFER_SIZE);
		if (0 == count || -1 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		con.fromNet(buffer, count);

		// Check if authentication finished
		if (status.m_isCalled_onAuthenticate) {
			CHECK_TEXT(status.m_isCalled_onAuthenticate &&
				status.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
				status.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_PASS,
				"fail at psk handshake init");
			// Finished
			return;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}
}



void failTLSProcess(dat::Connection & con,
	dat::DongleStatusListener& status,
	net::UdpClient & udp,
	std::string identity)
{
	unsigned char buffer[CLIENT_BUFFER_SIZE];
	common::Time startTime = os::Time::realUSeconds();

	con.authenticateByPIN(identity, "WrongPINWrongPIN");
	CHECK_TEXT(status.m_isCalled_onAuthenticate &&
		status.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		status.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	status.reset();

	while (true) {
		// Get data from net, pass it to connection
		auto count = udp.recv((char *)&buffer, CLIENT_BUFFER_SIZE);
		if (0 == count || -1 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		con.fromNet(buffer, count);

		// Check if authentication finished
		if (status.m_isCalled_onAuthenticate) {
			CHECK_TEXT(status.m_isCalled_onAuthenticate &&
				status.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
				status.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_FAIL,
				"NOT fail at psk handshake init");
			// Finished
			return;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}
}


TEST_GROUP(TLSUDP)
{
	TEST_SETUP()
	{
		MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
		auto hack = DongleSimulator::get();
		MemoryLeakWarningPlugin::turnOnNewDeleteOverloads();
		net::init();


		_simulator = DongleSimulator::get();
		_simulator->start(SERVER_UDP_PORT, true, true, SIMULATOR_TIMEOUT_IN_SEC);
		g_dtp_enabled = 0;

	}

	TEST_TEARDOWN()
	{
		_simulator->stop();
		dat_done();
	}
};

TEST(TLSUDP, Simple)
{

	dat::DongleStatusListener _dongleStatusListener;
	dat::DataListener _dataListener;
	net::UdpClient _smartphoneUdp("127.0.0.1", SERVER_UDP_PORT, 100);
	//CHECK(0 == _smartphoneUdp.bindSrcPort());
	MyTestOutputStreamUDP _outputStream(_smartphoneUdp);
	//DongleSimulator* _simulator = DongleSimulator::get();
	//_simulator->start(SERVER_UDP_PORT, true, true, 3);
	dat::Connection _con(_outputStream, _dongleStatusListener, _dataListener, updateProgressListener, true, true);
	extern SubscriptionManager g_DiagCodeSubscriptionManager;

	Sleep(200);

	unsigned char buffer[CLIENT_BUFFER_SIZE];

	_con.authenticateByPIN(TEST_IDENTITY, TEST_PIN);
	CHECK_TEXT(_dongleStatusListener.m_isCalled_onAuthenticate &&
		_dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		_dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	_dongleStatusListener.reset();
	//TODO: This loop should have a timeout
	common::Time startTime = os::Time::realUSeconds();

	while (true) {
		// Get data from net, pass it to connection
		auto count = _smartphoneUdp.recv((char *)&buffer, CLIENT_BUFFER_SIZE);
		if (0 == count || -1 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		_con.fromNet(buffer, count);

		// Check if authentication finished
		if (_dongleStatusListener.m_isCalled_onAuthenticate) {
			CHECK_TEXT(_dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
			CHECK_TEXT(_dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_PASS, "PIN Auth failed");
			break;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}

	// TLS established
	int transactionID = _con.getDongleSerialNumberAsync();
	CHECK_TEXT(dat::ITransaction::INVALID_TRANSACTION_ID != transactionID, "Invalid transaction ID");

	// Get data from net, pass it to connection
	
	while (true) {
		auto count = _smartphoneUdp.recv((char *)&buffer, CLIENT_BUFFER_SIZE);
		if (0 >= count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		_con.fromNet(buffer, count);

		// Check if transaction finished
		if (_dataListener.m_isCalled_onDongleSerialNumber) {
			_simulator->stop();
			return;
		}
	}
}

TEST(TLSUDP, Simple_Send_Per_Byte)
{

	dat::DongleStatusListener _dongleStatusListener;
	dat::DataListener _dataListener;
	net::UdpClient _smartphoneUdp("127.0.0.1", SERVER_UDP_PORT);
	//CHECK(0 == _smartphoneUdp.bindSrcPort());
	MyTestOutputStreamUDP _outputStream(_smartphoneUdp);
	DongleSimulator* _simulator = DongleSimulator::get();
	_simulator->setSendModePerByte(true);

	dat::Connection _con(_outputStream, _dongleStatusListener, _dataListener, updateProgressListener, true, false);
	extern SubscriptionManager g_DiagCodeSubscriptionManager;

	Sleep(200);

	unsigned char buffer[CLIENT_BUFFER_SIZE];

	_con.authenticateByPIN(TEST_IDENTITY, TEST_PIN);
	CHECK_TEXT(_dongleStatusListener.m_isCalled_onAuthenticate &&
		_dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN &&
		_dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_IN_PROGRESS,
		"fail at psk handshake init");

	_dongleStatusListener.reset();
	//TODO: This loop should have a timeout
	common::Time startTime = os::Time::realUSeconds();

	while (true) {
		// Get data from net, pass it to connection
		auto count = _smartphoneUdp.recv((char *)&buffer, CLIENT_BUFFER_SIZE);
		if (0 == count || -1 == count) {
			FAIL("Timeout / empty data before handshake finished");
		}

		// TEST: let's feed it one byte at a time, to see if it works
		for (int i = 0; i < count; ++i) {
			_con.fromNet(buffer + i, 1);
		}

		// Check if authentication finished
		if (_dongleStatusListener.m_isCalled_onAuthenticate) {
			CHECK_TEXT(_dongleStatusListener.m_last_onAuthenticate_param_AuthenticationMethod == dat::IDongleStatusListener::AUTH_METHOD_PIN, "Expected PIN (twss)");
			CHECK_TEXT(_dongleStatusListener.m_last_onAuthenticate_param_AuthenticationResult == dat::IDongleStatusListener::AUTH_RESULT_PASS, "PIN Auth failed");
			break;
		}
		else {
			// Not finished
		}

		common::Time currentTime(os::Time::realUSeconds());
		if (currentTime > (startTime + common::Time::uSeconds(TEST_MAX_TIME_IN_USECONDS))) {
			FAIL("Test time limit exceeded");
		}
	}

	// TLS established

	int transactionID = _con.getDongleSerialNumberAsync();
	CHECK_TEXT(dat::ITransaction::INVALID_TRANSACTION_ID != transactionID, "Invalid transaction ID");

	// Get data from net, pass it to connection
	auto count = _smartphoneUdp.recv((char *)&buffer, CLIENT_BUFFER_SIZE);
	if (0 == count) {
		FAIL("Timeout / empty data before handshake finished");
	}

	// TEST: let's feed it one byte at a time, to see if it works
	for (int i = 0; i < count; ++i) {
		_con.fromNet(buffer + i, 1);
	}

	// Check if authentication finished
	CHECK_TEXT(_dataListener.m_isCalled_onDongleSerialNumber, "not called m_isCalled_onFirmwareStatus");

	_simulator->stop();
}

//TODO: This can't work because you can't abort TLS handshake nicely
//IGNORE_TEST(TLSUDP, StopMidTLS_and_StartNewTLS)
//{
//
//	dat::DongleStatusListener _dongleStatusListener;
//	dat::DataListener _dataListener;
//	net::UdpClient _smartphoneUdp("127.0.0.1", SERVER_UDP_PORT, 100);
//	//CHECK(0 == _smartphoneUdp.bindSrcPort());
//	MyTestOutputStreamUDP _outputStream(_smartphoneUdp);
//	//_simulator->start(SERVER_UDP_PORT, true, true, 3);
//	dat::Connection _con(_outputStream, _dongleStatusListener, _dataListener, updateProgressListener, true, true);
//	extern SubscriptionManager g_DiagCodeSubscriptionManager;
//
//	Sleep(200);
//
//	uncompleteTLSProcess(_con, _dongleStatusListener, _smartphoneUdp, TEST_IDENTITY, TEST_PIN, 100);
//	completeTLSProcess(_con, _dongleStatusListener, _smartphoneUdp, TEST_IDENTITY, TEST_PIN);
//}

TEST(TLSUDP, FailTLS_and_StartNewTLS)
{

	dat::DongleStatusListener _dongleStatusListener;
	dat::DataListener _dataListener;
	net::UdpClient _smartphoneUdp("127.0.0.1", SERVER_UDP_PORT, 100);
	//CHECK(0 == _smartphoneUdp.bindSrcPort());
	MyTestOutputStreamUDP _outputStream(_smartphoneUdp);
	DongleSimulator* _simulator = DongleSimulator::get();
	dat::Connection _con(_outputStream, _dongleStatusListener, _dataListener, updateProgressListener, true, true);
	extern SubscriptionManager g_DiagCodeSubscriptionManager;

	Sleep(200);

	failTLSProcess(_con, _dongleStatusListener, _smartphoneUdp, TEST_IDENTITY);
	completeTLSProcess(_con, _dongleStatusListener, _smartphoneUdp, TEST_IDENTITY, TEST_PIN);
}

TEST(TLSUDP, MultipleTLS)
{
	dat::DongleStatusListener _dongleStatusListener;
	dat::DataListener _dataListener;
	net::UdpClient _smartphoneUdp("127.0.0.1", SERVER_UDP_PORT, 100);
	//CHECK(0 == _smartphoneUdp.bindSrcPort());
	MyTestOutputStreamUDP _outputStream(_smartphoneUdp);
	//_simulator->start(SERVER_UDP_PORT, true, true, 3);
	dat::Connection _con(_outputStream, _dongleStatusListener, _dataListener, updateProgressListener, true, true);
	extern SubscriptionManager g_DiagCodeSubscriptionManager;

	Sleep(200);

	completeTLSProcess(_con, _dongleStatusListener, _smartphoneUdp, TEST_IDENTITY, TEST_PIN);
	_con.reset();
	completeTLSProcess(_con, _dongleStatusListener, _smartphoneUdp, TEST_IDENTITY, TEST_PIN);
	_con.reset();
	completeTLSProcess(_con, _dongleStatusListener, _smartphoneUdp, TEST_IDENTITY, TEST_PIN);
}
